package com.example.roam.game.emeimountain.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class EmsWanPlaque2Spirit extends CSpirit {
    public EmsWanPlaque2Spirit(Context context) {
        super(context, 378,1087 ,756 ,1204 );
        this.text = GameConstant.EMS_WAN_PLAQUE_TEXT2;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
