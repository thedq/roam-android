package com.example.roam.game.jiuzhaigou.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import com.example.roam.game.jiuzhaigou.control.GameFirstJzgControl;

public class GameFirstJzgUI extends SurfaceView implements SurfaceHolder.Callback {

    private GameFirstJzgControl gameFirstJzgControl;
    private SurfaceHolder surfaceHolder;

    public GameFirstJzgUI(Context context) {
        super(context);
        initGame();
    }

    public GameFirstJzgUI(Context context, AttributeSet attrs) {
        super(context, attrs);
        initGame();
    }

    public GameFirstJzgUI(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initGame();
    }

    public void initGame(){
        surfaceHolder = this.getHolder();
        surfaceHolder.addCallback(this);
        gameFirstJzgControl = new GameFirstJzgControl(getContext());
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        Runner runner = new Runner();
        runner.start();
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {

    }

    class Runner extends Thread{
        @Override
        public void run() {

            while (gameFirstJzgControl.isVisible){
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                draw();
            }
        }
    }

    public void draw(){
        Canvas canvas = surfaceHolder.lockCanvas();
        gameFirstJzgControl.draw(canvas);
        surfaceHolder.unlockCanvasAndPost(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gameFirstJzgControl.onTouch(event);
        return true;
    }

    public GameFirstJzgControl getGameFirstJzgControl() {
        return gameFirstJzgControl;
    }
}
