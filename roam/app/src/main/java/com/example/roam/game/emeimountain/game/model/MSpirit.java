package com.example.roam.game.emeimountain.game.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.example.roam.R;

public class MSpirit extends Spirit{
    private Bitmap spirits[] = new Bitmap[4];
    private int type;
    private int add = 25;

    public MSpirit(Context context) {
        super(context, 0, -100, 200, 100);
        spirits[0] = BitmapFactory.decodeResource(context.getResources(), R.drawable.little_hb);
        spirits[1] = BitmapFactory.decodeResource(context.getResources(),R.drawable.mid_hb);
        spirits[2] = BitmapFactory.decodeResource(context.getResources(),R.drawable.big_hb);
        spirits[3] = BitmapFactory.decodeResource(context.getResources(),R.drawable.ghost);
    }

    @Override
    public void draw(Canvas canvas) {
        if( isVisible ){
            canvas.drawBitmap(spirits[type],null,rect,null);
            if( this.rect.top > 2000 ){  //不可见就回收
                this.isVisible = false;
            }
            this.rect.top += add;
            this.rect.bottom = this.rect.top + this.height;
        }
    }

    public boolean isPassLine(){  // 过线就添加
        return this.rect.top > 100;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getAdd() {
        return add;
    }

    public void setAdd(int add) {
        this.add = add;
    }
}
