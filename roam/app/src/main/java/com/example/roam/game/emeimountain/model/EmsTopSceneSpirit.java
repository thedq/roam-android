package com.example.roam.game.emeimountain.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.PaintUtil;

public class EmsTopSceneSpirit extends CSpirit {
    public EmsTopSceneSpirit(Context context) {
        super(context, 0,1070 ,590 ,1260 );
        this.text = "在金顶卧云底的东面，有一悬空600多米的断崖，雄险奇伟，为全山第一巨岩，名舍身崖。又因此处可以看“佛光”、“佛灯”，所以又叫睹光台。";
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
