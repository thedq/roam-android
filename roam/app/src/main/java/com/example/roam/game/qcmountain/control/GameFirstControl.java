package com.example.roam.game.qcmountain.control;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;

import com.example.roam.R;
import com.example.roam.game.app.leshan.GameSecondLsActivity;
import com.example.roam.game.tools.CatchBadgeSpirit;
import com.example.roam.game.tools.LeftSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.ui.activity.PersonalBadgeActivity;
import com.example.roam.game.app.qcmountain.GameSecondActivity;
import com.example.roam.game.qcmountain.model.BadgeSpirit;
import com.example.roam.game.tools.BarLayer;
import com.example.roam.game.qcmountain.model.BarOptionSpirit;
import com.example.roam.game.qcmountain.model.DoorSpirit;
import com.example.roam.game.qcmountain.model.EntranceBackgroundSpirit;
import com.example.roam.game.qcmountain.model.EntranceBuildingSpirit;
import com.example.roam.game.qcmountain.model.LionSpirit;
import com.example.roam.utils.BadgeUtils;

public class GameFirstControl {
    private Context context;
    private EntranceBackgroundSpirit entranceBackgroundSpirit;
    private DoorSpirit doorSpirit;
    private LionSpirit lionLeft,lionRight;
    private BarLayer barLayer;
    private BarOptionSpirit barOptionSpirit;
    public boolean isVisible = true;
    private EntranceBuildingSpirit entranceBuildingSpirit;
    private CatchBadgeSpirit catchBadgeSpirit;
    private LeftSpirit leftSpirit;
    // 底部切换动画，打开大门
    // 中部对话问题
    // 顶部游戏
    public static final String TAG = "TAG";
    public GameFirstControl(Context context){
        this.context = context;
        this.entranceBackgroundSpirit = new EntranceBackgroundSpirit(context);
        this.doorSpirit = new DoorSpirit(context);
        this.lionLeft = new LionSpirit(context,new Rect(0,1220,198,1555),1);
        this.lionRight = new LionSpirit(context,new Rect(900,1220,1100,1555),2);
        this.barLayer = new BarLayer(context);
        this.barOptionSpirit = new BarOptionSpirit(context);
        this.entranceBuildingSpirit = new EntranceBuildingSpirit(context);
        this.catchBadgeSpirit = new CatchBadgeSpirit(context, GameConstant.Qing_BADGE_A);
        this.leftSpirit = new LeftSpirit(context);
    }

    public void draw(Canvas canvas){
        entranceBackgroundSpirit.draw(canvas);
        doorSpirit.draw(canvas);
        lionLeft.draw(canvas);
        lionRight.draw(canvas);
        entranceBuildingSpirit.draw(canvas);
        catchBadgeSpirit.draw(canvas);
        leftSpirit.draw(canvas);
        barLayer.draw(canvas);
        barOptionSpirit.draw(canvas);
    }

    public void handleTouch(MotionEvent event){
        switch ( event.getAction() ){
            case MotionEvent.ACTION_DOWN:
                clickEntranceBackgroundSpirit(event);
                clickDoorSpirit(event);
                clickPostion(event);
                clickLionLeftSpirit(event);
                deleteBar(event);
                clickLionRightSpirit(event);
                clickEntranceBuildingSpirit(event); // 建筑大门
                clickDelete(event);
                clickOptionYes(event);
                clickOptionNo(event);
                clickLeft(event);
                break;
        }
    }

    private void clickLeft(MotionEvent event){
        if (leftSpirit.getRect().contains((int) event.getX(),(int) event.getY())) {
            leftSpirit.finsh(context);
        }
    }

    private void clickEntranceBackgroundSpirit(MotionEvent event){
        if( entranceBackgroundSpirit.getRect().contains((int)event.getX(),(int)event.getY())) {
            Log.i(TAG, "clickEntranceBackgroundSpirit被点击: x:" + event.getX() + " y:" + event.getY());
        }
    }

    private void clickDoorSpirit(MotionEvent event){
        if( doorSpirit.getRect().contains((int)event.getX(),(int)event.getY())){
            barOptionSpirit.setText("准备好开启旅程了吗？");
            barOptionSpirit.isVisible = true;
        }
    }

    private void clickDelete(MotionEvent event){
        if( barOptionSpirit.getBarDeleteSpirit().getRect().contains((int) event.getX(),(int) event.getY())){
            barOptionSpirit.isVisible = false;
        }
    }

    private void clickOptionYes(MotionEvent event){
        if (barOptionSpirit.getOptionYes().getRect().contains((int) event.getX(),(int) event.getY()) && !barOptionSpirit.isClickThrough()) {
            waitToStartActivity();
            barOptionSpirit.setClickThrough(true);
        }
    }

    public void waitToStartActivity(){  // 2
        if (!BadgeUtils.getBadges().getBadgeArray().contains(0)) {
            catchBadgeSpirit.isVisible = true;
            catchBadgeSpirit.setStartAnimation(true); // 启动动画
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(context,GameSecondActivity.class);
                            context.startActivity(intent);
                            GameFirstControl.this.isVisible = false;
                            barOptionSpirit.isVisible = false;
                            BadgeUtils.setBadgeIndex(0);
                            ((Activity) context).finish();
                        }
                    });
                }
            }).start();
        }else {
            Intent intent = new Intent(context,GameSecondActivity.class);
            ((Activity) context).finish();
            context.startActivity(intent);
            this.isVisible = false;
            barOptionSpirit.isVisible = false;
            BadgeUtils.setBadgeIndex(0);
        }
    }



    private void clickOptionNo(MotionEvent event){
        if (barOptionSpirit.getOptionNo().getRect().contains((int) event.getX(),(int) event.getY())) {
            barOptionSpirit.isVisible = false;
        }
    }

    private void clickLionLeftSpirit(MotionEvent event){
        if(lionLeft.getRect().contains((int) event.getX(),(int) event.getY())){
            barLayer.setText(lionLeft.text);
            barLayer.setVisible(true); // 确认展示，并且展示文字
        }
    }

    private void clickLionRightSpirit(MotionEvent event){
        if(lionRight.getRect().contains((int) event.getX(),(int) event.getY())){
            barLayer.setText(lionRight.text);
            barLayer.setVisible(true); // 确认展示，并且展示文字
        }
    }

    private void deleteBar(MotionEvent event){
        if (barLayer.getBarDeleteSpirit().getRect().contains((int)event.getX(),(int)event.getY() )) {
            barLayer.setVisible(false);
        }
    }

    private void clickPostion(MotionEvent event){
        Log.i(TAG, "clickPostion: x:"+event.getX()+" y:"+event.getY());
    }

    private void clickEntranceBuildingSpirit(MotionEvent event){
        if( entranceBuildingSpirit.getRect().contains((int) event.getX(),(int) event.getY())){
            barLayer.isVisible = true;
            barLayer.setText(context.getString(R.string.entranceBuildingScript));
        }
    }

}
