package com.example.roam.view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.roam.R;

/**
 * Illustration
 *
 * @author dengqing
 * @time 2021/11/1
 * @function 青城山弹出对话框
 */

public class QCSDialog extends Dialog implements View.OnClickListener {

    private TextView tv_title;
    private TextView qcs_scene, qcs_forum;
    private Context mContext;
    private OncloseListener listener;


    public QCSDialog(@NonNull Context context, int mdialog, OnClickListener onClickListener) {
        super(context);
        this.mContext = context;
        this.listener = listener;
    }

    public QCSDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.mContext = context;
        this.listener = listener;
    }

    protected QCSDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.mContext = context;
        this.listener = listener;
    }

    public QCSDialog(@NonNull Context context, int themeResId, OncloseListener listener) {
        super(context, themeResId);
        this.mContext = context;
        this.listener = listener;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_qcs_dialog);
        setCanceledOnTouchOutside(false);
        tv_title = findViewById(R.id.tv_title);
        qcs_scene = findViewById(R.id.qcs_scene);
        qcs_forum = findViewById(R.id.qcs_forum);
        qcs_scene.setOnClickListener(this);
        qcs_forum.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //青城山剧场
            case R.id.qcs_scene:
                if (listener != null) {
                    listener.onClick(true);
                }
                this.dismiss();
                break;
            //  青城山论坛
            case R.id.qcs_forum:
                if (listener != null) {
                    listener.onClick(false);
                }
                this.dismiss();
                break;
        }
    }

    public interface OncloseListener {
        void onClick(boolean confirm);
    }

    private void startNew(Intent intent, Context context1, Context context2) {

    }
}
