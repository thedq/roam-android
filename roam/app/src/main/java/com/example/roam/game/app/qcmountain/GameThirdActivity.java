package com.example.roam.game.app.qcmountain;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.roam.R;
import com.example.roam.game.qcmountain.control.GameThirdControl;
import com.example.roam.game.qcmountain.view.GameThirdUI;
import com.example.roam.utils.BadgeUtils;
import com.example.roam.utils.StatusBarUtils;

public class GameThirdActivity extends AppCompatActivity {

    private GameThirdUI gameThirdUI;
    private GameThirdControl gameThirdControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_third);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        gameThirdUI = this.findViewById(R.id.gameui3);
        gameThirdControl = gameThirdUI.getGameThirdControl();
        int score = getIntent().getIntExtra("score", 0);
        if( score >= 10 ){
            gameThirdControl.setCanClick(false);
            BadgeUtils.setBadgeIndex(2);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameThirdControl.isVisible = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameThirdControl.isVisible = false;
    }
}