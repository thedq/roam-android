package com.example.roam.game.emeimountain.game.model;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.example.roam.R;


public class BgSpirit extends Spirit{
    public BgSpirit(Context context) {
        super(context, 0, 0, 1080, 1793);
        this.bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.collect_spirit_bg);

    }

    @Override
    public void draw(Canvas canvas) {
        if( isVisible ){
            canvas.drawBitmap(bitmap,null,rect,null);
        }
    }
}
