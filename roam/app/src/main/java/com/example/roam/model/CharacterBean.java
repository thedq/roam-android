package com.example.roam.model;

import com.example.roam.R;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Illustration
 *
 * @author dengqing
 * @time 2021/10/26
 * @function 虚拟人物数据类
 */

public class CharacterBean {
    private static Map<String, CharacterEntity> mapCharacter = new LinkedHashMap<String, CharacterEntity>();//虚拟人物数据信息
    /*用户的虚拟人物形象id
     *0：没有
     * 开头为+：代表男性
     * 开头为-：代表女性
     * 第二个数字：1 2 3
     * 性格标签[warm,cool,cute]
     *
     * */
    static {
        //男虚拟人物
        mapCharacter.put("man_lively", new CharacterEntity(1, "男", "活泼", "我是一个书生，叫小洲。十二三岁来到青城山，此后熟读诗书，学有小成，读遍万卷书，上知天文下知地理，通晓古今，愿功成名就，建功立业。", R.drawable.character_man1));
        mapCharacter.put("man_neutral", new CharacterEntity(2, "男", "中性", "本少爷叫雄雄，是个富二代，虽然是暴发户平时略显任性，但是我在这块地方也算是乐善好施与人为善，品行这块儿拿捏的死死的，四邻八坊同辈儿看见我老喜欢叫我一声大哥。", R.drawable.character_man2));
        mapCharacter.put("man_steady", new CharacterEntity(3, "男", "沉稳", ".我叫阿民，是一个郎中，从幼熟习医术，他们总是称我“华佗在世”我觉得只是浮名，救死扶伤乃是医者仁心之基，现在我在青城山脚下有了自己的医馆", R.drawable.character_man3));
        //女虚拟人物
        mapCharacter.put("woman_lively", new CharacterEntity(-1, "女", "活泼", "我是一个大小姐，叫小平，琴棋书画样样精通。相较于财富我更注重有趣的灵魂，憧憬着闲云野鹤的生活，此间山河，白云苍狗。", R.drawable.character_woman1));
        mapCharacter.put("woman_neutral", new CharacterEntity(-2, "女", "中性", "我叫楠楠，从小与父亲习武，18岁参军，10年参军间获勋无数，懂不懂什么叫“巾帼不让须眉”啊，如今退役隐居青城山照顾我年迈的父母，路见不平我便拔刀相助，哈！", R.drawable.character_woman3));
        mapCharacter.put("woman_steady", new CharacterEntity(-3, "女", "沉稳", "我叫阿青，从小就擅长做菜，煎炸蒸炒样样精通，食客无不交口称赞，凭一道玉米之炊名满青城，真的不来尝尝吗？", R.drawable.character_woman2));

    }

    // 获得所有的虚拟人物
    public static Collection<CharacterEntity> getAll() {
        return mapCharacter.values();
    }

    // 根据指定的id获得虚拟人物
    public static CharacterEntity getSingle(String id) {
        return mapCharacter.get(id);
    }
}
