package com.example.roam.game.qcmountain.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.util.BitmapUtil;
import com.example.roam.game.util.GameConstant;

public class RoadBackgroundSpirit extends Spirit{

    public RoadBackgroundSpirit(Context context) {
        super(context, 0, 0, 1080, 1920);
        this.bitmap = BitmapUtil.changeBitmapSize(context, GameConstant.ROAD,width,height);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawBitmap(bitmap,null,rect,null);
    }


}
