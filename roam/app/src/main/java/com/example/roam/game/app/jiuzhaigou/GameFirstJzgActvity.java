package com.example.roam.game.app.jiuzhaigou;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.roam.R;
import com.example.roam.game.jiuzhaigou.control.GameFirstJzgControl;
import com.example.roam.game.jiuzhaigou.view.GameFirstJzgUI;
import com.example.roam.utils.StatusBarUtils;

public class GameFirstJzgActvity extends AppCompatActivity {
    private GameFirstJzgUI gameFirstJzgUI;
    private GameFirstJzgControl gameFirstJzgControl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_first_jzg_actvity);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        gameFirstJzgUI = this.findViewById(R.id.scene1);
        gameFirstJzgControl = gameFirstJzgUI.getGameFirstJzgControl();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameFirstJzgControl.isVisible =false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameFirstJzgControl.isVisible = true;
    }
}