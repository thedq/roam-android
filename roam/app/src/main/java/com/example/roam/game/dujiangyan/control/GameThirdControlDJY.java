package com.example.roam.game.dujiangyan.control;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import com.example.roam.game.app.dujiangyan.GameSecondDjyActivity;
import com.example.roam.game.dujiangyan.game.view.CatchDemon;
import com.example.roam.game.dujiangyan.model.BaoPingEntranceSpirit;
import com.example.roam.game.dujiangyan.model.BaoPingSpirit;
import com.example.roam.game.dujiangyan.model.BuildingSpirit;
import com.example.roam.game.dujiangyan.model.DuJianYan2Spirit;
import com.example.roam.game.dujiangyan.model.DuJianYanSpirit;
import com.example.roam.game.dujiangyan.model.SceneBaoPingKou;
import com.example.roam.game.dujiangyan.model.SceneDuJiangYan;
import com.example.roam.game.qcmountain.model.BarOptionSpirit;
import com.example.roam.game.tools.BarLayer;
import com.example.roam.game.tools.LeftSpirit;
import com.example.roam.game.util.WinBarLayer;
import com.example.roam.ui.activity.MainActivity;

public class GameThirdControlDJY {
    private Context context;
    private SceneBaoPingKou sceneBaoPingKou;
    private LeftSpirit leftSpirit;
    private BaoPingEntranceSpirit baoPingEntranceSpirit;
    private DuJianYanSpirit duJianYanSpirit;
    private DuJianYan2Spirit duJianYan2Spirit;
    private BarLayer barLayer;
    private BarOptionSpirit barOptionSpirit;
    private BuildingSpirit buildingSpirit;
    private WinBarLayer winBarLayer;
    public boolean isVisible = true;
    public boolean isFinsh = false;

    public GameThirdControlDJY(Context context){
        this.context = context;
        sceneBaoPingKou = new SceneBaoPingKou(context);
        leftSpirit = new LeftSpirit(context);
        baoPingEntranceSpirit = new BaoPingEntranceSpirit(context);
        duJianYanSpirit = new DuJianYanSpirit(context);
        duJianYan2Spirit = new DuJianYan2Spirit(context);
        barLayer = new BarLayer(context);
        barOptionSpirit = new BarOptionSpirit(context);
        buildingSpirit = new BuildingSpirit(context);
        winBarLayer = new WinBarLayer(context,"都江堰");
    }

    public void draw(Canvas canvas) {
        if (isVisible) {
            sceneBaoPingKou.draw(canvas);
            leftSpirit.draw(canvas);
            baoPingEntranceSpirit.draw(canvas);
            duJianYanSpirit.draw(canvas);
            duJianYan2Spirit.draw(canvas);
            buildingSpirit.draw(canvas);
            barOptionSpirit.draw(canvas);
            barLayer.draw(canvas);
        }

        if(isFinsh){
            winBarLayer.draw(canvas);
        }
    }

    public void onTouch(MotionEvent event) {
        switch (event.getAction() ){
            case MotionEvent.ACTION_DOWN:
                clickLeftSpirit(event);
                if(!isFinsh) {
                    clickSceneBaoPingKou(event);
                    clickDelete(event);
                    clickBaoPing(event);
                    clickDuJianYan(event);
                    clickDuJianYan2(event);
                    clickGame(event);
                    clickOptionCancel(event);
                    clickOptionCertain(event);
                    clickOptionDelelte(event);
                }else {
                    clickWinBarlayerDelete(event);
                }
                break;
        }
    }

    private void clickWinBarlayerDelete(MotionEvent event){
        winBarLayer.getBarLayer().clickDelete(event);
    }

    private void clickGame(MotionEvent event){
        buildingSpirit.clickSpiritOption(event,barLayer,barOptionSpirit);
    }

    private void clickOptionCertain(MotionEvent event){
        if (barOptionSpirit.getOptionYes().getRect().contains((int) event.getX(),(int) event.getY())  && !barOptionSpirit.isClickThrough() ) {
            barOptionSpirit.isVisible = false;
            Intent intent = new Intent(context, CatchDemon.class);
            context.startActivity(intent);
            barOptionSpirit.setClickThrough(true);
            ((Activity)context).finish();
        }
    }

    private void clickOptionCancel(MotionEvent event){
        if (barOptionSpirit.getOptionNo().getRect().contains((int) event.getX(),(int) event.getY())) {
            barOptionSpirit.isVisible = false;
        }
    }

    private void clickOptionDelelte(MotionEvent event){
        if (barOptionSpirit.getBarDeleteSpirit().getRect().contains((int) event.getX(),(int) event.getY())) {
            barOptionSpirit.isVisible = false;
        }
    }

    private void clickDuJianYan2(MotionEvent event){
        duJianYan2Spirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickDuJianYan(MotionEvent event){
        duJianYanSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickBaoPing(MotionEvent event){
        baoPingEntranceSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickDelete(MotionEvent event){
        if (barLayer.getBarDeleteSpirit().getRect().contains((int) event.getX(),(int) event.getY())) {
            barLayer.isVisible = false;
        }
    }

    String TAG = "TAG";
    private void clickSceneBaoPingKou(MotionEvent event ){
        if (sceneBaoPingKou.getRect().contains((int)event.getX(),(int) event.getY())) {
            Log.i(TAG, "clickSceneBaoPingKou: x:"+event.getX()+" y:"+event.getY());
        }
    }

    public void clickLeftSpirit(MotionEvent event){
        if( leftSpirit.getRect().contains((int) event.getX(),(int) event.getY())){
            leftSpirit.finsh(context);
        }
    }

    public boolean isFinsh() {
        return isFinsh;
    }

    public void setFinsh(boolean finsh) {
        isFinsh = finsh;
    }
}
