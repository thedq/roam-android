package com.example.roam.game.jiuzhaigou.control;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import com.example.roam.game.app.jiuzhaigou.GameFirstJzgActvity;
import com.example.roam.game.app.jiuzhaigou.GameThirdJzgActvity;
import com.example.roam.game.app.question.GameQuestionActivity;
import com.example.roam.game.jiuzhaigou.model.JiuCuiHaiSpirit;
import com.example.roam.game.jiuzhaigou.model.JiuCuiMountainSpirit;
import com.example.roam.game.jiuzhaigou.model.SceneCuiHai;
import com.example.roam.game.jiuzhaigou.model.SceneEntrance;
import com.example.roam.game.qcmountain.model.BarOptionSpirit;
import com.example.roam.game.qcmountain.model.layer2.TaoistSpirit;
import com.example.roam.game.tools.BarLayer;
import com.example.roam.game.tools.LeftSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.TaoistLayer;
import com.example.roam.game.util.WinBarLayer;

public class GameSecondJzgControl {
    private Context context;
    private SceneCuiHai sceneCuiHai;
    private LeftSpirit leftSpirit;
    private JiuCuiHaiSpirit jiuCuiHaiSpirit;
    private BarLayer barLayer;
    private BarOptionSpirit barOptionSpirit;
    private JiuCuiMountainSpirit jiuCuiMountainSpirit;
    private TaoistSpirit taoistSpirit;
    private TaoistLayer taoistLayer;
    public boolean isVisible = true;

    public GameSecondJzgControl(Context context){
        this.context = context;
        sceneCuiHai = new SceneCuiHai(context);
        leftSpirit = new LeftSpirit(context);
        jiuCuiHaiSpirit = new JiuCuiHaiSpirit(context);
        barLayer = new BarLayer(context);
        barOptionSpirit = new BarOptionSpirit(context);
        jiuCuiMountainSpirit = new JiuCuiMountainSpirit(context);
        taoistSpirit = new TaoistSpirit(context);
        taoistSpirit.setTaoistRectLeft(795);
        taoistLayer = new TaoistLayer(context,taoistSpirit,GameConstant.JIU_TAOIST_LAYER);
    }

    public void draw(Canvas canvas){
        if( isVisible ){
            sceneCuiHai.draw(canvas);
            leftSpirit.draw(canvas);
            jiuCuiHaiSpirit.draw(canvas);
            jiuCuiMountainSpirit.draw(canvas);
            taoistSpirit.draw(canvas);
            taoistLayer.draw(canvas);
            barLayer.draw(canvas);
            barOptionSpirit.draw(canvas);
        }
    }

    public void onTouch(MotionEvent event){
        switch ( event.getAction() ){
            case MotionEvent.ACTION_DOWN:

                if(!taoistLayer.isClick()) {
                    clickSceneCuiHai(event);
                    clickLeftSpirit(event);
                    clickCuiHai(event);
                    clickJiuCuiMountain(event);
                    clickDelete(event);
                    clickTaoist(event);
                }else {
                    clickTaoistLayer(event);  //前者功能被后者掩盖。 isClick二执行一跳过一个执行
                    clickOptionCertain(event);
                    clickOptionCancel(event);
                    clickOptionDelete(event);
                }
                break;
        }
    }

    private void clickJiuCuiMountain(MotionEvent event){
        jiuCuiMountainSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickTaoist(MotionEvent event){
        if( taoistSpirit.isVisible && !barLayer.isVisible && taoistSpirit.getRect().contains((int) event.getX(),(int) event.getY()) && !barOptionSpirit.isVisible ){
            taoistLayer.isVisible = true;
            taoistSpirit.isVisible = false;
            taoistLayer.setClick(true);
            barOptionSpirit.setClickThrough(true);
        }
    }

    private void clickTaoistLayer(MotionEvent event){
        taoistLayer.clickDialogSpirit(event,GameConstant.JIU_TAOIST_LAYER);
        taoistLayer.clickOptionCancel(event);
        taoistLayer.clickOptionQuestion(event,barOptionSpirit);
    }

    private void clickOptionCertain(MotionEvent event){
        if ( barOptionSpirit.isVisible && barOptionSpirit.getOptionYes().getRect().contains((int) event.getX(),(int) event.getY())) {
            Intent intent = new Intent(context, GameQuestionActivity.class);
            intent.putExtra("gameIndex",2);
            leftSpirit.finsh(context);
            context.startActivity(intent);
            ((Activity)context).finish();
        }
    }

    private void clickOptionCancel(MotionEvent event){
        if ( barOptionSpirit.isVisible && barOptionSpirit.getOptionNo().getRect().contains((int) event.getX(),(int) event.getY())) {
            barOptionSpirit.isVisible = false;
        }
    }

    private void clickOptionDelete(MotionEvent event){
        if ( barOptionSpirit.getBarDeleteSpirit().getRect().contains((int) event.getX(),(int) event.getY())) {
            barOptionSpirit.isVisible = false;
        }
    }

    public void clickCuiHai(MotionEvent event){
        jiuCuiHaiSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    public void clickDelete(MotionEvent event){
        if (!taoistLayer.isVisible && barLayer.getBarDeleteSpirit().getRect().contains((int) event.getX(),(int) event.getY())) {
            barLayer.isVisible = false;

        }
    }

    public void clickSceneCuiHai(MotionEvent event){
        if (!taoistLayer.isVisible && sceneCuiHai.getRect().contains((int) event.getX(),(int) event.getY())) {
            Log.i(TAG, "clickLeftSpirit: x:"+event.getX()+" y:"+event.getY());
        }
    }

    String TAG = "TAG";
    public void clickLeftSpirit(MotionEvent event){
        if(!taoistLayer.isVisible && leftSpirit.getRect().contains((int) event.getX(),(int) event.getY())){
            leftSpirit.finsh(context);
        }
    }

}
