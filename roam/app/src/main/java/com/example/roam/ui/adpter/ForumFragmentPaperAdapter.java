package com.example.roam.ui.adpter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.roam.ui.fragment.NormalFragment;
import com.example.roam.ui.fragment.OfficeFragment;

/**
 * Create By Visen
 * Date: 2021/10/14
 */
public class ForumFragmentPaperAdapter extends FragmentPagerAdapter {
    public ForumFragmentPaperAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch ( position ){
            case 0:
                return new OfficeFragment();
            case 1:
                return new NormalFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
