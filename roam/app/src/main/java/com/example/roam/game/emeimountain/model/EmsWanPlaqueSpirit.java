package com.example.roam.game.emeimountain.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class EmsWanPlaqueSpirit extends CSpirit {
    public EmsWanPlaqueSpirit(Context context) {
        super(context, 375 ,840 ,733 ,981 );
        this.text = GameConstant.EMS_WAN_PLAQUE_TEXT;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
