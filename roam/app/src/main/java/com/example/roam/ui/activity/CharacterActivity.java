package com.example.roam.ui.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.roam.R;
import com.example.roam.api.Api;
import com.example.roam.model.CharacterBean;
import com.example.roam.model.CharacterEntity;
import com.example.roam.model.gson.User;
import com.example.roam.utils.Constans;
import com.example.roam.utils.DBHelper;
import com.example.roam.utils.HttpUtil;
import com.example.roam.utils.StatusBarUtils;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Illustration
 *
 * @author dengqing
 * @time 2021/10/12
 * @function 显示生成的虚拟人物形象
 */

public class CharacterActivity extends AppCompatActivity {
    private Button btn_confirm;//确认按钮
    private ImageView img_character,img_back;//虚拟人物图片
    private TextView tv_character;//虚拟人物描述
    private String gender, nature;//虚拟人物形象的性别[man,woman]和性格标签[warm,cool,cute]
    public static final int REQUEST_SPIRIT = 3;//跳转的景点精灵界面选择请求码
    private int character = 0;//用户虚拟图片
    private int character_select = 1 ;
    private Retrofit retrofit;
    private boolean isUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        //获取InformationActivity的传递过来的数据信息
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        isUpdate = intent.getBooleanExtra("isUpdate", false);
        gender = bundle.getString("gender");
        nature = bundle.getString("nature");
        findById();//初始化组件
    }


    //初始化组件
    private void findById() {
        img_back=findViewById(R.id.img_back);
        btn_confirm = findViewById(R.id.btn_confirm);
        img_character = findViewById(R.id.img_character);
        tv_character = findViewById(R.id.tv_character);
        //设置虚拟人物形象和文字描述
        setCharacter();
        //确认按钮监听方法
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUserInfo();
                if (!isUpdate) {
                    Intent intent = new Intent(CharacterActivity.this, SpiritActivity.class);
                    startActivityForResult(intent, REQUEST_SPIRIT);
                }else {
                    Intent intent = new Intent(CharacterActivity.this,MainActivity.class);
                    startActivity(intent);
                }
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    //设置虚拟人物图片和文字描述
    private void setCharacter() {
        //虚拟人物键值
        String key = gender + "_" + nature;
        //获取对应的虚拟人物数据
        CharacterEntity charcter = CharacterBean.getSingle(key);
        //设置虚拟人物图片和文字描述
        img_character.setImageResource(charcter.getImg());
        tv_character.setText(charcter.getInstruction());
        character =charcter.getImg();
        switch ( character ){
            case R.drawable.character_man1:
                Constans.u_character = Constans.C_MAN1;
                break;
            case R.drawable.character_man2:
                Constans.u_character = Constans.C_MAN2;
                break;
            case R.drawable.character_man3:
                Constans.u_character = Constans.C_MAN3;
                break;
            case R.drawable.character_woman1:
                Constans.u_character = Constans.C_WOMAN1;
                break;
            case R.drawable.character_woman2:
                Constans.u_character = Constans.C_WOMAN2;
                break;
            case R.drawable.character_woman3:
                Constans.u_character = Constans.C_WOMAN3;
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //跳转景点精灵界面结果返回
        if (requestCode == REQUEST_SPIRIT && resultCode == RESULT_OK) {

        }
    }
    // 插入 用户形象。 获取int值转为String
    public void updateUserTableInformation(){
        DBHelper dbHelper = new DBHelper(CharacterActivity.this,null,null,1);
        SQLiteDatabase ds = dbHelper.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("u_character",character);
        contentValues.put("character_select",character_select);
        Constans.u_character = character+"";
        Constans.character_select=character_select;
        ds.update("user",contentValues,"u_id = ?",new String[]{Constans.u_id+""});
    }

    public void updateUserInfo(){ // 更新用户数据：用户形象
        retrofit = new Retrofit.Builder().baseUrl(HttpUtil.ADDRESS).build();
        retrofit.create(Api.class).updateUser(Constans.u_id,Constans.u_username,Constans.u_password,Constans.u_character,Constans.u_spirit,Constans.u_head,Constans.character_select,Constans.spirit_select,Constans.head_select,HttpUtil.COOKIE).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private static final String TAG = "CharacterActivity";
}