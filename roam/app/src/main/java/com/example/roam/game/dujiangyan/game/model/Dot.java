package com.example.roam.game.dujiangyan.game.model;

public class Dot {
    public int x,y;
    public int status;
    public static final int STATUS_ON = 1; // 围住这个点
    public static final int STATUS_OFF = 0; // 可以走

    public static final int STATUS_IN = 9; // 猫在这个点

    public Dot(int x,int y){
        this.x = x;
        this.y = y;
        this.status = STATUS_OFF; // 默认可以走
    }


    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setXY(int x,int y){
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Dot{" +
                "x=" + x +
                ", y=" + y +
                ", status=" + status +
                '}';
    }
}
