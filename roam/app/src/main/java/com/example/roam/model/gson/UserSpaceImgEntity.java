package com.example.roam.model.gson;

/**
 * Copyright 2022 bejson.com
 */
/**
 * Auto-generated: 2022-03-23 2:31:41
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class UserSpaceImgEntity {

    private boolean success;
    private int code;
    private String message;
    private Data data;
    public void setSuccess(boolean success) {
        this.success = success;
    }
    public boolean getSuccess() {
        return success;
    }

    public void setCode(int code) {
        this.code = code;
    }
    public int getCode() {
        return code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }

    public void setData(Data data) {
        this.data = data;
    }
    public Data getData() {
        return data;
    }

    @Override
    public String toString() {
        return "UserSpaceImgEntity{" +
                "success=" + success +
                ", code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    /**
     * Copyright 2022 bejson.com
     */

    /**
     * Auto-generated: 2022-03-23 2:31:41
     *
     * @author bejson.com (i@bejson.com)
     * @website http://www.bejson.com/java2pojo/
     */
    public class Data {

        private String url;
        public void setUrl(String url) {
            this.url = url;
        }
        public String getUrl() {
            return url;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "url='" + url + '\'' +
                    '}';
        }
    }
}