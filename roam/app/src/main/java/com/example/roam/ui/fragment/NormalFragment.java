package com.example.roam.ui.fragment;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.roam.ui.activity.BaseFragment;
import com.example.roam.api.Api;
import com.example.roam.R;
import com.example.roam.ui.activity.UploadDiaryActivity;
import com.example.roam.ui.activity.WebActivity;
import com.example.roam.ui.adpter.ForumNormalAdapter;
import com.example.roam.model.gson.Forum;
import com.example.roam.utils.HttpUtil;
import com.example.roam.utils.LogUtils;
import com.lcodecore.tkrefreshlayout.RefreshListenerAdapter;
import com.lcodecore.tkrefreshlayout.TwinklingRefreshLayout;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Create By Visen
 * Date: 2021/11/8
 */
public class NormalFragment extends BaseFragment implements View.OnClickListener {  // 广场 4
    private ListView lv_normal;
    private ForumNormalAdapter forumNormalAdapter;
    private Retrofit retrofit;
    private TwinklingRefreshLayout twinklingRefreshLayout;
    private int currentPage = 0;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_normal;
    }

    @Override
    public void initView(View successView) {
        twinklingRefreshLayout = successView.findViewById(R.id.refreshLayout);
        lv_normal = successView.findViewById(R.id.lv_normal);
    }

    @Override
    public void initData() {
        twinklingRefreshLayout.setEnableRefresh(false);
        twinklingRefreshLayout.setEnableLoadmore(true);
        forumNormalAdapter = new ForumNormalAdapter(getContext());
        lv_normal.setAdapter(forumNormalAdapter);
    }

    @Override
    public void initEvent() {
        twinklingRefreshLayout.setOnRefreshListener(new RefreshListenerAdapter() {
            @Override
            public void onLoadMore(TwinklingRefreshLayout refreshLayout) {
                loadMoreData();
            }
        });
    }

    @Override
    public void loadData() {
        currentPage = 0;
        setLoading();
        retrofit = new Retrofit.Builder().baseUrl(HttpUtil.ADDRESS).addConverterFactory(GsonConverterFactory.create()).build();
        retrofit.create(Api.class).findAllScneicByUid(currentPage,0,HttpUtil.COOKIE).enqueue(new Callback<Forum>() {
            @Override
            public void onResponse(Call<Forum> call, Response<Forum> response) {
                Log.i(TAG, "onResponse: dddddddddd  "+response.body().getData().getAllInfo().toString());
                int code = response.code();
                if( code == HttpURLConnection.HTTP_OK ){
                    List<Forum.AllInfo> allInfo = response.body().getData().getAllInfo();
                    if(allInfo!=null && allInfo.size() !=0 ) {
                        forumNormalAdapter.setDataArray(response.body().getData().getAllInfo());
                        setSuccess();
                    }else {
                        setEmpty();
                    }
                }else {
                    setError();
                    LogUtils.e(NormalFragment.class,"服务器错误！");
                }
            }

            @Override
            public void onFailure(Call<Forum> call, Throwable t) {
                setError();
                LogUtils.e(NormalFragment.class,"服务器错误！");
            }
        });
    }

    String TAG = "TAG";

    public void loadMoreData(){
        currentPage += 10;
        retrofit = new Retrofit.Builder().baseUrl(HttpUtil.ADDRESS).addConverterFactory(GsonConverterFactory.create()).build();
        retrofit.create(Api.class).findAllScneicByUid(currentPage,0,HttpUtil.COOKIE).enqueue(new Callback<Forum>() {
            @Override
            public void onResponse(Call<Forum> call, Response<Forum> response) {
                int code = response.code();
                List<Forum.AllInfo> allInfo = response.body().getData().getAllInfo();
                Log.i(TAG, "onResponse: ");
                if( code == HttpURLConnection.HTTP_OK && allInfo!=null){
                    forumNormalAdapter.setMoreDataArray(response.body().getData().getAllInfo());
                    loadMoreSuccess();
                }else {
                    noMoreData();
                }
            }

            @Override
            public void onFailure(Call<Forum> call, Throwable t) {
                noMoreData();
            }
        });
    }

    public void loadMoreSuccess(){
        NormalFragment.this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                twinklingRefreshLayout.finishLoadmore();
            }
        });
    }

    public void noMoreData(){
        NormalFragment.this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext(),"没有更多数据了！",Toast.LENGTH_SHORT).show();
                if( currentPage > 0 ){
                    currentPage -= 10;
                }
                twinklingRefreshLayout.finishLoadmore();
            }
        });
    }


    @Override
    public void onClick(View v) {

    }
}
