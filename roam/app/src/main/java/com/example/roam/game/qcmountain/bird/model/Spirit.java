package com.example.roam.game.qcmountain.bird.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;

public abstract class Spirit {
    public Context context;
    public Bitmap bitmap;
    public int left,top;

    public Spirit(Context context){
        this.context = context;
    }

    public abstract void draw(Canvas canvas);

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    @Override
    public String toString() {
        return "Spirit{" +
                "left=" + left +
                ", top=" + top +
                '}';
    }
}
