package com.example.roam.game.dujiangyan.model;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.utils.Constans;

public class SceneMingMountain extends Spirit {

    public SceneMingMountain(Context context) {
        super(context, 0, 0, 1080, 1794);
        this.bitmap = BitmapFactory.decodeResource(context.getResources(),GameConstant.SCENE1_MING_MOUNTAIN);
    }

    @Override
    public void draw(Canvas canvas) {
        if( isVisible ){
            canvas.drawBitmap(bitmap,null,rect,null);
        }
    }
}
