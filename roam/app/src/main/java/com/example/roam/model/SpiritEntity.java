package com.example.roam.model;
/**
 * Illustration
 * @author dengqing
 * @time 2021/10/26
 * @function 景点精灵实体类==>暂时未用
 *
 */

public class SpiritEntity {
    private int id;//景点精灵编号
    private int img;//景点精灵图片
    private String name;//景点精灵图片
    private String words;//景点精灵话语
    private String instruction;//景点精灵描述

    public SpiritEntity() {
    }

    public SpiritEntity(int id, int img, String name, String words, String instruction) {
        this.id = id;
        this.img = img;
        this.name = name;
        this.words = words;
        this.instruction = instruction;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWords() {
        return words;
    }

    public void setWords(String words) {
        this.words = words;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }
}
