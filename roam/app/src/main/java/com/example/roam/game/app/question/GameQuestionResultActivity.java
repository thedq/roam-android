package com.example.roam.game.app.question;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.roam.R;
import com.example.roam.game.app.dujiangyan.GameSecondDjyActivity;
import com.example.roam.game.app.dujiangyan.GameThirdDjyActivity;
import com.example.roam.game.app.emeimountain.GameSecondEmsActivity;
import com.example.roam.game.app.emeimountain.GameThirdEmsActivity;
import com.example.roam.game.app.jiuzhaigou.GameSecondJzgActvity;
import com.example.roam.game.app.jiuzhaigou.GameThirdJzgActvity;
import com.example.roam.game.app.leshan.GameSecondLsActivity;
import com.example.roam.game.app.leshan.GameThirdLsActivity;
import com.example.roam.game.app.qcmountain.GameSecondActivity;
import com.example.roam.game.app.qcmountain.GameThirdActivity;
import com.example.roam.game.tools.CatchBadgeSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.utils.BadgeUtils;
import com.example.roam.utils.StatusBarUtils;

public class GameQuestionResultActivity extends AppCompatActivity {
    private Button btn_return;
    private int correctNum;
    private int gameIndex;
    private TextView txt_message;
    private boolean isPassTest;
    private ImageView badge ,getBadge;
    private Animation animation,animationEnd;
    private int badges[] = new int[]{GameConstant.Qing_BADGE_B,GameConstant.DU_BADGE_B,GameConstant.DU_BADGE_B,GameConstant.EMS_BADGE_B,GameConstant.EMS_BADGE_B};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_question_result);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        init();
        initData();
        initEvent();
    }

    public void init(){
        Intent intent = getIntent();
        correctNum = intent.getIntExtra("correctNum", 0);
        gameIndex = intent.getIntExtra("gameIndex", 0);
        if( correctNum == 3 ){ isPassTest = true; }
        btn_return = this.findViewById(R.id.btn_return);
        txt_message = this.findViewById(R.id.txt_message);
        badge = this.findViewById(R.id.badge);
        getBadge = this.findViewById(R.id.get_badge);
        badge.setImageResource(badges[gameIndex]);
        animation = AnimationUtils.loadAnimation(this,R.anim.badge_animation);
        animationEnd = AnimationUtils.loadAnimation(this,R.anim.badge_animation_end);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                GameQuestionResultActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        badge.startAnimation(animationEnd);
                        getBadge.startAnimation(animationEnd);
                    }
                });
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                GameQuestionResultActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        badge.setVisibility(View.GONE);
                        getBadge.setVisibility(View.GONE);
                    }
                });
            }
        }).start();
    }

    public void initData(){
        if( !isPassTest ){
            txt_message.setText("抱歉！你没有通过测试...");
        }else {
            txt_message.setText("恭喜你！完成了问答。");
            badge.startAnimation(animation);
            getBadge.startAnimation(animation);
        }
    }

    public void initEvent( ){
        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultLogic();
            }
        });
    }

    public void resultLogic() {
        Intent intent = null;
        if (isPassTest) {
            if (gameIndex == 0) {
                intent = new Intent(this, GameThirdActivity.class);
                BadgeUtils.setBadgeIndex(1);
            } else if (gameIndex == 1) {
                intent = new Intent(this, GameThirdDjyActivity.class);
                BadgeUtils.setBadgeIndex(4);
            } else if (gameIndex == 2) {
                intent = new Intent(this, GameThirdJzgActvity.class);
                BadgeUtils.setBadgeIndex(13);
            } else if (gameIndex == 3) {
                intent = new Intent(this, GameThirdLsActivity.class);
                BadgeUtils.setBadgeIndex(10);
            } else if (gameIndex == 4) {
                intent = new Intent(this, GameThirdEmsActivity.class);
                BadgeUtils.setBadgeIndex(7);
            } else if (gameIndex == 5) {

            }
        } else {
            if (gameIndex == 0) {
                intent = new Intent(this, GameSecondActivity.class);
            } else if (gameIndex == 1) {
                intent = new Intent(this, GameSecondDjyActivity.class);
            } else if (gameIndex == 2) {
                intent = new Intent(this, GameSecondJzgActvity.class);
            } else if (gameIndex == 3) {
                intent = new Intent(this, GameSecondLsActivity.class);
            } else if (gameIndex == 4) {
                intent = new Intent(this, GameSecondEmsActivity.class);
            } else if (gameIndex == 5) {

            }
        }
        intent.putExtra("gameIndex", gameIndex);
        // 需要传导的数据    //  correctNum 正确数
        intent.putExtra("correctNum", correctNum);
        intent.putExtra("questionStatus", 1);
        Log.i("TAG", "resultLogic: gameIndex: " + gameIndex + " isPass: "+isPassTest);
        GameQuestionResultActivity.this.finish();
        startActivity(intent);
    }


}