package com.example.roam.model;

import com.google.gson.annotations.SerializedName;

public class UserEntity {
    @SerializedName("userId")
    private int u_id;
    @SerializedName("userAcc")
    private String u_account;
    @SerializedName("userPwd")
    private String u_password;
    @SerializedName("userName")
    private String u_username;
    @SerializedName("userChara")
    private String u_character;
    @SerializedName("userSpirit")
    private String u_spirit;
    @SerializedName("userHead")
    private String u_head;
    private int account_select;
    @SerializedName("characterSelect")
    private int character_select;
    @SerializedName("spiritSelect")
    private int spirit_select;
    @SerializedName("headSelect")
    private int head_select;
    @SerializedName("userFirstTime")
    private String u_first_time;
    private String u_last_time;

    public UserEntity() {
    }

    public UserEntity(int u_id, String u_account, String u_password, String u_username, String u_character, String u_spirit, String u_head, int account_select, int character_select, int spirit_select, int head_select, String u_first_time, String u_last_time) {
        this.u_id = u_id;
        this.u_account = u_account;
        this.u_password = u_password;
        this.u_username = u_username;
        this.u_character = u_character;
        this.u_spirit = u_spirit;
        this.u_head = u_head;
        this.account_select = account_select;
        this.character_select = character_select;
        this.spirit_select = spirit_select;
        this.head_select = head_select;
        this.u_first_time = u_first_time;
        this.u_last_time = u_last_time;
    }

    public int getU_id() {
        return u_id;
    }

    public void setU_id(int u_id) {
        this.u_id = u_id;
    }

    public String getU_account() {
        return u_account;
    }

    public void setU_account(String u_account) {
        this.u_account = u_account;
    }

    public String getU_password() {
        return u_password;
    }

    public void setU_password(String u_password) {
        this.u_password = u_password;
    }

    public String getU_username() {
        return u_username;
    }

    public void setU_username(String u_username) {
        this.u_username = u_username;
    }

    public String getU_character() {
        return u_character;
    }

    public void setU_character(String u_character) {
        this.u_character = u_character;
    }

    public String getU_spirit() {
        return u_spirit;
    }

    public void setU_spirit(String u_spirit) {
        this.u_spirit = u_spirit;
    }

    public String getU_head() {
        return u_head;
    }

    public void setU_head(String u_head) {
        this.u_head = u_head;
    }

    public int getAccount_select() {
        return account_select;
    }

    public void setAccount_select(int account_select) {
        this.account_select = account_select;
    }

    public int getCharacter_select() {
        return character_select;
    }

    public void setCharacter_select(int character_select) {
        this.character_select = character_select;
    }

    public int getSpirit_select() {
        return spirit_select;
    }

    public void setSpirit_select(int spirit_select) {
        this.spirit_select = spirit_select;
    }

    public int getHead_select() {
        return head_select;
    }

    public void setHead_select(int head_select) {
        this.head_select = head_select;
    }

    public String getU_first_time() {
        return u_first_time;
    }

    public void setU_first_time(String u_first_time) {
        this.u_first_time = u_first_time;
    }

    public String getU_last_time() {
        return u_last_time;
    }

    public void setU_last_time(String u_last_time) {
        this.u_last_time = u_last_time;
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "u_id=" + u_id +
                ", u_account='" + u_account + '\'' +
                ", u_password='" + u_password + '\'' +
                ", u_username='" + u_username + '\'' +
                ", u_character='" + u_character + '\'' +
                ", u_spirit='" + u_spirit + '\'' +
                ", u_head='" + u_head + '\'' +
                ", account_select=" + account_select +
                ", character_select=" + character_select +
                ", spirit_select=" + spirit_select +
                ", head_select=" + head_select +
                ", u_first_time='" + u_first_time + '\'' +
                ", u_last_time='" + u_last_time + '\'' +
                '}';
    }
}