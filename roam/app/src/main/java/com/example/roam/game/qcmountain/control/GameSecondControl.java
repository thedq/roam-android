package com.example.roam.game.qcmountain.control;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;

import androidx.annotation.NonNull;

import com.example.roam.R;
import com.example.roam.ui.activity.PersonalBadgeActivity;
import com.example.roam.game.app.qcmountain.GameFirstActivity;
import com.example.roam.game.app.question.GameQuestionActivity;
import com.example.roam.game.app.qcmountain.GameThirdActivity;
import com.example.roam.game.qcmountain.model.BadgeSpirit;
import com.example.roam.game.tools.BarLayer;
import com.example.roam.game.qcmountain.model.BarOptionSpirit;
import com.example.roam.game.tools.LeftSpirit;
import com.example.roam.game.qcmountain.model.MountainRoadEntranceSpirit;
import com.example.roam.game.qcmountain.model.MountainRoadSpirit;
import com.example.roam.game.qcmountain.model.RoadBackgroundSpirit;
import com.example.roam.game.qcmountain.model.RoadGameSpirit;
import com.example.roam.game.qcmountain.model.layer2.DialogSpirit;
import com.example.roam.game.qcmountain.model.layer2.OptionSpirit;
import com.example.roam.game.qcmountain.model.layer2.TaoistSpirit;
import com.example.roam.game.util.GameConstant;

public class GameSecondControl {
    private Context context;
    private RoadBackgroundSpirit roadBackgroundSpirit;
    private TaoistSpirit taoistSpirit ;
    private LeftSpirit leftSpirit;
    private GameSecondLayerControl gameSecondLayerControl;
    private RoadGameSpirit roadGameSpirit;
    private BarLayer barLayer;
    private BadgeSpirit badgeSpirit;
    private BarOptionSpirit barOptionSpirit;
    private MountainRoadSpirit mountainRoadSpirit;
    private MountainRoadEntranceSpirit mountainRoadEntranceSpirit;
    private int correctNum;
    public boolean isVisible = true;
    public boolean isClick = true; // 能否点击背景
    private static final String TAG = "TAG";
    private MyHandler myHandler;
    // 第二层分层

    public GameSecondControl(Context context){
        init(context);
        initData();
    }

    public void init(Context context){
        this.context = context;
        roadBackgroundSpirit = new RoadBackgroundSpirit(context);
        badgeSpirit = new BadgeSpirit(context);
        taoistSpirit = new TaoistSpirit(context);
        leftSpirit = new LeftSpirit(context);
        gameSecondLayerControl = new GameSecondLayerControl(context);
        roadGameSpirit = new RoadGameSpirit(context);
        barLayer = new BarLayer(context);
        myHandler = new MyHandler();
        mountainRoadSpirit = new MountainRoadSpirit(context);
        mountainRoadEntranceSpirit = new MountainRoadEntranceSpirit(context);
        barOptionSpirit = new BarOptionSpirit(context);
    }

    public void initData(){
        barLayer.isVisible = false;
        barOptionSpirit.setText("回答完问题才能通过路！");

    }

    public void draw(Canvas canvas){
        roadBackgroundSpirit.draw(canvas);
        badgeSpirit.draw(canvas);
        taoistSpirit.draw(canvas);
        roadGameSpirit.draw(canvas);
        leftSpirit.draw(canvas);
        gameSecondLayerControl.draw(canvas);
        mountainRoadSpirit.draw(canvas);
        barLayer.draw(canvas);
        barOptionSpirit.draw(canvas);
    }

    public void clickOptionYes(MotionEvent event){
        if( barOptionSpirit.getOptionYes().getRect().contains((int) event.getX(),(int)event.getY()) && !barOptionSpirit.isClickThrough() ){
            Intent intent = new Intent(context, GameQuestionActivity.class);
            intent.putExtra("gameIndex",0);
            ((Activity)context).finish();
            context.startActivity(intent);
            this.isVisible = false;
            barOptionSpirit.setClickThrough(true);

        }
    }

    public void clickOptionNo(MotionEvent event){
        if( barOptionSpirit.getOptionNo().getRect().contains((int) event.getX(),(int)event.getY())){
            barOptionSpirit.isVisible = false;
        }
    }

    public void clickBarOptionDelete(MotionEvent event){
        if( barOptionSpirit.getBarDeleteSpirit().getRect().contains((int) event.getX(),(int) event.getY())){
            barOptionSpirit.isVisible = false;
        }
    }

    public void clickTaoistSpirit(MotionEvent event){
        if( taoistSpirit.getRect().contains((int)event.getX(),(int)event.getY())){
            isClick = false; // 背景不可点击
            gameSecondLayerControl.isVisible = true;
        }
    }

    public void clickLeft(MotionEvent event){
        if( leftSpirit.getRect().contains((int)event.getX(),(int)event.getY())){
//            Intent intent = new Intent(context, GameFirstActivity.class);
//            context.startActivity(intent);
//            this.isVisible = false;
//            barOptionSpirit.isVisible = false;
            leftSpirit.finsh(context);
        }
    }

    public void clickOptionQuest(MotionEvent event){
        OptionSpirit optionSpiritQuest = gameSecondLayerControl.getOptionLayer().getOptionSpiritQuest();
        if(optionSpiritQuest.getRect().contains((int) (event.getX()),(int) (event.getY()))  ){
            barOptionSpirit.isVisible = true;
            gameSecondLayerControl.setText("想要通过此路？我得先考考你...");
        }
    }

    public void clickOptionLeave(MotionEvent event){
        OptionSpirit optionSpiritLeave = gameSecondLayerControl.getOptionLayer().getOptionSpiritLeave();
        if(optionSpiritLeave.getRect().contains((int) (event.getX()),(int) (event.getY()))  ){
            gameSecondLayerControl.isVisible = false;
            isClick = true; // 复原isClick
        }
    }

    public void clickRoadGameSpirit(MotionEvent event){
        if( roadGameSpirit.getRect().contains((int)event.getX(),(int) event.getY())){
            barLayer.isVisible = true;
            barLayer.text = context.getString(R.string.roadScript);
        }
    }

    public void clickDialog(MotionEvent event){
        DialogSpirit dialogSpirit = gameSecondLayerControl.getDialogSpirit();
        if( dialogSpirit.getRect().contains((int)event.getX(),(int) event.getY())){
            // 实现功能： 点击一次，展示后面的对话
            // 想法：提前分为String[]  或者 获取传递剩下。
            StringBuffer saveText = dialogSpirit.getSaveText();
            String newText = GameConstant.ALLS_SCRIPT.substring(GameConstant.ALLS_SCRIPT.indexOf(saveText.toString())+saveText.length());
            Log.i(TAG, "clickDialog: saveText当前阶段显示的Text :"+saveText);
            gameSecondLayerControl.getDialogSpirit().setText(newText);
        }
    }

    private void deleteBar(MotionEvent event){
        if (barLayer.getBarDeleteSpirit().getRect().contains((int)event.getX(),(int)event.getY() )) {
            barLayer.setVisible(false);
        }
    }

    public void clickBadgeSpirit(MotionEvent event){
        if( badgeSpirit.getRect().contains((int) event.getX(),(int) event.getY())){
            this.isVisible = false;
            Intent intent = new Intent(context, PersonalBadgeActivity.class);
            context.startActivity(intent);

        }
    }

    public void clickBackground(MotionEvent event){
        Log.i(TAG, "clickBackground: x:"+event.getX()+" y:"+event.getY());
    }

    public void clickMountainRoad(MotionEvent event){
        if( mountainRoadSpirit.getRect().contains((int) event.getX(),(int) event.getY())){
            barLayer.isVisible = true;
            barLayer.setText(context.getString(R.string.mountainRoadScript));
        }
    }

    public void clickMountainRoadEntranceSpirit(MotionEvent event){
        if( mountainRoadEntranceSpirit.getRect().contains((int) event.getX(),(int) event.getY()) ){
            barLayer.isVisible = true;
            barLayer.setText(context.getString(R.string.mountainRoadEntranceScript));
        }
    }

    public void handleTouch(MotionEvent event){
        switch ( event.getAction() ){
            case MotionEvent.ACTION_DOWN:
                if( isClick ) {  // 控制能否点击精灵
                    clickMountainRoadEntranceSpirit(event);
                    clickTaoistSpirit(event);
                    clickLeft(event);
                    clickRoadGameSpirit(event);
                    deleteBar(event);
                    clickBadgeSpirit(event);
                    clickBackground(event);
                    clickMountainRoad(event);
                }else if(!isClick) {
                    clickOptionQuest(event);
                    clickOptionLeave(event);
                    clickDialog(event);
                    clickOptionYes(event);
                    clickOptionNo(event);
                    clickBarOptionDelete(event);
                }
                break;
        }
    }

    // 设置通过考试状态
    public void setCorrectNumStatus(){
        this.isClick = false;
        gameSecondLayerControl.isVisible = true;
        gameSecondLayerControl.getDialogSpirit().setText(context.getString(R.string.pass));
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Message message = Message.obtain();
                message.what = 1;
                myHandler.sendMessage(message);
            }
        }).start();
    }

    // 没有通过考试状态
    public void setMissNumStatus(){
        this.isClick = false;
        gameSecondLayerControl.isVisible = true;
        gameSecondLayerControl.getDialogSpirit().setText(context.getString(R.string.miss));
    }


    class MyHandler extends Handler{
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch ( msg.what ){
                case 1:
                    Intent intent = new Intent(context,GameThirdActivity.class);
                    leftSpirit.finsh(context);
                    context.startActivity(intent);
                    GameSecondControl.this.isVisible = false;
                    break;
            }
        }
    }

}
