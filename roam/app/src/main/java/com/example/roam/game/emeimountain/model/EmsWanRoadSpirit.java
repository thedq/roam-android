package com.example.roam.game.emeimountain.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class EmsWanRoadSpirit extends CSpirit {
    public EmsWanRoadSpirit(Context context) {
        super(context, 300, 1571, 780,1793 );
        this.text = GameConstant.EMS_WAN_ROAD_TEXT;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
