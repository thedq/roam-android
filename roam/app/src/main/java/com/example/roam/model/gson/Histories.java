package com.example.roam.model.gson;

import java.util.ArrayList;
import java.util.List;

public class Histories {
    private List<String> historyArray ;

    public List<String> getHistoryArray() {
        return historyArray;
    }

    public void setHistoryArray(List<String> historyArray) {
        this.historyArray = historyArray;
    }

    @Override
    public String toString() {
        return "Histories{" +
                "historyArray=" + historyArray +
                '}';
    }
}
