package com.example.roam.ui.activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.example.roam.R;
import com.example.roam.utils.LogUtils;

public abstract class BaseFragment extends Fragment {
    private View rootView;
    private FrameLayout frameLayout;
    private View errorView;
    private View loadingView;
    private View successView;
    private View noneView;
    private View emptyView;
    private State currentState = State.NONE;

    public enum State{
        ERROR,LOADING,SUCCESS,EMPTY,NONE
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_base,container,false);
        frameLayout = rootView.findViewById(R.id.base_container);
        loadStateView(inflater,container); // 加载各种状态view
        initView(successView);
        initData();
        initEvent();
        loadData();
        return rootView;
    }
    // 1. 获取Id
    public abstract int getLayoutId();

    public abstract void initView(View successView);

    public abstract void initData();

    public abstract void initEvent();

    public abstract void loadData();

    // 2. 使用base_layout替换基础。然后load其他状态的View
    public void loadStateView(LayoutInflater inflater,ViewGroup container){
        // errorView
        errorView = inflater.inflate(R.layout.activity_error, null, false);
        LinearLayout errorLayout = errorView.findViewById(R.id.errorLayout);
        errorLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.i(BaseFragment.class,"网络错误，点击重试！");
                loadData();
            }
        });
        frameLayout.addView(errorView);

        loadingView = inflater.inflate(R.layout.activity_loading,null,false);
        frameLayout.addView(loadingView);

        successView = inflater.inflate(getLayoutId(),null,false);
        frameLayout.addView(successView);

        noneView = inflater.inflate(R.layout.activity_none,null,false);
        frameLayout.addView(noneView);

        emptyView = inflater.inflate(R.layout.activity_empty,null,false);
        frameLayout.addView(emptyView);

        setState(State.NONE);
    }

    public void setState(State state){
        this.currentState = state;
        if (currentState == State.SUCCESS) {
            successView.setVisibility(View.VISIBLE);
        } else {
            successView.setVisibility(View.GONE);
        }

        if (currentState == State.LOADING) {
            loadingView.setVisibility(View.VISIBLE);
        } else {
            loadingView.setVisibility(View.GONE);
        }

        if (currentState == State.ERROR) {
            errorView.setVisibility(View.VISIBLE);
        } else {
            errorView.setVisibility(View.GONE);
        }

        if(currentState == State.EMPTY){
            emptyView.setVisibility(View.VISIBLE);
        }else {
            emptyView.setVisibility(View.GONE);
        }

        if(currentState == State.NONE){
            noneView.setVisibility(View.VISIBLE);
        }else {
            noneView.setVisibility(View.GONE);
        }
    }

    public void setError(){
        setState(State.ERROR);
    }

    public void setLoading(){
        setState(State.LOADING);
    }

    public void setSuccess(){
        setState(State.SUCCESS);
    }

    public void setEmpty(){
        setState(State.EMPTY);
    }

    public void setNone(){
        setState(State.NONE);
    }


}
