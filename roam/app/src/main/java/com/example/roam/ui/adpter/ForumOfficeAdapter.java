package com.example.roam.ui.adpter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.roam.R;
import com.example.roam.ui.activity.WebActivity;
import com.example.roam.api.Api;
import com.example.roam.model.gson.ForumOffice;
import com.example.roam.utils.HttpUtil;
import com.example.roam.utils.LogUtils;
import com.google.android.material.imageview.ShapeableImageView;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ForumOfficeAdapter extends BaseAdapter {
    private Context context;
    public List<ForumOffice.ScenicWithPageList> dataArray = new ArrayList<>();
    private int type = 1;
    private Drawable agreeFill,agree;

    public ForumOfficeAdapter(Context context){
        this.context = context;
        agreeFill = context.getResources().getDrawable(R.drawable.agree_fill);
        agreeFill.setBounds(0, 0, agreeFill.getMinimumWidth(), agreeFill.getMinimumHeight());
        agree = context.getResources().getDrawable(R.drawable.agree);
        agree.setBounds(0, 0, agree.getMinimumWidth(), agree.getMinimumHeight());
    }

    @Override
    public int getCount() {
        return dataArray.size();
    }

    @Override
    public Object getItem(int position) {
        return dataArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setDataArray(List<ForumOffice.ScenicWithPageList> dataArray){
        this.dataArray.clear();
        if( dataArray != null ) {
            this.dataArray.addAll(dataArray);
        }
        notifyDataSetChanged();
    }

    public void setLoaderDataArray(List<ForumOffice.ScenicWithPageList> dataArray){
        LogUtils.i(ForumOfficeAdapter.class,"moreDataArray:"+dataArray.toString());
        this.dataArray.addAll(dataArray);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ForumOffice.ScenicWithPageList data = dataArray.get(position);
        List<String> pictures = data.getImgs();
        List<String> tagTexts = data.getTagTexts();

        ViewHolder viewHolder = null;
        if( convertView == null ) {
            viewHolder = new ViewHolder();
           convertView = LayoutInflater.from(context).inflate(R.layout.item_fragment_forum, null, false);
           viewHolder.txt_title = convertView.findViewById(R.id.txt_title);
           viewHolder.img_c1 = convertView.findViewById(R.id.img_c1);
           viewHolder.img_c2 = convertView.findViewById(R.id.img_c2);
           viewHolder.img_c3 = convertView.findViewById(R.id.img_c3);
           viewHolder.txt_t1 = convertView.findViewById(R.id.txt_t1);
           viewHolder.txt_t2 = convertView.findViewById(R.id.txt_t2);
           viewHolder.txt_t3 = convertView.findViewById(R.id.txt_t3);
            viewHolder.txt_commentNum = convertView.findViewById(R.id.txt_commentNum);
            viewHolder.rb_agree = convertView.findViewById(R.id.rb_agree);
            viewHolder.img_c1.setVisibility(View.GONE);
            viewHolder.img_c2.setVisibility(View.GONE);
            viewHolder.img_c3.setVisibility(View.GONE);
            viewHolder.txt_t1.setVisibility(View.GONE);
            viewHolder.txt_t2.setVisibility(View.GONE);
            viewHolder.txt_t3.setVisibility(View.GONE);

            viewHolder.txt_title.setText(data.getSrInfo().getScenicReleaseTitle());
            viewHolder.txt_commentNum.setText(data.getComment()+"");
            viewHolder.rb_agree.setText(data.getSrInfo().getScenicReleaseGood()+"");
            if( data.isGood() ){
                viewHolder.rb_agree.setCompoundDrawables(agreeFill, null, null, null);
            }else {
                viewHolder.rb_agree.setCompoundDrawables(agree, null, null, null);
            }


            taglogic(tagTexts,viewHolder);
           pictureLogic(pictures,viewHolder);

           convertView.setTag(viewHolder);
       }else {
            viewHolder = (ViewHolder) convertView.getTag();
            viewHolder.img_c1.setVisibility(View.GONE);
            viewHolder.img_c2.setVisibility(View.GONE);
            viewHolder.img_c3.setVisibility(View.GONE);
            viewHolder.txt_title.setText(data.getSrInfo().getScenicReleaseTitle());
            viewHolder.rb_agree.setText(data.getSrInfo().getScenicReleaseGood()+"");
            viewHolder.txt_commentNum.setText(data.getComment()+"");
            if( data.isGood() ){
                viewHolder.rb_agree.setCompoundDrawables(agreeFill, null, null, null);
            }else {
                viewHolder.rb_agree.setCompoundDrawables(agree, null, null, null);
            }
            taglogic(tagTexts,viewHolder);
            pictureLogic(pictures,viewHolder);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initForumHtmlNet(data.getSrInfo().getScenicReleaseId());
            }
        });

        ViewHolder finalViewHolder = viewHolder;
        viewHolder.rb_agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Retrofit retrofit = new Retrofit.Builder().baseUrl(HttpUtil.ADDRESS).build();
                retrofit.create(Api.class).giveOrDeleGood(data.getSrInfo().getScenicReleaseId(),HttpUtil.COOKIE).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        data.setGood(!data.isGood());
                        // 按照data.isGood相反设置
                        if (data.isGood()) {
                            finalViewHolder.rb_agree.setCompoundDrawables(agreeFill,null,null,null);
                            finalViewHolder.rb_agree.setText(Integer.parseInt(finalViewHolder.rb_agree.getText().toString()) +1 +"" );
                            Toast.makeText(context,"点赞成功",Toast.LENGTH_SHORT).show();
                        }else {
                            finalViewHolder.rb_agree.setCompoundDrawables(agree,null,null,null);
                            finalViewHolder.rb_agree.setText(Integer.parseInt(finalViewHolder.rb_agree.getText().toString()) -1 +"" );
                            Toast.makeText(context,"取消点赞",Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }
        });

        return convertView;
    }

    public void pictureLogic(List<String> pictures,ViewHolder viewHolder){
        if (pictures.size() > 0) {
            viewHolder.img_c1.setVisibility(View.VISIBLE);
            Glide.with(context).load(pictures.get(0)).into(viewHolder.img_c1);
        }
        if (pictures.size() > 1) {
            viewHolder.img_c2.setVisibility(View.VISIBLE);
            Glide.with(context).load(pictures.get(1)).into(viewHolder.img_c2);
        }
        if (pictures.size() > 2) {
            viewHolder.img_c3.setVisibility(View.VISIBLE);
            Glide.with(context).load(pictures.get(2)).into(viewHolder.img_c3);
        }
    }

    public void taglogic(List<String> tagTexts,ViewHolder viewHolder){
        if( tagTexts.size() > 0 ){
            viewHolder.txt_t1.setVisibility(View.VISIBLE);
            viewHolder.txt_t1.setText("#"+tagTexts.get(0));
        }
        if( tagTexts.size() > 1){
            viewHolder.txt_t2.setVisibility(View.VISIBLE);
            viewHolder.txt_t2.setText("#"+tagTexts.get(1));
        }
        if( tagTexts.size() > 2){
            viewHolder.txt_t3.setVisibility(View.VISIBLE);
            viewHolder.txt_t3.setText("#"+tagTexts.get(2));
        }
    }

    public void initForumHtmlNet(int srid){
        Intent intent = new Intent(context, WebActivity.class);
        intent.putExtra("url", HttpUtil.ADDRESS+"getView/getScenicReleaseView/"+srid);
        context.startActivity(intent);
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    class ViewHolder{
        TextView txt_title ,txt_t1,txt_t2,txt_t3 ,txt_goodNum ,txt_commentNum;
        ShapeableImageView img_c1 , img_c2 , img_c3;
        RadioButton rb_agree;
    }
}
