package com.example.roam.game.qcmountain.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.util.BitmapUtil;
import com.example.roam.game.util.GameConstant;

public class EntranceBackgroundSpirit extends CSpirit{
    public EntranceBackgroundSpirit(Context context) {
        super(context,0,0,1080,1920);
        this.bitmap = BitmapUtil.changeBitmapSize(getContext(), GameConstant.BG,width,height);
    }

    @Override
    public void draw(Canvas canvas) {
        if (isVisible) {
            canvas.drawBitmap(bitmap, null, rect, null);
        }
    }
}
