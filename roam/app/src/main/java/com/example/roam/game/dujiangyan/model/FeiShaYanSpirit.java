package com.example.roam.game.dujiangyan.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class FeiShaYanSpirit extends CSpirit {

    public FeiShaYanSpirit(Context context) {
        super(context, 255, 767, 495, 1706);
        this.text = GameConstant.FEI_SHA_YANG;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }

}
