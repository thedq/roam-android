package com.example.roam.game.emeimountain.control;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import com.example.roam.game.app.dujiangyan.GameSecondDjyActivity;
import com.example.roam.game.app.emeimountain.GameSecondEmsActivity;
import com.example.roam.game.app.emeimountain.GameThirdEmsActivity;
import com.example.roam.game.app.leshan.GameSecondLsActivity;
import com.example.roam.game.emeimountain.model.EmsEntranceBottomSpirit;
import com.example.roam.game.emeimountain.model.EmsEntrancePlaque2Spirit;
import com.example.roam.game.emeimountain.model.EmsEntrancePlaque3Spirit;
import com.example.roam.game.emeimountain.model.EmsEntrancePlaqueSpirit;
import com.example.roam.game.emeimountain.model.EmsEntranceTopSpirit;
import com.example.roam.game.emeimountain.model.SceneEntrance;
import com.example.roam.game.emeimountain.view.GameSecondUiEms;
import com.example.roam.game.qcmountain.model.BarOptionSpirit;
import com.example.roam.game.tools.BarLayer;
import com.example.roam.game.tools.CatchBadgeSpirit;
import com.example.roam.game.tools.LeftSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.utils.BadgeUtils;

public class GameFirstControlEms {
    private SceneEntrance sceneEntrance;
    private Context context;
    private EmsEntranceTopSpirit emsEntranceTopSpirit;
    private EmsEntrancePlaqueSpirit emsEntrancePlaqueSpirit;
    private EmsEntrancePlaque2Spirit emsEntrancePlaque2Spirit;
    private EmsEntrancePlaque3Spirit emsEntrancePlaque3Spirit;
    private EmsEntranceBottomSpirit emsEntranceBottomSpirit;
    private BarLayer barLayer;
    private BarOptionSpirit barOptionSpirit;
    private CatchBadgeSpirit catchBadgeSpirit;
    private LeftSpirit leftSpirit;
    public boolean isVisible = true;

    public GameFirstControlEms(Context context){
        this.context = context;
        sceneEntrance = new SceneEntrance(context);
        emsEntranceTopSpirit = new EmsEntranceTopSpirit(context);
        emsEntranceBottomSpirit = new EmsEntranceBottomSpirit(context);
        emsEntrancePlaqueSpirit = new EmsEntrancePlaqueSpirit(context);
        emsEntrancePlaque2Spirit = new EmsEntrancePlaque2Spirit(context);
        emsEntrancePlaque3Spirit = new EmsEntrancePlaque3Spirit(context);
        barLayer = new BarLayer(context);
        barOptionSpirit = new BarOptionSpirit(context);
        catchBadgeSpirit = new CatchBadgeSpirit(context, GameConstant.EMS_BADGE_A);
        leftSpirit = new LeftSpirit(context);
    }

    public void draw(Canvas canvas){
        if( isVisible ){
            sceneEntrance.draw(canvas);
            emsEntranceTopSpirit.draw(canvas);
            emsEntranceBottomSpirit.draw(canvas);
            emsEntrancePlaqueSpirit.draw(canvas);
            emsEntrancePlaque2Spirit.draw(canvas);
            emsEntrancePlaque3Spirit.draw(canvas);
            leftSpirit.draw(canvas);
            barOptionSpirit.draw(canvas);
            barLayer.draw(canvas);
            catchBadgeSpirit.draw(canvas);
        }
    }

    public void onTouch(MotionEvent event){
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                clickSceneEntrance(event);
                clickEmsTop(event);
                clickEmsPla1(event);
                clickEmsPla2(event);
                clickEmsPla3(event);
                clickEmsBottom(event);
                clickDelete(event);
                clickOptionCertain(event);
                clickOptionCancel(event);
                clickOptionDelete(event);
                clickLeftSpirit(event);
                break;
        }
    }

    private void clickLeftSpirit(MotionEvent event){
        if(leftSpirit.getRect().contains((int) event.getX(),(int) event.getY())){
            leftSpirit.finsh(context);
        }
    }

    private void clickEmsBottom(MotionEvent event){
        emsEntranceBottomSpirit.clickSpiritOption(event,barLayer,barOptionSpirit);
    }

    private void clickEmsTop(MotionEvent event){
        emsEntranceTopSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickEmsPla1(MotionEvent event){
        emsEntrancePlaqueSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickEmsPla2(MotionEvent event){
        emsEntrancePlaque2Spirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickEmsPla3(MotionEvent event){
        emsEntrancePlaque3Spirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickDelete(MotionEvent event){
        barLayer.clickDelete(event);
    }

    private void clickOptionCertain(MotionEvent event){
        if (barOptionSpirit.getOptionYes().getRect().contains((int) event.getX(),(int) event.getY()) && !barOptionSpirit.isClickThrough()) {
            waitToStartActivity();
            barOptionSpirit.setClickThrough(true);
        }
    }

    public void waitToStartActivity(){  // 2
        if (!BadgeUtils.getBadges().getBadgeArray().contains(6)) {
            catchBadgeSpirit.isVisible = true;
            catchBadgeSpirit.setStartAnimation(true); // 启动动画
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            BadgeUtils.setBadgeIndex(6);
                            Intent intent = new Intent(context,GameSecondEmsActivity.class);
                            context.startActivity(intent);
                            leftSpirit.finsh(context);
                        }
                    });
                }
            }).start();
        }else {
            BadgeUtils.setBadgeIndex(6);
            Intent intent = new Intent(context, GameSecondEmsActivity.class);
            context.startActivity(intent);
            leftSpirit.finsh(context);
        }
    }

    private void clickOptionCancel(MotionEvent event){
        barOptionSpirit.clickOptionCancel(event);
    }

    private void clickOptionDelete(MotionEvent event){
        barOptionSpirit.clickOptionDelete(event);
    }

    String TAG = "TAG";
    public void clickSceneEntrance(MotionEvent event){
        if (sceneEntrance.getRect().contains((int) event.getX(),(int) event.getY())) {

            Log.i(TAG, "clickSceneEntrance: x:"+event.getX()+" y:"+event.getY());
        }
    }
}
