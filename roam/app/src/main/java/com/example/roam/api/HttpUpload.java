package com.example.roam.api;

import com.example.roam.model.gson.InformationHead;
import com.example.roam.model.gson.UserSpaceImgEntity;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface HttpUpload {
    /**
     *  用户上传空间图片
     *  part : 图片
     * */
    @POST("OSS/uploadUserSpaceImg")
    @Multipart
    Call<UserSpaceImgEntity> uploadUserSpaceImg(@Part MultipartBody.Part part);


    @POST("OSS/uploadUserHeadImg")
    @Multipart
    Call<InformationHead> uploadUserHeadImg(@Part MultipartBody.Part part);
}
