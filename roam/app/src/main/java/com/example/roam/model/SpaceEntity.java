package com.example.roam.model;

import com.google.gson.annotations.SerializedName;

public class SpaceEntity {
    @SerializedName("spaceId")
    private int space_id; //个人空间编号
    private String space_title; //个人空间标题
    @SerializedName("spaceContent")
    private String space_content; //个人空间内容
    @SerializedName("spaceTime")
    private String space_release_time; // 空间发布时间
    @SerializedName("spaceGood")
    private int space_good; //空间点赞数
    private String space_img; //空间展示图片
    private int user_id; //用户id

    public SpaceEntity() {
    }

    public SpaceEntity(int space_id, String space_title, String space_content, String space_release_time, int space_good, String space_img, int user_id) {
        this.space_id = space_id;
        this.space_title = space_title;
        this.space_content = space_content;
        this.space_release_time = space_release_time;
        this.space_good = space_good;
        this.space_img = space_img;
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return "SpaceEntity{" +
                "space_id=" + space_id +
                ", space_title='" + space_title + '\'' +
                ", space_content='" + space_content + '\'' +
                ", space_release_time='" + space_release_time + '\'' +
                ", space_good=" + space_good +
                ", space_img='" + space_img + '\'' +
                ", user_id=" + user_id +
                '}';
    }

    public int getSpace_id() {
        return space_id;
    }

    public void setSpace_id(int space_id) {
        this.space_id = space_id;
    }

    public String getSpace_title() {
        return space_title;
    }

    public void setSpace_title(String space_title) {
        this.space_title = space_title;
    }

    public String getSpace_content() {
        return space_content;
    }

    public void setSpace_content(String space_content) {
        this.space_content = space_content;
    }

    public String getSpace_release_time() {
        return space_release_time;
    }

    public void setSpace_release_time(String space_release_time) {
        this.space_release_time = space_release_time;
    }

    public int getSpace_good() {
        return space_good;
    }

    public void setSpace_good(int space_good) {
        this.space_good = space_good;
    }

    public String getSpace_img() {
        return space_img;
    }

    public void setSpace_img(String space_img) {
        this.space_img = space_img;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
