package com.example.roam.ui.adpter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.roam.R;

import java.util.List;

public class InformationGenderAdapter extends BaseAdapter {
    private Context mContext;

    private List<Integer> mList;
    private int selectorPosition;

    public InformationGenderAdapter(Context context, List<Integer> mList) {
        this.mContext = context;
        this.mList = mList;

    }

    @Override
    public int getCount() {
        return mList != null ? mList.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mList != null ? mList.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return mList != null ? position : 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = View.inflate(mContext, R.layout.information_gender_item, null);
        LinearLayout linearLayout = (LinearLayout ) convertView.findViewById(R.id.layout);
        ImageView imageView=(ImageView)convertView.findViewById(R.id.img_gender);
        imageView.setImageResource(mList.get(position));
        linearLayout.setBackgroundColor(Color.parseColor("#ccece9"));
        //如果当前的position等于传过来点击的position,就去改变他的状态
        if (selectorPosition == position) {
            linearLayout.setBackgroundColor(Color.parseColor("#89adaa"));
        } else {
            //其他的恢复原来的状态
            linearLayout.setBackgroundColor(Color.parseColor("#ccece9"));
        }
        return convertView;
    }


    public void changeState(int pos) {
        selectorPosition = pos;
        notifyDataSetChanged();
    }
}
