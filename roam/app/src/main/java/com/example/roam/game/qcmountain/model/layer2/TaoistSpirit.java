package com.example.roam.game.qcmountain.model.layer2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.BitmapUtil;
import com.example.roam.game.util.GameConstant;
import com.example.roam.utils.Constans;

public class TaoistSpirit extends Spirit {
    public TaoistSpirit(Context context) {
        super(context, 695, 1140, 695+282, 1140+601);
        this.bitmap = BitmapUtil.changeBitmapSize(getContext(), GameConstant.TOAST, width,height);
        this.isVisible = true;
    }

    @Override
    public void draw(Canvas canvas) {
        if (isVisible) {
            canvas.drawBitmap(bitmap, null, rect, null);
        }
    }

    public void setTaoistRectLeft(int left){
        this.rect.left = (int)(left * Constans.SCALE_WIDTH);
        this.rect.right = rect.left+(int)(282*Constans.SCALE_WIDTH);
    }

    public void setTaoistRectTop(int top){
        this.rect.top = (int)(top * Constans.SCALE_HEIGHT);
        this.rect.bottom = rect.top+(int)(601 * Constans.SCALE_HEIGHT);
    }

    public void setTaoistBitmap(int bitmapId){
        this.bitmap = BitmapUtil.changeBitmapSize(getContext(),bitmapId,width,height);
    }
}
