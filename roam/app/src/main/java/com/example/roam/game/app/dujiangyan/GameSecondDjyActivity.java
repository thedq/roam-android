package com.example.roam.game.app.dujiangyan;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.example.roam.R;
import com.example.roam.game.dujiangyan.control.GameSecondControlDJY;
import com.example.roam.game.dujiangyan.view.GameSecondUiDJY;
import com.example.roam.model.gson.Badges;
import com.example.roam.utils.BadgeUtils;
import com.example.roam.utils.StatusBarUtils;

public class GameSecondDjyActivity extends AppCompatActivity {
    private GameSecondUiDJY gameSecondUiDJY;
    private GameSecondControlDJY gameSecondControlDJY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_second_djy);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        gameSecondUiDJY = this.findViewById(R.id.scene2);
        gameSecondControlDJY = gameSecondUiDJY.getGameSecondControlDJY();
        Badges badges = BadgeUtils.getBadges();
        Log.i("TAG", "onCreate: badges:"+badges.toString());

    }

    @Override
    protected void onResume() {
        super.onResume();
        gameSecondControlDJY.isVisible = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameSecondControlDJY.isVisible = false;
    }
}