package com.example.roam.game.app.question;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.roam.R;
import com.example.roam.game.util.GameConstant;
import com.example.roam.utils.Constans;
import com.example.roam.utils.StatusBarUtils;

public class GameQuestionActivity extends AppCompatActivity {
    private RelativeLayout[] relativeLayouts ;
    private TextView txt_question ,txt_num;
    private CheckBox[] checkBoxes;
    private Button btn_certain;
    private ImageView img_spirit,img_character;
    private boolean isCertain; // 是否已选择
    private char option = 65; //  选择选项
    private String answer = ""; // 正确答案
    private int questionNum = 0 , correctNum = 0 ; // questionNum 当前题目索引，correctNum 回答正确题目数
    private int gameIndex;
    // 青城山，都江堰，九寨沟，乐山，峨眉山
    // 3个U盘。
    // 我64G*2，知识512G Android，JDK，python，mysql，idea，pycharm clon windows10 Photoshop，Hbuilder，

    private int [][] questionArray = new int[][]{
            {R.string.qcs_question1,R.string.qcs_question2,R.string.qcs_question2},
            {R.string.question1,R.string.question2,R.string.question3},
            {R.string.jzg_question1,R.string.jzg_question2,R.string.jzg_question3},
            {R.string.ls_question1,R.string.ls_question2,R.string.ls_question3},
            {R.string.ems_question1,R.string.ems_question2,R.string.ems_question3}};

    private int [][][] optionArray = new int[][][]{
            {{R.string.qcs_opt1a,R.string.qcs_opt1b,R.string.qcs_opt1c,R.string.qcs_opt1d},{R.string.qcs_opt2a,R.string.qcs_opt2b,R.string.qcs_opt2c,R.string.qcs_opt2d},
            {R.string.qcs_opt3a,R.string.qcs_opt3b,R.string.qcs_opt3c,R.string.qcs_opt3d}},

            {{R.string.q1_opta,R.string.q1_optb,R.string.q1_optc,R.string.q1_optd}, {R.string.q2_opta,R.string.q2_optb,R.string.q2_optc,R.string.q2_optd},
            {R.string.q3_opta,R.string.q3_optb,R.string.q3_optc,R.string.q3_optd}},

            {{R.string.jzg_opa1a,R.string.jzg_opa1b,R.string.jzg_opa1c,R.string.jzg_opa1d},{R.string.jzg_opa2a,R.string.jzg_opa2b, R.string.jzg_opa2c,R.string.jzg_opa2d},
            {R.string.jzg_opa3a,R.string.jzg_opa3b,R.string.jzg_opa3c,R.string.jzg_opa3d}},

            {{R.string.ls_opt1a,R.string.ls_opt1b,R.string.ls_opt1c,R.string.ls_opt1d}, {R.string.ls_opt2a,R.string.ls_opt2b,R.string.ls_opt2c,R.string.ls_opt2d},
            {R.string.ls_opt3a,R.string.ls_opt3b,R.string.ls_opt3c,R.string.ls_opt3d}},

            {{R.string.ems_opa1a,R.string.ems_opa1b,R.string.ems_opa1c,R.string.ems_opa1d},{R.string.ems_opa2a,R.string.ems_opa2b,R.string.ems_opa2c,R.string.ems_opa2d},
            {R.string.ems_opa3a,R.string.ems_opa3b,R.string.ems_opa3c,R.string.ems_opa3d}}};

    private int [][] ansArray = new int[][]{
            {R.string.qcs_ans1,R.string.qcs_ans2,R.string.qcs_ans3},
            {R.string.q1_ans,R.string.q2_ans,R.string.q3_ans},
            {R.string.jzg_ans1,R.string.jzg_ans2,R.string.jzg_ans3},
            {R.string.ls_ans1,R.string.ls_ans2,R.string.ls_ans3},
            {R.string.ems_ans1,R.string.ems_ans2,R.string.ems_ans3}};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色
        Intent intent = getIntent();
        gameIndex = intent.getIntExtra("gameIndex", 0);
        init();
        initData();
        initEvent();
    }

    public void init(){
        relativeLayouts = new RelativeLayout[4];
        relativeLayouts[0] = this.findViewById(R.id.re_a);
        relativeLayouts[1] = this.findViewById(R.id.re_b);
        relativeLayouts[2] = this.findViewById(R.id.re_c);
        relativeLayouts[3] = this.findViewById(R.id.re_d);

        checkBoxes = new CheckBox[4];
        checkBoxes[0] = this.findViewById(R.id.ans_a);
        checkBoxes[1] = this.findViewById(R.id.ans_b);
        checkBoxes[2] = this.findViewById(R.id.ans_c);
        checkBoxes[3] = this.findViewById(R.id.ans_d);

        txt_question = this.findViewById(R.id.txt_question);
        btn_certain = this.findViewById(R.id.btn_certain);
        img_spirit = this.findViewById(R.id.img_spirit);
        img_character = this.findViewById(R.id.img_character);
        txt_num = this.findViewById(R.id.txt_num);
    }

    public void initData(){
        txt_question.setText(this.getText(questionArray[gameIndex][0]));
        checkBoxes[0].setText(this.getText(optionArray[gameIndex][0][0]));
        checkBoxes[1].setText(this.getText(optionArray[gameIndex][0][1]));
        checkBoxes[2].setText(this.getText(optionArray[gameIndex][0][2]));
        checkBoxes[3].setText(this.getText(optionArray[gameIndex][0][3]));
        answer = this.getString(ansArray[gameIndex][0]);
        Glide.with(this).load(Constans.u_spirit).into(img_spirit);
        Glide.with(this).load(Constans.u_character).into(img_character);
    }

    public void initEvent(){
        for (int i = 0; i < checkBoxes.length; i++) {
            int finalI = i;
            checkBoxes[i].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        relativeLayouts[finalI].setBackground(GameQuestionActivity.this.getDrawable(R.drawable.option_shape_selecte)); //
                        for (CheckBox checkBox : checkBoxes) {
                            if( checkBox.getId() != compoundButton.getId()){
                                checkBox.setChecked(false);
                            }
                        }
                        btn_certain.setBackground(GameQuestionActivity.this.getDrawable(R.drawable.btn_certain_select_shape));
                        isCertain = true;
                        option = (char) (option+finalI);
                    }else {
                        relativeLayouts[finalI].setBackground(GameQuestionActivity.this.getDrawable(R.drawable.option_shape));  //
                        btn_certain.setBackground(GameQuestionActivity.this.getDrawable(R.drawable.shape_btn_certain));
                        isCertain = false;
                        option = 65;
                    }
                }
            });
        }

        for (int i = 0; i < relativeLayouts.length; i++) {
            int finalI = i;
            relativeLayouts[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkBoxes[finalI].setChecked(!checkBoxes[finalI].isChecked());
                }
            });
        }

        btn_certain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCertain) {
                    String checkOpt = option+"";
                    boolean equals = checkOpt.equals(answer);
                    if( equals ){
                        correctNum++;
                    }
                    Toast.makeText(GameQuestionActivity.this,"选项："+checkOpt+" 答案："+answer+" 本题:"+equals,Toast.LENGTH_SHORT).show();
                    int answerI = answer.toCharArray()[0] - 65;
                    relativeLayouts[answerI].setBackground(GameQuestionActivity.this.getDrawable(R.drawable.option_correct));
                    questionNum++;
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    isCertain = false;
                                    btn_certain.setBackground(GameQuestionActivity.this.getDrawable(R.drawable.shape_btn_certain));

                                }
                            });

                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if( questionNum == 3 ){
                                        Intent intent = new Intent(GameQuestionActivity.this,GameQuestionResultActivity.class);
                                        intent.putExtra("correctNum",correctNum);
                                        intent.putExtra("gameIndex",gameIndex);
                                        startActivity(intent);
                                        GameQuestionActivity.this.finish();
                                    } else {
                                        txt_num.setText("本题：" + (questionNum+1) + "/3题");
                                        setDefault(answerI);// 全部否定
                                        setQuestText();
                                    }
                                }
                            });
                            // 到最终题目返回
                        }
                    }).start();
                }
            }
        });
    }

    public void setQuestText(){
        txt_question.setText(this.getText(questionArray[gameIndex][questionNum]));
        checkBoxes[0].setText(this.getText(optionArray[gameIndex][questionNum][0]));
        checkBoxes[1].setText(this.getText(optionArray[gameIndex][questionNum][1]));
        checkBoxes[2].setText(this.getText(optionArray[gameIndex][questionNum][2]));
        checkBoxes[3].setText(this.getText(optionArray[gameIndex][questionNum][3]));
        answer = this.getString(ansArray[gameIndex][questionNum]);
    }

    public void setDefault(int answerI){
        for (CheckBox checkBox : checkBoxes) {
            checkBox.setChecked(false);
        }
        relativeLayouts[answerI].setBackground(GameQuestionActivity.this.getDrawable(R.drawable.option_shape));
        isCertain = true;
        btn_certain.setBackground(GameQuestionActivity.this.getDrawable(R.drawable.btn_certain_select_shape));

    }
}