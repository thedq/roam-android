package com.example.roam.game.app.emeimountain;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.roam.R;
import com.example.roam.game.emeimountain.control.GameFirstControlEms;
import com.example.roam.game.emeimountain.view.GameFirstUiEms;
import com.example.roam.utils.StatusBarUtils;

public class GameFirstEmsActivity extends AppCompatActivity {

    private GameFirstUiEms gameFirstUiEms;
    private GameFirstControlEms gameFirstControlEms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_first_ems);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        gameFirstUiEms = this.findViewById(R.id.scene1);
        gameFirstControlEms = gameFirstUiEms.getGameFirstControlEms();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameFirstControlEms.isVisible = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameFirstControlEms.isVisible = true;
    }
}