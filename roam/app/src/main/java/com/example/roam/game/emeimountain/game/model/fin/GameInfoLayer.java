package com.example.roam.game.emeimountain.game.model.fin;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.example.roam.game.util.State;

public class GameInfoLayer {
    private Context context;
    private InfoBarSpirit infoBarSpirit;
    private Rect timeBarRect;
    private ScoreNumBarSpirit scoreNumBarSpirit;
    private TimeNumBarSpirit timeNumBarSpirit;
    private long beginTime = 0;
    private long runTime = 0;
    private State currentState;
    private boolean isPause;
    private boolean isVisible = false;
    private String timeStr;

    public GameInfoLayer(Context context){
        this.context = context;
        timeBarRect = new Rect(0,0,1080,150);
        infoBarSpirit = new InfoBarSpirit(context,0,0,1080,150);
        scoreNumBarSpirit = new ScoreNumBarSpirit(context,50,0,0 + 350,150);
        timeNumBarSpirit = new TimeNumBarSpirit(context,1080 - 300, 0,1080,150);
    }

    String TAG = "TAG";
    public void draw(Canvas canvas){
        if (isVisible) {
            infoBarSpirit.draw(canvas);
            scoreNumBarSpirit.draw(canvas);
            timeNumBarSpirit.draw(canvas);
        }
    }

    public void setScore(int score){
        scoreNumBarSpirit.setScore(score);
    }

    public void resetScore(){
        scoreNumBarSpirit.resetScore();

    }

    public void resetTime(){
        timeNumBarSpirit.resetTime();
        isPause = false;
    }

    public void pauseTime(){
        timeNumBarSpirit.pauseTime();
        isPause = true;
    }

    public void resumeTime(){
        timeNumBarSpirit.resumeTime();
        isPause = false;
    }

    public ScoreNumBarSpirit getScoreNumBarSpirit() {
        return scoreNumBarSpirit;
    }

    public void setScoreNumBarSpirit(ScoreNumBarSpirit scoreNumBarSpirit) {
        this.scoreNumBarSpirit = scoreNumBarSpirit;
    }

    public TimeNumBarSpirit getTimeNumBarSpirit() {
        return timeNumBarSpirit;
    }

    public void setTimeNumBarSpirit(TimeNumBarSpirit timeNumBarSpirit) {
        this.timeNumBarSpirit = timeNumBarSpirit;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public boolean isPause() {
        return isPause;
    }
}
