package com.example.roam.model;

import com.example.roam.R;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Illustration
 *
 * @author dengqing
 * @time 2021/10/26
 * @function 景点精灵数据类
 */

public class SpiritBean {
    private static Map<String, SpiritEntity> mapSpirit = new LinkedHashMap<String, SpiritEntity>();//景点精灵信息

    static {
        mapSpirit.put("1", new SpiritEntity(1, R.drawable.spirit_giant_panda, "圆圆", "(我叫圆圆，不爱睡觉觉，有俩黑眼圈。)", "\t大熊猫为食肉目熊科大熊猫属动物，是熊科、大熊猫亚科和大熊猫属唯一的哺乳动物，被誉为“活化石”和“中国国宝”，属中国国家一级保护动物。大熊猫是中国特有物种，主要栖息地为四川、陕西和甘肃的山区。"));
        mapSpirit.put("2", new SpiritEntity(2, R.drawable.spirit_monkey, "阿肃", "(我叫阿肃，每天数自己尾巴上的圈圈，有几个来着？)", "\t小熊猫生活在2500-4800米的落叶和针叶林中的温带气候中。栖居于大的树洞或石洞和岩石缝中。早晚出来活动觅食，白天多在洞里或大树的荫凉处睡觉。成年小熊猫在交配季节之外很少互动。吃浆果、花朵、鸟蛋、竹叶和其他植物的小叶子。 竹子的叶子是小熊猫的主要食物来源。食物被前爪抓住并在坐着、站立或仰卧时送入口中。以这种方式抓住的食物被插入嘴巴的一侧，剪断，然后在吞咽之前广泛咀嚼。生活在缅甸北部、尼泊尔、印度锡金地区以及中国四川西部和云南地区海拔2200至4800米的喜马拉雅山脉中。"));
        mapSpirit.put("3", new SpiritEntity(3, R.drawable.spirit_little_panda, "小呆", "(我叫小呆，超级爱鸟蛋！好好吃喔…)", "\t川金丝猴是灵长目猴科动物，为国家一级保护动物。其体型为中等猴类。鼻孔向上仰，颜面部为蓝色，无颊囊。颊部及颈侧棕红，肩背具长毛，色泽金黄，尾与体等长或更长。主要天敌有豺、狼、金猫、豹、雕、鹫、鹰等。仅分布于中国四川、甘肃、陕西和湖北。"));


    }

    // 获得所有的景点精灵
    public static Collection<SpiritEntity> getAll() {
        return mapSpirit.values();
    }

    // 根据指定的id获得景点精灵
    public static SpiritEntity getSingle(String id) {
        return mapSpirit.get(id);
    }
}
