package com.example.roam.game.emeimountain.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.PaintUtil;

public class EmsTopPlazaSpirit extends CSpirit {
    public EmsTopPlazaSpirit(Context context) {
        super(context, 38, 1392, 522, 1793);
        this.text = "峨眉山金顶始建于唐朝，屋顶为锡瓦所盖，元代时又被称为“银顶”。寺侧有卧云庵，内有饭堂和客房可供游客食宿。在金顶可观看峨眉四大奇观——日出、云海、佛光、圣灯。金顶与千佛顶、万佛顶相邻，万佛顶上亦有建筑，且有观光索道连接金顶与接引殿。";
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
