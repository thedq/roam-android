package com.example.roam.game.leshan.model;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.GameConstant;

public class SceneGiantBuddha extends Spirit {
    public SceneGiantBuddha(Context context) {
        super(context, 0, 0, 1080, 1794);
        bitmap = BitmapFactory.decodeResource(context.getResources(), GameConstant.SCENE3_GIANT_BUDDHA);
    }

    @Override
    public void draw(Canvas canvas) {
        if( isVisible ){
            canvas.drawBitmap(bitmap,null,rect,null);
        }
    }
}
