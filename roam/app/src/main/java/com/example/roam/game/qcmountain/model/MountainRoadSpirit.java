package com.example.roam.game.qcmountain.model;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.example.roam.game.util.PaintUtil;

public class MountainRoadSpirit extends CSpirit{
    private Paint paint;
    public MountainRoadSpirit(Context context) { //  x 380 750  y 780 1100
        super(context, 300,780 , 800, 1200);
        paint = new Paint();
        paint.setStrokeWidth(50);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLUE);
        this.isVisible = true;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if( isVisible ){
            canvas.drawRect( rect, PaintUtil.getPaint());
        }
    }
}
