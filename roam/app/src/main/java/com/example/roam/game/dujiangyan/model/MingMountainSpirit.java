package com.example.roam.game.dujiangyan.model;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class MingMountainSpirit extends CSpirit {
    public MingMountainSpirit(Context context) {
        super(context, 0, 623, 296, 1031);
        this.text = GameConstant.MING_MOUNTAIN_TEXT;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if( isVisible ){
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
