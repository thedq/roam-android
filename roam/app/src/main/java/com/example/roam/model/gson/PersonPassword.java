package com.example.roam.model.gson;

import com.google.gson.annotations.SerializedName;

public class PersonPassword {

    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "PersonPassword{" +
                "password='" + password + '\'' +
                '}';
    }
}
