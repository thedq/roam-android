package com.example.roam.game.util;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class PaintUtil {

    private static Paint paint = new Paint(Paint.STRIKE_THRU_TEXT_FLAG);

    public static Paint getPaint(){
        paint.setColor(Color.parseColor("#00aa0000"));
        paint.setStrokeWidth(20);
        return paint;
    }
}
