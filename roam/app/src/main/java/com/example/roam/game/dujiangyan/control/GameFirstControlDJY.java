package com.example.roam.game.dujiangyan.control;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import com.example.roam.game.app.dujiangyan.GameSecondDjyActivity;
import com.example.roam.game.dujiangyan.model.AheadSpirit;
import com.example.roam.game.dujiangyan.model.MingJianSpirit;
import com.example.roam.game.dujiangyan.model.MingMountainSpirit;
import com.example.roam.game.dujiangyan.model.SceneMingMountain;
import com.example.roam.game.qcmountain.model.BadgeSpirit;
import com.example.roam.game.qcmountain.model.BarOptionSpirit;
import com.example.roam.game.tools.BarLayer;
import com.example.roam.game.tools.CatchBadgeSpirit;
import com.example.roam.game.tools.LeftSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.ui.activity.PersonalBadgeActivity;
import com.example.roam.utils.BadgeUtils;
import com.example.roam.utils.JsonCacheUtil;

public class GameFirstControlDJY {
    private Context context;
    private SceneMingMountain sceneMingMountain;
    private MingMountainSpirit mingMountainSpirit;
    private AheadSpirit aheadSpirit;
    private MingJianSpirit mingJianSpirit;
    private BarLayer barLayer;
    private BarOptionSpirit barOptionSpirit;
    private CatchBadgeSpirit catchBadgeSpirit;
    private LeftSpirit leftSpirit;
    public boolean isVisible = true;
    private JsonCacheUtil jsonCacheUtil;

    public GameFirstControlDJY(Context context){
        this.context = context;
        sceneMingMountain = new SceneMingMountain(context);
        mingMountainSpirit = new MingMountainSpirit(context);
        aheadSpirit = new AheadSpirit(context);
        mingJianSpirit = new MingJianSpirit(context);
        barLayer = new BarLayer(context);
        barOptionSpirit = new BarOptionSpirit(context);
        catchBadgeSpirit = new CatchBadgeSpirit(context, GameConstant.Qing_BADGE_A);
//        badgeSpirit = new BadgeSpirit(context);
        jsonCacheUtil = new JsonCacheUtil();
        leftSpirit = new LeftSpirit(context);
    }

    public void draw(Canvas canvas) {
        if (isVisible) {
            sceneMingMountain.draw(canvas);
            mingMountainSpirit.draw(canvas);
            aheadSpirit.draw(canvas);
            mingJianSpirit.draw(canvas);
            catchBadgeSpirit.draw(canvas);
            leftSpirit.draw(canvas);
//            badgeSpirit.draw(canvas);
            barLayer.draw(canvas);
            barOptionSpirit.draw(canvas);
        }
    }

    public void onTouch(MotionEvent event){
        switch ( event.getAction() ){
            case MotionEvent.ACTION_DOWN:
                clickMingMountain(event);
                clickMingMountainSpirit(event);
                clickMingJianSpirit(event);
                clickDelete(event);
                clickAheadSpirit(event);
                clickLeftSpirit(event);
                clickOptionCertain(event);
                clickOptionCancel(event);
                clickOptionDelete(event);
//                clickBadgeSpirit(event);
                break;
        }
    }

    private void clickLeftSpirit(MotionEvent event){
        if(leftSpirit.getRect().contains((int) event.getX(),(int) event.getY())) {
            ((Activity) context).finish();
        }
    }

//    private void clickBadgeSpirit(MotionEvent event){
//        if(badgeSpirit.getRect().contains((int) event.getX(),(int) event.getY())){
//            Intent intent = new Intent(context, PersonalBadgeActivity.class);
//            context.startActivity(intent);
//        }
//    }

    public void clickMingJianSpirit(MotionEvent event){
        mingJianSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    public void clickAheadSpirit(MotionEvent event){
//        if( aheadSpirit.getRect().contains((int) event.getX(),(int) event.getY())){
//            barOptionSpirit.isVisible = true;
//            barOptionSpirit.text = ;
//        }
        aheadSpirit.clickSpiritOption(event,barLayer,barOptionSpirit);
    }

    public void clickOptionCertain(MotionEvent event){
        if (barOptionSpirit.getOptionYes().getRect().contains((int) event.getX(),(int) event.getY()) && !barOptionSpirit.isClickThrough() ) {
            waitToStartActivity();
            BadgeUtils.setBadgeIndex(3);
            barOptionSpirit.isVisible = false;
            barOptionSpirit.setClickThrough(true);
        }
    }

    public void waitToStartActivity(){  // 2
        if (!BadgeUtils.getBadges().getBadgeArray().contains(3)) {
            catchBadgeSpirit.setStartAnimation(true); // 启动动画
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Intent intent = new Intent(context, GameSecondDjyActivity.class);
                    context.startActivity(intent);
                    ((Activity)context).finish();
                }
            }).start();
        }else {
            Intent intent = new Intent(context, GameSecondDjyActivity.class);
            ((Activity)context).finish();
            context.startActivity(intent);
        }
    }

    public void clickOptionCancel(MotionEvent event){
        if(barOptionSpirit.getOptionNo().getRect().contains((int) event.getX(),(int) event.getY())){
            barOptionSpirit.isVisible = false;
        }
    }

    public void clickOptionDelete(MotionEvent event){
        if (barOptionSpirit.getBarDeleteSpirit().getRect().contains((int) event.getX(),(int) event.getY())) {
            barOptionSpirit.isVisible = false;
        }
    }

    public void clickMingMountainSpirit(MotionEvent event){
//        if( mingMountainSpirit.getRect().contains((int) event.getX(),(int) event.getY())){
//            barLayer.setText(mingMountainSpirit.getText());
//            barLayer.isVisible = true;
//        }
        mingMountainSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    public void clickDelete(MotionEvent event){
        if( barLayer.getBarDeleteSpirit().getRect().contains((int) event.getX(),(int) event.getY())){
            barLayer.isVisible = false;
        }
    }

    String TAG = "TAG";
    public void clickMingMountain(MotionEvent event){
        if( sceneMingMountain.getRect().contains((int)event.getX(),(int)event.getY()) ){

            Log.i(TAG, "clickMingMountain: x:"+event.getX()+" y:"+event.getY());
        }
    }


}
