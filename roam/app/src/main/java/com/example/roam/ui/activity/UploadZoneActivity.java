package com.example.roam.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.roam.R;
import com.example.roam.api.Api;
import com.example.roam.model.gson.Forum;
import com.example.roam.ui.adpter.ZoneAdapter;
import com.example.roam.ui.fragment.NormalFragment;
import com.example.roam.utils.Constans;
import com.example.roam.utils.HttpUtil;
import com.example.roam.utils.LogUtils;
import com.example.roam.utils.StatusBarUtils;
import com.lcodecore.tkrefreshlayout.RefreshListenerAdapter;
import com.lcodecore.tkrefreshlayout.TwinklingRefreshLayout;
import java.net.HttpURLConnection;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UploadZoneActivity extends AppCompatActivity {

    private static final String TAG = "UploadZoneActivity";
    private ImageView img_left , img_head;
    private TextView txt_uname;
    private FrameLayout baseContainer;
    private TwinklingRefreshLayout refreshLayout;
    private ListView listView;
    private ZoneAdapter zoneAdapter;
    private int currentPage = 0;
    private State currentState = State.NONE;
    private View errorView;
    private View loadingView;
    private View successView;
    private View emptyView;
    private View noneView;
    private Retrofit retrofit;

    public enum State{
        LOADING,ERROR,SUCCESS,EMPTY,NONE;
    }
    // ...
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_zone);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        baseContainer = this.findViewById(R.id.base_container);
        loadStateView(); // 给framLayout加载各种view
        iniView();
        iniData();
        iniEvent();
        loadData();
    }

    public void loadStateView(){
        errorView = LayoutInflater.from(this).inflate(R.layout.activity_error,null,false);
        baseContainer.addView(errorView);
        loadingView = LayoutInflater.from(this).inflate(R.layout.activity_loading,null,false);
        baseContainer.addView(loadingView);
        successView = LayoutInflater.from(this).inflate(R.layout.item_zone_success,null,false);
        baseContainer.addView(successView);
        emptyView = LayoutInflater.from(this).inflate(R.layout.activity_empty,null,false);
        baseContainer.addView(emptyView);
        noneView = LayoutInflater.from(this).inflate(R.layout.activity_none,null,false);
        baseContainer.addView(noneView);
        setState(State.NONE);
    }

    public void setState(State state) {
        this.currentState = state;
        if (currentState == state.SUCCESS) {
            successView.setVisibility(View.VISIBLE);
        } else {
            successView.setVisibility(View.GONE);
        }
        if (currentState == State.LOADING) {
            loadingView.setVisibility(View.VISIBLE);
        } else {
            loadingView.setVisibility(View.GONE);
        }
        if (currentState == State.ERROR) {
            if( currentPage > 0 ){ currentPage -= 10;}
            errorView.setVisibility(View.VISIBLE);
        } else {
            errorView.setVisibility(View.GONE);
        }
        if(currentState == State.EMPTY){
            emptyView.setVisibility(View.VISIBLE);
        }else {
            emptyView.setVisibility(View.GONE);
        }
        if(currentState == State.NONE){
            noneView.setVisibility(View.VISIBLE);
        }else {
            noneView.setVisibility(View.GONE);
        }
    }

    private void iniView() {
        img_left = this.findViewById(R.id.img_left);
        img_head = this.findViewById(R.id.img_head);
        txt_uname = this.findViewById(R.id.txt_uname);

        refreshLayout = successView.findViewById(R.id.refreshLayout);
        listView = successView.findViewById(R.id.listView);
        zoneAdapter = new ZoneAdapter(this);
        listView.setAdapter(zoneAdapter);
    }

    private void iniData() {
        Glide.with(this).load(Constans.u_head).into(img_head);
        txt_uname.setText(Constans.u_username);
        refreshLayout.setEnableRefresh(false);
        refreshLayout.setEnableLoadmore(true);
    }

    private void iniEvent() {
        img_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UploadZoneActivity.this.finish();
            }
        });

        refreshLayout.setOnRefreshListener(new RefreshListenerAdapter() {
            @Override
            public void onLoadMore(TwinklingRefreshLayout refreshLayout) {
                loadMoreData();
            }
        });
    }

    public void loadData() {
        currentPage = 0;
        setState(State.LOADING);
        retrofit = new Retrofit.Builder().baseUrl(HttpUtil.ADDRESS).addConverterFactory(GsonConverterFactory.create()).build();
        retrofit.create(Api.class).findAllScneicByUid(currentPage,0,HttpUtil.COOKIE).enqueue(new Callback<Forum>() {
            @Override
            public void onResponse(retrofit2.Call<Forum> call, Response<Forum> response) {
                Log.i(TAG, "onResponse: response: "+response.body());
                int code = response.code();
                if( code == HttpURLConnection.HTTP_OK && response.body() != null && response.body().getData().getAllInfo()!=null ){
                    List<Forum.AllInfo> allInfo = response.body().getData().getAllInfo();
                    zoneAdapter.setData(allInfo);
                    setState(State.SUCCESS);
                }else {
                    setState(State.EMPTY);
                }
            }

            @Override
            public void onFailure(Call<Forum> call, Throwable t) {
                setState(State.ERROR);
                LogUtils.e(NormalFragment.class,"服务器错误！");
            }
        });
    }

    public void loadMoreData() {
        currentPage += 10;
        retrofit = new Retrofit.Builder().baseUrl(HttpUtil.ADDRESS).addConverterFactory(GsonConverterFactory.create()).build();
        retrofit.create(Api.class).findAllScneicByUid(currentPage,0,HttpUtil.COOKIE).enqueue(new Callback<Forum>() {
            @Override
            public void onResponse(Call<Forum> call, Response<Forum> response) {
                int code = response.code();
                List<Forum.AllInfo> allInfo = response.body().getData().getAllInfo();
                if( code == HttpURLConnection.HTTP_OK && allInfo!=null){
                    setState(State.SUCCESS);
                    zoneAdapter.setMoreData(allInfo);
                    loadMoreSuccess();
                }else {
                    noMoreData();
                    loadMoreSuccess();
                }
            }

            @Override
            public void onFailure(Call<Forum> call, Throwable t) {
                setState(State.ERROR);
                loadMoreSuccess();
            }
        });
    }

    public void noMoreData(){
        UploadZoneActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(UploadZoneActivity.this,"没有更多数据了！",Toast.LENGTH_SHORT).show();
                if( currentPage > 0 ){
                    currentPage -= 10;
                }
            }
        });
    }

    public void loadMoreSuccess(){
        UploadZoneActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                refreshLayout.finishLoadmore();
            }
        });
    }

}