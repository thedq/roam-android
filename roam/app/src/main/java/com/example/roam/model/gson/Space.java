package com.example.roam.model.gson;

import com.example.roam.model.SpaceEntity;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Create By Visen
 * Date: 2021/11/30
 */
public class Space {
    private boolean success;
    private int code;
    private String message;
    private data data;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public data getData() {
        return data;
    }

    public void setData(data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Space{" +
                "success=" + success +
                ", code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public class data{
        @SerializedName("userSpaceList")
        private ArrayList<SpaceEntity> userSpaceList;

        public ArrayList<SpaceEntity> getUserSpaceList() {
            return userSpaceList;
        }

        public void setUserSpaceList(ArrayList<SpaceEntity> userSpaceList) {
            this.userSpaceList = userSpaceList;
        }

        @Override
        public String toString() {
            return "data{" +
                    "userSpaceList=" + userSpaceList +
                    '}';
        }
    }

}
