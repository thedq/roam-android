package com.example.roam.game.dujiangyan.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import com.example.roam.game.dujiangyan.control.GameFirstControlDJY;
import com.example.roam.game.qcmountain.control.GameFirstControl;

public class GameFirstUiDJY extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder surfaceHolder;
    private GameFirstControlDJY gameFirstControlDJY;

    public GameFirstUiDJY(Context context) {
        super(context);
        initGame();
    }

    public GameFirstUiDJY(Context context, AttributeSet attrs) {
        super(context, attrs);
        initGame();
    }

    public GameFirstUiDJY(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initGame();
    }

    public void initGame(){
        surfaceHolder = this.getHolder();
        surfaceHolder.addCallback(this);
        gameFirstControlDJY = new GameFirstControlDJY(getContext());
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        Runner runner = new Runner();
        runner.start();
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {

    }

    class Runner extends Thread{
        @Override
        public void run() {
            while ( gameFirstControlDJY.isVisible ){
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                draw();
            }
        }
    }

    public void draw(){
        Canvas canvas = surfaceHolder.lockCanvas();
        gameFirstControlDJY.draw(canvas);
        surfaceHolder.unlockCanvasAndPost(canvas);
    }

    public GameFirstControlDJY getGameFirstControlDJY() {
        return gameFirstControlDJY;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gameFirstControlDJY.onTouch(event);
        return true;
    }
}
