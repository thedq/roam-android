package com.example.roam.game.tools;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;

import com.example.roam.game.qcmountain.model.BarDeleteSpirit;
import com.example.roam.game.qcmountain.model.Spirit;

public class BarLayer { // 游戏选项面板图层
    // 解决，用图层，基于面板的位置放置组件！
    // 门口的石狮依然是尊贵、不可侵犯的标记
    // 点击精灵的弹窗包含对应的信息。信息储存(Constant)和怎么多点多弹窗
    // 文字换行问题 11个字符一行,限制为7行
    public Context context;
    public BarSpirit barSpirit;
    public BarDeleteSpirit barDeleteSpirit;
    public String text;
    private Paint paint;
    public boolean isVisible = false;

    public BarLayer(Context context){
        this.context = context;
        this.barSpirit = new BarSpirit(context,145,520,930,1240);
        this.barSpirit.setVisible(false);
        this.barDeleteSpirit = new BarDeleteSpirit(context,930-100,520,930,520+100);
        this.paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.paint.setColor(Color.BLACK);
        this.paint.setTextSize(50);
        this.paint.setStyle(Paint.Style.FILL);
    }
    // 提前设置text
    public void draw(Canvas canvas){
        if(isVisible){
            canvas.drawBitmap(barSpirit.bitmap,null,barSpirit.getRect(),null);
            canvas.drawBitmap(barDeleteSpirit.bitmap,null,barDeleteSpirit.getRect(),null);
            drawText(canvas);
        }
    }

    public void setDetail(){
        barSpirit.setDetail();
    }

    // 换行文本方法
    public void drawText(Canvas canvas){
        // 1. 切割成11个字符每个String[]
        char[] chars = text.toCharArray();
        StringBuffer stringBuffer = new StringBuffer();
        int row = 0; //行数，每行间隔80px
        int count = 0;
        for (int i = 0; i < chars.length; i++) {
            stringBuffer.append(chars[i]); // i%11!=0继续 且 i!=chars.length-1
            count++;
            if(row == 7 ){
                break; // 限制为7行
            }
           if( count==11){
               canvas.drawText(stringBuffer.toString(),(float) (barSpirit.left+barSpirit.width*0.1),(float) (barSpirit.top+barSpirit.top*0.3+row*70),paint); // 左右80%的区域，上面20%开始
               row++;
               stringBuffer = new StringBuffer();
               count = 0;  //复原字符数量
           }
           if( i == chars.length -1 && !stringBuffer.toString().equals("")){                // i到最后而且stingBuffer有内容
               canvas.drawText(stringBuffer.toString(),(float) (barSpirit.left+barSpirit.width*0.1),(float) (barSpirit.top+barSpirit.top*0.3+row*70),paint); // 左右80%的区域，上面20%开始
           }
        }
    }

    public void clickDelete(MotionEvent event){
        if ( isVisible && this.getBarDeleteSpirit().getRect().contains((int) event.getX(),(int) event.getY())) {
            this.isVisible = false;
        }
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public BarDeleteSpirit getBarDeleteSpirit() {
        return barDeleteSpirit;
    }

    public BarSpirit getBarSpirit() {
        return barSpirit;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
