package com.example.roam.game.app.qcmountain;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.roam.R;
import com.example.roam.game.qcmountain.view.GameSecondUI;
import com.example.roam.utils.StatusBarUtils;

public class GameSecondActivity extends AppCompatActivity {
    private GameSecondUI gameSecondUI;
    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_second);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        init();
        initData();
    }

    public void init(){
        intent = getIntent();
        gameSecondUI = this.findViewById(R.id.gameui);
    }

    public void initData(){
        if( intent.getIntExtra("questionStatus",0)  == 1) {         // 在答题状态下
            if (intent.getIntExtra("correctNum", 0) == 3) { // 对照答题正确数
                gameSecondUI.getGameSecondControl().setCorrectNumStatus();
            } else if (intent.getIntExtra("correctNum", 0) != 3) {
                gameSecondUI.getGameSecondControl().setMissNumStatus();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameSecondUI.getGameSecondControl().isVisible = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameSecondUI.getGameSecondControl().isVisible = false;
    }
}