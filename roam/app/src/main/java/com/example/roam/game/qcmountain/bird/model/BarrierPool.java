package com.example.roam.game.qcmountain.bird.model;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

public class BarrierPool {

    /**
     *  对象池逻辑：
     *  1. 一个对象数组，添加一定量对象
     *  2. 获取对象（也是删除）
     *  3. 返回对象
     *
     *
     * */

    private static List<Barrier> pool = new ArrayList<>();
    public Context context;

    public BarrierPool (Context context){
        this.context = context;
        for (int i = 0; i < 16; i++) {
            pool.add(new Barrier(context));
        }
    }
    // 从池中获取对象
    public Barrier getBarrier(){
        int size = pool.size();
        if( size > 0 ){ // 当池中有对象，才可以拿
            return pool.remove(size-1); // 移除对象
        }else {
            return new Barrier(context); // 没有对象智能new
        }
    }

    //对象归还给容器
    public void setPool(Barrier barrier){
        pool.add(barrier);
    }
}
