package com.example.roam.model;

/**
 * Illustration
 * @author dengqing
 * @time 2021/10/17
 * @function 人物标签选择数据项
 *
 */

public class LabelViewGroupItem {
    private String key;
    private String value;

    public LabelViewGroupItem(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
