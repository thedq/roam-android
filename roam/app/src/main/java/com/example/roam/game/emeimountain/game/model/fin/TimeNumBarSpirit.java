package com.example.roam.game.emeimountain.game.model.fin;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.example.roam.R;
import com.example.roam.game.emeimountain.game.model.Spirit;
import com.example.roam.game.util.State;

public class TimeNumBarSpirit extends Spirit {
    private NumLayer numLayer;
    private long beginTime;
    private long nowTime;
    private long runTime;
    private long pauseTime;
    private long pausePointTime;
    private boolean isPause;
    private String timeStr;
    private State currentState;

    public TimeNumBarSpirit(Context context,int left,int top,int right,int bottom) {
        super(context, left, top, right, bottom);
        this.bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.sc_bar);
        numLayer = new NumLayer(context,1080 - 300+80,0,1080,150);
    }

    String TAG = "TAG";
    @Override
    public void draw(Canvas canvas) {
        if (isVisible) {
            canvas.drawBitmap(bitmap,null,rect,null);
            numLayer.draw(canvas);
            numLogic(canvas);
        }
    }

    private void numLogic(Canvas canvas){
        if(!isPause && currentState == State.START ) {
            runTime = nowTime - beginTime - pauseTime;
            String time = String.valueOf(runTime);
            if( time.length() > 3 ) {
                timeStr = time.substring(0, time.length() - 3);
                numLayer.setTimeStr(timeStr);
            }
            numLayer.draw(canvas);
        }
    }

    public void resetTime(){
        numLayer.setTimeStr("000");
        pauseTime = 0;
        nowTime = System.currentTimeMillis();
        beginTime = System.currentTimeMillis();
        isPause = false;
    }

    public String getTimeStr() {
        return timeStr;
    }

    public void pauseTime(){
        isPause = true;
        pausePointTime = System.currentTimeMillis();
    }

    public void resumeTime(){
        isPause = false;
        pauseTime = System.currentTimeMillis() - pausePointTime;
    }

    public long getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(long beginTime) {
        this.beginTime = beginTime;
    }

    public long getNowTime() {
        return nowTime;
    }

    public void setNowTime(long nowTime) {
        this.nowTime = nowTime;
    }

    public long getRunTime() {
        return runTime;
    }

    public void setRunTime(long runTime) {
        this.runTime = runTime;
    }

    public boolean isPause() {
        return isPause;
    }

    public void setPause(boolean pause) {
        isPause = pause;
    }

    public State getCurrentState() {
        return currentState;
    }

    public void setCurrentState(State currentState) {
        this.currentState = currentState;
    }
}
