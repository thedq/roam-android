package com.example.roam.game.qcmountain.model.layer2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;


public class OptionLayer {
    private OptionSpirit optionSpiritQuest, optionSpiritLeave;
    private String textQuest,textLeave;
    private Rect questRect,leaveRect;
    private Context context;
    private Paint paint;
    public boolean isVisible ;

    public OptionLayer(Context context){
        this.context = context;
        this.optionSpiritQuest = new OptionSpirit(context);
        this.optionSpiritLeave = new OptionSpirit(context);
        this.paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.textQuest = "询 问";
        this.textLeave = "离 开";
        initData();
    }

    public void initData(){
        this.optionSpiritQuest.setRect(new Rect(730, 1110, 730+315, 1110+120));
        this.optionSpiritLeave.setRect(new Rect(730,1260,730+315,1260+120));
        this.optionSpiritQuest.isVisible = true;
        this.optionSpiritLeave.isVisible = true;
        this.paint.setTextSize(50);
        this.paint.setColor(Color.BLACK);
        this.paint.setTypeface(Typeface.DEFAULT_BOLD);
        this.paint.setStyle(Paint.Style.FILL);
        this.questRect = optionSpiritQuest.getRect();
        this.leaveRect = optionSpiritLeave.getRect();
    }

    public void draw(Canvas canvas){
        if(isVisible){
            optionSpiritQuest.draw(canvas);
            optionSpiritLeave.draw(canvas);
            canvas.drawText(textQuest,(float)(questRect.left+optionSpiritQuest.width/3),(float) (questRect.top+optionSpiritQuest.height*0.55),paint );
            canvas.drawText(textLeave,(float)(leaveRect.left+optionSpiritLeave.width/3),(float) (leaveRect.top+optionSpiritLeave.height*0.55),paint );
        }
    }

    public OptionSpirit getOptionSpiritQuest() {
        return optionSpiritQuest;
    }

    public OptionSpirit getOptionSpiritLeave() {
        return optionSpiritLeave;
    }
}
