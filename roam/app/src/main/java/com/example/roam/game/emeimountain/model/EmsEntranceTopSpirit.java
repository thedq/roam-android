package com.example.roam.game.emeimountain.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class EmsEntranceTopSpirit extends CSpirit {

    public EmsEntranceTopSpirit(Context context) {
        super(context, 26, 166, 1026, 512);
        this.text = GameConstant.EMS_ENTRANCE_TOP_TEXT;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }

}
