package com.example.roam.ui.fragment;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.example.roam.R;
import com.example.roam.ui.activity.BaseFragment;
import com.example.roam.ui.adpter.ForumOfficeAdapter;
import com.example.roam.api.Api;
import com.example.roam.model.gson.ForumOffice;
import com.example.roam.utils.HttpUtil;
import com.example.roam.utils.LogUtils;
import com.lcodecore.tkrefreshlayout.RefreshListenerAdapter;
import com.lcodecore.tkrefreshlayout.TwinklingRefreshLayout;

import java.net.HttpURLConnection;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
/**
 * Create By Visen
 * Date: 2021/10/14
 */
public class IntroduceFragment extends BaseFragment {  // 介绍 1
    private ListView lv_introduce;
    private ForumOfficeAdapter forumOfficeAdapter;
    private Retrofit retrofit;
    private TwinklingRefreshLayout twinklingRefreshLayout;
    private int currentPageSize = 0;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_introduce;
    }

    @Override
    public void initView(View successView) {
        lv_introduce = successView.findViewById(R.id.lv_introduce);
        forumOfficeAdapter = new ForumOfficeAdapter(getContext());
        twinklingRefreshLayout = successView.findViewById(R.id.refreshLayout);
    }

    @Override
    public void initData() {
        twinklingRefreshLayout.setEnableRefresh(false);
        twinklingRefreshLayout.setEnableLoadmore(true);
        lv_introduce.setAdapter(forumOfficeAdapter);
        forumOfficeAdapter.setType(1);
    }

    @Override
    public void initEvent() {
        twinklingRefreshLayout.setOnRefreshListener(new RefreshListenerAdapter() {
            @Override
            public void onLoadMore(TwinklingRefreshLayout refreshLayout) {
                loaderMoreData();
            }
        });
    }

    @Override
    public void loadData() {
        setLoading();
        retrofit = new Retrofit.Builder().baseUrl(HttpUtil.ADDRESS).addConverterFactory(GsonConverterFactory.create()).build();
        retrofit.create(Api.class).findAllScenicWithPage(currentPageSize,1,HttpUtil.COOKIE).enqueue(new Callback<ForumOffice>() {
            @Override
            public void onResponse(Call<ForumOffice> call, Response<ForumOffice> response) {
                int code = response.code();
                if( code == 200 ){
                    // 问题： 文章是否被当前用户点赞怎么判定
                    // 问题： 点赞后怎么获取被点赞数字？
                    List<ForumOffice.ScenicWithPageList> scenicWithPageList = response.body().getData().getScenicWithPageList();
                    forumOfficeAdapter.setDataArray(scenicWithPageList);
                    IntroduceFragment.this.setSuccess();
                    Log.i("TAG", "onResponse: allIntroduceArray: "+response.body().toString());
                }else {
                    setError();
                    LogUtils.w(IntroduceFragment.class,"网络错误！");
                }
            }
            @Override
            public void onFailure(Call<ForumOffice> call, Throwable t) {
                setError();
                LogUtils.w(IntroduceFragment.class,"网络错误！");
            }
        });
    }

    public void loaderMoreData(){
        currentPageSize += 5;
        retrofit.create(Api.class).findAllScenicWithPage(currentPageSize,1,HttpUtil.COOKIE).enqueue(new Callback<ForumOffice>() {
            @Override
            public void onResponse(Call<ForumOffice> call, Response<ForumOffice> response) {
                ForumOffice body = response.body();
                LogUtils.i(IntroduceFragment.class,body.toString());
                int code = response.code();
                if( code == HttpURLConnection.HTTP_OK && body.getData().getScenicWithPageList().size() != 0){
                    List<ForumOffice.ScenicWithPageList> scenicWithPageList = response.body().getData().getScenicWithPageList();
                    forumOfficeAdapter.setLoaderDataArray(scenicWithPageList);
                }else if(body.getData().getScenicWithPageList().size() == 0 ){
                    currentPageSize -= 5;
                    Toast.makeText(IntroduceFragment.this.getContext(),"没有更多数据了！",Toast.LENGTH_SHORT).show();
                }else {
                    currentPageSize -= 5;
                    Toast.makeText(IntroduceFragment.this.getContext(),"服务器异常",Toast.LENGTH_SHORT).show();
                }
                twinklingRefreshLayout.finishLoadmore();
            }

            @Override
            public void onFailure(Call<ForumOffice> call, Throwable t) {
                currentPageSize -= 5;
                Toast.makeText(IntroduceFragment.this.getContext(),"服务器异常",Toast.LENGTH_SHORT).show();
                twinklingRefreshLayout.finishLoadmore();
            }
        });
    }



}
