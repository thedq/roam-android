package com.example.roam.game.dujiangyan.game.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.example.roam.R;
import com.example.roam.game.dujiangyan.game.control.GameControl;
import com.example.roam.game.emeimountain.game.model.TextSpirit;
import com.example.roam.game.tools.CatchBadgeSpirit;
import com.example.roam.game.util.GameConstant;


public class FinSpirit extends Spirit {
    private MaskRect maskRect;
    private OptionAgainSpirit optionAgainSpirit;
    private OptionReturnSpirit optionReturnSpirit,optionReturnSpirit2;
    private Bitmap bitmaps[] = new Bitmap[2];
    private GameControl.FinState currentState = GameControl.FinState.NONE;
    private CatchBadgeSpirit catchBadgeSpirit;
    private TextSpirit winSpirit,winSpirit2,lossSpirit,lossSpirit2;

    public FinSpirit(Context context ) {
        super(context, 530-(int)(450*1.5 / 2),300, 530-(int)(450*1.5 / 2) + (int)(450 * 1.5)+10,300+(int)(338*3) );
        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.drawable.fin_show);
        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.drawable.fin_show_win);
        maskRect = new MaskRect(context);
        optionAgainSpirit = new OptionAgainSpirit(context,530-(int)(450*1.5 / 2) ,300+(int)(338*3)+30,530-(int)(450*1.5 / 2)+10+300,300+(int)(338*3)+30+150);
        optionReturnSpirit = new OptionReturnSpirit(context,530-(int)(450*1.5 / 2) + (int)(450 * 1.5) -300 ,300+(int)(338*3)+30, 530-(int)(450*1.5 / 2) + (int)(450 * 1.5)+10 ,300+(int)(338*3)+30+150 ) ;
        optionReturnSpirit2 = new OptionReturnSpirit(context,530-(int)(450*1.5 / 2) + (int)(450 * 1.5) -300 ,300+(int)(338*3)+30, 530-(int)(450*1.5 / 2) + (int)(450 * 1.5)+10,300+(int)(338*3)+30+150);
        catchBadgeSpirit = new CatchBadgeSpirit(context, GameConstant.DU_BADGE_B);

        winSpirit = new TextSpirit(context,530-(int)(450*1.5 / 2)+this.width/3-40,300+(this.height/3)+30);
        winSpirit2 = new TextSpirit(context,530-(int)(450*1.5 / 2)+this.width/3-40,300+(this.height/3)+30+50 );
        lossSpirit = new TextSpirit(context,530-(int)(450*1.5 / 2)+this.width/3-40,300+(this.height/3)+30);
        lossSpirit2 = new TextSpirit(context,530-(int)(450*1.5 / 2)+this.width/3-40,300+(this.height/3)+30+50);
        winSpirit.setmText("游戏胜利！");
        winSpirit2.setmText("妖怪被你捉住啦！");
        winSpirit.setTextSize(42);
        lossSpirit.setmText("游戏失败");
        lossSpirit2.setmText("你让妖怪逃走了....");
        lossSpirit.setTextSize(42);
        winSpirit.isVisible = true;
        lossSpirit.isVisible = true;
        winSpirit2.isVisible = true;
        lossSpirit2.isVisible = true;
        winSpirit2.setTextSize(42);
        lossSpirit2.setTextSize(42);
    }

    @Override
    public void draw(Canvas canvas) {
        if( isVisible ){
            if( currentState == GameControl.FinState.WIN ){
                maskRect.draw(canvas);
                canvas.drawBitmap(bitmaps[1],null,rect,null);
                optionAgainSpirit.draw(canvas);
                optionReturnSpirit.draw(canvas);
                catchBadgeSpirit.draw(canvas);
                winSpirit.draw(canvas);
                winSpirit2.draw(canvas);
            }else if( currentState == GameControl.FinState.LOSE ){
                maskRect.draw(canvas);
                canvas.drawBitmap(bitmaps[0],null,rect,null);
                optionAgainSpirit.draw(canvas);
                optionReturnSpirit.draw(canvas);
                lossSpirit.draw(canvas);
                lossSpirit2.draw(canvas);
            }
        }
    }

    public GameControl.FinState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(GameControl.FinState currentState) {
        this.currentState = currentState;
    }

    public OptionAgainSpirit getOptionAgainSpirit() {
        return optionAgainSpirit;
    }

    public void setOptionAgainSpirit(OptionAgainSpirit optionAgainSpirit) {
        this.optionAgainSpirit = optionAgainSpirit;
    }

    public OptionReturnSpirit getOptionReturnSpirit() {
        return optionReturnSpirit;
    }

    public void setOptionReturnSpirit(OptionReturnSpirit optionReturnSpirit) {
        this.optionReturnSpirit = optionReturnSpirit;
    }

    public CatchBadgeSpirit getCatchBadgeSpirit() {
        return catchBadgeSpirit;
    }

    public void setCatchBadgeSpirit(CatchBadgeSpirit catchBadgeSpirit) {
        this.catchBadgeSpirit = catchBadgeSpirit;
    }

}
