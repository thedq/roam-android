package com.example.roam.ui.adpter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.roam.R;

import java.util.List;

public class InformationNatureAdapter extends BaseAdapter {
    private Context mContext;
    private List<String> mList;
    private int selectorPosition;

    public InformationNatureAdapter(Context context, List<String> mList) {
        this.mContext = context;
        this.mList = mList;

    }

    @Override
    public int getCount() {
        return mList != null ? mList.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mList != null ? mList.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return mList != null ? position : 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = View.inflate(mContext, R.layout.information_nature_item, null);
        LinearLayout linearLayout = (LinearLayout ) convertView.findViewById(R.id.layout);
        TextView textView = (TextView) convertView.findViewById(R.id.tv_nature);
        textView.setText(mList.get(position));
        //如果当前的position等于传过来点击的position,就去改变他的状态
        if (selectorPosition == position) {
            textView.setBackgroundResource(R.drawable.label_item_btn_selected);
            textView.setTextColor(Color.parseColor("#e5e5e5"));
        } else {
            //其他的恢复原来的状态
            textView.setBackgroundResource(R.drawable.label_item_btn_normal);
            textView.setTextColor(Color.parseColor("#89adaa"));
        }
        return convertView;
    }


    public void changeState(int pos) {
        selectorPosition = pos;
        notifyDataSetChanged();
    }
}
