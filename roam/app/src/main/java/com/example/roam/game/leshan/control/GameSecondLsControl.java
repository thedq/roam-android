package com.example.roam.game.leshan.control;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.util.EventLog;
import android.util.Log;
import android.view.MotionEvent;

import com.example.roam.game.app.leshan.GameFirstLsActivity;
import com.example.roam.game.app.leshan.GameSecondLsActivity;
import com.example.roam.game.app.leshan.GameThirdLsActivity;
import com.example.roam.game.app.question.GameQuestionActivity;
import com.example.roam.game.leshan.model.LeBodySpirit;
import com.example.roam.game.leshan.model.LeGoingSpirit;
import com.example.roam.game.leshan.model.LeHeadSpirit;
import com.example.roam.game.leshan.model.SceneEntrance;
import com.example.roam.game.leshan.model.SceneHead;
import com.example.roam.game.qcmountain.model.BarOptionSpirit;
import com.example.roam.game.qcmountain.model.layer2.TaoistSpirit;
import com.example.roam.game.tools.BarLayer;
import com.example.roam.game.tools.LeftSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.TaoistLayer;

import java.lang.ref.PhantomReference;

public class GameSecondLsControl {
    private Context context;
    private SceneHead sceneHead;
    private LeftSpirit leftSpirit;
    private BarLayer barLayer;
    private BarOptionSpirit barOptionSpirit;
    private LeHeadSpirit leHeadSpirit;
    private LeBodySpirit leBodySpirit;
    private LeGoingSpirit leGoingSpirit;
    private TaoistSpirit taoistSpirit;
    private TaoistLayer taoistLayer;
    public boolean isVisible = true;

    public GameSecondLsControl(Context context){
        this.context = context;
        sceneHead = new SceneHead(context);
        leftSpirit = new LeftSpirit(context);
        leHeadSpirit = new LeHeadSpirit(context);
        leBodySpirit = new LeBodySpirit(context);
        barLayer = new BarLayer(context);
        barOptionSpirit = new BarOptionSpirit(context);
        leGoingSpirit = new LeGoingSpirit(context);
        taoistSpirit = new TaoistSpirit(context);
        taoistSpirit.setTaoistRectLeft(795);
        taoistLayer = new TaoistLayer(context,taoistSpirit,GameConstant.LE_TAOIST_LAYER);
    }

    public void draw(Canvas canvas){
        if(isVisible){
            sceneHead.draw(canvas);
            leftSpirit.draw(canvas);
            leHeadSpirit.draw(canvas);
            leBodySpirit.draw(canvas);
            leGoingSpirit.draw(canvas);
            taoistSpirit.draw(canvas);
            taoistLayer.draw(canvas);
            barLayer.draw(canvas);
            barOptionSpirit.draw(canvas);
        }
    }

    public void onTouch(MotionEvent event){
        switch ( event.getAction() ){
            case MotionEvent.ACTION_DOWN:
                if (!taoistLayer.isClick()) {
                    clickSceneHead(event);
                    clickLeHead(event);
                    clickBody(event);
                    clickGoing(event);
                    clickDelete(event);
                    clickLeftSpirit(event);
                    clickTaoist(event);
                }else {
                    clickTaoistLayer(event);
                    clickOptionDelete(event);
                    clickOptionCertain(event);
                    clickOptionCancel(event);
                }
                break;
        }
    }

    private void clickTaoist(MotionEvent event){
        if(taoistSpirit.isVisible && taoistSpirit.getRect().contains((int) event.getX(),(int) event.getY())){
            taoistLayer.isVisible = true;
            taoistLayer.setClick(true);
            taoistSpirit.isVisible = false;
        }
    }

    private void clickTaoistLayer(MotionEvent event){
        taoistLayer.clickDialogSpirit(event, GameConstant.LE_TAOIST_LAYER);
        taoistLayer.clickOptionQuestion(event,barOptionSpirit);
        taoistLayer.clickOptionCancel(event);
    }

    private void clickLeHead(MotionEvent event) {
        leHeadSpirit.clickSpiritText(event, barLayer,barOptionSpirit);
    }

    private void clickDelete(MotionEvent event){
        barLayer.clickDelete(event);
    }

    private void clickBody(MotionEvent event){
        leBodySpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickGoing(MotionEvent event){
        leGoingSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickOptionDelete(MotionEvent event){
        barOptionSpirit.clickOptionDelete(event);
    }

    private void clickOptionCertain(MotionEvent event){
        if(barOptionSpirit.getOptionYes().getRect().contains((int) event.getX(),(int) event.getY()) && !barOptionSpirit.isClickThrough() ){
            Intent intent = new Intent(context, GameQuestionActivity.class);
            intent.putExtra("gameIndex",3);
            leftSpirit.finsh(context);
            context.startActivity(intent);
            barOptionSpirit.setClickThrough(true);
        }
    }

    private void clickOptionCancel(MotionEvent event){
        barOptionSpirit.clickOptionCancel(event);
    }

    String TAG = "TAG";
    public void clickSceneHead(MotionEvent event){
        if (sceneHead.getRect().contains((int) event.getX(),(int) event.getY())) {
            Log.i(TAG, "clickSceneHead: x:"+event.getX()+" y:"+event.getY());
        }
    }

    public void clickLeftSpirit(MotionEvent event){
        if (leftSpirit.getRect().contains((int) event.getX(),(int) event.getY())) {
            leftSpirit.finsh(context);
        }
    }
}
