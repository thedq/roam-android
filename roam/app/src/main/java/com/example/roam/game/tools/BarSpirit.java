package com.example.roam.game.tools;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.example.roam.R;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.BitmapUtil;
import com.example.roam.game.util.GameConstant;

public class BarSpirit extends Spirit {
    private boolean isVisible = true;
    public BarSpirit(Context context,int left,int top,int right,int bottom) {
        super(context, left, top, right, bottom); // 0 , 1730
        this.bitmap = BitmapFactory.decodeResource(context.getResources(),GameConstant.BAR);
    }

    @Override
    public void draw(Canvas canvas) {
        if (isVisible) {
            canvas.drawBitmap(bitmap, null, rect, null);
        }
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public void setDetail(){
        this.bitmap = BitmapUtil.changeBitmapSize(context, R.drawable.bar_detial,this.width,this.height);
    }
}
