package com.example.roam.api;

import com.example.roam.model.gson.Forum;
import com.example.roam.model.gson.ForumOffice;
import com.example.roam.model.gson.ForumSearch;
import com.example.roam.model.gson.UpdatePassword;
import com.example.roam.model.gson.User;
import com.example.roam.model.gson.Verify;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface Api {
    /**
     * /ScenicRelease/findAllScneicByUid
     * 根据用户id——uid查询景点发布信息
     * 参数：无
     * **/
    /**
     *
     * 1. 换图标
     * 2.
     * */

    @GET("ScenicRelease/findAllScneicByUid")
    public Call<Forum> findAllScneicByUid(@Query("pagesize")int pageSize,@Query("spaceReleaseType")int spaceReleaseType,@Header("Cookie") String Cookie);

    @GET("ScenicRelease/findAllScenicWithPage")  // 获取所有文章
    public Call<ForumOffice> findAllScenicWithPage(@Query("pageSize") int pageSize,@Query("spaceReleaseType")int spaceReleaseType, @Header("Cookie")String cookie );

    @GET("getView/getScenicReleaseView")
    public Call<ResponseBody> getScenicReleaseView(@Query("srid")int srid,@Header("Cookie")String cookie);

    @POST("UserInfo/updateUser") // 修改用户信息，
    @FormUrlEncoded
    Call<ResponseBody> updateUser(@Field("userId")int userId, @Field("userName")String userName, @Field("userPwd")String userPwd, @Field("userChara")String userChara,
                          @Field("userSpirit")String userSpirit, @Field("userHead")String userHead, @Field("characterSelect")int characterSelect,
                          @Field("spiritSelect")int spiritSelect, @Field("headSelect")int headSelect, @Header("Cookie")String cookie);
    /**
     *  用户填加一个景点发布信息
     *  scenicReleaseTitle:信息标题
     *  scenicReleaseText:信息内容
     *  spaceReleaseType:信息类型
     *  scenicId:景点编号
     *  userId:用户id
     *  anoUser:是否匿名 1 不匿名
     *  tagText:tagText集合
     * */
    @POST("ScenicRelease/addOneSRI")
    @FormUrlEncoded
    Call<ResponseBody> postHtml(@Field("scenicReleaseTitle") String scenicReleaseTitle, @Field("scenicReleaseText")String scenicReleaseText,
                                @Field("spaceReleaseType")Integer spaceReleaseType, @Field("scenicId")Integer scenicId,
                                @Field("userId")Integer userId, @Field("anoUser")Integer anoUser,
                                @Field("tagTextList")String[] tagText,@Header("Cookie")String Cookie);

    @GET("ScenicRelease/giveOrDeleGood")
    Call<ResponseBody> giveOrDeleGood(@Query("srid")int srid,@Header("Cookie")String Cookie);

    @GET("ScenicRelease/findAllByTagText")
    Call<ForumSearch> findAllByTagText(@Query("tagText")String tagText,@Query("pageSize")int pageSize, @Header("Cookie")String Cookie);

    @POST("UserInfo/updatePassword")
    Call<UpdatePassword> updatePassword(@Query("password")String password, @Query("userAcc")String userAcc,@Header("Cookie")String Cookie);

    @GET("email/put")
    Call<ResponseBody> tomail(@Query("tomail")String tomail,@Header("Cookie")String Cookie);

    @POST("email/verify") // 验证邮箱
    @FormUrlEncoded
    Call<Verify> toVerify(@Field("inNumber")String inNumber,@Header("Cookie")String Cookie);

}
