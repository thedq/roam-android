package com.example.roam.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.roam.R;
import com.example.roam.ui.adpter.TagEditAdapter;
import com.example.roam.utils.StatusBarUtils;

import java.util.ArrayList;

public class TagChooseActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText edt_tag;
    private TextView txt_add;
    private ArrayList<String> dataArray;
    private GridView grid_choose_tag;
    private TagEditAdapter tagEditAdapter;
    private Button btn_cancel, btn_certain;
    private int tagCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_choose);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        initView();
        initData();
        initEvent();
    }

    public void initView(){
        edt_tag = this.findViewById(R.id.edt_tag);
        txt_add = this.findViewById(R.id.txt_add);
        grid_choose_tag = this.findViewById(R.id.grid_choose_tag);
        tagEditAdapter = new TagEditAdapter(this,0);
        btn_cancel = this.findViewById(R.id.btn_cancel);
        btn_certain = this.findViewById(R.id.btn_certain);
        dataArray = getIntent().getStringArrayListExtra("dataArray");
    }

    public void initData(){
        grid_choose_tag.setAdapter(tagEditAdapter);
        tagEditAdapter.setDataArray(dataArray);
    }

    public void initEvent(){
        txt_add.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        btn_certain.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch ( v.getId() ){
            case R.id.txt_add:
                String strTag = edt_tag.getText().toString();
                if(tagEditAdapter.getDataArray().size() < 3 && !strTag.equals("")) {
                    tagEditAdapter.setData(edt_tag.getText().toString());
                    edt_tag.setText("");
                }else if( strTag.equals("")){
                    Toast.makeText(this,"标签不能为null",Toast.LENGTH_SHORT).show();
                    edt_tag.setText("");
                } else {
                    Toast.makeText(this,"标签不能超过三个！",Toast.LENGTH_SHORT).show();
                    edt_tag.setText("");
                }
                break;
            case R.id.btn_certain:
                Intent intent = new Intent(TagChooseActivity.this,UploadDiaryActivity.class);
                intent.putStringArrayListExtra("dataArray", tagEditAdapter.getDataArray());
                setResult(200,intent);
                TagChooseActivity.this.finish();
                break;
            case R.id.btn_cancel:
                Intent intent1 = new Intent(TagChooseActivity.this,UploadDiaryActivity.class);
                startActivity(intent1);
                TagChooseActivity.this.finish();
                break;
        }
    }


}