package com.example.roam.game.app.jiuzhaigou;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.roam.R;
import com.example.roam.game.jiuzhaigou.control.GameSecondJzgControl;
import com.example.roam.game.jiuzhaigou.view.GameSecondJzgUI;
import com.example.roam.utils.StatusBarUtils;

public class GameSecondJzgActvity extends AppCompatActivity {
    private GameSecondJzgUI gameSecondJzgUI;
    private GameSecondJzgControl gameSecondJzgControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_second_jzg_actvity);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        gameSecondJzgUI = this.findViewById(R.id.scene2);
        gameSecondJzgControl = gameSecondJzgUI.getGameSecondJzgControl();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameSecondJzgControl.isVisible = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameSecondJzgControl.isVisible = true;
    }
}