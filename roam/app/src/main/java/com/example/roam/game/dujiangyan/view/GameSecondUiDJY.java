package com.example.roam.game.dujiangyan.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import com.example.roam.game.dujiangyan.control.GameFirstControlDJY;
import com.example.roam.game.dujiangyan.control.GameSecondControlDJY;

public class GameSecondUiDJY extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder surfaceHolder;
    private GameSecondControlDJY gameSecondControlDJY;
    public GameSecondUiDJY(Context context) {
        super(context);
        initGame();
    }

    public GameSecondUiDJY(Context context, AttributeSet attrs) {
        super(context, attrs);
        initGame();
    }

    public GameSecondUiDJY(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initGame();
    }

    public void initGame(){
        surfaceHolder = this.getHolder();
        surfaceHolder.addCallback(this);
        gameSecondControlDJY = new GameSecondControlDJY(getContext());
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        Runner runner = new Runner();
        runner.start();
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {

    }

    class Runner extends Thread{
        @Override
        public void run() {
            while ( gameSecondControlDJY.isVisible ){
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Canvas canvas = surfaceHolder.lockCanvas();
                redraw(canvas);
                surfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }

    public void redraw(Canvas canvas){
        gameSecondControlDJY.draw(canvas);
    }

    public GameSecondControlDJY getGameSecondControlDJY() {
        return gameSecondControlDJY;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gameSecondControlDJY.onTouch(event);
        return true;
    }
}
