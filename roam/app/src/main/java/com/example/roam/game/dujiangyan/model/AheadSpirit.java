package com.example.roam.game.dujiangyan.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class AheadSpirit extends CSpirit {

    public AheadSpirit(Context context) {
        super(context, 300, 565, 970, 1030);
        this.text = "准备好出发冒险了吗？ 下一站飞沙嘴！";
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if(isVisible){
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
