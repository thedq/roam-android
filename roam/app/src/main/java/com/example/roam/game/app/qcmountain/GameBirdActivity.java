package com.example.roam.game.app.qcmountain;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.roam.R;
import com.example.roam.game.qcmountain.bird.view.GameUI;

public class GameBirdActivity extends AppCompatActivity {

    private GameUI gameUI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_bird);
        gameUI = findViewById(R.id.gameui);
    }


    @Override
    protected void onPause() {
        super.onPause();
        gameUI.getGameControl().setOver(true);
        this.finish();
    }
}