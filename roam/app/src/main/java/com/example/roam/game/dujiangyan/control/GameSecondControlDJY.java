package com.example.roam.game.dujiangyan.control;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import com.example.roam.game.app.dujiangyan.GameFirstDjyActivity;
import com.example.roam.game.app.dujiangyan.GameThirdDjyActivity;
import com.example.roam.game.app.question.GameQuestionActivity;
import com.example.roam.game.dujiangyan.model.BaoPingSpirit;
import com.example.roam.game.dujiangyan.model.BirdMouseSpirit;
import com.example.roam.game.dujiangyan.model.DMingJianSpirit;
import com.example.roam.game.dujiangyan.model.FeiShaYanSpirit;
import com.example.roam.game.dujiangyan.model.SceneBaoPingKou;
import com.example.roam.game.dujiangyan.model.SceneDuJiangYan;
import com.example.roam.game.dujiangyan.model.SceneMingMountain;
import com.example.roam.game.qcmountain.model.BarOptionSpirit;
import com.example.roam.game.qcmountain.model.layer2.TaoistSpirit;
import com.example.roam.game.tools.BarLayer;
import com.example.roam.game.tools.LeftSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.TaoistLayer;

public class GameSecondControlDJY {
    private Context context;
    private SceneDuJiangYan sceneDuJiangYan;
    private LeftSpirit leftSpirit;
    private BirdMouseSpirit birdMouseSpirit;
    private BarLayer barLayer;
    private BarOptionSpirit barOptionSpirit;
    private FeiShaYanSpirit feiShaYanSpirit;
    private DMingJianSpirit dMingJianSpirit;
    private BaoPingSpirit baoPingSpirit;
    private TaoistSpirit taoistSpirit;
    private TaoistLayer taoistLayer;
    public boolean isVisible = true;

    public GameSecondControlDJY(Context context){
        this.context = context;
        sceneDuJiangYan = new SceneDuJiangYan(context);
        leftSpirit = new LeftSpirit(context);
        birdMouseSpirit = new BirdMouseSpirit(context);
        barLayer = new BarLayer(context);
        barOptionSpirit = new BarOptionSpirit(context);
        feiShaYanSpirit = new FeiShaYanSpirit(context);
        dMingJianSpirit = new DMingJianSpirit(context);
        baoPingSpirit = new BaoPingSpirit(context);
        taoistSpirit = new TaoistSpirit(context);
        taoistSpirit.setTaoistRectLeft(795);
        taoistLayer = new TaoistLayer(context,taoistSpirit,GameConstant.DU_TAOIST_LAYER);
    }

    public void draw(Canvas canvas) {
        if (isVisible) {
            sceneDuJiangYan.draw(canvas);
            leftSpirit.draw(canvas);
            birdMouseSpirit.draw(canvas);
            dMingJianSpirit.draw(canvas);
            feiShaYanSpirit.draw(canvas);
            baoPingSpirit.draw(canvas);
            barLayer.draw(canvas);
            taoistSpirit.draw(canvas);
            taoistLayer.draw(canvas);
            barOptionSpirit.draw(canvas);
        }
    }

    public void onTouch(MotionEvent event) {
        switch ( event.getAction() ){
            case MotionEvent.ACTION_DOWN:
                if(!taoistLayer.isClick()) {
                    clickTaoist(event);
                    clickSceneDuJiangYan(event);
                    clickLeft(event);
                    clickDelete(event);
                    clickBirdMouse(event);
                    clickFeiShaYan(event);
                    clickDMingJian(event);
                    clickBaoPing(event);
                }else {
                    clickOptionCertain(event);
                    clickOptionDelete(event);
                    clickOptionCancel(event);
                    clickTaoistLayer(event);
                }
                break;
        }
    }

    private void clickBaoPing(MotionEvent event){
//        if (baoPingSpirit.getRect().contains((int) event.getX(),(int) event.getY())) {
//            barLayer.isVisible = true;
//            barLayer.setText(GameConstant.STOP);
//        }
        baoPingSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickTaoist(MotionEvent event){
        if(taoistSpirit.isVisible && taoistSpirit.getRect().contains((int) event.getX(),(int) event.getY()) ){
            taoistLayer.isVisible = true;
            taoistLayer.setClick(true);
            taoistSpirit.isVisible = false;
        }
    }

    private void clickTaoistLayer(MotionEvent event){
        taoistLayer.clickDialogSpirit(event,GameConstant.DU_TAOIST_LAYER);
        taoistLayer.clickOptionQuestion(event,barOptionSpirit);
        taoistLayer.clickOptionCancel(event);
    }

    private void clickOptionCertain(MotionEvent event){
        if (barOptionSpirit.getOptionYes().getRect().contains((int) event.getX(),(int) event.getY()) && !barOptionSpirit.isClickThrough() ) {
            Intent intent = new Intent(context, GameQuestionActivity.class);
            intent.putExtra("gameIndex",1);
            leftSpirit.finsh(context);
            context.startActivity(intent);
            barOptionSpirit.setClickThrough(true);
            ((Activity)context).finish();
        }
    }

    private void clickOptionCancel(MotionEvent event){
        if (barOptionSpirit.getOptionNo().getRect().contains((int) event.getX(),(int) event.getY())) {
            barOptionSpirit.isVisible = false;
        }
    }

    private void clickOptionDelete(MotionEvent event){
        if( barOptionSpirit.getBarDeleteSpirit().getRect().contains((int) event.getX(),(int) event.getY())){
            barOptionSpirit.isVisible = false;
        }
    }

    private void clickDMingJian(MotionEvent event){
        dMingJianSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickFeiShaYan(MotionEvent event){
//        if (feiShaYanSpirit.getRect().contains((int) event.getX(),(int) event.getY())) {
//            barLayer.isVisible = true;
//            barLayer.text = feiShaYanSpirit.getText();
//        }
        feiShaYanSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickBirdMouse(MotionEvent event){
//        if (birdMouseSpirit.getRect().contains((int) event.getX(),(int) event.getY())) {
//            barLayer.isVisible = true;
//            barLayer.text = birdMouseSpirit.text;
//        }
        birdMouseSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickDelete(MotionEvent event){
        if (barLayer.getBarDeleteSpirit().getRect().contains((int) event.getX(),(int) event.getY())) {
            barLayer.isVisible = false;
        }
    }

    private void clickSceneDuJiangYan(MotionEvent event){
        if (sceneDuJiangYan.getRect().contains((int)event.getX(),(int)event.getY())) {
            Log.i("TAG", "clickSceneDuJiangYan: x:"+event.getX()+" y:"+event.getY());
        }
    }

    public void clickLeft(MotionEvent event){
        if( leftSpirit.getRect().contains((int) event.getX(),(int) event.getY())){
            leftSpirit.finsh(context);
        }
    }
}
