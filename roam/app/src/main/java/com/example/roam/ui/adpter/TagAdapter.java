package com.example.roam.ui.adpter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class TagAdapter extends BaseAdapter {

    private ArrayList<HashMap<String,String>> dataArray;
    private Context context;

    public TagAdapter(Context context , ArrayList<HashMap<String,String>> dataArray){
        this.context = context;
        this.dataArray = dataArray;
    }

    @Override
    public int getCount() {
        return dataArray.size();
    }

    @Override
    public Object getItem(int position) {
        return dataArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;

        return null;
    }

    class ViewHolder{
        ImageView imageView;
        TextView txt_tag;
        CheckBox check_tag;
    }
}
