package com.example.roam.game.dujiangyan.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class BirdMouseSpirit extends CSpirit {

    public BirdMouseSpirit(Context context) {
        super(context, 550, 1000, 628, 1243);
        this.text = GameConstant.YU_ZHUI_TEXT;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if( isVisible ){
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
