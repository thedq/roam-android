package com.example.roam.game.emeimountain.game.model.fin;

import android.content.Context;
import android.graphics.Canvas;
import android.util.Log;

import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.utils.Constans;

public class NumLayer extends Spirit {
    private NumSpirit[] numSpirits;
    private String timeStr;

    public NumLayer(Context context, int left, int top, int right, int bottom) {
        super(context, left, top, right, bottom);
        numSpirits = new NumSpirit[3];
        for (int i = 0; i < numSpirits.length; i++) {
            numSpirits[i] = new NumSpirit(context,(int)(rect.left/ Constans.SCALE_WIDTH)+i*55,(int)(rect.top/Constans.SCALE_HEIGHT),(int)(rect.left/Constans.SCALE_WIDTH)+i*55+50,(int)(rect.bottom/Constans.SCALE_HEIGHT),0);
        }
    }

    @Override
    public void draw(Canvas canvas) {
        if (isVisible) {
            drawNumLogic(canvas);
        }
    }

    private void drawNumLogic(Canvas canvas){
        if( timeStr != null && !timeStr.equals("") ){
            if( timeStr.length() > 0 ){
                numSpirits[2].setType(Integer.parseInt(String.valueOf(timeStr.charAt(timeStr.length()-1)) ));
            }

            if( timeStr.length() > 1){
                numSpirits[1].setType(Integer.parseInt(String.valueOf(timeStr.charAt(timeStr.length()-2)) ));
            }

            if( timeStr.length() > 2){
                numSpirits[0].setType(Integer.parseInt(String.valueOf(timeStr.charAt(0)) ));
            }
        }
        for (NumSpirit numSpirit : numSpirits) {
            numSpirit.draw(canvas);
        }
    }
    String TAG= "TAG";

    public String getTimeStr() {
        return timeStr;
    }

    public void setTimeStr(String timeStr) {
        this.timeStr = timeStr;
    }
}
