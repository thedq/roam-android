package com.example.roam.game.dujiangyan.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class BaoPingSpirit extends CSpirit {

    public BaoPingSpirit(Context context) {
        super(context, 0, 163, 1070, 348);
        this.text = GameConstant.BAO_PING_TEXT;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if( isVisible ){
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
