package com.example.roam.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.roam.R;
import com.example.roam.api.Api;
import com.example.roam.game.app.dujiangyan.GameFirstDjyActivity;
import com.example.roam.game.app.emeimountain.GameFirstEmsActivity;
import com.example.roam.game.app.jiuzhaigou.GameFirstJzgActvity;
import com.example.roam.game.app.leshan.GameFirstLsActivity;
import com.example.roam.game.app.qcmountain.GameFirstActivity;
import com.example.roam.game.util.GameConstant;
import com.example.roam.model.gson.Badges;
import com.example.roam.utils.BadgeUtils;
import com.example.roam.utils.Constans;
import com.example.roam.utils.GuideUtil;
import com.example.roam.utils.HttpUtil;
import com.example.roam.utils.LogUtils;
import com.example.roam.utils.SizeUtils;
import com.example.roam.utils.StatusBarUtils;
import com.example.roam.view.MapContainer;
import com.example.roam.view.Marker;
import com.google.android.material.imageview.ShapeableImageView;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
/**
 * Illustration
 *
 * @author dengqing
 * @time 2021/10/12
 * @function 主页面，大地图，青城山图标，我的中心，人物形象，景点精灵
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    //图片： 用户中心 青城山景点 虚拟人物形象 景点精灵
    private ImageView  imgCharacter, imgSpirit;
    private static final String TAG = "MainActivity";
    private PopupWindow mPopWindow;//人物气泡框
    //人物气泡框按钮： APP介绍 景点推荐 民俗普及
    private Button btnApp, btnSpot, btnFolk,btn_forum;
    private MapContainer mapContainer;
    private ArrayList<Marker> markArray;
    private GuideUtil guideUtil;
    private TextView txt_locationName ,choose_bag_certain , txt_discoverNum;
    private LinearLayout chooseBar;
    private Spinner spinnerCity , spinnerScenic;
    private ProgressBar scenicProgress;
    // 4个字符串数组 成都，乐山，广元，阿贝洲
    private String[] cdStrs,lsStrs,gyStrs,abzStrs;
    private ArrayList<String> cd_array,ls_array,gy_array,abz_array;
    private ArrayAdapter<String> cityAdapter, scenicAdapter;
    private String chooseCity="成都",chooseScenic="青城山";
    private String[][] scenicStrs;
    private int position ;
    private int progress;

    /**
     * 论坛部分：点赞，api数据重复，数据
     * 个人信息部分：修改密码（表单）
     * 游戏部分：
     * 流程
     * 文本
     * 素材
     * bug
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色
        //获取SpiritActivity的传递过来的数据信息
        initView();  //初始化组件
        initData();
        initEvent();
        markArray = new ArrayList<>();
        Marker marker1 = new Marker(0.545f,0.435f,R.drawable.m,"青城山"); //青城山 0.548 0.431
        Marker marker2 = new Marker(0.553f,0.415f,R.drawable.z,"都江堰"); // 都江堰 0.55 0.42
        Marker marker3 = new Marker(0.572f,0.574f,R.drawable.ls,"乐山大佛"); // 乐山大佛 0.572 0.574
        Marker marker4 = new Marker(0.514f,0.587f,R.drawable.ems,"峨眉山"); // 峨眉山  0.514 0.587
        Marker marker5 = new Marker(0.553f,0.184f,R.drawable.z,"九寨沟"); // 九寨沟 0.553  0.184

        markArray.add(marker2);
        markArray.add(marker1);
        markArray.add(marker3);
        markArray.add(marker4);
        markArray.add(marker5);
        mapContainer = this.findViewById(R.id.mapContainer);
        mapContainer.getMapView().setImageResource(R.drawable.mpa_all);
        mapContainer.setMarkers(markArray);
        locationClick();
        Log.i(TAG, "onCreate: mima:"+Constans.u_password);
    }

    private void initView() {
        chooseBar = findViewById(R.id.choose_bag);
        imgSpirit = findViewById(R.id.img_spirit);
        imgCharacter = findViewById(R.id.img_character);
        guideUtil = GuideUtil.getInstance();
        guideUtil.initGuide(this,0);
        txt_locationName = this.findViewById(R.id.txt_locationName);
        spinnerCity = this.findViewById(R.id.m_spinner);
        spinnerScenic = this.findViewById(R.id.m_spinner2);
        choose_bag_certain = this.findViewById(R.id.choose_bag_certain);
        scenicProgress = this.findViewById(R.id.scenic_progress);
        txt_discoverNum = this.findViewById(R.id.txt_discoverNum);
    }

    public void initData(){
        initArray();
        Glide.with(this).load(Constans.u_character).into(imgCharacter);
        Glide.with(this).load(Constans.u_spirit).into(imgSpirit);
        txt_locationName.setOnClickListener(this);
        imgCharacter.setOnClickListener(this);
        imgSpirit.setOnClickListener(this);
        choose_bag_certain.setOnClickListener(this);
        cityAdapter = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,getResources().getStringArray(R.array.city));
        scenicAdapter = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,cdStrs);
        spinnerCity.setAdapter(cityAdapter);
        spinnerScenic.setAdapter(scenicAdapter);
        setProgress();
    }

    private void scaleResume(){
        Constans.DISPLAY_WIDTH_PX = SizeUtils.scalePx(this).widthPixels;
        Constans.DISPLAY_HEIGHT_PX = SizeUtils.scalePx(this).heightPixels;
        Constans.SCALE_WIDTH = Constans.DISPLAY_WIDTH_PX / 1080.0;
        Constans.SCALE_HEIGHT = Constans.DISPLAY_HEIGHT_PX / 1794.0;
    }

    public void initArray(){
        cdStrs = getResources().getStringArray(R.array.scenic_cd);
        lsStrs = getResources().getStringArray(R.array.scenic_ls);
        gyStrs = getResources().getStringArray(R.array.scenic_gy);
        abzStrs = getResources().getStringArray(R.array.scenic_abz);
        cd_array = new ArrayList<>();
        ls_array = new ArrayList<>();
        gy_array = new ArrayList<>();
        abz_array = new ArrayList<>();
        ArrayList<String> [] arrays = new ArrayList[]{cd_array,ls_array,gy_array,abz_array};
        scenicStrs = new String[][]{cdStrs,lsStrs,gyStrs,abzStrs};
        for (int i = 0; i < scenicStrs.length; i++) {
            for (int j = 0; j < scenicStrs[i].length; j++) {
                arrays[i].add(scenicStrs[i][j]);
            }
        }
    }

    public void initEvent(){
        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view;
                chooseCity = textView.getText().toString();
                Log.i(TAG, "onItemClick: chooseCity:"+chooseCity);
                scenicAdapter = new ArrayAdapter<String>(MainActivity.this,R.layout.support_simple_spinner_dropdown_item,scenicStrs[position]);
                spinnerScenic.setAdapter(scenicAdapter);
                scenicAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerScenic.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.i(TAG, "onItemSelected: position scenic:"+position);
                MainActivity.this.position = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setProgress() {
        Badges badges = BadgeUtils.getBadges();
        if (badges != null) {
            progress = badges.getBadgeArray().size() * 100 / Constans.SCENIC_COUNT;
            Log.i(TAG, "setProgress: progress:"+progress);
        }
        scenicProgress.setProgress(progress);
        txt_discoverNum.setText("探索度："+progress+"%");
    }

    //图片点击事件
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.choose_bag_certain:
                chooseBar.setVisibility(View.GONE);
                if( chooseCity.equals("成都市") ){
                    switch (position){
                        case 0:
                            mapContainer.bigLoc(1);
                            chooseScenic = "青城山";
                            break;
                        case 1:
                            mapContainer.bigLoc(0);
                            chooseScenic = "都江堰";
                            break;
                    }
                }

                if( chooseCity.equals("乐山市") ){
                    switch (position){
                        case 0:
                            mapContainer.bigLoc(2);
                            chooseScenic = "乐山大佛";
                            break;
                        case 1:
                            mapContainer.bigLoc(3);
                            chooseScenic = "峨眉山";
                            break;
                    }
                }

                if( chooseCity.equals("阿坝州")){
                    mapContainer.bigLoc(4);
                    chooseScenic = "九寨沟";
                }

                txt_locationName.setText(chooseScenic);
                Log.i(TAG, "onClick: position:"+position);
                mapContainer.getmMapView().onScaleToLocation();

                break;
            case R.id.txt_locationName:
                chooseBar.setVisibility(View.VISIBLE);
                break;
            //用户中心
            case R.id.img_character:
            //跳转用户中心， CharacterActivity.class--》修改
                Intent intent = new Intent(MainActivity.this, PersonZoneActivity.class);
                startActivity(intent);
                break;
            //人物形象
            case R.id.img_spirit:
                PopWindow();//显示气泡框
                break;
            //APP介绍
            //文字使用数据库
            case R.id.btn_app:
                //弹出对话框，文字
                guideUtil.setFirst(false);
                guideUtil.initGuide(this,0);
                mPopWindow.dismiss();
                break;
//            //景点推荐
//            case R.id.btn_spot:
//                String spot = "青城山\n全球道教全真道圣地\n成都十景之一\n位于四川省成都市都江堰市西南\n";
//                showCharacterDialog("景点推荐", spot);
//                break;
            //民俗普及
            case R.id.btn_folk:
                Intent intent1 = new Intent(MainActivity.this,SceneActivity.class);
                MainActivity.this.startActivity(intent1);
                break;
            //跳转旅蜀道论坛
            case R.id.btn_forum:
                Intent intent2 = new Intent(MainActivity.this, ForumActivity.class);
                startActivity(intent2);
                break;
        }
    }

    public void locationClick(){
        for (int i = 0; i < markArray.size(); i++) {
            Marker marker = markArray.get(i);
            ImageView markerView = marker.getMarkerView();
            if( marker.getMarkerName().equals("青城山") ){
                markerView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, GameFirstActivity.class);
                        startActivity(intent);
                    }
                });
            } else if( marker.getMarkerName().equals("都江堰")){
                markerView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent1 = new Intent(MainActivity.this, GameFirstDjyActivity.class);
                        startActivity(intent1);
                    }
                });
            }else if( marker.getMarkerName().equals("峨眉山")){
                markerView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, GameFirstEmsActivity.class);
                        startActivity(intent);
                    }
                });
            }else if( marker.getMarkerName().equals("乐山大佛")){
                markerView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, GameFirstLsActivity.class);
                        startActivity(intent);
                    }
                });
            }else if( marker.getMarkerName().equals("九寨沟")){
                markerView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, GameFirstJzgActvity.class);
                        startActivity(intent);
                    }
                });
            }
        }
    }

    //显示人物气泡框
    private void PopWindow() {
        //利用 LayoutInflater获取 R.layout.activity_popwindow 对应的 View
        View contentView = LayoutInflater.from(MainActivity.this).inflate(R.layout.main_popwindow, null,false);
        mPopWindow = new PopupWindow(contentView);
        /*
         * 设置窗体的长和宽
         * 代码强制设置 PopupWindow的 Height、Width(否则无法显示出来)
         * popupWindow的宽和高, 以这里代码设置的为准
         * */
        mPopWindow.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        mPopWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        //充满
//        mPopWindow.setWidth(ViewGroup.LayoutParams.FILL_PARENT);
//        mPopWindow.setHeight(ViewGroup.LayoutParams.FILL_PARENT);

        //设置动画所对应的style
//        mPopWindow.setAnimationStyle(R.style.contextMenuAnim);

        /*
         * 外部点击, 隐藏窗体的功能(三种方法)
         * */
        //1. 设置再次点击控件的时候，关闭窗口，不重新打开
        mPopWindow.setFocusable(true);
        //2.
        mPopWindow.setOutsideTouchable(true);
        //3.
        ColorDrawable dw = new ColorDrawable(0x00000000);
        mPopWindow.setBackgroundDrawable(dw);

        /*
         * 设置窗体在控件的相对位置
         * */
        // 显示在某个控件的正左下方，无偏移
//        mPopWindow.showAsDropDown(img_character);

        // 相对某个控件的位置，有偏移，xoff为 X轴的偏移量，yoff为 Y轴的偏移量
        mPopWindow.showAsDropDown(imgSpirit, 280, -350);
        //相对于父控件的位置（例如正中央Gravity.CENTER，下方Gravity.BOTTOM等），可以设置偏移或无偏移
//        mPopWindow.showAtLocation(tv, Gravity.BOTTOM, 10, 30);
        //初始化气泡框按钮并设置监听
        //APP介绍
        btnApp = contentView.findViewById(R.id.btn_app);
        btnApp.setOnClickListener(this);
//        //景点推荐
//        btnSpot = contentView.findViewById(R.id.btn_spot);
//        btnSpot.setOnClickListener(this);
        //民俗普及
        btnFolk = contentView.findViewById(R.id.btn_folk);
        btnFolk.setOnClickListener(this);
        //景点论坛
        btn_forum=contentView.findViewById(R.id.btn_forum);
        btn_forum.setOnClickListener(this);
    }

    //显示人物气泡框的具体内容
    private void showCharacterDialog(String title, String message) {
        /* @setIcon 设置对话框图标
         * @setTitle 设置对话框标题
         * @setMessage 设置对话框消息提示
         * setXXX方法返回Dialog对象，因此可以链式设置属性
         */
        //显示普通对话框
        final AlertDialog.Builder characterDialog =
                new AlertDialog.Builder(MainActivity.this);
        characterDialog.setTitle(title);
        characterDialog.setMessage(message);
        characterDialog.setPositiveButton("确定",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //...To-do
                    }
                });
        // 显示
        characterDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        scaleResume();
        setProgress();
        initData();
    }
}

