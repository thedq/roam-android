package com.example.roam.game.dujiangyan.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class MingJianSpirit extends CSpirit {

    public MingJianSpirit(Context context) {
        super(context, 0, 1028, 1068, 1562);
        this.text = GameConstant.MING_JIAN_TEXT;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if(isVisible){
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
