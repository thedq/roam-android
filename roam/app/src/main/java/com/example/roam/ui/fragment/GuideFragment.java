package com.example.roam.ui.fragment;

import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.example.roam.R;
import com.example.roam.ui.activity.BaseFragment;
import com.example.roam.ui.adpter.ForumOfficeAdapter;
import com.example.roam.api.Api;
import com.example.roam.model.gson.ForumOffice;
import com.example.roam.utils.HttpUtil;
import com.example.roam.utils.LogUtils;
import com.lcodecore.tkrefreshlayout.RefreshListenerAdapter;
import com.lcodecore.tkrefreshlayout.TwinklingRefreshLayout;

import java.net.HttpURLConnection;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Create By Visen
 * Date: 2021/10/14
 */

// 这里要论证，有没有必要这么写5个fragment
public class GuideFragment extends BaseFragment {  // 旅游 2
    private ListView lv_guide;
    private ForumOfficeAdapter forumOfficeAdapter;
    private Retrofit retrofit;
    private TwinklingRefreshLayout twinklingRefreshLayout;
    private int currentPageSize = 0;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_tour;
    }

    @Override
    public void initView(View successView) {
        lv_guide = successView.findViewById(R.id.lv_guide);
        twinklingRefreshLayout = successView.findViewById(R.id.refreshLayout);
        forumOfficeAdapter = new ForumOfficeAdapter(getContext());
    }

    @Override
    public void initData() {
        twinklingRefreshLayout.setEnableRefresh(false);
        twinklingRefreshLayout.setEnableLoadmore(true);
        lv_guide.setAdapter(forumOfficeAdapter);
        forumOfficeAdapter.setType(3);
    }

    @Override
    public void initEvent() {
        twinklingRefreshLayout.setOnRefreshListener(new RefreshListenerAdapter() {
            @Override
            public void onLoadMore(TwinklingRefreshLayout refreshLayout) {
            loaderMoreData();
            }
        });

    }

    @Override
    public void loadData() {
        setLoading();
        retrofit = new Retrofit.Builder().baseUrl(HttpUtil.ADDRESS).addConverterFactory(GsonConverterFactory.create()).build();
        retrofit.create(Api.class).findAllScenicWithPage(currentPageSize,2,HttpUtil.COOKIE).enqueue(new Callback<ForumOffice>() {
            @Override
            public void onResponse(Call<ForumOffice> call, Response<ForumOffice> response) {
                Log.i("TAG", "onResponse: responseBody: "+response.body().toString());
                int code = response.code();
                if( code == 200 ){
                    List<ForumOffice.ScenicWithPageList> scenicWithPageList = response.body().getData().getScenicWithPageList();
                    forumOfficeAdapter.setDataArray(scenicWithPageList);
                    GuideFragment.this.setSuccess();
                }else {
                    setError();
                    LogUtils.w(GuideFragment.class,"网络错误！");
                }
            }
            @Override
            public void onFailure(Call<ForumOffice> call, Throwable t) {
                setError();
                LogUtils.w(GuideFragment.class,"网络错误！");
            }
        });
    }

    public void loaderMoreData(){
        currentPageSize += 5;
        retrofit.create(Api.class).findAllScenicWithPage(currentPageSize,2,HttpUtil.COOKIE).enqueue(new Callback<ForumOffice>() {
            @Override
            public void onResponse(Call<ForumOffice> call, Response<ForumOffice> response) {
                ForumOffice body = response.body();
                LogUtils.i(GuideFragment.class,body.toString());
                int code = response.code();
                if( code == HttpURLConnection.HTTP_OK && body.getData().getScenicWithPageList().size() != 0){
                    List<ForumOffice.ScenicWithPageList> scenicWithPageList = response.body().getData().getScenicWithPageList();
                    forumOfficeAdapter.setLoaderDataArray(scenicWithPageList);
                }else if(body.getData().getScenicWithPageList().size() == 0 ){
                    currentPageSize -= 5;
                    Toast.makeText(GuideFragment.this.getContext(),"没有更多数据了！",Toast.LENGTH_SHORT).show();
                }else {
                    currentPageSize -= 5;
                    Toast.makeText(GuideFragment.this.getContext(),"服务器异常",Toast.LENGTH_SHORT).show();
                }
                twinklingRefreshLayout.finishLoadmore();
            }

            @Override
            public void onFailure(Call<ForumOffice> call, Throwable t) {
                currentPageSize -= 5;
                Toast.makeText(GuideFragment.this.getContext(),"服务器异常",Toast.LENGTH_SHORT).show();
                twinklingRefreshLayout.finishLoadmore();
            }
        });
    }
}
