package com.example.roam.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.roam.R;
import com.example.roam.utils.Constans;
import com.example.roam.utils.StatusBarUtils;
import com.google.android.material.imageview.ShapeableImageView;


public class PersonZoneActivity extends AppCompatActivity {
    private String name;//用户昵称
    private ImageView left ,img_character;
    private ShapeableImageView img_head;
    private TextView txt_uname , txt_information , txt_zone , txt_badge , txt_update;
    private int head_select;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_zone);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        name = Constans.u_username;
        head_select=Constans.head_select;
        iniView();
        iniData();
        iniEvent();
    }

    public void iniView() {
        left = this.findViewById(R.id.img_left);
        img_head = this.findViewById(R.id.img_head);
        txt_uname = this.findViewById(R.id.txt_uname);
        txt_badge = this.findViewById(R.id.txt_badge);
        txt_zone = this.findViewById(R.id.txt_zone);
        txt_information = this.findViewById(R.id.txt_information);
        img_character = this.findViewById(R.id.img_character);
    }

    public void iniData() {
        Glide.with(this).load(Constans.u_head).into(img_head);
        Glide.with(this).load(Constans.u_character).into(img_character);
        txt_uname.setText(Constans.u_username);
    }

    public void iniEvent() {
        MyListener myListener = new MyListener();
        txt_information.setOnClickListener(myListener);
        txt_zone.setOnClickListener(myListener);
        txt_badge.setOnClickListener(myListener);
        left.setOnClickListener(myListener);
    }

    class MyListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.img_back:
                    // 回退事件
                    PersonZoneActivity.this.finish();
                    break;

                case R.id.txt_information:
                    Intent intent = new Intent(PersonZoneActivity.this,PersonalInformationActivity.class);
                    PersonZoneActivity.this.transferData(intent);
                    startActivity(intent);
                    break;

                case R.id.txt_zone:
                    Intent intent1 = new Intent(PersonZoneActivity.this,UploadZoneActivity.class);
                    startActivity(intent1);
                    break;

                case R.id.txt_badge:
                    Intent intent2 = new Intent(PersonZoneActivity.this, PersonalBadgeActivity.class);
                    startActivity(intent2);
                    break;

                case R.id.img_left:
                    PersonZoneActivity.this.finish();
                    break;
            }
        }
    }

    // 携带数据
    public void transferData(Intent intent){
        Bundle bundle = new Bundle();
        bundle.putString("name", name);
        intent.putExtras(bundle);
    }

    @Override
    protected void onResume() {
        super.onResume();
        iniData();
    }

    private static final String TAG = "PersonZoneActivity";
}