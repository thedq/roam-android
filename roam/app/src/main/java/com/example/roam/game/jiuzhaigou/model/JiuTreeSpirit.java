package com.example.roam.game.jiuzhaigou.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.PaintUtil;

public class JiuTreeSpirit extends CSpirit {
    public JiuTreeSpirit(Context context) {
        super(context,0 ,485 ,485 ,1342 );
        this.text = "探索九寨沟的深处，哪里有更多的秘密等待开发....";
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
