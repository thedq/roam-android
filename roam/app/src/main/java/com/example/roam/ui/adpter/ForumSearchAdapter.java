package com.example.roam.ui.adpter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.roam.R;
import com.example.roam.api.Api;
import com.example.roam.model.gson.Forum;
import com.example.roam.model.gson.ForumSearch;
import com.example.roam.ui.activity.WebActivity;
import com.example.roam.utils.HttpUtil;
import com.google.android.material.imageview.ShapeableImageView;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ForumSearchAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<ForumSearch.ScenicWithPageList> dataArray;
    private Drawable agree,agreeFill;
    public ForumSearchAdapter(Context context){
        this.context = context;
        dataArray = new ArrayList<>();
        agreeFill = context.getResources().getDrawable(R.drawable.agree_fill);
        agreeFill.setBounds(0, 0, agreeFill.getMinimumWidth(), agreeFill.getMinimumHeight());
        agree = context.getResources().getDrawable(R.drawable.agree);
        agree.setBounds(0, 0, agree.getMinimumWidth(), agree.getMinimumHeight());
    }

    @Override
    public int getCount() {
        return dataArray.size();
    }

    @Override
    public Object getItem(int position) {
        return dataArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setData(List<ForumSearch.ScenicWithPageList> dataArray){
        this.dataArray.clear();
        this.dataArray.addAll(dataArray);
        notifyDataSetChanged();
    }

    public void setMoreData(List<ForumSearch.ScenicWithPageList> dataArray){
        this.dataArray.addAll(dataArray);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ForumSearch.ScenicWithPageList data = dataArray.get(position);
        ViewHolder viewHolder = new ViewHolder();
        if( convertView == null ) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_fragment_forum_normal, parent, false);
            viewHolder.imgHead = convertView.findViewById(R.id.img_head);
            viewHolder.img_c1 = convertView.findViewById(R.id.img_c1);
            viewHolder.img_c2 = convertView.findViewById(R.id.img_c2);
            viewHolder.img_c3 = convertView.findViewById(R.id.img_c3);
            viewHolder.rb_agree = convertView.findViewById(R.id.rb_agree);
            viewHolder.img_comment = convertView.findViewById(R.id.img_comment);
            viewHolder.txtUserName = convertView.findViewById(R.id.txt_user_name);
            viewHolder.txtTitle = convertView.findViewById(R.id.txt_title);
            viewHolder.txt_t1 = convertView.findViewById(R.id.txt_t1);
            viewHolder.txt_t2 = convertView.findViewById(R.id.txt_t2);
            viewHolder.txt_t3 = convertView.findViewById(R.id.txt_t3);
            viewHolder.txt_commentNum = convertView.findViewById(R.id.txt_commentNum);
            viewHolder.img_c1.setVisibility(View.GONE);
            viewHolder.img_c2.setVisibility(View.GONE);
            viewHolder.img_c3.setVisibility(View.GONE);

            viewHolder.txt_t1.setVisibility(View.GONE);
            viewHolder.txt_t2.setVisibility(View.GONE);
            viewHolder.txt_t3.setVisibility(View.GONE);

            Glide.with(context).load(data.getHeadUrl()).into(viewHolder.imgHead);
            imgInto(viewHolder, data);
            tagInto(viewHolder, data);
            viewHolder.txtUserName.setText(data.getUserName());
            viewHolder.txtTitle.setText(data.getScenicReleaseInfo().getScenicReleaseTitle());
            viewHolder.rb_agree.setText(data.getScenicReleaseInfo().getScenicReleaseGood()+"");
            if( data.isGood() ){
                viewHolder.rb_agree.setCompoundDrawables(agreeFill, null, null, null);
            }else {
                viewHolder.rb_agree.setCompoundDrawables(agree, null, null, null);
            }
            viewHolder.txt_commentNum.setText(data.getCommentCount() + "");
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
            viewHolder.img_c1.setVisibility(View.GONE);
            viewHolder.img_c2.setVisibility(View.GONE);
            viewHolder.img_c3.setVisibility(View.GONE);

            viewHolder.txt_t1.setVisibility(View.GONE);
            viewHolder.txt_t2.setVisibility(View.GONE);
            viewHolder.txt_t3.setVisibility(View.GONE);

            Glide.with(context).load(data.getHeadUrl()).into(viewHolder.imgHead);
            imgInto(viewHolder, data);
            tagInto(viewHolder, data);
            viewHolder.txtUserName.setText(data.getUserName());
            viewHolder.txtTitle.setText(data.getScenicReleaseInfo().getScenicReleaseTitle());
            viewHolder.rb_agree.setText(data.getScenicReleaseInfo().getScenicReleaseGood()+"");
            if( data.isGood() ){
                viewHolder.rb_agree.setCompoundDrawables(agreeFill, null, null, null);
            }else {
                viewHolder.rb_agree.setCompoundDrawables(agree, null, null, null);
            }
            viewHolder.txt_commentNum.setText(data.getCommentCount() + "");
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initForumHtmlNet(data.getScenicReleaseInfo().getScenicReleaseId());
            }
        });

        ViewHolder finalViewHolder = viewHolder;
        viewHolder.rb_agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Retrofit retrofit = new Retrofit.Builder().baseUrl(HttpUtil.ADDRESS).build();
                retrofit.create(Api.class).giveOrDeleGood(data.getScenicReleaseInfo().getScenicReleaseId(),HttpUtil.COOKIE).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.code() == HttpURLConnection.HTTP_OK){
                            data.setGood(!data.isGood());
                            // 按照data.isGood相反设置
                            if (data.isGood()) {
                                finalViewHolder.rb_agree.setCompoundDrawables(agreeFill,null,null,null);
                                finalViewHolder.rb_agree.setText(Integer.parseInt(finalViewHolder.rb_agree.getText().toString()) +1 +"" );
                                Toast.makeText(context,"点赞成功",Toast.LENGTH_SHORT).show();
                            }else {
                                finalViewHolder.rb_agree.setCompoundDrawables(agree,null,null,null);
                                finalViewHolder.rb_agree.setText(Integer.parseInt(finalViewHolder.rb_agree.getText().toString()) -1 +"" );
                                Toast.makeText(context,"取消点赞",Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(context,"点赞失败",Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(context,"点赞失败",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


        return convertView;
    }

    class ViewHolder {
        ImageView img_good , img_comment ;
        ShapeableImageView img_c1,img_c2,img_c3;
        TextView txtUserName , txtTitle , txt_t1 , txt_t2 ,txt_t3 ,txt_goodNum ,txt_commentNum ;
        ShapeableImageView imgHead;
        RadioButton rb_agree;
    }

    private void imgInto(ViewHolder viewHolder, ForumSearch.ScenicWithPageList data){
        if (data.getPictures().size() > 0) {
            viewHolder.img_c1.setVisibility(View.VISIBLE);
            Glide.with(context).load(data.getPictures().get(0)).into(viewHolder.img_c1);
        }
        if( data.getPictures().size() > 1 ){
            viewHolder.img_c2.setVisibility(View.VISIBLE);
            Glide.with(context).load(data.getPictures().get(1)).into(viewHolder.img_c2);
        }
        if( data.getPictures().size() > 2){
            viewHolder.img_c3.setVisibility(View.VISIBLE);
            Glide.with(context).load(data.getPictures().get(2)).into(viewHolder.img_c3);
        }
    }

    private void tagInto(ViewHolder viewHolder,ForumSearch.ScenicWithPageList data){
        if( data.getTags().size() > 0){
            viewHolder.txt_t1.setVisibility(View.VISIBLE);
            viewHolder.txt_t1.setText("#"+data.getTags().get(0));
        }

        if( data.getTags().size() > 1){
            viewHolder.txt_t2.setVisibility(View.VISIBLE);
            viewHolder.txt_t2.setText("#"+data.getTags().get(1));
        }

        if( data.getTags().size() > 2 ){
            viewHolder.txt_t3.setVisibility(View.VISIBLE);
            viewHolder.txt_t3.setText("#"+data.getTags().get(2));
        }
    }

    public ArrayList<ForumSearch.ScenicWithPageList> getDataArray() {
        return dataArray;
    }

    public void setDataArray(ArrayList<ForumSearch.ScenicWithPageList> dataArray) {
        this.dataArray = dataArray;
    }

    public void initForumHtmlNet(int srid){
        Intent intent = new Intent(context, WebActivity.class);
        intent.putExtra("url", HttpUtil.ADDRESS+"getView/getScenicReleaseView/"+srid);
        context.startActivity(intent);
    }
}
