package com.example.roam.game.app.leshan;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.roam.R;
import com.example.roam.game.leshan.control.GameSecondLsControl;
import com.example.roam.game.leshan.view.GameSecondLsUI;
import com.example.roam.utils.StatusBarUtils;

public class GameSecondLsActivity extends AppCompatActivity {
    private GameSecondLsUI gameSecondLsUI;
    private GameSecondLsControl gameSecondLsControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_second_ls);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        gameSecondLsUI = this.findViewById(R.id.scene2);
        gameSecondLsControl = gameSecondLsUI.getGameSecondLsControl();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameSecondLsControl.isVisible = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameSecondLsControl.isVisible = true;
    }
}
