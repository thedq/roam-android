package com.example.roam.game.qcmountain.model.layer2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.Log;

import com.example.roam.game.app.qcmountain.GameThirdActivity;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.BitmapUtil;
import com.example.roam.game.util.GameConstant;

public class DialogSpirit extends Spirit {
    private String text;
    private StringBuffer saveText;
    private Paint paint;
    private int correctNum;
    private boolean isFirstClick;

    public DialogSpirit(Context context) {
        super(context, 10, 1420, 10+1060 , 1770 );
        this.bitmap = BitmapUtil.changeBitmapSize(context, GameConstant.DIALOG,width,height);
        this.context = context;
        this.isVisible = false;
        this.paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.saveText = new StringBuffer();
        initData();
    }

    public void initData(){
        this.paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setTextSize(40);
        paint.setColor(Color.BLACK);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
    }

    @Override
    public void draw(Canvas canvas) {
        if( isVisible ){
            canvas.drawBitmap(bitmap,null,rect,null);
            drawText(canvas);
        }
    }

    // 换行文本方法
    public void drawText(Canvas canvas){
        // 1. 切割成11个字符每个String[]
        saveText = new StringBuffer();
        char[] chars = text.toCharArray();
        StringBuffer stringBuffer = new StringBuffer();
        int row = 0; //行数，每行间隔80px
        int count = 0;
        for (int i = 0; i < chars.length; i++) {
            if(row == 4){
                break; // 限制为4行
            }
            stringBuffer.append(chars[i]); // i%11!=0继续 且 i!=chars.length-1
            count++;

            if( count>=23){
                canvas.drawText(stringBuffer.toString(),(float) (rect.left+70),(float) (rect.top+height*0.22+row*70),paint); // 左右80%的区域，上面20%开始
                saveText.append(stringBuffer.toString());
                row++;
                stringBuffer = new StringBuffer();
                count = 0;  //复原字符数量
                // 出现问题 出现三行为69 第四行不足23个字符
                // 如果刚好：末尾还加上不为null
            }
        }
        if(  !stringBuffer.toString().equals("")){                // i到最后而且stingBuffer有内容
            canvas.drawText(stringBuffer.toString(),(float) (rect.left+70),(float) (rect.top+height*0.22+row*70),paint); // 左右80%的区域，上面20%开始
            saveText.append(stringBuffer.toString());
        }

    }

    // 如果correctNum == 3
    public void passTest(){
        if( correctNum == 3 ){
            Intent intent = new Intent(context, GameThirdActivity.class);
            context.startActivity(intent);
        }
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String getText() {
        return text;
    }

    public StringBuffer getSaveText() {
        return saveText;
    }

    public void setSaveText(StringBuffer saveText) {
        this.saveText = saveText;
    }

    public int getCorrectNum() {
        return correctNum;
    }

    public void setCorrectNum(int correctNum) {
        this.correctNum = correctNum;
    }

    public boolean isFirstClick() {
        return isFirstClick;
    }

    public void setFirstClick(boolean firstClick) {
        isFirstClick = firstClick;
    }
}
