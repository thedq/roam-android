package com.example.roam.game.jiuzhaigou.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class JiuMountainSpirit extends CSpirit {

    public JiuMountainSpirit(Context context) {
        super(context, 570,595 , 1080,1050 );
        this.text = GameConstant.JIU_MOUNTAIN_TEXT;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
