package com.example.roam.game.app.emeimountain;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.roam.R;
import com.example.roam.game.emeimountain.control.GameSecondControlEms;
import com.example.roam.game.emeimountain.view.GameSecondUiEms;
import com.example.roam.utils.StatusBarUtils;

public class GameSecondEmsActivity extends AppCompatActivity {
    private GameSecondUiEms gameSecondUiEms;
    private GameSecondControlEms gameSecondControlEms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_second_ems);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        gameSecondUiEms = this.findViewById(R.id.scene2);
        gameSecondControlEms = gameSecondUiEms.getGameSecondControlEms();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameSecondControlEms.isVisible = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameSecondControlEms.isVisible = true;
    }
}