package com.example.roam.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.roam.R;
import com.example.roam.utils.Constans;
import com.example.roam.utils.StatusBarUtils;

public class WebActivity extends AppCompatActivity {
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        initView();;
        initData();
    }

    public void initView(){
        webView = this.findViewById(R.id.webView);
    }

    public void initData(){
        Intent intent = getIntent();
        webView.setWebViewClient(new WebViewClient());
        String url = intent.getStringExtra("url") + "?uid="+ Constans.u_id;
        webView.loadUrl(url);
        Log.i("URL", "initData: "+url);
        webView.getSettings().setJavaScriptEnabled(true);
    }
}