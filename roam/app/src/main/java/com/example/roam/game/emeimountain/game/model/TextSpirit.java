package com.example.roam.game.emeimountain.game.model;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

public class TextSpirit extends Spirit{
    private Paint paint;
    private String mText;

    public TextSpirit(Context context, int left, int top) {
        super(context, left, top, 0, 0);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setTextSize(52);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLACK);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawText(mText,left,top,paint);
    }

    public void setLeft(int left){
        this.left = left;
    }

    public void setTop(int top){
        this.top = top;
    }

    public void setTextSize(int size){
        paint.setTextSize(size);
    }

    public String getmText() {
        return mText;
    }

    public void setmText(String mText) {
        this.mText = mText;
    }
}
