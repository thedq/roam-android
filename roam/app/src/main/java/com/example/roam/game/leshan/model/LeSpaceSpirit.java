package com.example.roam.game.leshan.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class LeSpaceSpirit extends CSpirit {
    public LeSpaceSpirit(Context context) {
        super(context, 196, 0, 774, 368);
        this.text = GameConstant.LE_SPACE_TEXT;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
