package com.example.roam.utils;

import android.content.Context;
import android.view.animation.Animation;

/**
 * Create By Visen
 * Date: 2021/10/17
 */
public class AnimationUtils {
    public static Animation getAnimation(Context context ,int id){
        return android.view.animation.AnimationUtils.loadAnimation(context,id);
    }
}
