package com.example.roam.game.qcmountain.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.util.BitmapUtil;
import com.example.roam.game.util.GameConstant;

public class BadgeSpirit extends CSpirit{

    public BadgeSpirit(Context context) {
        super(context, 970, 50, 1080,160 );
        this.bitmap = BitmapUtil.changeBitmapSize(context, GameConstant.BADGE_SPIRIT,width,height);

    }

    @Override
    public void draw(Canvas canvas) {
        if (isVisible) {
            canvas.drawBitmap(bitmap, null, rect, null);
        }
    }
}
