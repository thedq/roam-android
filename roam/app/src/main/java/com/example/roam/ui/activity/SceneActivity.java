package com.example.roam.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.roam.R;
import com.example.roam.game.app.dujiangyan.GameFirstDjyActivity;
import com.example.roam.game.app.emeimountain.GameFirstEmsActivity;
import com.example.roam.game.app.jiuzhaigou.GameFirstJzgActvity;
import com.example.roam.game.app.leshan.GameFirstLsActivity;
import com.example.roam.game.app.qcmountain.GameFirstActivity;
import com.example.roam.utils.Constans;
import com.example.roam.utils.StatusBarUtils;
import com.example.roam.view.MapContainer;
import com.example.roam.view.Marker;

import java.util.ArrayList;

public class SceneActivity extends AppCompatActivity {
    private ImageView img_character,  img_npc , img_next , img_left;
    private RadioGroup rg_options;
    private RadioButton btn_optionA , btn_optionB , btn_optionC , btn_optionExit;
    private TextView txt_content ,txt_locationName ,txt_holidayName;
    private int content_front  = 0, content_after = 0 , content_lenth = 0; // 对话索引标记，对话句数
    private String conversation = ""; //对话总剧本
    private String content = ""; // 对话框里展示的剧本
    private String question = ""; //问题
    private int questionIndex = 0;
    private int character_select=1;
    private int character;
    private static final String TAG = "SceneActivity";
    private ArrayList<Marker> markArray;
    private MapContainer mapContainer;
    private Spinner mSpinnerSc , mSpinnerHoliday;
    private ArrayAdapter<String> scAdapter, holidayAdapter;
    private TextView choose_bag_certain;
    // 5个字符串数组  青城山，都江堰，峨眉山，乐山大佛，九寨沟
    private String[] qcsStrs,djyStrs,emsStrs,lsStrs,jzgStrs;
    private ArrayList<String> qcs_array,djy_array,ls_array,ems_array,jzg_array;
    private ArrayList<String> [] arrays;
    private int position;
    private String chooseSc = "青城山" , chooseHoliday="青城三月三采茶节";
    private LinearLayout choose_bag;
    private boolean isFistChange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scene);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        iniView();
        iniData();
        iniEvent();
        markArray = new ArrayList<>();
        Marker marker1 = new Marker(0.545f,0.435f,R.drawable.m,"青城山"); //青城山 0.548 0.431
        Marker marker2 = new Marker(0.553f,0.415f,R.drawable.z,"都江堰"); // 都江堰 0.55 0.42
        Marker marker3 = new Marker(0.572f,0.574f,R.drawable.ls,"乐山大佛"); // 乐山大佛 0.572 0.574
        Marker marker4 = new Marker(0.514f,0.587f,R.drawable.ems,"峨眉山"); // 峨眉山  0.514 0.587
        Marker marker5 = new Marker(0.553f,0.184f,R.drawable.z,"九寨沟"); // 九寨沟 0.553  0.184
        markArray.add(marker2);
        markArray.add(marker1);
        markArray.add(marker3);
        markArray.add(marker4);
        markArray.add(marker5);
        mapContainer = this.findViewById(R.id.mapContainer);
        mapContainer.getMapView().setImageResource(R.drawable.mpa_all);
        mapContainer.setMarkers(markArray);
        locationClick();
    }

    public void initArray(){
        qcsStrs = getResources().getStringArray(R.array.qcs_holiday_array);
        djyStrs = getResources().getStringArray(R.array.djy_holiday_array);
        lsStrs = getResources().getStringArray(R.array.ls_holiday_array);
        emsStrs = getResources().getStringArray(R.array.ems_holiday_array);
        jzgStrs = getResources().getStringArray(R.array.jzg_holiday_array);
        String[][] holidayStrs = new String[][]{qcsStrs, djyStrs, lsStrs, emsStrs,jzgStrs};
        qcs_array = new ArrayList<>();
        djy_array = new ArrayList<>();
        ls_array = new ArrayList<>();
        ems_array = new ArrayList<>();
        jzg_array = new ArrayList<>();
         arrays = new ArrayList[]{qcs_array,djy_array,ls_array,ems_array,jzg_array}; // 在这些index地区中加入具体节日
        for (int i = 0; i < holidayStrs.length; i++) {
            for (int j = 0; j < holidayStrs[i].length; j++) {
                arrays[i].add(holidayStrs[i][j]);
            }
        }
    }

    public void iniView(){
        initArray();
        txt_holidayName = this.findViewById(R.id.txt_holidayName);
        choose_bag = this.findViewById(R.id.choose_bag);
        txt_locationName = this.findViewById(R.id.txt_locationName);
        img_left = this.findViewById(R.id.img_left);
        img_next = this.findViewById(R.id.img_next);
        img_npc = this.findViewById(R.id.img_npc);
        img_character = this.findViewById(R.id.img_character);
        rg_options = this.findViewById(R.id.rg_options);
        txt_content = this.findViewById(R.id.txt_content);
        btn_optionA = this.findViewById(R.id.btn_optionA);
        btn_optionB = this.findViewById(R.id.btn_optionB);
        btn_optionC = this.findViewById(R.id.btn_optionC);
        btn_optionExit = this.findViewById(R.id.btn_optionExit);
        mSpinnerSc = this.findViewById(R.id.m_spinner_sc);
        mSpinnerHoliday = this.findViewById(R.id.m_spinner_holiday);
        choose_bag_certain = this.findViewById(R.id.choose_bag_certain);
        scAdapter = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,getResources().getStringArray(R.array.sc));
        holidayAdapter = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,qcsStrs);
    }

    public void iniData(){
        Glide.with(SceneActivity.this).load(Constans.u_character).into(img_character);
        conversation = getResources().getString(R.string.scene_content);  // 获取放在string.xml的剧本资源,对话剧本
        txt_content.setText(this.getContent()); // 一次只显示2句话
        mSpinnerSc.setAdapter(scAdapter);
        mSpinnerHoliday.setAdapter(holidayAdapter);

    }

    public void iniEvent(){
        MyListener myListener = new MyListener();
        img_character.setOnClickListener(myListener);
        btn_optionA.setOnClickListener(myListener);
        btn_optionB.setOnClickListener(myListener);
        btn_optionC.setOnClickListener(myListener);
        btn_optionExit.setOnClickListener(myListener);
        img_left.setOnClickListener(myListener);
        img_next.setOnClickListener(myListener);
        txt_locationName.setOnClickListener(myListener);
        mSpinnerSc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view;
                chooseSc = textView.getText().toString();
                holidayAdapter = new ArrayAdapter<String>(SceneActivity.this,R.layout.support_simple_spinner_dropdown_item,arrays[position]);
                mSpinnerHoliday.setAdapter(holidayAdapter);
                holidayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mSpinnerHoliday.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SceneActivity.this.position = position;
                Log.i(TAG, "onItemSelected: position: "+position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        choose_bag_certain.setOnClickListener(myListener);
    }

    class MyListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch ( v.getId() ){
                case R.id.txt_locationName:
                    choose_bag.setVisibility(View.VISIBLE);
                    break;
                case R.id.choose_bag_certain:
                    getHolidayIntroduceContent();
                    choose_bag.setVisibility(View.GONE);
                    break;

                case R.id.img_left:
                    SceneActivity.this.finish();
                    break;

                case R.id.btn_optionA:
//                    setBtn_optionA();
                    break;

                case R.id.btn_optionB:
//                    setBtn_optionB();
                    break;

                case R.id.btn_optionC:
                    break;

                case R.id.img_next:
                    txt_content.setText(SceneActivity.this.getContent());
                    break;
            }
        }
    }

    private void getHolidayIntroduceContent(){
        if( chooseSc.equals("青城山")){
            switch (position) {
                case 0:
                    chooseHoliday = "青城三月三采茶节";
                    conversation = this.getResources().getString(R.string.qcs_holiday_sys);
                    break;
            }
            txt_locationName.setText("青城山");
            mapContainer.bigLoc(0);
        }else if(chooseSc.equals("都江堰")){
            switch (position){
                case 0:
                    chooseHoliday = "放水节";
                    conversation = this.getResources().getString(R.string.djy_holiday_fsj);
                    break;
                case 1:
                    chooseHoliday = "河西插柳";
                    conversation = this.getResources().getString(R.string.djy_holiday_hxcl);
                    break;
            }
            txt_locationName.setText("都江堰");
            mapContainer.bigLoc(1);
        }else if(chooseSc.equals("乐山大佛")){
            switch (position){
                case 0:
                    chooseHoliday = "龙舟竞赛";
                    conversation = this.getResources().getString(R.string.ls_holiday_lzjs);
                    break;
                case 1:
                    chooseHoliday = "乐山踩门";
                    conversation = this.getResources().getString(R.string.ls_holiday_lscm);
                    break;
            }
            txt_locationName.setText("乐山大佛");
            mapContainer.bigLoc(2);
        }else if(chooseSc.equals("峨眉山")){
            switch (position){
                case 0:
                    chooseHoliday = "朝山会";
                    conversation = this.getResources().getString(R.string.ems_holiday_csh);
                    break;
            }
            txt_locationName.setText("峨眉山");
            mapContainer.bigLoc(3);
        }else if(chooseSc.equals("九寨沟")){
            switch (position){
                case 0:
                    chooseHoliday="冰瀑节";
                    conversation = this.getResources().getString(R.string.jzg_holiday_bpj);
                    break;
                case 1:
                    chooseHoliday="民俗风情月";
                    conversation = this.getResources().getString(R.string.jzg_holiday_msfqy);
                    break;
                case 2:
                    chooseHoliday="嘛智文化节";
                    conversation = this.getResources().getString(R.string.jzg_holiday_mzwhj);
                    break;
                case 3:
                    chooseHoliday="日桑文化节";
                    conversation = this.getResources().getString(R.string.jzg_holiday_rswhj);
                    break;
            }
            txt_locationName.setText("九寨沟");
            mapContainer.bigLoc(4);
        }
        content_front = 0;
        txt_content.setText(this.getContent()); // 一次只显示2句话
        txt_holidayName.setText(chooseHoliday);
    }

    //剧本的切割处理
    public String getContent(){
        String pauseContent = "";
        pauseContent = conversation.replace("。", "。\n;");
        String[] split = pauseContent.split(";");
        if( content_front < split.length ) {  //不是最后一句话就继续对话。
            content = "";
            int count = 0;
            for (int i = content_front; i < split.length && i < content_front + 2; i++) {
                content += split[i];
                count++;
            }
            content_front += count;
        }

        return content;
    }

    public void locationClick(){
        for (int i = 0; i < markArray.size(); i++) {
            Marker marker = markArray.get(i);
            ImageView markerView = marker.getMarkerView();
            if( marker.getMarkerName().equals("青城山") ){
                markerView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mapContainer.bigLoc(0);
                        mSpinnerSc.setSelection(0);
                        choose_bag.setVisibility(View.VISIBLE);
                    }
                });
            } else if( marker.getMarkerName().equals("都江堰")){
                markerView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mapContainer.bigLoc(1);
                        mSpinnerSc.setSelection(1);
                        choose_bag.setVisibility(View.VISIBLE);

                    }
                });
            }else if( marker.getMarkerName().equals("峨眉山")){
                markerView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mapContainer.bigLoc(3);
                        mSpinnerSc.setSelection(3);
                        choose_bag.setVisibility(View.VISIBLE);

                    }
                });
            }else if( marker.getMarkerName().equals("乐山大佛")){
                markerView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mapContainer.bigLoc(2);
                        mSpinnerSc.setSelection(2);
                        choose_bag.setVisibility(View.VISIBLE);
                    }
                });
            }else if( marker.getMarkerName().equals("九寨沟")){
                markerView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mapContainer.bigLoc(4);
                        mSpinnerSc.setSelection(4);
                        choose_bag.setVisibility(View.VISIBLE);
                    }
                });
            }
        }

    }
}