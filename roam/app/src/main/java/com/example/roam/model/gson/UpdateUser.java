package com.example.roam.model.gson;

import com.example.roam.model.UserEntity;
import com.google.gson.annotations.SerializedName;

/**
 * Create By Visen
 * Date: 2021/11/23
 */
public class UpdateUser {
    private boolean success;
    private int code;
    private String message;

    @SerializedName("data")
    private data data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UpdateUser.data getData() {
        return data;
    }

    public void setData(UpdateUser.data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "User{" +
                "success=" + success +
                ", code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public class data{
        @SerializedName("修改后的UserInfo")
        private UserEntity userEntity;

        public UserEntity getUserEntity() {
            return userEntity;
        }

        public void setUserEntity(UserEntity userEntity) {
            this.userEntity = userEntity;
        }

        @Override
        public String toString() {
            return "data{" +
                    "userEntity=" + userEntity +
                    '}';
        }
    }
}
