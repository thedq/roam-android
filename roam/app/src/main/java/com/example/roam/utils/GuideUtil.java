package com.example.roam.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.example.roam.R;

public class GuideUtil {
    private Context context;
    private ImageView imgView;
    private ViewGroup content; // activity基本Layout
    private static GuideUtil instance = null;
    private boolean isFirst = true;
    private int i =1;
    int img[] = new int[]{R.mipmap.guide_1,R.mipmap.guide_2,R.mipmap.guide_3};
//    int detailImageArray[] = new int[]{R.mipmap.zhangjie_pic, R.mipmap.banben_pic, R.mipmap.xuxie_pic};

    private GuideUtil() {
    }

    public static GuideUtil getInstance() {
        synchronized (GuideUtil.class) {
            if (null == instance) {
                instance = new GuideUtil();
            }
        }
        return instance;
    }

    private Handler handler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 1:
                    // 设置LayoutParams参数
                    final WindowManager.LayoutParams params = new WindowManager.LayoutParams();
                    params.type = WindowManager.LayoutParams.TYPE_PHONE;
                    // 设置显示格式
                    params.format = PixelFormat.RGBA_8888;
                    // 设置对齐方式
                    params.gravity = Gravity.LEFT | Gravity.BOTTOM;
                    // 设置宽高
//                    params.width = Util.getScreenWidth(context);
//                    params.height = Util.getScreenHeight(context);
                    params.width = SizeUtils.scalePx((Activity) context).widthPixels;
                    params.height = SizeUtils.scalePx((Activity) context).heightPixels;
                    // 设置动画
//                    params.windowAnimations = R.style.view_anim;
                    // 添加到当前的界面上
                    content.addView(imgView, params);  //基本容器添加图片
                    break;
            }
        };
    };

    public void initGuide(Activity context, int flag) {
        if (isFirst) { return; }  // 如果不是第一次登录，返回
        this.context = context; // 获取context
        content = (ViewGroup) context.findViewById(android.R.id.content);  // 应该是查询基本layout
        // 动态初始化图层
        imgView = new ImageView(context);  // 生成imgView对象
        imgView.setLayoutParams(new WindowManager.LayoutParams(
                android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                android.view.ViewGroup.LayoutParams.MATCH_PARENT));
        imgView.setScaleType(ImageView.ScaleType.FIT_XY);
//        imgView.setImageResource(mipmapRourcesId);
        imgView.setImageResource(R.mipmap.guide_1);
        handler.sendEmptyMessage(1); // 给viewgroup添加imageView
        // 点击图层之后，将图层移除
        imgView.setOnClickListener(new View.OnClickListener() {  // 给该imgView添加点击事件
            @Override
            public void onClick(View arg0) {
                int[] stepImageArray = new int[]{};  // 存放步骤图片数组
                if (flag == 0) {                         //首页 flag控制不同类型引导页
                    stepImageArray = img;                //flag1：stepImageArray = img
                }
//                } else if (flag == 1) {
////                    stepImageArray = detailImageArray;   // flag2：stepImageArray = img
//                }
                if (i==stepImageArray.length){   // 复原
                    i=0;
                    content.removeView(imgView);
                    isFirst = true;
                }

                if ( i < stepImageArray.length && i < stepImageArray.length) {
                    imgView.setImageResource(stepImageArray[i++]);  // 点击切换图片
                }

            }
        });
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }
}
