package com.example.roam.game.emeimountain.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class EmsEntrancePlaque2Spirit extends CSpirit {
    public EmsEntrancePlaque2Spirit(Context context) {
        super(context, 200,776 ,862 ,920 );
        text = GameConstant.EMS_ENTRANCE_PLAQUE_TEXT2;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
