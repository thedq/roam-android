package com.example.roam.game.emeimountain.control;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import com.example.roam.game.app.emeimountain.GameSecondEmsActivity;
import com.example.roam.game.emeimountain.game.ui.CollectSpiritsActivity;
import com.example.roam.game.emeimountain.model.EmsTopJinDinSpirit;
import com.example.roam.game.emeimountain.model.EmsTopPlazaSpirit;
import com.example.roam.game.emeimountain.model.EmsTopSceneSpirit;
import com.example.roam.game.emeimountain.model.SceneGlodTop;
import com.example.roam.game.qcmountain.model.BarOptionSpirit;
import com.example.roam.game.tools.BarLayer;
import com.example.roam.game.tools.LeftSpirit;
import com.example.roam.game.util.WinBarLayer;
import com.example.roam.ui.activity.MainActivity;

public class GameThirdControlEms {

    private Context context;
    private SceneGlodTop sceneGlodTop;
    private LeftSpirit leftSpirit;
    private BarLayer barLayer;
    private BarOptionSpirit barOptionSpirit;
    private EmsTopJinDinSpirit emsTopJinDinSpirit;
    private EmsTopPlazaSpirit emsTopPlazaSpirit;
    private EmsTopSceneSpirit emsTopSceneSpirit;
    private WinBarLayer winBarLayer;
    public boolean isVisible = true;
    public boolean isFinsh = false;

    public GameThirdControlEms(Context context){
        this.context = context;
        sceneGlodTop = new SceneGlodTop(context);
        leftSpirit = new LeftSpirit(context);
        barLayer = new BarLayer(context);
        barOptionSpirit = new BarOptionSpirit(context);
        emsTopJinDinSpirit = new EmsTopJinDinSpirit(context);
        emsTopPlazaSpirit = new EmsTopPlazaSpirit(context);
        emsTopSceneSpirit = new EmsTopSceneSpirit(context);
        winBarLayer = new WinBarLayer(context,"峨眉山");
    }

    public void draw(Canvas canvas){
        if( isVisible ){
            sceneGlodTop.draw(canvas);
            leftSpirit.draw(canvas);
            emsTopJinDinSpirit.draw(canvas);
            emsTopSceneSpirit.draw(canvas);
            emsTopPlazaSpirit.draw(canvas);
            barLayer.draw(canvas);
            barOptionSpirit.draw(canvas);
        }

        if(isFinsh){
            winBarLayer.draw(canvas);
        }
    }

    public void onTouch(MotionEvent event){
        switch ( event.getAction() ){
            case MotionEvent.ACTION_DOWN:
                clickSceneGlodTop(event);
                clickTopScence(event);
                clickTopPlaza(event);
                clickLeftSpirit(event);
                clickJinDing(event);
                clickDelete(event);
                clickOptionCancel(event);
                clickOptionDelete(event);
                clickOptionCertain(event);
                clickWinBarLayerDelete(event);
                break;
        }
    }

    private void clickWinBarLayerDelete(MotionEvent event){
        winBarLayer.getBarLayer().clickDelete(event);
    }

    private void clickTopPlaza(MotionEvent event){
        emsTopPlazaSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickTopScence(MotionEvent event){
        emsTopSceneSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickOptionDelete(MotionEvent event){
        barOptionSpirit.clickOptionDelete(event);
    }

    private void clickOptionCancel(MotionEvent event){
        barOptionSpirit.clickOptionCancel(event);
    }

    String TAG = "TAG";
    public void clickSceneGlodTop(MotionEvent event){
        if (sceneGlodTop.getRect().contains((int) event.getX(),(int) event.getY())  ) {
//            Toast.makeText(context,"金顶",Toast.LENGTH_SHORT).show();
            Log.i(TAG, "clickSceneGlodTop: x:"+event.getX()+" y:"+event.getY());
        }
    }

    public void clickDelete(MotionEvent event){
        barLayer.clickDelete(event);
    }

    public void clickJinDing(MotionEvent event) {
        if (!isFinsh) {
            emsTopJinDinSpirit.clickSpiritOption(event, barLayer, barOptionSpirit);
        }
    }

    public void clickOptionCertain(MotionEvent event){
        if (barOptionSpirit.isVisible &&  barOptionSpirit.getOptionYes().getRect().contains((int) event.getX(),(int) event.getY()) && !barOptionSpirit.isClickThrough() ) {
            Intent intent = new Intent(context, CollectSpiritsActivity.class);
            context.startActivity(intent);
            barOptionSpirit.setClickThrough(true);
            ((Activity)context).finish();
        }
    }

    public void clickLeftSpirit(MotionEvent event){
        if (leftSpirit.getRect().contains((int) event.getX(),(int) event.getY())) {
            leftSpirit.finsh(context);
        }
    }

    public boolean isFinsh() {
        return isFinsh;
    }

    public void setFinsh(boolean finsh) {
        isFinsh = finsh;
    }
}
