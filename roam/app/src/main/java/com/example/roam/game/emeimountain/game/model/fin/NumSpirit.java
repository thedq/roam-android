package com.example.roam.game.emeimountain.game.model.fin;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.example.roam.R;
import com.example.roam.game.emeimountain.game.model.Spirit;


public class NumSpirit extends Spirit {
    private Bitmap[] numBitmaps = new Bitmap[10];
    private int type;
    private int[] nums = new int[]{R.drawable.gn_0,R.drawable.gn_1,R.drawable.gn_2,R.drawable.gn_3,R.drawable.gn_4,R.drawable.gn_5,R.drawable.gn_6,R.drawable.gn_7,R.drawable.gn_8,R.drawable.gn_9};
    public NumSpirit(Context context, int left, int top, int right, int bottom,int type) {
        super(context, left, top, right, bottom);
        for (int i = 0; i < numBitmaps.length; i++) {
            numBitmaps[i] = BitmapFactory.decodeResource(context.getResources(),nums[i]);
        }
    }

    @Override
    public void draw(Canvas canvas) {
        if (isVisible) {
            switch ( type ){
            case 0:
                canvas.drawBitmap(numBitmaps[0],null,rect,null);
                break;
            case 1:
                canvas.drawBitmap(numBitmaps[1],null,rect,null);
                break;
            case 2:
                canvas.drawBitmap(numBitmaps[2],null,rect,null);
                break;
            case 3:
                canvas.drawBitmap(numBitmaps[3],null,rect,null);
                break;
            case 4:
                canvas.drawBitmap(numBitmaps[4],null,rect,null);
                break;
            case 5:
                canvas.drawBitmap(numBitmaps[5],null,rect,null);
                break;
            case 6:
                canvas.drawBitmap(numBitmaps[6],null,rect,null);
                break;
            case 7:
                canvas.drawBitmap(numBitmaps[7],null,rect,null);
                break;
            case 8:
                canvas.drawBitmap(numBitmaps[8],null,rect,null);
                break;
            case 9:
                canvas.drawBitmap(numBitmaps[9],null,rect,null);
                break;
        }
        }
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
