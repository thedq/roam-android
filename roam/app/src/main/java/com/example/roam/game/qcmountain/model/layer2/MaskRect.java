package com.example.roam.game.qcmountain.model.layer2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.example.roam.game.qcmountain.model.Spirit;

public class MaskRect extends Spirit {
    private Paint paint;
    public MaskRect(Context context) {
        super(context, 0, 0 ,1080, 1920);
        this.paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.paint.setStyle(Paint.Style.FILL);
        this.paint.setColor(Color.parseColor("#80000000"));
        this.paint.setStrokeWidth(28);
        this.isVisible = false;
    }

    @Override
    public void draw(Canvas canvas) {
        if (isVisible) {
            canvas.drawRect(rect, paint);
        }
    }
}
