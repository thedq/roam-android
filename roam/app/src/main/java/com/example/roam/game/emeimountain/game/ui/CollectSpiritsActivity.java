package com.example.roam.game.emeimountain.game.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import com.example.roam.R;


public class CollectSpiritsActivity extends AppCompatActivity {
    private GameUI gameUI;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect_spirits);
        gameUI = this.findViewById(R.id.gameui_ems);
        Intent intent = getIntent();
        int clazz = intent.getIntExtra("clazz", 0);
        gameUI.getGameControl().setClazz(clazz);
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameUI.getGameControl().setEnd(true);
        this.finish();
    }
}