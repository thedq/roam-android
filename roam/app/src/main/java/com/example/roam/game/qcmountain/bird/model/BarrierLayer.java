package com.example.roam.game.qcmountain.bird.model;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.example.roam.utils.Constans;

import java.util.ArrayList;
import java.util.Random;

public class BarrierLayer {
    private ArrayList<Barrier> barrierArray;
    private Context context;
    private Random random = new Random();
    private BarrierPool barrierPool;
    private int numberTop,numberDown;
    private int score;
    private Paint scorePaint;

    public BarrierLayer(Context context){
        this.context = context;
        init();
    }

    private void init(){
        this.barrierArray = new ArrayList<>();
        this.barrierPool = new BarrierPool(this.context);
        Rect dstRectUp = new Rect(300,1330,456,1730);
        Rect dstRectDown = new Rect(300,0,456,400);//1730
        Barrier barrier = new Barrier(context,dstRectUp,0);
        Barrier barrier1 = new Barrier(context,dstRectDown,1);
        scaleRect(dstRectUp);
        scaleRect(dstRectDown);
        barrierArray.add(barrier);
        barrierArray.add(barrier1);
        this.scorePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.scorePaint.setColor(Color.WHITE);
        this.scorePaint.setTextSize(100);
    }

    public void draw(Canvas canvas,Bird bird){
        logic(bird);
        for (int i = 0; i < barrierArray.size(); i++) {
            Barrier barrier = barrierArray.get(i);
            if (barrier.isVisible()) {  // 如果还可见就画，不可见就回收
                barrier.draw(canvas,bird);
            } else {
                barrierArray.remove(barrier); // 删除
                barrierPool.setPool(barrier); // 返回给对象池
                i--;
            }
        }
        collideBird(bird);
        getScoreAndresetBar(bird);
        /**
         *  过线和不可见
         *  过线添加，不可见回收
         * */
    }

    public void setPauseGame(){
        for (int i = 0; i < barrierArray.size(); i++) {
            barrierArray.get(i).setPauseGame();
        }
    }

    public void setResumeGame(){
        for (int i = 0; i < barrierArray.size(); i++) {
            barrierArray.get(i).setResumeGame();
        }
    }

    // 逻辑生成Barrier
    public void logic(Bird bird){
        if(bird.isLife && !barrierArray.isEmpty() && barrierArray.get(barrierArray.size()-1).isLastBarrier()){   // 如果这是array里最后一个spirit到达终点
            ran();
            Rect dstRectDown = new Rect(1080,0,1236,numberDown);//1730
            Rect dstRectUp = new Rect(1080,1920-numberTop,1236,1920);
            scaleRect(dstRectUp);
            scaleRect(dstRectDown);
            insert(dstRectUp,0);  // 从池里获取两个spirit，加入array
            insert(dstRectDown,1);
        }
    }
    // 从池中获取对象方法
    public void insert(Rect dstRect,int type){
        Barrier barrier = barrierPool.getBarrier();
        barrier.setDstRect(dstRect);
        barrier.setType(type);
        barrier.setVisible(true);
        barrier.setScore(true); // 重置得分点
        barrierArray.add(barrier);
    }
    public void ran(){
        // 算法一 .
        // 控制空袭时固定值
        numberTop = random.nextInt(800)+600;
        numberDown = random.nextInt(800)+600;
        if( numberDown + numberTop < 1400 || numberDown + numberTop > 1410 ){
            ran();
        }
    }

    // 碰撞
    public void collideBird(Bird bird){
        for (int i = 0; i < barrierArray.size(); i++) {
            if( bird.getRect().intersect(barrierArray.get(i).getDstRect())){
                bird.isLife = false;
                System.out.println("小鸟死亡！");
            }
        }
    }
    //得分. 得分点为true
    public void getScoreAndresetBar(Bird bird){
        for (int i = 0; i < barrierArray.size(); i++) {
            Barrier barrier = barrierArray.get(i);
            if( barrier.isScore() && barrier.getDstRect().right < bird.left){
                score++;
                barrier.setScore(false);
            }
        }
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    /**
     * 1. 全部还给对象池
     * 2. 重new
     * */
    public void tryAgain(){
        init();
    }

    private void scaleRect(Rect rect){
        rect.left *= Constans.SCALE_WIDTH;
        rect.right *= Constans.SCALE_WIDTH;
        rect.top *= Constans.SCALE_HEIGHT;
        rect.bottom *= Constans.SCALE_HEIGHT;
    }
}
