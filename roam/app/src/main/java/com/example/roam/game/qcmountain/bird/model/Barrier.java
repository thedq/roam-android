package com.example.roam.game.qcmountain.bird.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.example.roam.game.util.BitmapUtil;
import com.example.roam.game.util.GameConstant;


public class Barrier extends Spirit{
    private Bitmap bitmaps[];
    private Rect dstRect;
    private int type;
    private int speed = 10; // 1s= 500px
    private boolean visible;
    private boolean isScore = true; //得分点
    private boolean isPause ;

    public Barrier(Context context ,Rect dstRect, int type ) {
        super(context);
        bitmaps = new Bitmap[2];
        for (int i = 0; i < bitmaps.length; i++) {
            bitmaps[i] = BitmapUtil.changeBitmapSize(context, GameConstant.BARRIER_BITMAP[i],52*3,320);
        }
        this.dstRect = dstRect;
        this.type = type;
        isPause = false;
    }

    public Barrier(Context context){
        super(context);
        bitmaps = new Bitmap[2];
        for (int i = 0; i < bitmaps.length; i++) {
            bitmaps[i] = BitmapUtil.changeBitmapSize(context, GameConstant.BARRIER_BITMAP[i],52*3,320);
        }
    }
    @Override
    public void draw(Canvas canvas) {

    }

    public void draw(Canvas canvas,Bird bird){
        switch (type){
            case 0:
                canvas.drawBitmap(bitmaps[0],null,dstRect,null);
                break;
            case 1:
                canvas.drawBitmap(bitmaps[1],null,dstRect,null);
                break;
        }
        if( bird.isLife && !isPause ) {
            dstRect.left -= speed;
            dstRect.right -= speed;
        }
        if( dstRect.right < 0 ){
            visible = false;
        }
    }

    public void setPauseGame(){
        isPause = true;
    }

    public void setResumeGame(){
        isPause = false;
    }

    public boolean isLastBarrier(){
        return dstRect.right < 700;
    }

    public Rect getDstRect() {
        return dstRect;
    }

    public void setDstRect(Rect dstRect) {
        this.dstRect = dstRect;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setContext(Context context){
        this.context = context;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isScore() {
        return isScore;
    }

    public void setScore(boolean score) {
        isScore = score;
    }
}
