package com.example.roam.game.app.jiuzhaigou;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.roam.R;
import com.example.roam.game.app.qcmountain.GameThirdActivity;
import com.example.roam.game.jiuzhaigou.control.GameThirdJzgControl;
import com.example.roam.game.jiuzhaigou.view.GameThirdJzgUI;
import com.example.roam.utils.BadgeUtils;
import com.example.roam.utils.StatusBarUtils;

public class GameThirdJzgActvity extends AppCompatActivity {
    private GameThirdJzgUI gameThirdJzgUI;
    private GameThirdJzgControl gameThirdJzgControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_third_jzg_actvity);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色
        gameThirdJzgUI = this.findViewById(R.id.scene3);
        gameThirdJzgControl = gameThirdJzgUI.getGameThirdJzgControl();
        boolean isFinsh = getIntent().getBooleanExtra("isFinsh", false);
        gameThirdJzgControl.setIsFinsh(isFinsh);
        if(isFinsh){
            BadgeUtils.setBadgeIndex(14);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameThirdJzgControl.isVisible = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameThirdJzgControl.isVisible = true;
    }
}