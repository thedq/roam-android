package com.example.roam.game.dujiangyan.model;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class DMingJianSpirit extends CSpirit {
    public DMingJianSpirit(Context context) {
        super(context,742 , 659, 1061, 1190);
        this.text = GameConstant.MING_JIAN_TEXT;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
