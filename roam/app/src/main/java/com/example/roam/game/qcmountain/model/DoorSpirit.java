package com.example.roam.game.qcmountain.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.util.BitmapUtil;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class DoorSpirit extends CSpirit{
    public DoorSpirit(Context context) {
        super(context, 363, 1090, 363+400, 1090+286);
//        this.bitmap = BitmapUtil.changeBitmapSize(context, 0,width,height);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect( rect, PaintUtil.getPaint());
        }
    }

}
