package com.example.roam.game.qcmountain.model.layer2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.BitmapUtil;
import com.example.roam.game.util.GameConstant;

public class OptionSpirit extends Spirit {
    private Paint paint;
    public OptionSpirit(Context context) { // 755 182
        super(context, 720, 1150, 720+325, 1150+140);
        this.bitmap = BitmapUtil.changeBitmapSize(context,GameConstant.OPTION,width,height);
        this.paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setTextSize(50);
        paint.setColor(Color.BLACK);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
    }

    @Override
    public void draw(Canvas canvas) {
        if(isVisible){
            canvas.drawBitmap(bitmap,null,rect,null);
        }
    }

    public void draw(Canvas canvas,String text){
        if(isVisible){
            canvas.drawBitmap(bitmap,null,rect,null);
            canvas.drawText(text,rect.left+45,rect.top+70,paint);
        }
    }

}
