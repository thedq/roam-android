package com.example.roam.ui.test;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.example.roam.R;
import com.example.roam.model.UserEntity;
import com.example.roam.utils.DBHelper;

import java.util.ArrayList;

public class TestActivity extends AppCompatActivity {
    private static final String TAG = "TestActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test2);
        DBHelper dbHelper = new DBHelper(TestActivity.this, null, null, 1);
        SQLiteDatabase ds = dbHelper.getReadableDatabase();
        Cursor cursor = ds.query("user", new String[]{"u_id", "u_account", "u_password", "u_username", "u_character", "u_spirit", "u_head", "account_select", "character_select", "spirit_select", "head_select", "u_first_time", "u_last_time"},null , null, null, null, null);
        ArrayList<UserEntity> userEntityArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            UserEntity userEntity = new UserEntity();
            userEntity.setU_id(cursor.getInt (cursor.getColumnIndex("u_id")));
            userEntity.setU_account(cursor.getString(cursor.getColumnIndex("u_account")));
            userEntity.setU_password(cursor.getString(cursor.getColumnIndex("u_password")));
            userEntity.setU_username(cursor.getString(cursor.getColumnIndex("u_username")));
            userEntity.setU_character(cursor.getString(cursor.getColumnIndex("u_character")));
            userEntity.setU_spirit(cursor.getString(cursor.getColumnIndex("u_spirit")));
            userEntity.setU_head(cursor.getString(cursor.getColumnIndex("u_head")));
            userEntity.setAccount_select(cursor.getInt(cursor.getColumnIndex("account_select")));
            userEntity.setCharacter_select(cursor.getInt(cursor.getColumnIndex("character_select")));
            userEntity.setSpirit_select(cursor.getInt(cursor.getColumnIndex("spirit_select")));
            userEntity.setHead_select(cursor.getInt(cursor.getColumnIndex("head_select")));
            userEntity.setU_first_time(cursor.getString(cursor.getColumnIndex("u_first_time")));
            userEntity.setU_last_time(cursor.getString(cursor.getColumnIndex("u_last_time")));
            userEntityArrayList.add(userEntity);
            System.out.println(userEntity);
        }
        System.out.println("over!");

        deleteUser(1);
    }

    public void deleteUser(String u_account){
        DBHelper dbHelper = new DBHelper(TestActivity.this, null, null, 1);
        SQLiteDatabase ds = dbHelper.getReadableDatabase();
        ds.delete("user","u_account=?",new String[]{u_account});
    }

    public void deleteUser(int uid){
        DBHelper dbHelper = new DBHelper(TestActivity.this, null, null, 1);
        SQLiteDatabase ds = dbHelper.getReadableDatabase();
        ds.delete("user","",new String[]{});
    }

}