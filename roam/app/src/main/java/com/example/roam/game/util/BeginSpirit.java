package com.example.roam.game.util;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.MotionEvent;

import com.example.roam.R;
import com.example.roam.game.emeimountain.game.model.Spirit;
import com.example.roam.game.tools.BarLayer;
import com.example.roam.game.util.BitmapUtil;
import com.example.roam.game.util.GameConstant;

public class BeginSpirit extends Spirit {
    private RectF startRect;
    private RectF detailRect;
    private String startText;
    private String detailText;
    private Paint buttonPaint;
    private Paint paint;
    private final int startBaseLine;
    private final int detailBaseLine;
    private BarLayer barLayer;

    public BeginSpirit(Context context,String text) {
        super(context, 0, 0, 1080, 1793);
        this.bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.bar_begin);
        startRect = new RectF(340,1543,740,1693); // 300px 150px
        startText = GameConstant.GAME_START_BUTTON_TEXT;
        detailRect = new RectF(340,1353,740,1503);
        detailText = "游戏目标";
        barLayer = new BarLayer(context);
        barLayer.text = text;

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.WHITE);
        paint.setTextSize(50);
        paint.setTextAlign(Paint.Align.CENTER);
        Paint.FontMetricsInt fontMetricsInt = paint.getFontMetricsInt();
        startBaseLine = (int)(startRect.top +startRect.bottom -fontMetricsInt.bottom - fontMetricsInt.top) / 2;
        detailBaseLine = (int)(detailRect.top + detailRect.bottom - fontMetricsInt.bottom - fontMetricsInt.top) / 2;

        buttonPaint = new Paint();
        buttonPaint.setColor(Color.parseColor("#44605F"));
        this.isVisible = true;

    }

    public void setGameStartBitmap(int imageId){
        this.bitmap = BitmapFactory.decodeResource(context.getResources(),imageId);
    }

    @Override
    public void draw(Canvas canvas) {
        if( isVisible ){
            canvas.drawBitmap(bitmap,null,rect,null);
            canvas.drawRoundRect(startRect,50,50,buttonPaint);
            canvas.drawText(startText,startRect.centerX(),startBaseLine,paint);
            canvas.drawRoundRect(detailRect,50,50,buttonPaint);
            canvas.drawText(detailText,detailRect.centerX(),detailBaseLine,paint);
            barLayer.draw(canvas);
        }
    }

    public RectF getStartRect() {
        return startRect;
    }

    public RectF getDetailRect() {
        return detailRect;
    }

    public void clickDetail(MotionEvent event){
        if (isVisible && detailRect.contains((int)event.getX(),(int)event.getY())) {
            barLayer.isVisible = true;
        }
    }

    public void clickDetailDelete(MotionEvent event){
        barLayer.clickDelete(event);
    }

}
