package com.example.roam.game.dujiangyan.game.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import com.example.roam.R;
public class CatchDemon extends AppCompatActivity {
    private GameUI gameUI;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catch_demon);
        Intent intent = getIntent();
        int clazz = intent.getIntExtra("clazz", 0);
        gameUI = this.findViewById(R.id.gameui);
        gameUI.getGameControl().setClazz(clazz);
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameUI.getGameControl().setFinsh(true);
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}