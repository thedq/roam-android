package com.example.roam.game.app.emeimountain;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.roam.R;
import com.example.roam.game.emeimountain.control.GameThirdControlEms;
import com.example.roam.game.emeimountain.view.GameThirdUiEms;
import com.example.roam.utils.BadgeUtils;
import com.example.roam.utils.StatusBarUtils;

public class GameThirdEmsActivity extends AppCompatActivity {
    private GameThirdUiEms gameThirdUiEms;
    private GameThirdControlEms gameThirdControlEms;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_third_ems);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        boolean isFinsh = getIntent().getBooleanExtra("isFinsh", false);
        if(isFinsh){
            BadgeUtils.setBadgeIndex(8);
        }
        gameThirdUiEms = this.findViewById(R.id.scene3);
        gameThirdControlEms = gameThirdUiEms.getGameThirdControlEms();
        gameThirdControlEms.setFinsh(isFinsh);
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameThirdControlEms.isVisible = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameThirdControlEms.isVisible = true;
    }
}