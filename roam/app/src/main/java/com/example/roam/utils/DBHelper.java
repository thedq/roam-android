package com.example.roam.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

/**
 * Create By Visen
 * Date: 2021/10/26
 */
public class DBHelper extends SQLiteOpenHelper {
    public DBHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        //创建roam数据库
        super(context, "roam", factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //创建数据表
        // user用户信息表(正式) id（整型）, 邮箱 （字符串）， 密码 （字符串），用户名（字符串），虚拟人物（字符串），精灵（字符串），用户头像(字符串)，用户账户选择：邮箱还是其他(整型) 用户头像选择：来自系统还是相册(整型) 用户虚拟人物选择：来自系统还是网站图片(整型) 用户景点精灵选择：来自系统还是网站图片(整型) 用户头像选择：来自系统还是相册(整型) 用户注册时间(字符串) 用户最后一次登录时间
        db.execSQL("create table user (u_id integer primary key autoincrement, u_account text , u_password text , u_username text , u_character text , u_spirit text,u_head text,account_select integer,character_select integer,spirit_select integer,head_select integer,u_first_time text,u_last_time text)");

        // zone表(test2模拟) id 整型，内容 字符串 ，图片1 字符串，图片2 字符串
        db.execSQL("create table zone (z_id integer primary key autoincrement, z_content text , z_img1 text , z_img2 text, u_id integer )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
