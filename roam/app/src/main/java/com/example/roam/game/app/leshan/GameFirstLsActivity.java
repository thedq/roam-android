package com.example.roam.game.app.leshan;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.roam.R;
import com.example.roam.game.leshan.control.GameFirstLsControl;
import com.example.roam.game.leshan.view.GameFirstLsUI;
import com.example.roam.utils.StatusBarUtils;

public class GameFirstLsActivity extends AppCompatActivity {
    private GameFirstLsUI gameFirstLsUI;
    private GameFirstLsControl gameFirstLsControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_first_ls);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        gameFirstLsUI = this.findViewById(R.id.scene1);
        gameFirstLsControl = gameFirstLsUI.getGameFirstLsControl();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameFirstLsControl.isVisible = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameFirstLsControl.isVisible = true;
    }
}