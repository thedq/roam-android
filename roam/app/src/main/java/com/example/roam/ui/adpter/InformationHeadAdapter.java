package com.example.roam.ui.adpter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.roam.R;

import java.util.List;


public class InformationHeadAdapter extends BaseAdapter {
    private List<String> image;
    private boolean isChice[];
    private Context context;
    private boolean multiChoose;                 //表示当前适配器是否允许多选
    private int lastPosition = -1;             //记录上一次选中的图片位置，-1表示未选中任何图片
    private int head_select = 0;

    public InformationHeadAdapter(Context context, List<String> image, boolean multiChoose) {
        this.image = image;
        this.multiChoose = multiChoose;
        Log.i("hck", image.size() + "lenght");
        isChice = new boolean[image.size()];
        for (int i = 0; i < image.size(); i++) {
            isChice[i] = false;
        }
        this.context = context;
    }

    public int getLastPosition() {
        return lastPosition;
    }

    public void setLastPosition(int lastPosition) {
        this.lastPosition = lastPosition;
    }

    public int getHead_select() {
        return head_select;
    }

    public void setHead_select(int head_select) {
        this.head_select = head_select;
    }

    @Override
    public int getCount() {
        return image != null ? image.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return image != null ? image.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return image != null ? position : 0;
    }

    public boolean isMultiChoose() {
        return multiChoose;
    }

    public void setMultiChoose(boolean multiChoose) {
        this.multiChoose = multiChoose;
    }

    @Override
    public View getView(int arg0, View arg1, ViewGroup arg2) {
        View view = arg1;
        GetView getView = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.information_head_item, null);
            getView = new GetView();
            getView.imageView = (ImageView) view.findViewById(R.id.img_head);
            view.setTag(getView);
        } else {
            getView = (GetView) view.getTag();
        }
        getView.imageView.setImageDrawable(makeUpView(arg0, head_select));
        return view;
    }


    static class GetView {
        ImageView imageView;
    }


    //主要就是下面的代码了
    private LayerDrawable makeUpView(int post, int head_select) {

        Bitmap bitmap = ((BitmapDrawable) context.getResources().getDrawable(Integer.parseInt(image.get(post)))).getBitmap();
        Bitmap bitmap2 = null;
        LayerDrawable la = null;
        if ((isChice[post] == true) && (head_select == 1)&&(lastPosition==post)) {
            bitmap2 = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.head_selected);
        } else {
            bitmap2 = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.head_normal);
        }
        Drawable[] array = new Drawable[2];
        array[0] = new BitmapDrawable(bitmap);
        array[1] = new BitmapDrawable(bitmap2);
        la = new LayerDrawable(array);

        la.setLayerInset(0, 0, 0, 0, 0);   //第几张图离各边的间距
        //la.setLayerInset(1, 0, 65, 65, 0);
        // la.setLayerInset(1, 0, -5, 60, 45 );
        //左边 上边 右边 下边
        la.setLayerInset(1, 0, 0, 0, 0);//第几张图离各边的间距

        return la; // 返回叠加后的图
    }

    public void chiceState(int position) {
        // 多选时
        if (multiChoose == true) {
            isChice[position] = isChice[position] == true ? false : true; //直接取反即可
        }
        // 单选时
        else {
            if (lastPosition != -1)
                isChice[lastPosition] = false;
            isChice[position] = isChice[position] == true ? false : true;
            lastPosition = position;                 //记录本次选中的位置
        }
        notifyDataSetChanged();         //通知适配器进行更新
    }
}
