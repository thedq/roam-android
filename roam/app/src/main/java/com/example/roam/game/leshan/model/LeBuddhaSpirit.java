package com.example.roam.game.leshan.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class LeBuddhaSpirit extends CSpirit {
    public LeBuddhaSpirit(Context context) {
        super(context, 300, 550, 880, 1321);
        this.text = GameConstant.LE_BUDDHA_TEXT;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
