package com.example.roam.game.app.qcmountain;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.example.roam.R;
import com.example.roam.game.dujiangyan.control.GameFirstControlDJY;
import com.example.roam.game.dujiangyan.view.GameFirstUiDJY;
import com.example.roam.game.qcmountain.control.GameFirstControl;
import com.example.roam.game.qcmountain.view.GameFirstUI;
import com.example.roam.utils.StatusBarUtils;

public class GameFirstActivity extends AppCompatActivity {
    private GameFirstUI gameFirstUI;
    private GameFirstControl gameFirstControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_first);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        gameFirstUI = this.findViewById(R.id.first);
        gameFirstControl = gameFirstUI.getGameFirstControl();  // 这样占用内存过大，放弃
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameFirstControl.isVisible = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameFirstControl.isVisible = false;
    }
}