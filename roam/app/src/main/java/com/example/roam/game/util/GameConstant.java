package com.example.roam.game.util;

import com.example.roam.R;

public class GameConstant {

    public static final int BG = R.drawable.bg_adpter2_game ;
    public static final int ROAD = R.drawable.road_game;
    public static final int TOP_BACKGROUND = R.drawable.top_background_game;
    public static final int LEFT = R.drawable.left;
    public static final int TOAST = R.drawable.npc;
    public static final int BAR = R.drawable.bar_game;
    public static final int BAR_DELETE = R.drawable.bar_delete_game;
    public static final int LOADING_BACKGROUND = R.drawable.login_background;
    public static final int BADGE_SPIRIT = R.drawable.badge;

    //得分
    public static final String PASS_SCORE = "目标分数：";
    public static final String EARN_SCORE = "得分：";
    public static final String TIME_COST = "耗时：";
    public static final String GAME_CATCH_SPIRIT = "点击棋盘使道士占领棋格，堵截小妖怪即可通关。利用随机生成占领点更易通关呦。";
    public static final String GAME_FLY_BIRD = "飞翔的小鸟，可千万别让它被撞到了。通过十次石柱即可通关。";

    //青城山
    public static final int Qing_BADGE_A = R.drawable.q_a;
    public static final int Qing_BADGE_B = R.drawable.q_b;
    public static final int Qing_BADGE_C = R.drawable.q_c;

    // 都江堰
    public static final int SCENE1_MING_MOUNTAIN = R.drawable.scene1_djy;
    public static final int SCENE2_DU_JIAN_YAN = R.drawable.scene2_djy;
    public static final int SCENE3_BAO_PING_KOU = R.drawable.scene3_djy;
    public static final int DU_BADGE_A = R.drawable.d_a;
    public static final int DU_BADGE_B = R.drawable.d_b;
    public static final int DU_BADGE_C = R.drawable.d_c;

    // 峨眉山
    public static final int SCENE1_ENTRANCE = R.drawable.scene1_entrance;
    public static final int SCENE3_GLAD_TOP = R.drawable.scene3_glod_top;
    public static final int SCENE2_ANCIENT_TEMPLE = R.drawable.scene2_ancient_temple;
    public static final int EMS_BADGE_A = R.drawable.e_a;
    public static final int EMS_BADGE_B = R.drawable.e_b;
    public static final int EMS_BADGE_C = R.drawable.e_c;

    // 九寨沟
    public static final int SCENE1_ENTRANCE_JZG = R.drawable.scene1_entrance_jzg;
    public static final int SCENE2_CUI_HAI = R.drawable.scene2_cui_hai;
    public static final int SCENE3_DIE_PU = R.drawable.scene3_die_pu;
    public static final int JZG_BADGE_A = R.drawable.e_a;
    public static final int JZG_BADGE_B = R.drawable.e_b;
    public static final int JZG_BADGE_C = R.drawable.e_c;

    // 乐山大佛
    public static final int SCENE1_ENTRANCE_LESHAN = R.drawable.scene1_entrance_leshan;
    public static final int SCENE2_HEAD = R.drawable.scene2_head;
    public static final int SCENE3_GIANT_BUDDHA = R.drawable.scene3_giant_buddha;
    public static final int LS_BADGE_A = R.drawable.e_a;
    public static final int LS_BADGE_B = R.drawable.e_b;
    public static final int LS_BADGE_C = R.drawable.e_c;

    // GAME
    public static final int BACKGROUND_BITMAP = R.drawable.bg_day;
    public static final int[] BIRD_BITMAP = new int[]{R.drawable.bird1_0,R.drawable.bird1_1,R.drawable.bird1_2};
    public static final int[] BARRIER_BITMAP = new int[]{R.drawable.pipe_up,R.drawable.pipe_down};
    public static final String STOP = "道士挡住了前方得道路...";

    // 游戏文本
    public static final String LION_LEFT_TEXT = "在宫殿大门外一般都有一对石狮或铜狮，按建筑方位，古代宫殿大门前成对的石狮一般都是左雄右雌（石狮子在大门两口的摆放都是以人从大门里出来的方向为参照的），符合中国传统男左女右的阴阳哲学。";
    public static final String LION_RIGHT_TEXT = "在民间，狮子成为一种勇武、强大和吉祥的化身。表演艺术和造型艺术往往难解难分。狮舞中的狮子形象是民间艺人按自己的审美理想创造的，活泼顽皮可爱，完全失去了“百兽之王”的威风。";
    public static final String ALLS_SCRIPT = "青城山，世界文化遗产青城山-都江堰的主体景区、全国重点文物保护单位、国家重点风景名胜区、国家AAAAA级旅游景区、全真龙门派圣地，十大洞天之一，中国四大道教名山之一，五大仙山之一，成都十景之一。" +
            "青城山位于四川省成都市都江堰市西南，东距成都市区68千米，处于都江堰水利工程西南10千米处。景区面积200平方千米，最高峰老君阁海拔1260米，青城山分为前山和后山，群峰环绕起伏、林木葱茏幽翠，享有“青城天下幽”的美誉.全山林木青翠，四季常青，诸峰环峙，状若城廓，故名青城山。丹梯千级，曲径通幽，以幽洁取胜。景区内外，天师洞和圆明宫幽静是青城山的一大特色。2020年3月30日至4月30日，景区推出“你消费，我免费，这个四月青城山—都江堰任你游”活动，凡是在都江堰市消费的游客，就有机会享受免费游景区.";


    // 都江堰
    public static final String MING_MOUNTAIN_TEXT = "岷山，自中国甘肃省西南部延伸至四川省北部的一褶皱山脉，大致呈南北走向，全长约500公里，主峰雪宝顶位于四川省松潘县境内，海拔5588米。";
    public static final String MING_JIAN_TEXT = "长江上游支流岷江（古代长江正源）﹑涪江﹑嘉陵江上源白龙江和黄河支流白河﹑黑河的分水源地。中国大熊猫主要分布区﹐著名自然风景旅游区。岷山北起甘肃东南岷县南部，南止四川盆地西部峨眉山﹐南北逶迤700多公里﹐有“千里岷山”之说。";
    public static final String YU_ZHUI_TEXT = "鱼嘴位置选择的科学性：在岷江上利用河心州的淤滩修建分水工程，可以将宽阔的岷江河道缩窄，将河道形势由宽浅式转化为窄深式，一方面有利于非汛期沱江口的进水，另一方面，利用凿开的宝瓶口将汛期的洪峰引进成都平原，可以最大限度地削减岷江洪峰对沿途带来的洪水威胁。";
    public static final String FEI_SHA_YANG = "唐代，应发生了较为巨大的地质灾害，山崩地裂，巨浪淘天，沱江进口段两岸的山体崩塌，沱江进水口断面不断缩小而成百丈渠，内江水大时，只靠狭小的百丈渠和内江右岸的平水槽泄水已不能满足内江河段泄水的需要，如不在内江右岸及时修建旁侧溢洪道，在洪期，宝瓶口的进水量势必加大从而对成都平原形成洪水灾害威胁。在此情况下，飞沙堰的修建成了顺理成章的事，唐一官至侍郎的官员主持修建了这一旁侧溢洪道，后人为了纪念他而将该溢洪道称作侍郎堰。";
    public static final String BAO_PING_TEXT = "前面就是都江堰市啦！";
    public static final String BAO_PING_ENTRANCE_TEXT = "宝瓶口，是指起“节制闸”的作用的河口，能自动控制内江进水量，是在湔山（今名灌口山、玉垒山）伸向岷江的长脊上凿开的一个口子。它是人工凿成控制内江进水的咽喉，因它形似瓶口而功能奇特，故名宝瓶口。";
    public static final String DU_JIAN_YAN_TEXT = "都江堰简称“灌”，是一座具有2000多年建城史，因堰而起、因水而兴的城市；在新石器时代就是古蜀先民聚居的地区之一。都江堰市以山、水、林、堰、桥浑然一体，体现城中有水、水在城中.为此有着“拜水都江堰、问道青城山”之美誉";
    public static final String DU_JIAN_YAN_PART2_TEXT = "都江堰市位于四川省中部成都平原西北边缘，地处岷江上游和中游结合部的岷江出山口。东南距四川省会成都市48公里。西、北与阿坝藏族羌族自治州汶川县交界，东与彭州市、郫都区、温江区相连，南与崇州市接壤。";
    public static final String DU_JIAN_YAN_GAME_TEXT = "进入游戏！";
    public static final String DU_TAOIST_LAYER = "都江堰，位于四川省成都市都江堰市城西，坐落在成都平原西部的岷江上，是由渠首枢纽（鱼嘴、飞沙堰、宝瓶口）、灌区各级引水渠道，各类工程建筑物和大中小型水库和塘堰等所构成的一个庞大的工程系统，渠首占地面积200余亩。它担负着四川盆地中西部地区7市40县1130万余亩农田的灌溉、成都市多家重点企业和城市生活供水，以及防洪、发电、漂水、水产、养殖、林果、旅游、环保等多项目标综合服务，是四川省国民经济发展不可替代的水利基础设施。";

    // 九寨沟
    public static final String JIU_ROAD_TEXT = "九寨沟位于四川省西北部岷山山脉南段的阿坝藏族羌族自治州九寨沟县漳扎镇境内，地处岷山南段弓杆岭的东北侧，距离成都市400多千米，系长江水系嘉陵江上游白水江源头的一条大支沟";
    public static final String JIU_ENTRANCE_TEXT = "九寨沟国家级自然保护区内有树正沟、则查洼沟、日则沟、扎如沟四个景点，其中又分为多个大大小小的景点,以高原钙华湖群、钙华瀑群和钙华滩流等水景为主体。";
    public static final String JIU_MOUNTAIN_TEXT= "千百年来，九寨沟隐藏在川西北高原的崇山峻岭中，人类的活动显得微不足道。该区藏民几乎与世隔绝，过着自给自足的农牧生活。由于山高路远，九寨沟一向鲜为人知。";
    public static final String JIU_CUI_HAI_TEXT = "翠海是九寨沟五绝之一，指的是九寨沟的水，清澈见底，翠波荡漾。九寨沟其他四绝指的是彩林、雪峰、叠瀑和藏情。";
    public static final String JIU_DIE_PU_TEXT = "珍珠滩瀑布海拔2433米，均高21米，瀑宽270米。由第四纪冰川遗存的冰碛台地，再经钙化作用，使瀑布的坡坎更加高耸坚固，瀑面后凹呈新月型。瀑布跌落谷底，发出震耳欲聋的水声。此处的负氧离子含量极为丰富，有“天然氧吧”之称。";
    public static final String JIU_TAOIST_LAYER = "九寨沟国家级自然保护区地势南高北低，山谷深切，高差悬殊，区北缘九寨沟口海拔仅2000米，中部峰岭均在4000米以上，南缘达4500米以上，主沟长30多千米。峰顶和两侧山峰基本终年积雪。九寨沟地处青藏高原向四川盆地过渡地带，地质背景复杂，碳酸盐分布广泛，褶皱断裂发育，新构造运动强烈，地壳抬升幅度大，多种营力交错复合，造就了多种多样的地貌，发育了大规模喀斯特作用的钙华沉积，以植物喀斯特钙华沉积为主导，形成九寨沟艳丽典雅的群湖，奔泻湍急的溪流，飞珠溅玉的瀑群，古穆幽深的林莽，连绵起伏的雪峰。";

    // 乐山大佛
    public static final String LE_BAR_TEXT = "这里就是乐山大佛景区的入口，门板赫然写上乐山大佛四大字。";
    public static final String LE_DOOR_TEXT = "可以从这里进入景区，乐山大佛就在这静静存在了上千年。";
    public static final String LE_SIN_TEXT = "“乐山大佛”，就是这里了呀！";
    public static final String LE_HEAD_TEXT = "乐山大佛，又名凌云大佛，位于四川省乐山市南岷江东岸凌云寺侧，濒大渡河、青衣江和岷江三江汇流处。大佛为弥勒佛坐像，通高71米，是中国最大的一尊摩崖石刻造像。";
    public static final String LE_BODY_TEXT = "乐山大佛在漫长岁月中，仍免不了遭到各种各样的破坏，有自然的，也有人为的。各个朝代都对它进行过维修。自明、清以来的数百年间，大佛饱受自然风雨侵蚀，以致佛身千疮百孔，面目全非。";
    public static final String LE_GOING_TEXT = "前方的路被挡住了....";
    public static final String LE_BUDDHA_TEXT = "大佛通高71米，头高14.7米，头宽10米，发髻1051个 [9]  ，耳长7米，鼻长5.6米，眉长5.6米，嘴巴和眼长3.3米，颈高3米，肩宽24米，手指长8.3米，从膝盖到脚背28米，脚背宽8.5米，脚面可围坐百人以上。";
    public static final String LE_STONE_tEXT = "大佛左侧，沿“洞天”下去就是近代开凿的凌云栈道的始端，全长近500米。右侧是唐代开凿大佛时留下的施工和礼佛通道——九曲栈道。";
    public static final String LE_STONE_RIGHT_TEXT = "在大佛左右两侧沿江崖壁上，还有两尊身高超过16米的护法天王石刻，与大佛一起形成了一佛二天王的格局。与天王共存的还有数百龛上千尊石刻造像，宛然汇集成庞大的佛教石刻艺术群。";
    public static final String LE_SPACE_TEXT = "想要在大佛顶上的天空遨游一番吗？";
    public static final String LE_TAOIST_LAYER = "建造于唐代的这座大佛，真实的官方名称却一直是谜。事实上，根据后来诸多专家考察证实，这座被称为“乐山大佛”的石刻雕像的真实官方名称应该是：嘉州凌云寺大弥勒石像。乐山大佛头与山齐，足踏大江，双手抚膝，大佛体态匀称，神势肃穆，依山凿成临江危坐。大佛通高71米，头高14.7米，头宽10米，发髻1051个，耳长7米，鼻长5.6米，眉长5.6米，嘴巴和眼长3.3米，颈高3米，肩宽24米，手指长8.3米，从膝盖到脚背28米，脚背宽8.5米，脚面可围坐百人以上。\n" +
            "在大佛左右两侧沿江崖壁上，还有两尊身高超过16米的护法天王石刻，与大佛一起形成了一佛二天王的格局。与天王共存的还有数百龛上千尊石刻造像，宛然汇集成庞大的佛教石刻艺术群。大佛左侧，沿“洞天”下去就是近代开凿的凌云栈道的始端，全长近500米。右侧是唐代开凿大佛时留下的施工和礼佛通道——九曲栈道。佛像雕刻成之后，曾建有七层楼阁覆盖（一说九层或十三层），时称“大佛阁”、“大像阁”；佛阁屡建屡毁，宋时重建“凌云阁”、“天宁阁”；元代建“宝鸿阁”；明代崇祯年间建“佛棚”、清代建“佛亭”，最终废毁殆尽。";

    //峨眉山
    public static final String EMS_ENTRANCE_TOP_TEXT = "入口大门类似中国古代宫殿式建筑，飞檐走壁，两端对称";
    public static final String EMS_ENTRANCE_BOTTOM_TEXT = "这里就是峨眉山的入口，现代化的玻璃门和古色古香的景区形成了鲜明对比。";
    public static final String EMS_ENTRANCE_PLAQUE_TEXT = "匾牌上写着“震业第一山！”";
    public static final String EMS_ENTRANCE_PLAQUE_TEXT2 = "峨眉山，位于北纬30度，四川盆地西南，因两山相峙，形如蛾眉而得名。其三峰耸立，绝壁临天，古往今来，3077米的金顶一直被人们称为天堂的阶梯。";
    public static final String EMS_ENTRANCE_PLAQUE_TEXT3 = "招牌上”游客中心“直接把我们拉回现代...";
    public static final String EMS_WAN_PLAQUE_TEXT = "匾牌上写着”万年寺“，这座寺庙莫非已经存在万年了";
    public static final String EMS_WAN_PLAQUE_TEXT2 = "匾牌上写着”火光明山“，有种正大光明的意味";
    public static final String EMS_WAN_ENTRANCE_TEXT = "前方的路被阻挡住了....";
    public static final String EMS_WAN_ROAD_TEXT = "呼呼呼~~~爬了好久，好累啊";
    public static final String EMS_TAOIST_LAYER = "北纬30度，地球上最神秘的地带，有着无数的不解之迷。而以“天府之国”著称的中国四川盆地，是世界上处于北纬30度古文明发祥地没有被沙漠化的地区。峨眉山，位于北纬30度，四川盆地西南，因两山相峙，形如蛾眉而得名。其三峰耸立，绝壁临天，古往今来，3077米的金顶一直被人们称为天堂的阶梯。\n" +
            "峨眉山地处长江上游，屹立于大渡河与青衣江之间，沉积着滚滚长江孕育的巴蜀文化，绵延千年是长江上游的自然和文化双遗产之地。在千百万年的沧海桑田、斗转星移中，峨眉山采撷天地之灵气，造就了集1000多种药用植物，3000多种高等植物，2300种动物于一体的人间乐园，修行者们修行的理想之地。早在5000年前，华夏之祖轩辕黄帝就两次来峨眉山问道，1000多年前，天真皇人论道峨眉山，这是道教在峨眉山之滥觞。而1900年前，一位修行者在峨眉山修建了长江流域的第一座禅院后，峨眉山便成为了长江流域的佛教发源地，列入中国佛教四大名山。峨眉山的血脉里仍然流淌着道之源、佛之始、儒之境三教深厚文化之遗韵，优雅而从容的体现着生命的哲理和生活的智慧。";

    // 游戏提示板
    public static final String GAME_WIN_MESSAGE_TEXT = "游戏通过的要求是%d,你已达到改分数，恭喜通过！";
    public static final String GAME_START_BUTTON_TEXT= "开始游戏";
    public static final String GAME_DETAIL_BUTTON_TEXT = "游戏目标";
    public static final String GAME_DETAIL_TEXT= "收集小精灵，不能点击炸弹。收集小精灵到100分即可通关！";

    public static final int DIALOG = R.drawable.information_bgd;
    public static final int OPTION = R.drawable.infomation_option;
    public static final String GAME_WIN_TIP_TEXT = "恭喜你通过%s所有的内容，点击右上角箭头即可回到导航界面！";
}
