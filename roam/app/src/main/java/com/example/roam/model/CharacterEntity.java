package com.example.roam.model;

/**
 * Illustration
 *
 * @author dengqing
 * @time 2021/10/26
 * @function 虚拟人物形象实体类
 */

public class CharacterEntity {
    private int id;//虚拟人物形象编号
    private String gender;//虚拟人物形象的性别[man,woman]
    private String nature;//性格标签[warm,cool,cute]
    private String instruction;//虚拟人物描述
    private int img;//虚拟人物图片

    public CharacterEntity(int id, String gender, String nature, String instruction, int img) {
        this.id = id;
        this.gender = gender;
        this.nature = nature;
        this.instruction = instruction;
        this.img = img;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    @Override
    public String toString() {
        return "CharacterEntity{" +
                "id=" + id +
                ", gender='" + gender + '\'' +
                ", nature='" + nature + '\'' +
                ", instruction='" + instruction + '\'' +
                ", img=" + img +
                '}';
    }
}