package com.example.roam.game.util;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.emeimountain.game.model.Spirit;
import com.example.roam.game.tools.BarLayer;

public class WinBarLayer extends Spirit {
    private BarLayer barLayer;
    public WinBarLayer(Context context,String address) {
        super(context,145,520,930,1240);
        barLayer = new BarLayer(context);
        barLayer.setText(String.format(GameConstant.GAME_WIN_TIP_TEXT, new Object[]{address}));
        barLayer.isVisible = true;
    }

    @Override
    public void draw(Canvas canvas) {
        barLayer.draw(canvas);
    }

    public BarLayer getBarLayer() {
        return barLayer;
    }
}
