package com.example.roam.game.emeimountain.model;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class EmsEntrancePlaque3Spirit extends CSpirit {
    public EmsEntrancePlaque3Spirit(Context context) {
        super(context, 231, 1107,856 ,1277 );
        this.text = GameConstant.EMS_ENTRANCE_PLAQUE_TEXT3;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
