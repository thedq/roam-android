package com.example.roam.game.tools;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.BitmapUtil;
import com.example.roam.game.util.GameConstant;

public class LeftSpirit extends Spirit {
    public LeftSpirit(Context context) {
        super(context, 50, 50, 120, 120);
        this.bitmap = BitmapUtil.changeBitmapSize(context, GameConstant.LEFT,width,height);
    }
    @Override
    public void draw(Canvas canvas) {
        canvas.drawBitmap(bitmap,null,rect,null);
    }

    public void finsh(Context context){
        ((Activity)context).finish();
    }

}
