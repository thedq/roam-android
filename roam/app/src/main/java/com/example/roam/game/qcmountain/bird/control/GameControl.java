package com.example.roam.game.qcmountain.bird.control;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;

import com.example.roam.R;
import com.example.roam.game.app.qcmountain.GameThirdActivity;
import com.example.roam.game.emeimountain.game.model.fin.GameInfoLayer;
import com.example.roam.game.qcmountain.bird.model.BackGround;
import com.example.roam.game.qcmountain.bird.model.BarrierLayer;
import com.example.roam.game.qcmountain.bird.model.Bird;
import com.example.roam.game.tools.CatchBadgeSpirit;
import com.example.roam.game.util.BeginSpirit;
import com.example.roam.game.util.FinSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.State;
import com.example.roam.utils.BadgeUtils;
import com.google.gson.internal.bind.util.ISO8601Utils;


public class GameControl {
    private Context context;
    private BackGround backGround;
    private Bird bird;
    private BarrierLayer barrierLayer;
    public boolean isLife = true;
    private Paint paint;
    private GameInfoLayer gameInfoLayer;
    public State currentState = State.NONE;
    private FinSpirit finSpirit;
    private BeginSpirit beginSpirit;
    private CatchBadgeSpirit catchBadgeSpirit;
    private boolean isAhead = false;
    private boolean isOver = false;

    public GameControl(Context context){
        this.context = context;
        this.backGround = new BackGround(context);
        this.bird = new Bird(context);
        this.bird.setLeft(200);
        this.bird.setTop(500);
        this.barrierLayer = new BarrierLayer(context);
        this.paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setTextSize(100);
        paint.setColor(Color.RED);
        gameInfoLayer = new GameInfoLayer(context);
        gameInfoLayer.setVisible(true);
        beginSpirit = new BeginSpirit(context,GameConstant.GAME_FLY_BIRD);
        beginSpirit.setGameStartBitmap(R.drawable.catch_spirit);
        finSpirit = new FinSpirit(context,300);
        catchBadgeSpirit = new CatchBadgeSpirit(context, GameConstant.Qing_BADGE_C);
    }

    public void draw(Canvas canvas){

        if( currentState == State.START ) {

        }

        if(!beginSpirit.isVisible && currentState != State.NONE){
            backGround.draw(canvas);
            bird.draw(canvas);
            barrierLayer.draw(canvas, bird);
            gameInfoLayer.draw(canvas);
            gameInfoLayer.getTimeNumBarSpirit().setNowTime(System.currentTimeMillis());
            gameInfoLayer.setScore(barrierLayer.getScore()/2);
            isLose();
            isWin();
        }

        beginSpirit.draw(canvas);
        finSpirit.draw(canvas);
        catchBadgeSpirit.draw(canvas);
    }

    public void onTouch(MotionEvent motionEvent){
        switch ( motionEvent.getAction() ){
            case MotionEvent.ACTION_DOWN:
                clickBirdLogic(motionEvent);
                clickStartGame(motionEvent);
                clickDetail(motionEvent);
                clickDetailDelete(motionEvent);
                clickOptionAgain(motionEvent);
                clickOptionAhead(motionEvent);
                clickOptionReturn(motionEvent);
                break;
        }
    }

    private void isLose(){
        if(currentState == State.START && !bird.isLife){
            currentState = State.LOSE;
            finSpirit.lose("10",barrierLayer.getScore()+"",gameInfoLayer.getTimeNumBarSpirit().getTimeStr());
            gameInfoLayer.pauseTime();
        }
    }

    private void isWin(){
        if( currentState == State.START && bird.isLife && barrierLayer.getScore()/2 >= 10 && !isAhead){
            currentState = State.WIN;
            barrierLayer.setPauseGame();
            bird.setPauseGame();
            finSpirit.win("10",barrierLayer.getScore()+"",gameInfoLayer.getTimeNumBarSpirit().getTimeStr());
            gameInfoLayer.pauseTime();
        }
    }

    private void clickBirdLogic(MotionEvent event) {
        if (bird.isLife) {
            bird.setState(0);
            bird.setSpeed(50); // 多次点击多次给力：还原50
        }
    }

    private void clickStartGame(MotionEvent event){  // 点击开始游戏，改变当前状态并且不可见。 设置时间
        if (beginSpirit.isVisible && beginSpirit.getStartRect().contains((int)event.getX(),(int)event.getY())) {
            currentState = State.START;
            beginSpirit.isVisible = false;
            gameInfoLayer.getTimeNumBarSpirit().setBeginTime(System.currentTimeMillis());
            gameInfoLayer.getTimeNumBarSpirit().setNowTime(System.currentTimeMillis());
            gameInfoLayer.getTimeNumBarSpirit().setCurrentState(currentState);
        }
    }

    private void clickDetail(MotionEvent event){
        beginSpirit.clickDetail(event);
    }

    private void clickDetailDelete(MotionEvent event){
        beginSpirit.clickDetailDelete(event);
    }

    private void clickOptionAgain(MotionEvent event){
        if (finSpirit.isVisible && currentState == State.LOSE && finSpirit.getOptionAgainSpirit().getRect().contains((int) event.getX(),(int) event.getY())) {
            currentState = State.START;
            finSpirit.isVisible = false;
            bird.isLife = true;
            this.bird.setLeft(200);
            this.bird.setTop(500);
            barrierLayer.tryAgain();
            barrierLayer.setScore(0);
            gameInfoLayer.resetTime();
            isAhead = false;
        }
    }

    private void clickOptionAhead(MotionEvent event){
        if (finSpirit.isVisible && currentState == State.WIN && finSpirit.getOptionAheadSpirit().getRect().contains((int) event.getX() , (int) event.getY())) {
            finSpirit.winAndGoAhead();
            barrierLayer.setResumeGame();
            bird.setResumeGame();
            currentState = State.START;
            isAhead = true;
            gameInfoLayer.resumeTime();
        }
    }

    private void clickOptionReturn(MotionEvent event){
        if( finSpirit.isVisible && finSpirit.getOptionReturnSpirit().getRect().contains((int) event.getX(),(int) event.getY()) ){
            waitToStartActivity();
        }
    }

    public void waitToStartActivity(){  // 1. 设置动画，结束，跳转。
        Intent intent = new Intent(context, GameThirdActivity.class);
        if ( barrierLayer.getScore()/2 >= 10 && !BadgeUtils.getBadges().getBadgeArray().contains(2) ) {
            catchBadgeSpirit.isVisible = true;
            catchBadgeSpirit.setStartAnimation(true); // 启动动画
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            intent.putExtra("score",barrierLayer.getScore()/2);
                            isOver = true;
                            context.startActivity(intent);
                        }
                    });
                }
            }).start();
        }else {
            intent.putExtra("score",barrierLayer.getScore()/2);
            isOver = true;
            context.startActivity(intent);
        }
        ((Activity)context).finish();
    }

    public boolean isOver() {
        return isOver;
    }

    public void setOver(boolean over) {
        isOver = over;
    }
}
