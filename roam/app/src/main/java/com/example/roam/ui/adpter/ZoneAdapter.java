package com.example.roam.ui.adpter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.roam.R;
import com.example.roam.api.Api;
import com.example.roam.model.gson.Forum;
import com.example.roam.ui.activity.WebActivity;
import com.example.roam.utils.HttpUtil;
import com.google.android.material.imageview.ShapeableImageView;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ZoneAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Forum.AllInfo> dataArray;
    public ZoneAdapter(Context context){
        this.context = context;
        dataArray = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return dataArray.size();
    }

    @Override
    public Object getItem(int position) {
        return dataArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setData(List<Forum.AllInfo> dataArray){
        this.dataArray.clear();
        this.dataArray.addAll(dataArray);
        notifyDataSetChanged();
    }

    public void setMoreData(List<Forum.AllInfo> dataArray){
        this.dataArray.addAll(dataArray);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Forum.AllInfo allInfo = dataArray.get(position);
        ViewHolder viewHolder = null;
        if( convertView == null ) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_zone,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.img_c1 = convertView.findViewById(R.id.img_c1);
            viewHolder.img_c2 = convertView.findViewById(R.id.img_c2);
            viewHolder.img_c3 = convertView.findViewById(R.id.img_c3);
            viewHolder.txt_title = convertView.findViewById(R.id.txt_title);
            viewHolder.rb_agree = convertView.findViewById(R.id.rb_agree);
            viewHolder.img_c1.setVisibility(View.GONE);
            viewHolder.img_c2.setVisibility(View.GONE);
            viewHolder.img_c3.setVisibility(View.GONE);
            imgLogic(allInfo, viewHolder);
            viewHolder.txt_title.setText(allInfo.getScenicReleaseInfo().getScenicReleaseTitle());
            viewHolder.rb_agree.setText(allInfo.getScenicReleaseInfo().getScenicReleaseGood()+"");
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
            viewHolder.img_c1.setVisibility(View.GONE);
            viewHolder.img_c2.setVisibility(View.GONE);
            viewHolder.img_c3.setVisibility(View.GONE);
            imgLogic(allInfo, viewHolder);
            viewHolder.txt_title.setText(allInfo.getScenicReleaseInfo().getScenicReleaseTitle());
            viewHolder.rb_agree.setText(allInfo.getScenicReleaseInfo().getScenicReleaseGood()+"");
            convertView.setTag(viewHolder);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initForumHtmlNet(allInfo.getScenicReleaseInfo().getScenicReleaseId());
            }
        });

        viewHolder.rb_agree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Retrofit retrofit = new Retrofit.Builder().baseUrl(HttpUtil.ADDRESS).build();
                retrofit.create(Api.class).giveOrDeleGood(allInfo.getScenicReleaseInfo().getScenicReleaseId(),HttpUtil.COOKIE).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Toast.makeText(context,"点赞成功！",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }
        });

        return convertView;
    }

    class ViewHolder{
        ShapeableImageView img_c1,img_c2,img_c3;
        TextView txt_title;
        RadioButton rb_agree;
    }

    private void imgLogic(Forum.AllInfo allInfo ,ViewHolder viewHolder){
        if( allInfo.getPictures().size() > 0){
            viewHolder.img_c1.setVisibility(View.VISIBLE);
            Glide.with(context).load(allInfo.getPictures().get(0)).into(viewHolder.img_c1);
        }
        if( allInfo.getPictures().size() > 1){
            viewHolder.img_c2.setVisibility(View.VISIBLE);
            Glide.with(context).load(allInfo.getPictures().get(1)).into(viewHolder.img_c2);
        }
        if( allInfo.getPictures().size() > 2){
            viewHolder.img_c3.setVisibility(View.VISIBLE);
            Glide.with(context).load(allInfo.getPictures().get(2)).into(viewHolder.img_c3);
        }
    }


    public void initForumHtmlNet(int srid){
        Intent intent = new Intent(context, WebActivity.class);
        intent.putExtra("url", HttpUtil.ADDRESS+"getView/getScenicReleaseView/"+srid);
        context.startActivity(intent);
    }
}
