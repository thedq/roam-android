package com.example.roam.model.gson;

public class CacheWithDuration {

    private String value;
    private long duration;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "CacheWithDuration{" +
                "value='" + value + '\'' +
                ", duration=" + duration +
                '}';
    }
}
