package com.example.roam.game.qcmountain.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.example.roam.R;
import com.example.roam.game.util.BitmapUtil;
import com.example.roam.game.util.GameConstant;

public class CSpirit extends Spirit{
    private Bitmap location;
    private int locLeft,locRight,locTop,locBottom;
    public CSpirit(Context context, int left, int top, int right, int bottom) {
        super(context, left, top, right, bottom);
        location = BitmapUtil.changeBitmapSize(context, R.drawable.tip2,150,150);
        locLeft = rect.left + (width / 2) - 55;
        locRight = locLeft + 75;
        locTop = rect.top + (height / 2) - 55;
        locBottom = locTop + 75;
    }

    @Override
    public void draw(Canvas canvas) {
        if( isVisible ){
            canvas.drawBitmap(location,null,new Rect(locLeft,locTop,locRight,locBottom),null);
        }
    }
}
