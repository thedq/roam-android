package com.example.roam.ui.adpter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.roam.R;
import com.example.roam.api.Api;
import com.example.roam.model.gson.Forum;
import com.example.roam.ui.activity.WebActivity;
import com.example.roam.utils.Constans;
import com.example.roam.utils.HttpUtil;
import com.google.android.material.imageview.ShapeableImageView;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ForumNormalAdapter extends BaseAdapter {

    private List<Forum.AllInfo> dataArray ;
    private Context context;
    private final Drawable agreeFill;
    private final Drawable agree;

    public ForumNormalAdapter(Context context){
        this.context = context;
        this.dataArray = new ArrayList<>();
        agreeFill = context.getResources().getDrawable(R.drawable.agree_fill);
        agreeFill.setBounds(0, 0, agreeFill.getMinimumWidth(), agreeFill.getMinimumHeight());
        agree = context.getResources().getDrawable(R.drawable.agree);
        agree.setBounds(0, 0, agree.getMinimumWidth(), agree.getMinimumHeight());

    }

    @Override
    public int getCount() {
        return dataArray.size();
    }

    @Override
    public Object getItem(int position) {
        return dataArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setDataArray(List<Forum.AllInfo> dataArray){
        this.dataArray.clear();
        if( dataArray != null ) {
            this.dataArray.addAll(dataArray);
        }
        notifyDataSetChanged();
    }

    public void setMoreDataArray(List<Forum.AllInfo> dataArray) {
        this.dataArray.addAll(dataArray);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        Forum.AllInfo allInfo = dataArray.get(position);
        Forum.ScenicReleaseInfo scenicReleaseInfo = allInfo.getScenicReleaseInfo();
        if( convertView == null ){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_fragment_forum_normal, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.txt_title = convertView.findViewById(R.id.txt_title);
            viewHolder.txt_t1 = convertView.findViewById(R.id.txt_t1);
            viewHolder.txt_commentNum = convertView.findViewById(R.id.txt_commentNum);
            viewHolder.rb_agree = convertView.findViewById(R.id.rb_agree);
            if( allInfo.isGood() ){
                viewHolder.rb_agree.setCompoundDrawables(agreeFill, null, null, null);
            }else {
                viewHolder.rb_agree.setCompoundDrawables(agree, null, null, null);
            }
            viewHolder.img_c1 = convertView.findViewById(R.id.img_c1);
            viewHolder.img_c2 = convertView.findViewById(R.id.img_c2);
            viewHolder.img_c3 = convertView.findViewById(R.id.img_c3);
            viewHolder.imgHead = convertView.findViewById(R.id.img_head);
            viewHolder.txt_user_name = convertView.findViewById(R.id.txt_user_name);
            viewHolder.txt_title.setText(scenicReleaseInfo.getScenicReleaseTitle());
            viewHolder.txt_user_name.setText(allInfo.getUserName());
            viewHolder.rb_agree.setText(allInfo.getScenicReleaseInfo().getScenicReleaseGood()+"");
            Glide.with(context).load(allInfo.getHeadUrl()).into(viewHolder.imgHead);
            pictureLogic(allInfo.getPictures(),viewHolder);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
            viewHolder.txt_title.setText(scenicReleaseInfo.getScenicReleaseTitle() );
            viewHolder.txt_user_name.setText(allInfo.getUserName());
            Glide.with(context).load(allInfo.getHeadUrl()).into(viewHolder.imgHead);
            viewHolder.rb_agree.setText(allInfo.getScenicReleaseInfo().getScenicReleaseGood()+"");
            if( allInfo.isGood() ){
                viewHolder.rb_agree.setCompoundDrawables(agreeFill, null, null, null);
            }else {
                viewHolder.rb_agree.setCompoundDrawables(agree, null, null, null);
            }
            pictureLogic(allInfo.getPictures(),viewHolder);
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int scenicReleaseId = dataArray.get(position).getScenicReleaseInfo().getScenicReleaseId();
                initForumHtmlNet(scenicReleaseId);

            }
        });

        ViewHolder finalViewHolder = viewHolder;
        viewHolder.rb_agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Retrofit retrofit = new Retrofit.Builder().baseUrl(HttpUtil.ADDRESS).build();
                retrofit.create(Api.class).giveOrDeleGood(allInfo.getScenicReleaseInfo().getScenicReleaseId(),HttpUtil.COOKIE).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if( response.isSuccessful() ){
                            allInfo.setGood(!allInfo.isGood());
                            // 按照data.isGood相反设置
                            if (allInfo.isGood()) {
                                finalViewHolder.rb_agree.setCompoundDrawables(agreeFill,null,null,null);
                                finalViewHolder.rb_agree.setText(Integer.parseInt(finalViewHolder.rb_agree.getText().toString()) +1 +"" );
                                Toast.makeText(context,"点赞成功",Toast.LENGTH_SHORT).show();
                            }else {
                                finalViewHolder.rb_agree.setCompoundDrawables(agree,null,null,null);
                                finalViewHolder.rb_agree.setText(Integer.parseInt(finalViewHolder.rb_agree.getText().toString()) -1 +"" );
                                Toast.makeText(context,"取消点赞",Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }
        });
        return convertView ;
    }

    public void pictureLogic(List<String> pictures, ViewHolder viewHolder){
        // 复用的锅
        if (pictures.size() > 0) {
            viewHolder.img_c1.setVisibility(View.VISIBLE);
            Glide.with(context).load(pictures.get(0)).into(viewHolder.img_c1);
        }
        if (pictures.size() > 1) {
            viewHolder.img_c2.setVisibility(View.VISIBLE);
            Glide.with(context).load(pictures.get(1)).into(viewHolder.img_c2);
        }
        if (pictures.size() > 2) {
            viewHolder.img_c3.setVisibility(View.VISIBLE);
            Glide.with(context).load(pictures.get(2)).into(viewHolder.img_c3);
        }

        if(pictures.size() == 0){
            viewHolder.img_c1.setVisibility(View.GONE);
            viewHolder.img_c2.setVisibility(View.GONE);
            viewHolder.img_c3.setVisibility(View.GONE);
        }

        if( pictures.size() == 1){
            viewHolder.img_c2.setVisibility(View.GONE);
            viewHolder.img_c3.setVisibility(View.GONE);
        }

        if( pictures.size() == 2){
            viewHolder.img_c3.setVisibility(View.GONE);
        }
    }

    class ViewHolder{
        TextView txt_title,txt_t1,txt_goodNum,txt_commentNum,txt_user_name;
        ShapeableImageView img_c1,img_c2,img_c3;
        ShapeableImageView imgHead;
        RadioButton rb_agree;
    }

    public void initForumHtmlNet(int srid){
        Intent intent = new Intent(context, WebActivity.class);
        intent.putExtra("url", HttpUtil.ADDRESS+"getView/getScenicReleaseView/"+srid);
        context.startActivity(intent);
    }

    public List<Forum.AllInfo> getDataArray() {
        return dataArray;
    }
}
