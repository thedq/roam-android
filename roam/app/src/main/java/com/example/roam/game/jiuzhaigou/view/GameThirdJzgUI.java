package com.example.roam.game.jiuzhaigou.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import com.example.roam.game.jiuzhaigou.control.GameSecondJzgControl;
import com.example.roam.game.jiuzhaigou.control.GameThirdJzgControl;

public class GameThirdJzgUI extends SurfaceView implements SurfaceHolder.Callback {

    private GameThirdJzgControl gameThirdJzgControl;
    private SurfaceHolder surfaceHolder;

    public GameThirdJzgUI(Context context) {
        super(context);
        initGame();
    }

    public GameThirdJzgUI(Context context, AttributeSet attrs) {
        super(context, attrs);
        initGame();
    }

    public GameThirdJzgUI(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initGame();
    }

    public void initGame(){
        surfaceHolder = this.getHolder();
        surfaceHolder.addCallback(this);
        gameThirdJzgControl = new GameThirdJzgControl(getContext());
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        Runner runner = new Runner();
        runner.start();
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {

    }

    class Runner extends Thread{
        @Override
        public void run() {

            while (gameThirdJzgControl.isVisible){
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                draw();
            }
        }
    }

    public void draw(){
        Canvas canvas = surfaceHolder.lockCanvas();
        gameThirdJzgControl.draw(canvas);
        surfaceHolder.unlockCanvasAndPost(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gameThirdJzgControl.onTouch(event);
        return true;
    }

    public GameThirdJzgControl getGameThirdJzgControl() {
        return gameThirdJzgControl;
    }
}
