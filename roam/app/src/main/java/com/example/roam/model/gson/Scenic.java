package com.example.roam.model.gson;

import com.example.roam.model.ScenicEntity;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Create By Visen
 * Date: 2021/11/3
 */
public class Scenic implements Serializable {
    private boolean success;
    private int code;
    private String message;
    private Data data;

    public Scenic() {
    }

    public Scenic(boolean success, int code, String message, Data data) {
        this.success = success;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Scenic{" +
                "success=" + success +
                ", code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public class Data implements Serializable{
        @SerializedName("findOneScenicBySid")
        private ScenicEntity scenicEntity;

        public Data() {
        }

        public Data(ScenicEntity scenicEntity) {
            this.scenicEntity = scenicEntity;
        }

        public ScenicEntity getScenicEntity() {
            return scenicEntity;
        }

        public void setScenicEntity(ScenicEntity scenicEntity) {
            this.scenicEntity = scenicEntity;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "scenicEntity=" + scenicEntity +
                    '}';
        }
    }
}
