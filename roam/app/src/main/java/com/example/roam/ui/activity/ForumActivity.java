package com.example.roam.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.roam.R;
import com.example.roam.ui.adpter.ForumFragmentPaperAdapter;
import com.example.roam.ui.fragment.NormalFragment;
import com.example.roam.utils.StatusBarUtils;

public class ForumActivity extends AppCompatActivity {
    private EditText edt_search;
    private RadioGroup rg_choice;
    private RadioButton rb_office , rb_normal;
    private ViewPager vp_forum;
    private ForumFragmentPaperAdapter forumFragmentPaperAdapter;
    private ImageView left,edt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        ini();
        iniData();
        iniEvent();
    }

    public void ini(){
        edt_search = this.findViewById(R.id.edt_search);
        rg_choice = this.findViewById(R.id.rg_choice);
        rb_office = this.findViewById(R.id.rb_office);
        rb_normal = this.findViewById(R.id.rb_normal);
        vp_forum = this.findViewById(R.id.vp_forum);
        forumFragmentPaperAdapter = new ForumFragmentPaperAdapter(getSupportFragmentManager());
        left = this.findViewById(R.id.left);
        edt = this.findViewById(R.id.edt);
    }

    public void iniData(){
        vp_forum.setAdapter(forumFragmentPaperAdapter);
    }

    public void iniEvent(){
        MyListener myListener = new MyListener();
        rg_choice.setOnCheckedChangeListener(myListener);
        vp_forum.setOnPageChangeListener(myListener);
        edt_search.setOnClickListener(myListener);
        left.setOnClickListener(myListener);
        edt.setOnClickListener(myListener);
    }

    class MyListener implements View.OnClickListener , RadioGroup.OnCheckedChangeListener , ViewPager.OnPageChangeListener {

        @Override
        public void onClick(View v) {
            switch ( v.getId() ){
                case R.id.edt_search:
                    Intent intent = new Intent(ForumActivity.this,SearchActivity.class);
                    startActivity(intent);
                    break;
                case R.id.left:
                    Intent intent1 = new Intent(ForumActivity.this,MainActivity.class);
                    startActivity(intent1);
                    ForumActivity.this.finish();
                    break;
                case R.id.edt:
                    Intent intent2 = new Intent(ForumActivity.this, UploadDiaryActivity.class);
                    startActivity(intent2);
                    break;
            }
        }

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch ( checkedId ){
                case R.id.rb_office:
                    rb_office.setTextColor(Color.parseColor("#ffffff"));
                    rb_normal.setTextColor(Color.parseColor("#44605F"));
                    vp_forum.setCurrentItem(0);
                    break;
                case R.id.rb_normal:
                    rb_normal.setTextColor(Color.parseColor("#ffffff"));
                    rb_office.setTextColor(Color.parseColor("#44605F"));
                    vp_forum.setCurrentItem(1);
                    break;

            }
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            switch ( position ){
                case 0:
                    rb_office.setChecked(true);
                    break;
                case 1:
                    rb_normal.setChecked(true);
                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

}