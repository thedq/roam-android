package com.example.roam.ui.adpter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.roam.R;
import com.example.roam.utils.Constans;
import com.example.roam.view.RoundHeadImage;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Create By Visen
 * Date: 2021/10/15
 */
public class UploadZoneAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<HashMap<String,String>> dataArray;
    public MyMethod myMethod;
    private static final String TAG = "UploadZoneAdapter";

    public UploadZoneAdapter(Context context , ArrayList<HashMap<String,String>> dataArray){
        this.context = context;
        this.dataArray = dataArray;
    }

    @Override
    public int getCount() {
        return dataArray.size();
    }

    @Override
    public Object getItem(int position) {
        return dataArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HashMap<String,String> data = dataArray.get(position);
        ViewHodler viewHodler = null;
        MyClickListener myClickListener = new MyClickListener(Integer.parseInt(data.get("z_id")));
        if ( convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.item_zone,parent,false);
            viewHodler = new ViewHodler();
            viewHodler.img_head = convertView.findViewById(R.id.img_head);
            viewHodler.txt_name = convertView.findViewById(R.id.txt_name);
            viewHodler.txt_introduce = convertView.findViewById(R.id.txt_introduce);
            viewHodler.img_comment = convertView.findViewById(R.id.img_comment);
            viewHodler.img_agree = convertView.findViewById(R.id.img_agree);
            viewHodler.txt_agree = convertView.findViewById(R.id.txt_agree);
            viewHodler.img_drop = convertView.findViewById(R.id.img_drop);
            viewHodler.img_t1 = convertView.findViewById(R.id.img_t1);
            viewHodler.img_t2 = convertView.findViewById(R.id.img_t2);
            viewHodler.img_t1.setImageBitmap(BitmapFactory.decodeFile(data.get("img_t1")));
            viewHodler.img_t2.setImageBitmap(BitmapFactory.decodeFile(data.get("img_t2")));
            viewHodler.txt_introduce.setText(data.get("txt_introduce"));
            viewHodler.txt_name.setText(Constans.u_username);
            viewHodler.img_head.setImageResource(Integer.parseInt(Constans.u_head));
            viewHodler.txt_agree.setText(data.get("space_good"));
            viewHodler.img_drop.setOnClickListener(myClickListener);
            viewHodler.img_head.setOnClickListener(myClickListener);
            viewHodler.img_agree.setOnClickListener(myClickListener);
            convertView.setTag(viewHodler);
        }else {
            viewHodler = (ViewHodler)convertView.getTag();
            viewHodler.img_t1.setImageBitmap(BitmapFactory.decodeFile(data.get("img_t1")));
            viewHodler.img_t2.setImageBitmap(BitmapFactory.decodeFile(data.get("img_t2")));
            viewHodler.txt_introduce.setText(data.get("txt_introduce"));
            viewHodler.txt_name.setText(Constans.u_username);
            viewHodler.img_head.setImageResource(Integer.parseInt(Constans.u_head));
            viewHodler.txt_agree.setText(data.get("space_good"));
            viewHodler.img_drop.setOnClickListener(myClickListener);
            viewHodler.img_head.setOnClickListener(myClickListener);
            viewHodler.img_agree.setOnClickListener(myClickListener);
        }
        return convertView;
    }

    class ViewHodler{
        RoundHeadImage img_head;
        TextView txt_name;
        TextView  txt_introduce;
        ImageView img_comment;
        ImageView img_agree;
        TextView  txt_agree;
        ImageView img_drop;
        ImageView img_t1;
        ImageView img_t2;
    }

    // 回调：将功能定义和功能实现分开的手段
    public interface MyMethod {
        void mOnClick(View v,int position);
    }

    public void setMyMethod(MyMethod myMethod){
        this.myMethod = myMethod;
    }

    class MyClickListener implements View.OnClickListener{
        private int z_id;

        public MyClickListener() {
        }

        public MyClickListener(int z_id) {
            this.z_id = z_id;
        }

        @Override
        public void onClick(View v) {
            myMethod.mOnClick(v,z_id);
        }
    }


}
