package com.example.roam.game.jiuzhaigou.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class JiuCuiMountainSpirit extends CSpirit {

    public JiuCuiMountainSpirit(Context context) {
        super(context, 0, 553, 1080,850 );
        this.text = GameConstant.STOP;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
