package com.example.roam.ui.adpter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.roam.R;

import java.util.ArrayList;

public class TagEditAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> dataArray = new ArrayList<>();
    private int type = 0;

    public TagEditAdapter(Context context,int type){
        this.context = context;
        this.type = type;
    }

    @Override
    public int getCount() {
        return dataArray.size();
    }

    @Override
    public Object getItem(int position) {
        return dataArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder();
        String strTag = dataArray.get(position);
        if( convertView == null ) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_tag_choose, null, false);
            viewHolder.tag = convertView.findViewById(R.id.txt_tag);
            viewHolder.tag_delete = convertView.findViewById(R.id.tag_delete);
            viewHolder.tag.setText("#"+strTag);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
            viewHolder.tag.setText("#"+strTag);
        }
        if( type == 1 ){
            viewHolder.tag_delete.setVisibility(View.GONE);
        }

        viewHolder.tag_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataArray.remove(position);
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    class ViewHolder {
        public TextView tag,tag_delete;
    }

    public void setData(String tag){
        dataArray.add(tag);
        notifyDataSetChanged();
    }

    public void setDataArray(ArrayList<String> dataArray){
        this.dataArray.clear();
        this.dataArray.addAll(dataArray);
        notifyDataSetChanged();
    }

    public ArrayList<String> getDataArray() {
        return dataArray;
    }
}
