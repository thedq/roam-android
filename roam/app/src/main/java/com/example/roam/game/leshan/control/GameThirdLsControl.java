package com.example.roam.game.leshan.control;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import com.example.roam.game.app.leshan.GameSecondLsActivity;
import com.example.roam.game.emeimountain.game.ui.CollectSpiritsActivity;
import com.example.roam.game.leshan.model.LeBuddhaSpirit;
import com.example.roam.game.leshan.model.LeSpaceSpirit;
import com.example.roam.game.leshan.model.LeStoneRightSpirit;
import com.example.roam.game.leshan.model.LeStoneSpirit;
import com.example.roam.game.leshan.model.SceneGiantBuddha;
import com.example.roam.game.leshan.model.SceneHead;
import com.example.roam.game.qcmountain.model.BarOptionSpirit;
import com.example.roam.game.tools.BarLayer;
import com.example.roam.game.tools.LeftSpirit;
import com.example.roam.game.util.WinBarLayer;
import com.example.roam.ui.activity.MainActivity;

public class GameThirdLsControl {
    private Context context;
    private SceneGiantBuddha sceneGiantBuddha;
    private LeftSpirit leftSpirit;
    private LeBuddhaSpirit leBuddhaSpirit;
    private BarLayer barLayer;
    private BarOptionSpirit barOptionSpirit;
    private LeStoneSpirit leStoneSpirit;
    private LeStoneRightSpirit leStoneRightSpirit;
    private LeSpaceSpirit leSpaceSpirit;
    private WinBarLayer winBarLayer;
    public boolean isVisible = true;
    private boolean isFinsh;

    public GameThirdLsControl(Context context){
        this.context = context;
        sceneGiantBuddha = new SceneGiantBuddha(context);
        leftSpirit = new LeftSpirit(context);
        // 打开要联动，关系管自身
        leBuddhaSpirit = new LeBuddhaSpirit(context);
        barLayer = new BarLayer(context);
        barOptionSpirit = new BarOptionSpirit(context);
        leStoneSpirit = new LeStoneSpirit(context);
        leStoneRightSpirit = new LeStoneRightSpirit(context);
        leSpaceSpirit = new LeSpaceSpirit(context);
        winBarLayer = new WinBarLayer(context,"乐山大佛");
    }

    public void draw(Canvas canvas){
        if(isVisible){
            sceneGiantBuddha.draw(canvas);
            leftSpirit.draw(canvas);
            leBuddhaSpirit.draw(canvas);
            barLayer.draw(canvas);
            barOptionSpirit.draw(canvas);
            leStoneSpirit.draw(canvas);
            leStoneRightSpirit.draw(canvas);
            leSpaceSpirit.draw(canvas);
        }

        if( isFinsh ){
            winBarLayer.draw(canvas);
        }
    }

    public void onTouch(MotionEvent event){
        switch ( event.getAction() ){
            case MotionEvent.ACTION_DOWN:
                clickLeftSpirit(event);
                if( !isFinsh ) {
                    clickSceneGiantBuddha(event);
                    clickBuddha(event);
                    clickLeStone(event);
                    clickLeStoneRight(event);
                    clickLeSpace(event);
                    clickOptionCertain(event);
                    clickOptionCancel(event);
                    clickOptionDelete(event);
                    clickDelete(event);
                }else {
                    clickWinBarlayerDelete(event);
                }
                break;
        }
    }

    private void clickWinBarlayerDelete(MotionEvent event){
        winBarLayer.getBarLayer().clickDelete(event);
    }

    private void clickBuddha(MotionEvent event){
        leBuddhaSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickDelete(MotionEvent event){
        barLayer.clickDelete(event);
    }

    private void clickLeStone(MotionEvent event){
        leStoneSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickLeStoneRight(MotionEvent event){
        leStoneRightSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickLeSpace(MotionEvent event){
        leSpaceSpirit.clickSpiritOption(event,barLayer,barOptionSpirit);
    }

    private void clickOptionCertain(MotionEvent event){
        if (barOptionSpirit.getOptionYes().getRect().contains((int) event.getX(),(int) event.getY()) && !barOptionSpirit.isClickThrough()) {
            Intent intent = new Intent(context, CollectSpiritsActivity.class);
            intent.putExtra("clazz",1);
            context.startActivity(intent);
            barOptionSpirit.setClickThrough(true);
            ((Activity)context).finish();
        }
    }

    private void clickOptionDelete(MotionEvent event){
        barOptionSpirit.clickOptionDelete(event);
    }

    private void clickOptionCancel(MotionEvent event){
        barOptionSpirit.clickOptionCancel(event);
    }

    String TAG = "TAG";
    public void clickSceneGiantBuddha(MotionEvent event){
        if (sceneGiantBuddha.getRect().contains((int) event.getX(),(int) event.getY())) {
//            Toast.makeText(context,"乐山大佛",Toast.LENGTH_SHORT).show();
            Log.i(TAG, "clickSceneGiantBuddha: x:"+event.getX()+" y:"+event.getY());
        }
    }

    public void clickLeftSpirit(MotionEvent event){
        if (leftSpirit.getRect().contains((int) event.getX(),(int) event.getY())) {
            leftSpirit.finsh(context);
        }
    }

    public boolean isFinsh() {
        return isFinsh;
    }

    public void setFinsh(boolean finsh) {
        isFinsh = finsh;
    }
}
