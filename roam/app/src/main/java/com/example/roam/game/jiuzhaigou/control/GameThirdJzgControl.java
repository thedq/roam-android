package com.example.roam.game.jiuzhaigou.control;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import com.example.roam.game.app.jiuzhaigou.GameSecondJzgActvity;
import com.example.roam.game.app.jiuzhaigou.GameThirdJzgActvity;
import com.example.roam.game.dujiangyan.game.view.CatchDemon;
import com.example.roam.game.jiuzhaigou.model.JiuDiePuSpirit;
import com.example.roam.game.jiuzhaigou.model.JiuTreeSpirit;
import com.example.roam.game.jiuzhaigou.model.SceneDiePu;
import com.example.roam.game.qcmountain.model.BarOptionSpirit;
import com.example.roam.game.tools.BarLayer;
import com.example.roam.game.tools.LeftSpirit;
import com.example.roam.game.util.WinBarLayer;
import com.example.roam.ui.activity.MainActivity;

public class GameThirdJzgControl {

    private Context context;
    private SceneDiePu sceneDiePu;
    private LeftSpirit leftSpirit;
    private JiuDiePuSpirit jiuDiePuSpirit;
    private BarLayer barLayer;
    private BarOptionSpirit barOptionSpirit;
    private JiuTreeSpirit jiuTreeSpirit;
    private WinBarLayer winBarLayer;
    public boolean isVisible = true;
    private boolean isFinsh;

    public GameThirdJzgControl(Context context){
        this.context = context;
        sceneDiePu = new SceneDiePu(context);
        leftSpirit = new LeftSpirit(context);
        jiuDiePuSpirit = new JiuDiePuSpirit(context);
        barLayer = new BarLayer(context);
        barOptionSpirit = new BarOptionSpirit(context);
        jiuTreeSpirit = new JiuTreeSpirit(context);
        winBarLayer = new WinBarLayer(context,"九寨沟");
    }

    public void draw(Canvas canvas){
        if( isVisible ){
            sceneDiePu.draw(canvas);
            leftSpirit.draw(canvas);
            jiuDiePuSpirit.draw(canvas);
            jiuTreeSpirit.draw(canvas);
            barLayer.draw(canvas);
            barOptionSpirit.draw(canvas);
        }

        if( isFinsh ){
            winBarLayer.draw(canvas);
        }
    }

    public void onTouch(MotionEvent event){
        switch ( event.getAction() ){
            case MotionEvent.ACTION_DOWN:
                clickLeftSpirit(event);
                if(!isFinsh) {
                    clickDiePu(event);
                    clickDelete(event);
                    clickGame(event);
                    clickSceneDiePu(event);
                    clickOptionCancel(event);
                    clickOptionCertain(event);
                    clickOptionDelete(event);
                }else {
                    clickWinBarLayerDelete(event);
                }
                break;
        }
    }

    private void clickWinBarLayerDelete(MotionEvent event){
        winBarLayer.getBarLayer().clickDelete(event);
    }

    private void clickGame(MotionEvent event){
        jiuTreeSpirit.clickSpiritOption(event,barLayer,barOptionSpirit);
    }

    private void clickOptionCertain(MotionEvent event){
        if ( barOptionSpirit.isVisible && barOptionSpirit.getOptionYes().getRect().contains((int) event.getX(),(int) event.getY())  && !barOptionSpirit.isClickThrough() ) {
            barOptionSpirit.isVisible = false;
            Intent intent = new Intent(context, CatchDemon.class);
            isFinsh = true;
            intent.putExtra("clazz",1);
            context.startActivity(intent);
            barOptionSpirit.setClickThrough(true);
            ((Activity)context).finish();
        }
    }

    private void clickOptionCancel(MotionEvent event){
        if (barOptionSpirit.isVisible && barOptionSpirit.getOptionNo().getRect().contains((int) event.getX(),(int) event.getY())) {
            barOptionSpirit.isVisible = false;
        }
    }

    private void clickOptionDelete(MotionEvent event){
        if (barOptionSpirit.isVisible && barOptionSpirit.getBarDeleteSpirit().getRect().contains((int) event.getX(),(int) event.getY())) {
            barOptionSpirit.isVisible = false;
        }
    }

    private void clickDelete(MotionEvent event){
        if (barLayer.getBarDeleteSpirit().getRect().contains((int) event.getX(),(int) event.getY())) {
            barLayer.isVisible = false;
        }
    }

    private void clickDiePu(MotionEvent event){
        jiuDiePuSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    String TAG = "TAG";
    public void clickSceneDiePu(MotionEvent event){
        if (sceneDiePu.getRect().contains((int) event.getX(),(int) event.getY())) {
            Log.i(TAG, "clickSceneDiePu: x:"+event.getX()+" y:"+event.getY());
        }
    }

    public void clickLeftSpirit(MotionEvent event){
        if( leftSpirit.getRect().contains((int) event.getX(),(int) event.getY())){
            leftSpirit.finsh(context);
        }
    }

    public void setIsFinsh(boolean isFinsh) {
        this.isFinsh = isFinsh;
    }
}
