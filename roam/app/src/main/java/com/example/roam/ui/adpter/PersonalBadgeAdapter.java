package com.example.roam.ui.adpter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.roam.R;
import com.example.roam.model.gson.Badges;
import com.example.roam.utils.BadgeUtils;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Create By Visen
 * Date: 2021/10/26
 */
public class PersonalBadgeAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<HashMap<String,String>> dataArray = new ArrayList<>();
    private Badges badges;

    public PersonalBadgeAdapter(Context context,ArrayList<HashMap<String,String>> dataArray){
        this.context = context;
        this.dataArray = dataArray;
        badges = BadgeUtils.getBadges();
    }

    @Override
    public int getCount() {
        return dataArray.size();
    }

    @Override
    public Object getItem(int position) {
        return dataArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HashMap<String, String> hashMap = dataArray.get(position);
        ViewHodler viewHodler = null;
        if( convertView == null ){
            viewHodler = new ViewHodler();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_badge,parent,false);
            viewHodler.img_badge = convertView.findViewById(R.id.img_badge);
            viewHodler.txt_content = convertView.findViewById(R.id.txt_content);
            viewHodler.txt_content.setText(hashMap.get("txt_content"));
            viewHodler.img_badge.setImageResource(Integer.parseInt(hashMap.get("img_badge")));
            convertView.setTag(viewHodler);
        }else {
            viewHodler = (ViewHodler)convertView.getTag();
            viewHodler.txt_content.setText(hashMap.get("txt_content"));
            viewHodler.img_badge.setImageResource(Integer.parseInt(hashMap.get("img_badge")));
        }

        if(!badges.getBadgeArray().contains(position)){
            viewHodler.img_badge.setAlpha(0.3f);
        }

        return convertView;
    }

    class ViewHodler {
        ImageView img_badge;
        TextView txt_content;
    }
}
