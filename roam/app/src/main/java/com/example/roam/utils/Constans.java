package com.example.roam.utils;

/**
 * Create By Visen
 * Date: 2021/11/1
 */
public class Constans {
    // 1273804669@qq.com  123123
    // 常量---
    // 记录用户的数据，为了能够跳过注册的时候直接调用。
    //user用户信息表的字段
    public static String SALT = "_ROAM";
    public static int u_id;//id
    public static String u_account; //用户账户
    public static String u_password; //用户密码
    public static String u_username; //用户昵称
    public static String u_character; //虚拟人物图片地址
    public static String u_spirit; //景点精灵图片地址
    public static String u_head; //用户头像地址
    public static int account_select; //用户登录的账户选择：邮箱还是其他(整型)；1是邮箱，2是其他
    public static int character_select; //用户虚拟人物选择：来自系统还是网络图片;1是软件提供的；2是网络图片
    public static int spirit_select; //用户景点精灵选择：来自系统还是网络图片;1是软件提供的；2是网络图片
    public static int head_select; //用户头像选择：来自系统还是相册;1是软件提供的；2是用户相册图片
    public static String u_first_time; //用户注册时间(字符串)
    public static String u_last_time; // 用户最后一次登录时间
    public static String cookie; //本次登录的cookie 废弃：使用HTTPUTIL.COOKIE
    public static final String WOMAN1 = "http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userHeadImg/20220428/d3114a6e52414fa8813910b16c78e28c.png";
    public static final String WOMAN2 = "http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userHeadImg/20220428/19c7782ab75b4586902e75db0eb80e8d.png";
    public static final String WOMAN3 = "http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userHeadImg/20220428/6a1ba51379b54d3bb4aae6984061550a.png";
    public static final String MAN1 = "http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userHeadImg/20220428/a78c3b19a7d94cd9a34fc094b610f069.png";
    public static final String MAN2 = "http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userHeadImg/20220428/fb790666a53d492f9a9c50cdffa0412a.png";
    public static final String MAN3 = "http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userHeadImg/20220428/fd86f5738d3f4622a80e913d2da70376.png";
    public static final String C_MAN1 = "http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userHeadImg/20220428/36f16fe1a4154ecab1b1e83b1ad51bf0.png";
    public static final String C_MAN2 = "http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userHeadImg/20220428/90ea983a89f441fca70a04b777150136.png";
    public static final String C_MAN3 = "http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userHeadImg/20220428/5b38874216874885b3f1940dcd5d3a0d.png";
    public static final String C_WOMAN1 = "http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userHeadImg/20220428/317f22d9635147efa5ddee693576ced0.png";
    public static final String C_WOMAN2 = "http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userHeadImg/20220428/0d5a3804a5104afab761f50e138f3a0f.png";
    public static final String C_WOMAN3 = "http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userHeadImg/20220428/59696a59dfd74a65ac3ebd24bfbf0bf2.png";
    public static final String PANDA = "http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userHeadImg/20220428/4f78f16c636a4b72aa4e2f28ac5b0816.png";
    public static final String LITTLE_PANDA = "http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userHeadImg/20220428/18cf209b80754c2985e757dfb8c786c2.png";
    public static final String MONKEY = "http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userHeadImg/20220428/68cb22f5bd744bff9f4ef356073cc452.png";

    public static int DISPLAY_WIDTH_PX ;
    public static int DISPLAY_HEIGHT_PX ;
    public static double SCALE_HEIGHT;
    public static double SCALE_WIDTH;

    // 地点名称
    public static final String QING_CHEN_SHAN = "青城山";
    public static final String DU_JIAN_YAN = "都江堰";
    public static final String E_MEI_SHAN = "峨眉山";
    public static final String LE_SHAN = "乐山";
    public static final String JIU_ZHAI_GOU = "九寨沟";
    public static final String JIAN_MENG_GUAN = "剑门关";

    public static final int SCENIC_COUNT = 15;

}
