package com.example.roam.game.qcmountain.model;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.roam.game.util.BitmapUtil;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class RoadGameSpirit extends CSpirit{

    public RoadGameSpirit(Context context) {
        super(context, 374, 400, 374+400, 400+366);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if( isVisible ){
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
