package com.example.roam.game.qcmountain.view;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import com.example.roam.game.qcmountain.control.GameThirdControl;
public class GameThirdUI extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder surfaceHolder;
    private GameThirdControl gameThirdControl;
    public GameThirdUI(Context context) {
        super(context);
        init();
    }
    public GameThirdUI(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public GameThirdUI(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    public GameThirdUI(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }
    public void init(){
        surfaceHolder = this.getHolder();
        surfaceHolder.addCallback(this);
        gameThirdControl = new GameThirdControl(getContext());
    }
    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        Runner runner = new Runner();
        runner.start();
        //
    }
    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {
        // 到游戏，徽章，来回切换，底部，中部
    }
    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {
    }
    class Runner extends Thread{
        @Override
        public void run() {
            while (gameThirdControl.isVisible){
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                draw();
            }
        }
    }
    public void draw(){
        Canvas canvas = surfaceHolder.lockCanvas();
        gameThirdControl.draw(canvas);
        surfaceHolder.unlockCanvasAndPost(canvas);
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gameThirdControl.handleTouch(event);
        return true;
    }

    public GameThirdControl getGameThirdControl() {
        return gameThirdControl;
    }
}
