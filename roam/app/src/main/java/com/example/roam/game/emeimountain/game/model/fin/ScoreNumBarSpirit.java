package com.example.roam.game.emeimountain.game.model.fin;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.example.roam.R;
import com.example.roam.game.emeimountain.game.model.Spirit;

public class ScoreNumBarSpirit extends Spirit {
    private NumLayer numLayer;
    public ScoreNumBarSpirit(Context context, int left, int top, int right, int bottom) {
        super(context, left, top, right, bottom);
        this.bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.sc_bar);
        numLayer = new NumLayer(context,120,0,0 + 350,150);
    }

    @Override
    public void draw(Canvas canvas) {
        if (isVisible) {
            canvas.drawBitmap(bitmap,null,rect,null);
            numLayer.draw(canvas);
        }
    }

    public void setScore(int score){
        numLayer.setTimeStr(String.valueOf(score));
    }

    public void resetScore(){
        numLayer.setTimeStr("000");
    }

    public NumLayer getNumLayer() {
        return numLayer;
    }
}
