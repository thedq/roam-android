package com.example.roam.game.util;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.example.roam.R;
import com.example.roam.game.emeimountain.game.model.Spirit;
import com.example.roam.game.emeimountain.game.model.TextSpirit;

public class FinSpirit extends Spirit {
    private MaskRect maskRect;
    private OptionAgainSpirit optionAgainSpirit;
    private OptionReturnSpirit optionReturnSpirit;
    private OptionAheadSpirit optionAheadSpirit;
    private Bitmap bitmaps[] = new Bitmap[2];
    private State currentState = State.NONE;
    private String text;
    private TextSpirit passScoreText,earScoreText,timeText;

    public FinSpirit(Context context , int bgYLocation) {
        super(context, 540-(int)(450*1.5 / 2),bgYLocation, 540-(int)(450*1.5 / 2) + (int)(450 * 1.5),bgYLocation+(int)(338*3) );
        bitmaps[0] = BitmapFactory.decodeResource(context.getResources(), R.drawable.fin_show);
        bitmaps[1] = BitmapFactory.decodeResource(context.getResources(), R.drawable.fin_show_win);
        maskRect = new MaskRect(context);
        optionAgainSpirit = new OptionAgainSpirit(context,540-(int)(450*1.5 / 2),bgYLocation+(int)(338*3)+30,540-(int)(450*1.5 / 2)+300,bgYLocation+(int)(338*3)+30+150);
        optionReturnSpirit = new OptionReturnSpirit(context,540-(int)(450*1.5 / 2) + (int)(450 * 1.5) -300 ,bgYLocation+(int)(338*3)+30, 540-(int)(450*1.5 / 2) + (int)(450 * 1.5),bgYLocation+(int)(338*3)+30+150 ) ;
        optionAheadSpirit = new OptionAheadSpirit(context,540-(int)(450*1.5 / 2),bgYLocation+(int)(338*3)+30,540-(int)(450*1.5 / 2)+300,bgYLocation+(int)(338*3)+30+150);

        passScoreText = new TextSpirit(context,540-(int)(450*1.5 / 2)+this.width/3-20,bgYLocation+((int)(338*3)/2)-100 );
        earScoreText = new TextSpirit(context, 540-(int)(450*1.5 / 2)+this.width/3-20,bgYLocation+((int)(338*3)/2-20));
        timeText = new TextSpirit(context,540-(int)(450*1.5 / 2)+this.width/3-20,bgYLocation+((int)(338*3)/2)+130);
        timeText.setTextSize(45);
    }

    public void setPassScore(String passScore){
        passScoreText.setmText(GameConstant.PASS_SCORE+passScore);
    }

    public void setEarnScore(String earnScore){
        earScoreText.setmText(GameConstant.EARN_SCORE+earnScore);
    }

    public void setTimeCost(String timeCost){
        timeText.setmText(GameConstant.TIME_COST+timeCost+"s");
    }

    @Override
    public void draw(Canvas canvas) {
        if( isVisible ){

            if( currentState == State.WIN ){
                maskRect.draw(canvas);
                canvas.drawBitmap(bitmaps[1],null,rect,null);
                optionAheadSpirit.draw(canvas);
                optionReturnSpirit.draw(canvas);
                passScoreText.draw(canvas);
                earScoreText.draw(canvas);
                timeText.draw(canvas);
            }else if( currentState == State.LOSE ){
                maskRect.draw(canvas);
                canvas.drawBitmap(bitmaps[0],null,rect,null);
                optionAgainSpirit.draw(canvas);
                optionReturnSpirit.draw(canvas);
                passScoreText.draw(canvas);
                earScoreText.draw(canvas);
                timeText.draw(canvas);
            }
        }
    }

//    public void drawText(Canvas canvas){
//        // 1. 切割成11个字符每个String[]
//        char[] chars = text.toCharArray();
//        StringBuffer stringBuffer = new StringBuffer();
//        int row = 0; //行数，每行间隔80px
//        int count = 0;
//        for (int i = 0; i < chars.length; i++) {
//            stringBuffer.append(chars[i]); // i%11!=0继续 且 i!=chars.length-1
//            count++;
//            if(row == 7 ){
//                break; // 限制为7行
//            }
//            if( count==11){
//                canvas.drawText(stringBuffer.toString(),(float) (barSpirit.left+barSpirit.width*0.1),(float) (barSpirit.top+barSpirit.top*0.3+row*70),paint); // 左右80%的区域，上面20%开始
//                row++;
//                stringBuffer = new StringBuffer();
//                count = 0;  //复原字符数量
//            }
//            if( i == chars.length -1 && !stringBuffer.toString().equals("")){                // i到最后而且stingBuffer有内容
//                canvas.drawText(stringBuffer.toString(),(float) (barSpirit.left+barSpirit.width*0.1),(float) (barSpirit.top+barSpirit.top*0.3+row*70),paint); // 左右80%的区域，上面20%开始
//            }
//        }
//    }

    public void lose(String passScore,String earnScore,String timeCost){
        setPassScore(passScore);
        setEarnScore(earnScore);
        setTimeCost(timeCost);
        currentState = State.LOSE;
        this.isVisible = true;
    }

    public void win(String passScore,String earnScore,String timeCost){
        setPassScore(passScore);
        setEarnScore(earnScore);
        setTimeCost(timeCost);
        currentState = State.WIN;
        this.isVisible = true;
    }

    public void winAndGoAhead(){
        currentState = State.START;
        this.isVisible = false;
    }

    public State getCurrentState() {
        return currentState;
    }

    public void setCurrentState(State currentState) {
        this.currentState = currentState;
    }

    public OptionAgainSpirit getOptionAgainSpirit() {
        return optionAgainSpirit;
    }

    public void setOptionAgainSpirit(OptionAgainSpirit optionAgainSpirit) {
        this.optionAgainSpirit = optionAgainSpirit;
    }

    public OptionReturnSpirit getOptionReturnSpirit() {
        return optionReturnSpirit;
    }  // 结束

    public void setOptionReturnSpirit(OptionReturnSpirit optionReturnSpirit) {
        this.optionReturnSpirit = optionReturnSpirit;
    }

    public OptionAheadSpirit getOptionAheadSpirit() {
        return optionAheadSpirit;
    }


}
