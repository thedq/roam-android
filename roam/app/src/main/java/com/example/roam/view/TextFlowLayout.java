package com.example.roam.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.roam.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TextFlowLayout extends ViewGroup {
    private List<String> mTextList = new ArrayList<>();
    public static final int DEFAULT_SPACE = 10;
    private float itemHoriaontalSpace = DEFAULT_SPACE ;
    private float itemVerticalSpac = DEFAULT_SPACE;
    private int selfWidth ;
    private int itemHeight;
    private List<List<View>> lines = new ArrayList<>();
    private OnFlowTextItemClickListener itemClickListener = null;
    private int selfHeight;
    public int getContentSize(){
        return mTextList.size();
    }
    public TextFlowLayout(Context context) {
        super(context);
    }
    public TextFlowLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public TextFlowLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        // 去拿到相关属性
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.FlowTextStyle);
        itemHoriaontalSpace =  typedArray.getDimension(R.styleable.FlowTextStyle_horizontalSpace,DEFAULT_SPACE); // 水平方向
        itemVerticalSpac = typedArray.getDimension(R.styleable.FlowTextStyle_verticalSpace,DEFAULT_SPACE);
        typedArray.recycle();
    }

    public void setTextList(List<String> mTextList) {  // 逆序设置历史记录
        removeAllViews();
        this.mTextList.clear();
        this.mTextList.addAll(mTextList);
        Collections.reverse(mTextList);
        for (String text : mTextList) {
            TextView item = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.flow_text_view,this,false);  // true == false and addView()
            item.setText("#"+text);
            item.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onFlowItemClick(text);
                }
            });
            addView(item);
        }
    }

    public interface OnFlowTextItemClickListener {
        void onFlowItemClick(String text);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if( getChildCount() == 0 ){
            setMeasuredDimension(0, 0);
            return;
        }
        List<View> line = null;
        lines.clear();
        selfWidth = MeasureSpec.getSize(widthMeasureSpec) - getPaddingRight() - getPaddingLeft();
        // 测量孩子
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View itemView = getChildAt(i);
            if (itemView.getVisibility() != VISIBLE) {
                continue;
            }
            measureChild(itemView,widthMeasureSpec,heightMeasureSpec);
            if( line == null ){
                line = createNewLine(itemView);
            }else {
                if (canBeAdd(itemView,line)) {
                    line.add(itemView);
                }else {
                    line = createNewLine(itemView);
                }
            }
        }
        // 测量自己
        itemHeight = getChildAt(0).getMeasuredHeight();
        selfHeight = (int)(lines.size() * itemHeight + itemVerticalSpac*(lines.size()+1)+0.5f);
        setMeasuredDimension(selfWidth, selfHeight);
    }

    private List<View> createNewLine(View itemView){
        List<View> line = new ArrayList<>();
        line = new ArrayList<>();
        line.add(itemView);
        lines.add(line);
        return line;
    }

    // 判断当前行是否可再继续添加数据
    private boolean canBeAdd(View itemView, List<View> line) {
        int totalWidth = itemView.getMeasuredWidth();
        for (View view : line) {
            totalWidth += view.getMeasuredWidth();
        }
        totalWidth += itemHoriaontalSpace * (line.size()+1);
        return totalWidth <= selfWidth ;
    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int topOffset = 0;
        for (List<View> views : lines) {
            int leftOffset = 0 ;
            for (int i = 0; i < views.size(); i++) {
                View view = views.get(i);
                view.layout(leftOffset, topOffset, leftOffset + view.getMeasuredWidth(), topOffset + view.getMeasuredHeight());
                leftOffset += view.getMeasuredWidth() + itemHoriaontalSpace;
            }
            topOffset += itemHeight+itemVerticalSpac;
        }
    }

    public void setOnFlowTextItemClickListener(OnFlowTextItemClickListener listener){
        this.itemClickListener = listener;
    }

    public float getItemHoriaontalSpace() {
        return itemHoriaontalSpace;
    }

    public void setItemHoriaontalSpace(float itemHoriaontalSpace) {
        this.itemHoriaontalSpace = itemHoriaontalSpace;
    }

    public float getItemVerticalSpac() {
        return itemVerticalSpac;
    }

    public void setItemVerticalSpac(float itemVerticalSpac) {
        this.itemVerticalSpac = itemVerticalSpac;
    }
}

