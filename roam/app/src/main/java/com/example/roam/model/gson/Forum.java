package com.example.roam.model.gson;
/**
 * Copyright 2022 bejson.com
 */
import java.util.List;


/**
 * Auto-generated: 2022-03-27 19:19:16
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Forum {

    private boolean success;
    private int code;
    private String message;
    private Data data;
    public void setSuccess(boolean success) {
        this.success = success;
    }
    public boolean getSuccess() {
        return success;
    }

    public void setCode(int code) {
        this.code = code;
    }
    public int getCode() {
        return code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }

    public void setData(Data data) {
        this.data = data;
    }
    public Data getData() {
        return data;
    }

    @Override
    public String toString() {
        return "Forum{" +
                "success=" + success +
                ", code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    /**
     * Copyright 2022 bejson.com
     */
    /**
     * Auto-generated: 2022-03-27 19:19:16
     *
     * @author bejson.com (i@bejson.com)
     * @website http://www.bejson.com/java2pojo/
     */
    public class ScenicReleaseInfo {

        private int scenicReleaseId;
        private String scenicReleaseTitle;
        private String scenicReleaseText;
        private int scenicReleaseGood;
        private long spaceReleaseTime;
        private int spaceReleaseType;
        private int scenicId;
        private int userId;
        private int anoUser;
        public void setScenicReleaseId(int scenicReleaseId) {
            this.scenicReleaseId = scenicReleaseId;
        }
        public int getScenicReleaseId() {
            return scenicReleaseId;
        }

        public void setScenicReleaseTitle(String scenicReleaseTitle) {
            this.scenicReleaseTitle = scenicReleaseTitle;
        }
        public String getScenicReleaseTitle() {
            return scenicReleaseTitle;
        }

        public void setScenicReleaseText(String scenicReleaseText) {
            this.scenicReleaseText = scenicReleaseText;
        }
        public String getScenicReleaseText() {
            return scenicReleaseText;
        }

        public void setScenicReleaseGood(int scenicReleaseGood) {
            this.scenicReleaseGood = scenicReleaseGood;
        }
        public int getScenicReleaseGood() {
            return scenicReleaseGood;
        }

        public void setSpaceReleaseTime(long spaceReleaseTime) {
            this.spaceReleaseTime = spaceReleaseTime;
        }
        public long getSpaceReleaseTime() {
            return spaceReleaseTime;
        }

        public void setSpaceReleaseType(int spaceReleaseType) {
            this.spaceReleaseType = spaceReleaseType;
        }
        public int getSpaceReleaseType() {
            return spaceReleaseType;
        }

        public void setScenicId(int scenicId) {
            this.scenicId = scenicId;
        }
        public int getScenicId() {
            return scenicId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }
        public int getUserId() {
            return userId;
        }

        public void setAnoUser(int anoUser) {
            this.anoUser = anoUser;
        }
        public int getAnoUser() {
            return anoUser;
        }

        @Override
        public String toString() {
            return "ScenicReleaseInfo{" +
                    "scenicReleaseId=" + scenicReleaseId +
                    ", scenicReleaseTitle='" + scenicReleaseTitle + '\'' +
                    ", scenicReleaseText='" + scenicReleaseText + '\'' +
                    ", scenicReleaseGood=" + scenicReleaseGood +
                    ", spaceReleaseTime=" + spaceReleaseTime +
                    ", spaceReleaseType=" + spaceReleaseType +
                    ", scenicId=" + scenicId +
                    ", userId=" + userId +
                    ", anoUser=" + anoUser +
                    '}';
        }
    }

    /**
     * Copyright 2022 bejson.com
     */

    /**
     * Auto-generated: 2022-03-27 19:19:16
     *
     * @author bejson.com (i@bejson.com)
     * @website http://www.bejson.com/java2pojo/
     */
    public static class AllInfo {

        private ScenicReleaseInfo scenicReleaseInfo;
        private String headUrl;
        private String userName;
        private List<String> pictures;
        private boolean isGood;
        private int commentCount;
        private List<String> tags;
        public void setScenicReleaseInfo(ScenicReleaseInfo scenicReleaseInfo) {
            this.scenicReleaseInfo = scenicReleaseInfo;
        }

        public boolean isGood() {
            return isGood;
        }

        public void setGood(boolean good) {
            isGood = good;
        }

        public ScenicReleaseInfo getScenicReleaseInfo() {
            return scenicReleaseInfo;
        }

        public void setHeadUrl(String headUrl) {
            this.headUrl = headUrl;
        }
        public String getHeadUrl() {
            return headUrl;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }
        public String getUserName() {
            return userName;
        }

        public void setPictures(List<String> pictures) {
            this.pictures = pictures;
        }
        public List<String> getPictures() {
            return pictures;
        }

        public void setCommentCount(int commentCount) {
            this.commentCount = commentCount;
        }
        public int getCommentCount() {
            return commentCount;
        }

        public void setTags(List<String> tags) {
            this.tags = tags;
        }
        public List<String> getTags() {
            return tags;
        }

        @Override
        public String toString() {
            return "AllInfo{" +
                    "scenicReleaseInfo=" + scenicReleaseInfo +
                    ", headUrl='" + headUrl + '\'' +
                    ", userName='" + userName + '\'' +
                    ", pictures=" + pictures +
                    ", isGood=" + isGood +
                    ", commentCount=" + commentCount +
                    ", tags=" + tags +
                    '}';
        }
    }

    /**
     * Copyright 2022 bejson.com
     */

    /**
     * Auto-generated: 2022-03-27 19:19:16
     *
     * @author bejson.com (i@bejson.com)
     * @website http://www.bejson.com/java2pojo/
     */
    public class Data {

        private List<AllInfo> AllInfo;
        public void setAllInfo(List<AllInfo> AllInfo) {
            this.AllInfo = AllInfo;
        }
        public List<AllInfo> getAllInfo() {
            return AllInfo;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "AllInfo=" + AllInfo +
                    '}';
        }
    }

}