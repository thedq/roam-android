package com.example.roam.game.emeimountain.game.control;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import com.example.roam.R;
import com.example.roam.game.app.emeimountain.GameThirdEmsActivity;
import com.example.roam.game.app.leshan.GameThirdLsActivity;
import com.example.roam.game.emeimountain.game.model.BgSpirit;
import com.example.roam.game.emeimountain.game.model.MSpirit;
import com.example.roam.game.emeimountain.game.model.MSpiritLayer;
import com.example.roam.game.emeimountain.game.model.fin.GameInfoLayer;
import com.example.roam.game.leshan.control.GameThirdLsControl;
import com.example.roam.game.tools.CatchBadgeSpirit;
import com.example.roam.game.util.BeginSpirit;
import com.example.roam.game.util.FinSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.State;
import com.example.roam.utils.BadgeUtils;

public class GameControl {
    private Context context;
    private BgSpirit bgSpirit;
    private MSpiritLayer mSpiritLayer;
    private BeginSpirit beginSpirit;
    private FinSpirit finSpirit;
    private GameInfoLayer gameInfoLayer;
    private CatchBadgeSpirit catchBadgeSpirit;
    private int choose; // 判断是否继续
    private boolean isEnd = false;
    private int clazz;

    public GameControl(Context context){
        this.context = context;
        this.bgSpirit = new BgSpirit(context);
        this.mSpiritLayer = new MSpiritLayer(context);
        this.beginSpirit = new BeginSpirit(context,GameConstant.GAME_DETAIL_TEXT);
        this.beginSpirit.setGameStartBitmap(R.drawable.collect_spirit);
        this.finSpirit = new FinSpirit(context,320);
        this.gameInfoLayer = new GameInfoLayer(context);
        this.catchBadgeSpirit = new CatchBadgeSpirit(context, GameConstant.EMS_BADGE_C);
    }

    public void draw(Canvas canvas) {
        if (!isEnd) {
            bgSpirit.draw(canvas);
            mSpiritLayer.draw(canvas);
            finSpirit.draw(canvas);
            beginSpirit.draw(canvas);
            gameInfoLayer.draw(canvas);
            gameInfoLayer.getTimeNumBarSpirit().setNowTime(System.currentTimeMillis());
            catchBadgeSpirit.draw(canvas);
        }
    }

    public void onTouch(MotionEvent event){
        clickStartGame(event);
        clickGameDetail(event);
        clickType(event);
        clickAgain(event);
        clickBarOptionGoAhead(event);
        clickBarOptionEnd(event);
        clickBarOptionEndWithOver(event);
        clickGameDetailDelete(event);
    }

    public void clickType(MotionEvent event){
        for (MSpirit mSpirit : mSpiritLayer.getmSpiritArray()) {
            if ( !gameInfoLayer.isPause() && mSpirit.isVisible && mSpirit.getRect().contains((int)event.getX(),(int)event.getY())) {
                switch ( mSpirit.getType() ){
                    case 0:
                        mSpiritLayer.setCount(mSpiritLayer.getCount()+1);
                        mSpirit.isVisible = false;
                        break;
                    case 1:
                        mSpiritLayer.setCount(mSpiritLayer.getCount()+2);
                        mSpirit.isVisible = false;
                        break;
                    case 2:
                        mSpiritLayer.setCount(mSpiritLayer.getCount()+3);
                        mSpirit.isVisible = false;
                        break;
                    case 3:
                        lose();
                        break;
                }
                isWin();
            }
        }
        gameInfoLayer.setScore(mSpiritLayer.getCount());
    }

    String TAG = "TAG";
    private void isWin(){
        win();
    }

    private void clickStartGame(MotionEvent event){
        if( beginSpirit.isVisible && beginSpirit.getStartRect().contains((int)event.getX(),(int)event.getY())){
            mSpiritLayer.setCurrentState(State.START);
            beginSpirit.isVisible = false;
            gameInfoLayer.getTimeNumBarSpirit().setBeginTime(System.currentTimeMillis());
            gameInfoLayer.getTimeNumBarSpirit().setCurrentState(State.START);
            gameInfoLayer.setVisible(true);
        }
    }
    
    private void clickGameDetail(MotionEvent event){
        beginSpirit.clickDetail(event);
    }

    private void clickGameDetailDelete(MotionEvent event){
        beginSpirit.clickDetailDelete(event);
    }

    private void clickBarOptionGoAhead(MotionEvent event){  // 胜利：继续按钮
        if ( finSpirit.getCurrentState() == State.WIN && finSpirit.getOptionAheadSpirit().getRect().contains((int)event.getX(),(int) event.getY())) {
            mSpiritLayer.winAndGoAhead(); // 改变状态和精灵速度
            finSpirit.winAndGoAhead(); // 改变状态
            choose = 1;
            gameInfoLayer.resumeTime();
        }
    }

    public void waitToStartActivity(Intent intent){  // 1. 设置动画，结束，跳转。
        if (
               catchBadgeSpirit.getImgBadge() == GameConstant.EMS_BADGE_C && !BadgeUtils.getBadges().getBadgeArray().contains(8) ||
                catchBadgeSpirit.getImgBadge() == GameConstant.LS_BADGE_C && !BadgeUtils.getBadges().getBadgeArray().contains(11)
        ) {
            catchBadgeSpirit.isVisible = true;
            catchBadgeSpirit.setStartAnimation(true); // 启动动画
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            intent.putExtra("isFinsh",true);
                            isEnd = true;
                            context.startActivity(intent);
                        }
                    });
                }
            }).start();
        }else {
            intent.putExtra("isFinsh",true);
            isEnd = true;
            context.startActivity(intent);
        }
        ((Activity)context).finish();
    }

    private void clickBarOptionEnd(MotionEvent event){ // 胜利：结束按钮
        if( finSpirit.getCurrentState() == State.WIN && finSpirit.getOptionReturnSpirit().getRect().contains((int) event.getX(),(int) event.getY())){
            Intent intent = null;
            if(clazz == 0){
                intent = new Intent(context,GameThirdEmsActivity.class);
                catchBadgeSpirit.setImgBadge(GameConstant.EMS_BADGE_C);
            }else if( clazz == 1){
                intent = new Intent(context, GameThirdLsActivity.class);
                catchBadgeSpirit.setImgBadge(GameConstant.LS_BADGE_C);
            }
            waitToStartActivity(intent);
        }
    }

    private void clickAgain(MotionEvent event){  // 失败：再来一局按钮
        if ( finSpirit.getCurrentState() == State.LOSE && finSpirit.getOptionAgainSpirit().getRect().contains((int) event.getX(),(int) event.getY())  ) {
            finSpirit.setCurrentState(State.START);
            mSpiritLayer.setAgain();
            choose = 0;
            mSpiritLayer.setCount(0);
            gameInfoLayer.resetTime(); // 重置时间
            gameInfoLayer.resetScore(); // 重置分数
        }
    }

    private void clickBarOptionEndWithOver(MotionEvent event){  // 失败：结束按钮
        // 进行一个分数判断
        if(finSpirit.getCurrentState() == State.LOSE && finSpirit.getOptionReturnSpirit().getRect().contains((int) event.getX(),(int) event.getY())){
            Intent intent = new Intent(context,GameThirdEmsActivity.class);
            if(clazz == 0){
                intent = new Intent(context,GameThirdEmsActivity.class);
            }else if( clazz == 1){
                intent = new Intent(context, GameThirdLsActivity.class);
            }
            if( mSpiritLayer.getCount() >= 100 ){
                intent.putExtra("isFinsh",true);
            }else {
                intent.putExtra("isFinsh",false);
            }
            isEnd = true;
            context.startActivity(intent);
        }
    }

    private void lose() {
        finSpirit.lose("100",gameInfoLayer.getScoreNumBarSpirit().getNumLayer().getTimeStr(),gameInfoLayer.getTimeNumBarSpirit().getTimeStr());
        mSpiritLayer.lose();
        gameInfoLayer.pauseTime();
    }

    private void win(){
        if( mSpiritLayer.getCount() >= 100 && choose == 0 ){
            finSpirit.win("100",gameInfoLayer.getScoreNumBarSpirit().getNumLayer().getTimeStr(),gameInfoLayer.getTimeNumBarSpirit().getTimeStr());
            mSpiritLayer.win();
            gameInfoLayer.pauseTime(); // 暂停时间
        }
    }

    public boolean isEnd() {
        return isEnd;
    }

    public void setEnd(boolean end) {
        isEnd = end;
    }

    public int getClazz() {
        return clazz;
    }

    public void setClazz(int clazz) {
        this.clazz = clazz;
    }
}
