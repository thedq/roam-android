package com.example.roam.game.qcmountain.control;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.example.roam.R;
import com.example.roam.game.qcmountain.model.layer2.DialogSpirit;
import com.example.roam.game.qcmountain.model.layer2.MaskRect;
import com.example.roam.game.qcmountain.model.layer2.OptionLayer;
import com.example.roam.game.qcmountain.model.layer2.TaoistSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.TaoistLayer;

public class GameSecondLayerControl {
    private MaskRect maskRect;
    private OptionLayer optionLayer;
    private DialogSpirit dialogSpirit;
    private TaoistSpirit taoistSpiritb;
    public boolean isVisible ;
    private Context context;
    private static final String TAG = "TAG";

    public GameSecondLayerControl(Context context){
        this.context = context;
        this.maskRect = new MaskRect(context);
        this.taoistSpiritb = new TaoistSpirit(context);
        this.optionLayer = new OptionLayer(context);
        this.dialogSpirit = new DialogSpirit(context);
        initData();
    }

    public void initData(){
        this.taoistSpiritb.setRect(new Rect(0,1025,282,1025+601));
        taoistSpiritb.setTaoistBitmap(R.drawable.npc_flip);
        maskRect.isVisible = true;
        taoistSpiritb.isVisible = true;
        optionLayer.isVisible = true;
        dialogSpirit.isVisible = true;
        dialogSpirit.setText(GameConstant.ALLS_SCRIPT);

    }

    public void defaultText(){
        taoistSpiritb.setText(GameConstant.ALLS_SCRIPT);
    }

    public void draw(Canvas canvas) {
        if (isVisible) {
            maskRect.draw(canvas);
            taoistSpiritb.draw(canvas);
            optionLayer.draw(canvas);
            dialogSpirit.draw(canvas);
        }else {
            defaultText(); //复原默认文本
        }
    }

    public void setText(String text){
        dialogSpirit.setText(text);
    }

    public OptionLayer getOptionLayer() {
        return optionLayer;
    }

    public DialogSpirit getDialogSpirit() {
        return dialogSpirit;
    }
}
