package com.example.roam.game.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.widget.Toast;

import com.example.roam.R;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.utils.Constans;
import com.google.android.material.bottomappbar.BottomAppBar;

public class CatchBadgeSpirit extends Spirit {
    private Paint paint;
    private int alpha = 0;
    private Bitmap badge ;
    private int imgBadge;
    private Rect badgeRect;
    private boolean isStartAnimation = false;
    private int time = 0;

    public CatchBadgeSpirit(Context context,int imgBadge) {
        super(context, 0, 0, 1080, 1793);
        this.imgBadge = imgBadge;
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setAlpha(alpha);
        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.get_badge);
        badge = BitmapFactory.decodeResource(context.getResources(),imgBadge);
        badgeRect = new Rect(400,1446,650,1702);
        badgeRect.left *= Constans.SCALE_WIDTH;
        badgeRect.right *= Constans.SCALE_WIDTH;
        badgeRect.top *= Constans.SCALE_HEIGHT;
        badgeRect.bottom *= Constans.SCALE_HEIGHT;
        this.isVisible = false;
    }

    @Override
    public void draw(Canvas canvas) {
        if (isVisible) {
            canvas.drawBitmap(bitmap,null,rect,paint);
            canvas.drawBitmap(badge,null,badgeRect,paint);
            badgeAnimation();
        }
    }

    private void badgeAnimation(){             // +25到250，然后消失
        if( isStartAnimation ) {
            isVisible = true;
            if (alpha < 250) {
                alpha += 50;
            } else if (alpha >= 250) {
                time += 20;
                if( time >= 400 ){
                    isStartAnimation = false;
                    isVisible = false;
                    time = 0;
                    alpha = 0;
                }
            }
            paint.setAlpha(alpha);
        }
    }

    public void setStartAnimation(boolean isStartAnimation){
        this.isVisible = true;
        this.isStartAnimation = isStartAnimation;
    }

    public int getImgBadge() {
        return imgBadge;
    }

    public void setImgBadge(int imgBadge) {
        this.imgBadge = imgBadge;
        this.badge = BitmapFactory.decodeResource(context.getResources(),imgBadge);
    }
}
