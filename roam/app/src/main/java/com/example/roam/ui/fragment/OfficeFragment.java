package com.example.roam.ui.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.viewpager.widget.ViewPager;

import com.example.roam.R;
import com.example.roam.ui.activity.BaseFragment;
import com.example.roam.ui.activity.UploadDiaryActivity;
import com.example.roam.ui.adpter.ForumOfficeFragmentPaperAdapter;

/**
 * Create By Visen
 * Date: 2021/11/8
 */
public class OfficeFragment extends BaseFragment {
    private RadioGroup rg_bar;
    private int radioButtons [] = {R.id.rb_introduce,R.id.rb_scenic,R.id.rb_guide};
    private ViewPager vp_office;
    private ForumOfficeFragmentPaperAdapter forumOfficeFragmentPaperAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_office;
    }

    @Override
    public void initView(View successView) {
        rg_bar = successView.findViewById(R.id.rg_bar);
        vp_office = successView.findViewById(R.id.vp_office);
        forumOfficeFragmentPaperAdapter = new ForumOfficeFragmentPaperAdapter(getChildFragmentManager());
    }

    @Override
    public void initData() {
        vp_office.setAdapter(forumOfficeFragmentPaperAdapter);
        vp_office.setOffscreenPageLimit(1);
    }

    @Override
    public void initEvent() {
        MyListener myListener = new MyListener();
        rg_bar.setOnCheckedChangeListener(myListener);
        vp_office.setOnPageChangeListener(myListener);
    }

    @Override
    public void loadData() {
        setSuccess();
    }

    class MyListener implements RadioGroup.OnCheckedChangeListener , ViewPager.OnPageChangeListener , View.OnClickListener {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch ( checkedId ){
                case R.id.rb_introduce:
                     check(R.id.rb_introduce,group);
                     vp_office.setCurrentItem(0);
                    break;
                case R.id.rb_scenic:
                    check(R.id.rb_scenic,group);
                    vp_office.setCurrentItem(1);
                    break;
                case R.id.rb_guide:
                    check(R.id.rb_guide,group);
                    vp_office.setCurrentItem(2);
                    break;

            }
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            switch ( position ){
                case 0:
                    RadioButton radioButton = rg_bar.findViewById(R.id.rb_introduce);
                    radioButton.setChecked(true);
                    break;
                case 1:
                    RadioButton radioButton2 = rg_bar.findViewById(R.id.rb_scenic);
                    radioButton2.setChecked(true);
                    break;
                case 2:
                    RadioButton radioButton3 = rg_bar.findViewById(R.id.rb_guide);
                    radioButton3.setChecked(true);
                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }

        @Override
        public void onClick(View v) {

        }
    }

    public void check(int checkedId,RadioGroup group){
        RadioButton checkButton = group.findViewById(checkedId);
        checkButton.setTextColor(Color.parseColor("#E5E5E5"));
        for (int i = 0; i < radioButtons.length; i++) {
            if( radioButtons[i] != checkedId){
                RadioButton radioButton = group.findViewById(radioButtons[i]);
                radioButton.setTextColor(Color.parseColor("#97C4C0"));
            }
        }
    }
}
