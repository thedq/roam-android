package com.example.roam.model.gson;

import java.util.List;

/**
 * Copyright 2022 bejson.com
 */
/**
 * Copyright 2022 bejson.com
 */

public class ForumOffice {

    private boolean success;
    private int code;
    private String message;
    private Data data;
    public void setSuccess(boolean success) {
        this.success = success;
    }
    public boolean getSuccess() {
        return success;
    }

    public void setCode(int code) {
        this.code = code;
    }
    public int getCode() {
        return code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }

    public void setData(Data data) {
        this.data = data;
    }
    public Data getData() {
        return data;
    }

    @Override
    public String toString() {
        return "ForumOffice{" +
                "success=" + success +
                ", code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    /**
     * Copyright 2022 bejson.com
     */

    /**
     * Auto-generated: 2022-05-06 17:3:19
     *
     * @author bejson.com (i@bejson.com)
     * @website http://www.bejson.com/java2pojo/
     */
    public class SrInfo {

        private int scenicReleaseId;
        private String scenicReleaseTitle;
        private String scenicReleaseText;
        private int scenicReleaseGood;
        private long spaceReleaseTime;
        private int spaceReleaseType;
        private int scenicId;
        private int userId;
        private int anoUser;
        public void setScenicReleaseId(int scenicReleaseId) {
            this.scenicReleaseId = scenicReleaseId;
        }
        public int getScenicReleaseId() {
            return scenicReleaseId;
        }

        public void setScenicReleaseTitle(String scenicReleaseTitle) {
            this.scenicReleaseTitle = scenicReleaseTitle;
        }
        public String getScenicReleaseTitle() {
            return scenicReleaseTitle;
        }

        public void setScenicReleaseText(String scenicReleaseText) {
            this.scenicReleaseText = scenicReleaseText;
        }
        public String getScenicReleaseText() {
            return scenicReleaseText;
        }

        public void setScenicReleaseGood(int scenicReleaseGood) {
            this.scenicReleaseGood = scenicReleaseGood;
        }
        public int getScenicReleaseGood() {
            return scenicReleaseGood;
        }

        public void setSpaceReleaseTime(long spaceReleaseTime) {
            this.spaceReleaseTime = spaceReleaseTime;
        }
        public long getSpaceReleaseTime() {
            return spaceReleaseTime;
        }

        public void setSpaceReleaseType(int spaceReleaseType) {
            this.spaceReleaseType = spaceReleaseType;
        }
        public int getSpaceReleaseType() {
            return spaceReleaseType;
        }

        public void setScenicId(int scenicId) {
            this.scenicId = scenicId;
        }
        public int getScenicId() {
            return scenicId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }
        public int getUserId() {
            return userId;
        }

        public void setAnoUser(int anoUser) {
            this.anoUser = anoUser;
        }
        public int getAnoUser() {
            return anoUser;
        }

        @Override
        public String toString() {
            return "SrInfo{" +
                    "scenicReleaseId=" + scenicReleaseId +
                    ", scenicReleaseTitle='" + scenicReleaseTitle + '\'' +
                    ", scenicReleaseText='" + scenicReleaseText + '\'' +
                    ", scenicReleaseGood=" + scenicReleaseGood +
                    ", spaceReleaseTime=" + spaceReleaseTime +
                    ", spaceReleaseType=" + spaceReleaseType +
                    ", scenicId=" + scenicId +
                    ", userId=" + userId +
                    ", anoUser=" + anoUser +
                    '}';
        }
    }

    /**
     * Copyright 2022 bejson.com
     */

    /**
     * Auto-generated: 2022-05-06 17:3:19
     *
     * @author bejson.com (i@bejson.com)
     * @website http://www.bejson.com/java2pojo/
     */
    public class ScenicWithPageList {

        private SrInfo srInfo;
        private List<String> imgs;
        private List<String> tagTexts;
        private int comment;
        private boolean isGood;
        public void setSrInfo(SrInfo srInfo) {
            this.srInfo = srInfo;
        }
        public SrInfo getSrInfo() {
            return srInfo;
        }

        public void setImgs(List<String> imgs) {
            this.imgs = imgs;
        }
        public List<String> getImgs() {
            return imgs;
        }

        public void setTagTexts(List<String> tagTexts) {
            this.tagTexts = tagTexts;
        }
        public List<String> getTagTexts() {
            return tagTexts;
        }

        public void setComment(int comment) {
            this.comment = comment;
        }
        public int getComment() {
            return comment;
        }

        public boolean isGood() {
            return isGood;
        }

        public void setGood(boolean good) {
            isGood = good;
        }

        @Override
        public String toString() {
            return "ScenicWithPageList{" +
                    "srInfo=" + srInfo +
                    ", imgs=" + imgs +
                    ", tagTexts=" + tagTexts +
                    ", comment=" + comment +
                    ", isGood=" + isGood +
                    '}';
        }
    }

    /**
     * Copyright 2022 bejson.com
     */

    /**
     * Auto-generated: 2022-05-06 17:3:19
     *
     * @author bejson.com (i@bejson.com)
     * @website http://www.bejson.com/java2pojo/
     */
    public class Data {

        private List<ScenicWithPageList> scenicWithPageList;
        public void setScenicWithPageList(List<ScenicWithPageList> scenicWithPageList) {
            this.scenicWithPageList = scenicWithPageList;
        }
        public List<ScenicWithPageList> getScenicWithPageList() {
            return scenicWithPageList;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "scenicWithPageList=" + scenicWithPageList +
                    '}';
        }
    }
}