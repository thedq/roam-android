package com.example.roam.game.qcmountain.control;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.view.MotionEvent;

import com.example.roam.game.app.qcmountain.GameBirdActivity;
import com.example.roam.game.app.qcmountain.GameSecondActivity;
import com.example.roam.game.qcmountain.model.BarOptionSpirit;
import com.example.roam.game.tools.LeftSpirit;
import com.example.roam.game.qcmountain.model.TopBackground;
import com.example.roam.game.qcmountain.model.TopSpirit;
import com.example.roam.game.util.WinBarLayer;
import com.example.roam.ui.activity.MainActivity;

public class GameThirdControl {
    private Context context;
    public boolean isVisible = true;
    private TopBackground topBackground;
    private TopSpirit topSpirit;
    private LeftSpirit leftSpirit;
    private BarOptionSpirit barOptionSpirit;
    private WinBarLayer winBarLayer;
    private static final String TAG= "TAG";
    private boolean canClick = true;

    public GameThirdControl(Context context){
      init(context);
      initData();
    }

    public void init(Context context){
        this.context = context;
        this.topBackground = new TopBackground(context);
        this.topSpirit = new TopSpirit(context);
        this.leftSpirit = new LeftSpirit(context);
        this.barOptionSpirit = new BarOptionSpirit(context);
        this.winBarLayer = new WinBarLayer(context,"青城山");
    }

    public void initData(){
        this.barOptionSpirit.setText("通过小游戏才能获取奖励！");
    }

    public void draw(Canvas canvas){
        topBackground.draw(canvas);
        topSpirit.draw(canvas);
        leftSpirit.draw(canvas);
        barOptionSpirit.draw(canvas);
        if(!canClick){
            winBarLayer.draw(canvas);
        }
    }

    public void handleTouch(MotionEvent event){
        switch ( event.getAction() ){
            case MotionEvent.ACTION_DOWN:
                clickLeft(event);
                if(canClick) {
                    clickTop(event);
                    clickBarOptionYes(event);
                    clickBarOptionNo(event);
                    clickBarOptionDelete(event);
                }else {
                    clickWinTipDelete(event);
                }
                break;
        }
    }

    private void clickWinTipDelete(MotionEvent event){
        if(!canClick){
            winBarLayer.getBarLayer().clickDelete(event);
        }
    }

    public void  clickLeft(MotionEvent event){
        if(  leftSpirit.getRect().contains( (int)event.getX(),(int)event.getY())){
            leftSpirit.finsh(context);
            this.isVisible = false;
            barOptionSpirit.isVisible = false;

        }

    }

    public void clickTop(MotionEvent event){
        if( topSpirit.getRect().contains((int)event.getX(),(int)event.getY())){
            barOptionSpirit.isVisible = true;
        }
    }

    public void clickBarOptionYes(MotionEvent event){
        if( barOptionSpirit.isVisible && barOptionSpirit.getOptionYes().getRect().contains((int) event.getX(),(int) event.getY()) && !barOptionSpirit.isClickThrough() ){
            this.isVisible = false;
            Intent intent = new Intent(context,GameBirdActivity.class);
            leftSpirit.finsh(context);
            context.startActivity(intent);
            barOptionSpirit.setClickThrough(true);
        }
    }

    public void clickBarOptionNo(MotionEvent event){
        if (barOptionSpirit.isVisible && barOptionSpirit.getOptionNo().getRect().contains((int) event.getX(),(int) event.getY())) {
            barOptionSpirit.isVisible = false;
        }
    }

    public void clickBarOptionDelete(MotionEvent event){
        if( barOptionSpirit.isVisible && barOptionSpirit.getBarDeleteSpirit().getRect().contains((int) event.getX(),(int) event.getY())){
            barOptionSpirit.isVisible = false;
        }
    }

    public void setCanClick(boolean b) {
        this.canClick = b;
    }
}
