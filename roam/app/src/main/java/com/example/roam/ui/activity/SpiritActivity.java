package com.example.roam.ui.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.roam.R;
import com.example.roam.api.Api;
import com.example.roam.model.SpiritBean;
import com.example.roam.model.SpiritEntity;
import com.example.roam.model.gson.User;
import com.example.roam.utils.Constans;
import com.example.roam.utils.DBHelper;
import com.example.roam.utils.HttpUtil;
import com.example.roam.utils.StatusBarUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Retrofit;

/**
 * Illustration
 *
 * @author dengqing
 * @time 2021/10/17
 * @function 景点精灵页面，选择景点精灵
 */

public class SpiritActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView Spirit_little_panda, spirit_monkey, spirit_giant_panda, img_back;//景点精灵：猫 狗 鱼
    private Button btn_confirm;//确认按钮
    private TextView tv_spirit_name, tv_spirit_words, tv_spirit_instruction;//景点精灵描述
    private Map<String, String> map_spirit_name = new HashMap<>();//景点精灵名称
    private Map<String, String> map_spirit_words = new HashMap<>();//景点精灵话语
    private Map<String, String> map_spirit_instruction = new HashMap<>();//景点精灵描述
    private int spirit_select = 1;
    private int spirit =R.drawable.spirit_little_panda ;
    private Retrofit retrofit;
    public static final int REQUEST_SHOWUSER = 4;//跳转的用户信息展示界面请求码
    private boolean isUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spirit);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        //初始化组件
        findById();
    }

    //初始化组件
    private void findById() {
        img_back = findViewById(R.id.img_back);
        Spirit_little_panda = findViewById(R.id.img_spirit_little_panda);
        spirit_monkey = findViewById(R.id.img_spirit_monkey);
        spirit_giant_panda = findViewById(R.id.img_spirit_giant_panda);
        btn_confirm = findViewById(R.id.btn_confirm);
        tv_spirit_name = findViewById(R.id.tv_spirit_name);
        tv_spirit_words = findViewById(R.id.tv_spirit_words);
        tv_spirit_instruction = findViewById(R.id.tv_spirit_instruction);
        //初始化景点精灵文字描述
        setMapSpirit();
        Spirit_little_panda.setOnClickListener(this);
        spirit_monkey.setOnClickListener(this);
        spirit_giant_panda.setOnClickListener(this);
        img_back.setOnClickListener(this);
        btn_confirm.setOnClickListener(this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //用户信息展示界面结果返回
        if (requestCode == REQUEST_SHOWUSER && resultCode == RESULT_OK) {

        }
    }

    private void setMapSpirit() {
        for (int i = 1; i <= 3; i++) {
            //获取对应的虚拟人物数据
            SpiritEntity entity = SpiritBean.getSingle("" + i);
            //景点精灵文字描述
            map_spirit_name.put(i + "", entity.getName());
            map_spirit_words.put(i + "", entity.getWords());
            map_spirit_instruction.put(i + "", entity.getInstruction());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_spirit_giant_panda:
                spirit = R.drawable.spirit_giant_panda;
                tv_spirit_name.setText(map_spirit_name.get(1 + ""));
                tv_spirit_words.setText(map_spirit_words.get(1 + ""));
                tv_spirit_instruction.setText(map_spirit_instruction.get(1 + ""));
                Spirit_little_panda.setBackgroundResource(R.drawable.spirit_img_normal);
                spirit_monkey.setBackgroundResource(R.drawable.spirit_img_normal);
                spirit_giant_panda.setBackgroundResource(R.drawable.spirit_img_selected);
                break;
            case R.id.img_spirit_little_panda:
                spirit = R.drawable.spirit_little_panda;
                Spirit_little_panda.setBackgroundResource(R.drawable.spirit_img_selected);
                spirit_monkey.setBackgroundResource(R.drawable.spirit_img_normal);
                spirit_giant_panda.setBackgroundResource(R.drawable.spirit_img_normal);
                tv_spirit_name.setText(map_spirit_name.get(2 + ""));
                tv_spirit_words.setText(map_spirit_words.get(2 + ""));
                tv_spirit_instruction.setText(map_spirit_instruction.get(2 + ""));
                break;
            case R.id.img_spirit_monkey:
                spirit = R.drawable.spirit_monkey;
                tv_spirit_name.setText(map_spirit_name.get(3 + ""));
                tv_spirit_words.setText(map_spirit_words.get(3 + ""));
                tv_spirit_instruction.setText(map_spirit_instruction.get(3 + ""));
                Spirit_little_panda.setBackgroundResource(R.drawable.spirit_img_normal);
                spirit_monkey.setBackgroundResource(R.drawable.spirit_img_selected);
                spirit_giant_panda.setBackgroundResource(R.drawable.spirit_img_normal);
                break;
            case R.id.btn_confirm:
                switch ( spirit ){
                    case R.drawable.spirit_giant_panda:
                        Constans.u_spirit = Constans.PANDA;
                        break;
                    case R.drawable.spirit_little_panda:
                        Constans.u_spirit = Constans.LITTLE_PANDA;
                        break;
                    case R.drawable.spirit_monkey:
                        Constans.u_spirit = Constans.MONKEY;
                        break;
                }
//                    updateUserTable(); // 更新用户数据
                updateUserInfo();  // 向服务器更新用户数据
                break;
            case R.id.img_back:
                finish();
                break;
        }
    }

    public void updateUserTable() {
        DBHelper dbHelper = new DBHelper(SpiritActivity.this, null, null, 1);
        SQLiteDatabase ds = dbHelper.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("u_spirit", spirit + "");
        contentValues.put("spirit_select", spirit_select);
        Constans.u_spirit = spirit + "";
        Constans.spirit_select = spirit_select;
        ds.update("user", contentValues, "u_id = ?", new String[]{Constans.u_id + ""});
    }

    public void updateUserInformation(){
        DBHelper dbHelper = new DBHelper(SpiritActivity.this,null,null,1);
        SQLiteDatabase sq = dbHelper.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("u_id",Constans.u_id);
        contentValues.put("u_username",Constans.u_username);
        contentValues.put("u_character",Constans.u_character);
        contentValues.put("u_spirit",Constans.u_spirit);
        contentValues.put("u_head",Constans.u_head);
        contentValues.put("character_select",Constans.character_select);
        contentValues.put("spirit_select",Constans.spirit_select);
        contentValues.put("head_select",Constans.head_select);
        long user = sq.insert("user", null, contentValues);
        if( user > 0 ){
            Log.i(TAG, "updateUserInformation: 插入数据成功！");
        }
    }

    public void updateUserInfo(){ // 更新用户数据：用户形象
        retrofit = new Retrofit.Builder().baseUrl(HttpUtil.ADDRESS).build();
        retrofit.create(Api.class).updateUser(Constans.u_id,Constans.u_username,Constans.u_password,Constans.u_character,Constans.u_spirit,Constans.u_head,Constans.character_select,Constans.spirit_select,Constans.head_select,HttpUtil.COOKIE).enqueue(new retrofit2.Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!isUpdate) {
                            Intent intent = new Intent(SpiritActivity.this, MainActivity.class);
                            startActivityForResult(intent, REQUEST_SHOWUSER);
                            SpiritActivity.this.finish();
                        }else {
                            SpiritActivity.this.finish();
                        }
                    }
                });
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    private static final String TAG = "SpiritActivity";
}

