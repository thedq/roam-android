package com.example.roam.utils;

import android.util.Log;

public class LogUtils {
    // 我发誓 5月1号前。不管考研的内容!
    // current控制，查看信息等级
    // current 1. 让上线后改变current不显示所有log

    private static int current = 5;
    private static final int DEBUG_LEV = 4;
    private static final int INFO_LEV = 3;
    private static final int WARING_LEV = 2;
    private static final int ERROR_LEV = 1;

    public static void d(Class clazz,String log){
        if( current > DEBUG_LEV ){
            Log.d(clazz.getSimpleName(), log);
        }
    }

    public static void i(Class clazz,String log){
        if( current > INFO_LEV ){
            Log.i(clazz.getSimpleName(), log);
        }
    }

    public static void w(Class clazz,String log){
        if( current > WARING_LEV ){
            Log.w(clazz.getSimpleName(), log );
        }
    }

    public static void e(Class clazz,String log){
        if( current > ERROR_LEV ){
            Log.e(clazz.getSimpleName(), log );
        }
    }
}
