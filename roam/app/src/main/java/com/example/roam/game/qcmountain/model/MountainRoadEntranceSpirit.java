package com.example.roam.game.qcmountain.model;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.example.roam.game.util.PaintUtil;

public class MountainRoadEntranceSpirit extends CSpirit{
    private Paint paint;
    public MountainRoadEntranceSpirit(Context context) {
        super(context, 200, 0, 880, 390);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.BLUE);
        paint.setStrokeWidth(50);
        paint.setStyle(Paint.Style.FILL);
        this.isVisible = true;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if( isVisible ){
            canvas.drawRect( rect, PaintUtil.getPaint());
        }
    }
}
