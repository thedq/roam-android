package com.example.roam.ui.adpter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.roam.ui.fragment.GuideFragment;
import com.example.roam.ui.fragment.IntroduceFragment;
import com.example.roam.ui.fragment.ScenicFragment;

/**
 * Create By Visen
 * Date: 2021/11/8
 */

// 景点论坛下的导航栏
public class ForumOfficeFragmentPaperAdapter extends FragmentPagerAdapter {

    public ForumOfficeFragmentPaperAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch ( position ){
            case 0:
                return new IntroduceFragment();
            case 1:
                return new ScenicFragment();
            case 2:
                return new GuideFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

}
