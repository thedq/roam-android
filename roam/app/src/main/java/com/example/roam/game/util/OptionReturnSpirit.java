package com.example.roam.game.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.example.roam.R;
import com.example.roam.game.emeimountain.game.model.Spirit;

public class OptionReturnSpirit extends Spirit {
    public OptionReturnSpirit(Context context, int left, int top, int right, int bottom) {
        super(context, left, top, right, bottom);
        this.bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.option_return);

    }

    @Override
    public void draw(Canvas canvas) {
        if( isVisible ){
            canvas.drawBitmap(bitmap,null,rect,null);
        }
    }
}
