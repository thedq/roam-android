package com.example.roam.utils;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

public class SizeUtils {
    public static DisplayMetrics scalePx(Activity activity){
        // 以小米9为例子
        // 要：地图自动填满整个手机：
        // 长和长比，宽和宽比。拉伸坐标
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm;
    }

}
