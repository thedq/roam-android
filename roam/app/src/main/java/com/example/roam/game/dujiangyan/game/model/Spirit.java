package com.example.roam.game.dujiangyan.game.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.example.roam.utils.Constans;

public abstract class Spirit {
    /**
     *  x,y,context,Rect,bitmap
     * */
    public Context context;
    public Rect rect;
    public int left,top,right,bottom;
    public int width,height;
    public String text;
    public boolean isVisible ;

    public Bitmap bitmap;
    public Spirit(Context context,int left,int top,int right,int bottom){
        this.context = context;
        this.left = (int)(left* Constans.SCALE_WIDTH);
        this.top = (int)(top* Constans.SCALE_HEIGHT );
        this.right = (int)(right* Constans.SCALE_WIDTH );
        this.bottom = (int)(bottom* Constans.SCALE_HEIGHT );
        this.rect = new Rect(this.left,this.top,this.right,this.bottom);
        this.width = this.right - this.left;
        this.height = this.bottom - this.top;
        this.isVisible = true;
    }

    public abstract void draw(Canvas canvas);

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Rect getRect() {
        return rect;
    }

    public void setRect(Rect rect) {
        this.rect = rect;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    public int getBottom() {
        return bottom;
    }

    public void setBottom(int bottom) {
        this.bottom = bottom;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
