package com.example.roam.view;

import android.widget.ImageView;

/**
 * Create By Visen
 * Date: 2021/11/16
 */
public class Marker {
    private float scaleX , scaleY;
    private ImageView markerView;
    private int imgSrcId;
    private String markerName;

    public Marker() {
    }

    public Marker(float scaleX, float sclaeY, int imgSrcId, String markerName) {
        this.scaleX = scaleX;
        this.scaleY = sclaeY;
        this.imgSrcId = imgSrcId;
        this.markerName = markerName;
    }

    public float getScaleX() {
        return scaleX;
    }

    public void setScaleX(float scaleX) {
        this.scaleX = scaleX;
    }

    public float getScaleY() {
        return scaleY;
    }

    public void setScaleY(float sclaeY) {
        this.scaleY = sclaeY;
    }

    public ImageView getMarkerView() {
        return markerView;
    }

    public void setMarkerView(ImageView markerView) {
        this.markerView = markerView;
    }

    public int getImgSrcId() {
        return imgSrcId;
    }

    public void setImgSrcId(int imgSrcId) {
        this.imgSrcId = imgSrcId;
    }

    public String getMarkerName() {
        return markerName;
    }

    public void setMarkerName(String markerName) {
        this.markerName = markerName;
    }
}
