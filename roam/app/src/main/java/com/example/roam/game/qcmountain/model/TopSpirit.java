package com.example.roam.game.qcmountain.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.util.BitmapUtil;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class TopSpirit extends CSpirit{
    public TopSpirit(Context context) {
        super(context, 183, 1160, 183+701, 1160+400);
        this.isVisible = true;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if( isVisible ) {
            canvas.drawRect( rect, PaintUtil.getPaint());
        }
    }
}
