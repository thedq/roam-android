package com.example.roam.game.qcmountain.bird.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.example.roam.game.util.BitmapUtil;
import com.example.roam.game.util.GameConstant;
// 出现的问题
// 1. 小鸟下降的时候抽搐
// 突然上升50.
// flyLogic 下降speed实50
// 2. 小鸟碰到障碍物就抽搐
// 原因：不清楚
// 解决：线程内设置是否重画

// 1.基本架子搭好后
// 2.游戏要素分析。 小鸟动起来只是小鸟自己上下移动
// 2.2: 小鸟轮播图
// 2.3: 管道的逻辑 速度，最后，新生成，随机高，对象池(不用对象池特别卡)
// 2.4：碰撞

public class Bird extends Spirit{
    private Bitmap[] bitmaps;
    private int state = 2 ; // 小鸟的状态：上升0，下降2。同时控制显示那张图片
    private int speed = 50; // 小鸟自由落体速度。下滑速度.
    private int a = 5;
    private int count = 0;
    private Rect rect;
    public boolean isLife = true; // 小鸟生命
    private int newWidth = 128;
    private int newHeight = 90;
    private boolean isPause;
    public Bird(Context context) {
        super(context);
        bitmaps = new Bitmap[3];
        for (int i = 0; i < bitmaps.length; i++) {
            bitmaps[i] = BitmapUtil.changeBitmapSize(this.context, GameConstant.BIRD_BITMAP[i],newWidth,newHeight);
        }
        rect = new Rect(); // 小鸟的矩形
    }
    @Override
    public void draw(Canvas canvas) {
        if( count == bitmaps.length * 3 ){
            count = 0;
        }
        if( isLife && !isPause) {
            canvas.drawBitmap(bitmaps[(count++) / 3], left, top, null);
        }else {
            canvas.drawBitmap(bitmaps[0],left,top,null);
        }
        flyLogic();
        rect.left = this.left;
        rect.top = this.top;
        rect.right = this.left + newWidth;
        rect.bottom = this.top + newHeight;
    }

    public void setPauseGame(){
        isPause = true;
    }

    public void setResumeGame(){
        isPause = false;
    }


    public void flyLogic(){
        if( this.isLife && !isPause ) {
            switch (state) {
                case 0:
                    if (top > 0) {
                        jiaSuDuUp();
                        top -= speed;
                    }
                    break;
                case 2:
                    if (top < 1600) {
                        top += speed;
                        jiaSuDuDown();
                    }
                    break;
            }
        }
    }
    public void jiaSuDuUp(){
        if( speed > 0){
            speed -= a;
        }else if(speed <= 0){
            state = 2;
        }
    }
    public void jiaSuDuDown(){
        if( speed < 50 ){
            speed += a;
        }
    }
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public Rect getRect() {
        return rect;
    }
    public void setRect(Rect rect) {
        this.rect = rect;
    }
}
