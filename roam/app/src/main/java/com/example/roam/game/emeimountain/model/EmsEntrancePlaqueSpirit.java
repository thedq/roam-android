package com.example.roam.game.emeimountain.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class EmsEntrancePlaqueSpirit extends CSpirit {
    public EmsEntrancePlaqueSpirit(Context context) {
        super(context, 334, 562, 753, 720);
        this.text = GameConstant.EMS_ENTRANCE_PLAQUE_TEXT;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
