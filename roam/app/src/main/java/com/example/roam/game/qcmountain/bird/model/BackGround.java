package com.example.roam.game.qcmountain.bird.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.example.roam.game.util.BitmapUtil;
import com.example.roam.game.util.GameConstant;
import com.example.roam.utils.Constans;

public class BackGround {
    private Context context;
    private Bitmap bitmap;
    private int left , top;

    public BackGround(Context context) {
        this.context = context;
        this.bitmap = BitmapUtil.changeBitmapSize(context, GameConstant.BACKGROUND_BITMAP,Constans.DISPLAY_WIDTH_PX, Constans.DISPLAY_HEIGHT_PX);
    }

    public void draw(Canvas canvas){
        canvas.drawBitmap(bitmap,left,top,null);
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    @Override
    public String toString() {
        return "BackGround{" +
                "left=" + left +
                ", top=" + top +
                '}';
    }
}
