package com.example.roam.game.jiuzhaigou.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class RoadSpirit extends CSpirit {
    public RoadSpirit(Context context) {
        super(context, 0, 1330, 1080, 1793);
        this.text = GameConstant.JIU_ENTRANCE_TEXT;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
