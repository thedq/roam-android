package com.example.roam.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.roam.model.gson.Badges;
import com.example.roam.ui.activity.BaseApplication;

import java.util.ArrayList;

import retrofit2.http.PUT;

public class BadgeUtils {
    private static JsonCacheUtil jsonCacheUtil = new JsonCacheUtil();
    private static final String key = "badges";

    public static void setBadgeIndex(int index){
        Badges badges = jsonCacheUtil.getValue(key, Badges.class);

        if( badges == null ){
            badges = new Badges();
            ArrayList<Integer> badgeArray = new ArrayList<>();
            badges.setBadgeArray(badgeArray);
        }

        if(!badges.getBadgeArray().contains(index)){
            badges.getBadgeArray().add(index);
            jsonCacheUtil.saveCacheWithDuration(key,badges,60*60*24*365);
        }
    }

    public static Badges getBadges(){
        Badges badges = jsonCacheUtil.getValue(key, Badges.class);
        if( badges == null ){
            badges = new Badges();
            ArrayList<Integer> badgeArray = new ArrayList<>();
            badges.setBadgeArray(badgeArray);
        }
        return badges;
    }

}
