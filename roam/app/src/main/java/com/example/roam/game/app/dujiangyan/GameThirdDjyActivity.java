package com.example.roam.game.app.dujiangyan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.roam.R;
import com.example.roam.game.dujiangyan.control.GameThirdControlDJY;
import com.example.roam.game.dujiangyan.view.GameThirdUiDJY;
import com.example.roam.utils.BadgeUtils;
import com.example.roam.utils.StatusBarUtils;

public class GameThirdDjyActivity extends AppCompatActivity {
    private GameThirdUiDJY gameThirdUiDJY;
    private GameThirdControlDJY gameThirdControlDJY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_third_djy);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        Intent intent = getIntent();
        boolean isFinsh = intent.getBooleanExtra("isFinsh", false);
        if( isFinsh ){
            BadgeUtils.setBadgeIndex(5);
        }
        gameThirdUiDJY = this.findViewById(R.id.scene3);
        gameThirdControlDJY = gameThirdUiDJY.getGameThirdControlDJY();
        gameThirdControlDJY.setFinsh(isFinsh);
        Log.i("TAG", "onCreate: badges:"+ BadgeUtils.getBadges());
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameThirdControlDJY.isVisible = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameThirdControlDJY.isVisible = false;
    }
}