package com.example.roam.model.gson;

import java.util.ArrayList;

public class Badges {
    private ArrayList<Integer> badgeArray;

    public ArrayList<Integer> getBadgeArray() {
        return badgeArray;
    }

    public void setBadgeArray(ArrayList<Integer> badgeArray) {
        this.badgeArray = badgeArray;
    }

    @Override
    public String toString() {
        return "Badges{" +
                "badgeArray=" + badgeArray +
                '}';
    }
}
