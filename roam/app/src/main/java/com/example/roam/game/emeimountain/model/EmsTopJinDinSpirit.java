package com.example.roam.game.emeimountain.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.PaintUtil;

public class EmsTopJinDinSpirit extends CSpirit {

    public EmsTopJinDinSpirit(Context context) {
        super(context, 786, 107, 1067, 1392);
        this.text = "普贤菩萨是中国佛教的四大菩萨之一，象征着理德、行德，与象征着智德、正德的文殊菩萨相对应，同为释迦牟尼佛的左、右胁侍。";
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }


}
