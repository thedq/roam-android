package com.example.roam.game.emeimountain.game.model;

import android.content.Context;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import java.util.ArrayList;

import javax.security.auth.login.LoginException;

public class MSpiritPool {

    public ArrayList<MSpirit> mSpiritArray = new ArrayList<>();
    public ArrayList<MSpirit> mSpiritArrayEvent = new ArrayList<>();
    private Context context;
    private int maxSize = 25;

    public MSpiritPool(Context context) {
        this.context = context;
        for (int i = 0; i < maxSize; i++) {
            MSpirit mSpirit = new MSpirit(context);
            mSpiritArray.add(mSpirit);
            mSpiritArrayEvent.add(mSpirit);
        }
    }

    public MSpirit getMSpirit(){
        int size = mSpiritArray.size();
        if( size > 0 ){
            return mSpiritArray.remove(0);
        }else {
            return new MSpirit(context);
        }
    }

    public void setmSpirit(MSpirit mSpirit){
        mSpiritArray.add(mSpirit);
    }

    public ArrayList<MSpirit> getmSpiritArrayEvent() {
        return mSpiritArrayEvent;
    }
}
