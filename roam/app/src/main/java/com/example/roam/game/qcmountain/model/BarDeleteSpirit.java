package com.example.roam.game.qcmountain.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.util.BitmapUtil;
import com.example.roam.game.util.GameConstant;

public class BarDeleteSpirit extends Spirit{

    public BarDeleteSpirit(Context context, int left, int top, int right, int bottom) {
        super(context, left, top, right, bottom);
        this.bitmap = BitmapUtil.changeBitmapSize(context, GameConstant.BAR_DELETE,width,height);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawBitmap(bitmap,null,rect,null);
    }
}
