package com.example.roam.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.roam.R;
import com.example.roam.ui.adpter.PersonalBadgeAdapter;
import com.example.roam.utils.Constans;
import com.example.roam.utils.StatusBarUtils;
import com.google.android.material.imageview.ShapeableImageView;

import java.util.ArrayList;
import java.util.HashMap;

public class PersonalBadgeActivity extends AppCompatActivity {
    private ImageView img_back;
    private GridView gv;
    private PersonalBadgeAdapter personalBadgeAdapter;
    private ArrayList<HashMap<String,String>> dataArray = new ArrayList<>();
    private String [] str = {"初旅蜀道","遍历蜀道","蜀道如故","大堰凝慧","福泽天国","江堰恒久","佛坊兀出","佛光初现","山陡岩峭","山绝色寰","峨眉巍峨","山危云眉","叠瀑濑清","九寨瑞福","水灵地岁"};
    private int [] badges = {R.drawable.q_a,R.drawable.q_b,R.drawable.q_c,R.drawable.d_a,R.drawable.d_b,R.drawable.d_c,R.drawable.e_a,R.drawable.e_b,R.drawable.e_c,R.drawable.l_a,R.drawable.l_b,R.drawable.l_c,R.drawable.j_a,R.drawable.j_b,R.drawable.j_c};
    private ShapeableImageView img_head;
    private TextView txt_uname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_badge);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        iniView();
        iniData();
        iniEvent();
    }

    public void iniView(){
        img_back = this.findViewById(R.id.img_back);
        gv = this.findViewById(R.id.gv);
        img_head = this.findViewById(R.id.img_head);
        txt_uname = this.findViewById(R.id.txt_uname);
    }

    public void iniData(){
        for (int i = 0; i < badges.length; i++) {
            HashMap<String,String> hashMap = new HashMap<>();
            hashMap.put("txt_content",str[i]);
            hashMap.put("img_badge",badges[i]+"");
            dataArray.add(hashMap);
        }
        personalBadgeAdapter = new PersonalBadgeAdapter(this,dataArray);
        gv.setAdapter(personalBadgeAdapter);
        txt_uname.setText(Constans.u_username);
        Glide.with(PersonalBadgeActivity.this).load(Constans.u_head).into(img_head);
    }

    public void iniEvent(){
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PersonalBadgeActivity.this.finish();
            }
        });
    }


}