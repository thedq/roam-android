package com.example.roam.game.dujiangyan.model;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.example.roam.R;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.GameConstant;

public class SceneDuJiangYan extends Spirit {
    public SceneDuJiangYan(Context context) {
        // 1794
        super(context, 0, 0, 1080, 1794);
        bitmap = BitmapFactory.decodeResource(context.getResources(), GameConstant.SCENE2_DU_JIAN_YAN);
    }

    @Override
    public void draw(Canvas canvas) {
        if( isVisible ){
            canvas.drawBitmap(bitmap,null,rect,null);
        }
    }
}
