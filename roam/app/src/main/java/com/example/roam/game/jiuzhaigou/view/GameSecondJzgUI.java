package com.example.roam.game.jiuzhaigou.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import com.example.roam.game.jiuzhaigou.control.GameFirstJzgControl;
import com.example.roam.game.jiuzhaigou.control.GameSecondJzgControl;

public class GameSecondJzgUI extends SurfaceView implements SurfaceHolder.Callback {

    private GameSecondJzgControl gameSecondJzgControl;
    private SurfaceHolder surfaceHolder;

    public GameSecondJzgUI(Context context) {
        super(context);
        initGame();
    }

    public GameSecondJzgUI(Context context, AttributeSet attrs) {
        super(context, attrs);
        initGame();
    }

    public GameSecondJzgUI(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initGame();
    }

    public void initGame(){
        surfaceHolder = this.getHolder();
        surfaceHolder.addCallback(this);
        gameSecondJzgControl = new GameSecondJzgControl(getContext());
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        Runner runner = new Runner();
        runner.start();
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {

    }

    class Runner extends Thread{
        @Override
        public void run() {

            while (gameSecondJzgControl.isVisible){
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                draw();
            }
        }
    }

    public void draw(){
        Canvas canvas = surfaceHolder.lockCanvas();
        gameSecondJzgControl.draw(canvas);
        surfaceHolder.unlockCanvasAndPost(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gameSecondJzgControl.onTouch(event);
        return true;
    }

    public GameSecondJzgControl getGameSecondJzgControl() {
        return gameSecondJzgControl;
    }
}
