package com.example.roam.game.jiuzhaigou.control;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.EditText;

import com.example.roam.game.app.emeimountain.GameSecondEmsActivity;
import com.example.roam.game.app.jiuzhaigou.GameFirstJzgActvity;
import com.example.roam.game.app.jiuzhaigou.GameSecondJzgActvity;
import com.example.roam.game.jiuzhaigou.model.JiuEntranceSpirit;
import com.example.roam.game.jiuzhaigou.model.JiuMountainSpirit;
import com.example.roam.game.jiuzhaigou.model.RoadSpirit;
import com.example.roam.game.jiuzhaigou.model.SceneEntrance;
import com.example.roam.game.qcmountain.model.BarOptionSpirit;
import com.example.roam.game.tools.BarLayer;
import com.example.roam.game.tools.CatchBadgeSpirit;
import com.example.roam.game.tools.LeftSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.utils.BadgeUtils;

public class GameFirstJzgControl {
    private Context context;
    private SceneEntrance sceneEntrance;
    private RoadSpirit roadSpirit;
    private JiuEntranceSpirit jiuEntranceSpirit;
    private JiuMountainSpirit jiuMountainSpirit;
    private BarLayer barLayer;
    private BarOptionSpirit barOptionSpirit;
    private CatchBadgeSpirit catchBadgeSpirit;
    private LeftSpirit leftSpirit;
    public boolean isVisible = true;

    public GameFirstJzgControl(Context context){
        this.context = context;
        sceneEntrance = new SceneEntrance(context);
        roadSpirit = new RoadSpirit(context);
        barOptionSpirit = new BarOptionSpirit(context);
        jiuEntranceSpirit = new JiuEntranceSpirit(context);
        jiuMountainSpirit = new JiuMountainSpirit(context);
        catchBadgeSpirit = new CatchBadgeSpirit(context,GameConstant.JZG_BADGE_A);
        barLayer = new BarLayer(context);
        leftSpirit = new LeftSpirit(context);
    }

    public void draw(Canvas canvas){
        if( isVisible ){
            sceneEntrance.draw(canvas);
            roadSpirit.draw(canvas);
            jiuEntranceSpirit.draw(canvas);
            jiuMountainSpirit.draw(canvas);
            catchBadgeSpirit.draw(canvas);
            leftSpirit.draw(canvas);
            barOptionSpirit.draw(canvas);
            barLayer.draw(canvas);
        }
    }

    public void onTouch(MotionEvent event){
        switch ( event.getAction() ){
            case MotionEvent.ACTION_DOWN:
                clickSceneEntrance(event);
                clickRoad(event);
                clickJiuEntrance(event);
                clickJiuMountain(event);
                clickDelete(event);
                clickLeftSpirit(event);
                clickOptionCancel(event);
                clickOptionDelete(event);
                clickOptionCertain(event);

                break;
        }
    }

    private void clickLeftSpirit(MotionEvent event){
        if( leftSpirit.getRect().contains((int) event.getX(),(int) event.getY())){
            leftSpirit.finsh(context);
        }
    }

    private void clickRoad(MotionEvent event){
//        if (roadSpirit.getRect().contains((int) event.getX(),(int) event.getY())) {
//            barLayer.isVisible = true;
//            barLayer.text = roadSpirit.getText();
//        }
        roadSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickJiuEntrance(MotionEvent event){
//        if (jiuEntranceSpirit.getRect().contains((int) event.getX(),(int) event.getY())) {
//            barOptionSpirit.isVisible = true;
//            barOptionSpirit.text = jiuMountainSpirit.getText();
//        }
        jiuEntranceSpirit.clickSpiritOption(event,barLayer,barOptionSpirit);
    }

    private void clickOptionCertain(MotionEvent event){
        if (barOptionSpirit.isVisible && barOptionSpirit.getOptionYes().getRect().contains((int) event.getX(),(int) event.getY()) && !barOptionSpirit.isClickThrough  ) {
            waitToStartActivity();
            barOptionSpirit.setClickThrough(true);
        }
    }

    public void waitToStartActivity(){  // 2
        if (!BadgeUtils.getBadges().getBadgeArray().contains(12)) {
            catchBadgeSpirit.isVisible = true;
            catchBadgeSpirit.setStartAnimation(true); // 启动动画
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            BadgeUtils.setBadgeIndex(12);
                            Intent intent = new Intent(context, GameSecondJzgActvity.class);
                            context.startActivity(intent);
                            leftSpirit.finsh(context);
                        }
                    });
                }
            }).start();
        }else {
            BadgeUtils.setBadgeIndex(12);
            Intent intent = new Intent(context, GameSecondJzgActvity.class);
            leftSpirit.finsh(context);
            context.startActivity(intent);
        }

    }

    private void clickOptionCancel(MotionEvent event){
        if (barOptionSpirit.getOptionNo().getRect().contains((int) event.getX(),(int) event.getY())) {
            barOptionSpirit.isVisible = false;
        }
    }

    private void clickOptionDelete(MotionEvent event){
        if (barOptionSpirit.getBarDeleteSpirit().getRect().contains((int) event.getX(),(int) event.getY())) {
            barOptionSpirit.isVisible = false;
        }
    }

    private void clickJiuMountain(MotionEvent event){
//        if(jiuMountainSpirit.getRect().contains((int) event.getX(),(int) event.getY())){
//            barLayer.isVisible = true;
//            barLayer.text = jiuMountainSpirit.getText();
//        }
        jiuMountainSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickDelete(MotionEvent event){
        if (barLayer.getBarDeleteSpirit().getRect().contains((int) event.getX(),(int) event.getY())) {
            barLayer.isVisible = false;
        }
    }

    private String TAG= "TAG";
    public void clickSceneEntrance(MotionEvent event){
        if (sceneEntrance.getRect().contains((int) event.getX(),(int) event.getY())) {
            Log.i(TAG, "clickSceneEntrance: x:"+event.getX()+" y:"+ event.getY());
        }
    }
}
