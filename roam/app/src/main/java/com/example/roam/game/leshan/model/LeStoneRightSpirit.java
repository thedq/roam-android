package com.example.roam.game.leshan.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class LeStoneRightSpirit extends CSpirit {
    public LeStoneRightSpirit(Context context) {
        super(context, 880, 392, 1080, 843);
        this.text = GameConstant.LE_STONE_RIGHT_TEXT;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
