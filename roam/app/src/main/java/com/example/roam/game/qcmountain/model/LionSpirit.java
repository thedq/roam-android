package com.example.roam.game.qcmountain.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.example.roam.game.util.BitmapUtil;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

// 我保证 20号问老多要图
public class LionSpirit extends CSpirit{
    private int type;
    public LionSpirit(Context context, Rect dstRect, int type) {
        super(context, dstRect.left, dstRect.top, dstRect.right, dstRect.bottom); // dstRect x y 0,1200 , 198,295
        this.type = type;
        if (type == 1) {
            this.text = GameConstant.LION_LEFT_TEXT;
        }else {
            this.text = GameConstant.LION_RIGHT_TEXT;
        }
        this.isVisible = true;
    }
    // 创建model，设置text，
    // 给barOptionSpirit设置本文
    // 先了解地点，确定文本，写入

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if( isVisible ) {
            switch (type) {
                case 1:
                    canvas.drawRect( rect, PaintUtil.getPaint());
                    break;
                case 2:
                    canvas.drawRect( rect, PaintUtil.getPaint());
                    break;
            }
        }
    }
}
