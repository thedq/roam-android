package com.example.roam.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ScenicEntity implements Serializable {
    @SerializedName("scenicId")
    private int scenic_id; //景点编号
    @SerializedName("scenicTitle")
    private String scenic_title; //景点标题
    @SerializedName("scenicIntor")
    private String scenic_intor; //景点介绍
    @SerializedName("scenicIntorImg")
    private String scenic_intor_img; //景点图片
    @SerializedName("scenicLocal")
    private String scenic_local; //民俗科普
    @SerializedName("scenicLocalImg")
    private String scenic_local_img; //民俗图片
    @SerializedName("scenicTrave")
    private String scenic_trave; //旅游指南
    @SerializedName("scenicTraveImg")
    private String scenic_trave_img; //旅游指南图片
    @SerializedName("scenicGood")
    private int scenic_good; //景点点赞数
    @SerializedName("collectImg")
    private String collect_img; //收藏品商品地址

    public ScenicEntity() {
    }

    public ScenicEntity(int scenic_id, String scenic_title, String scenic_intor, String scenic_intor_img, String scenic_local, String scenic_local_img, String scenic_trave, String scenic_trave_img, int scenic_good, String collect_img) {
        this.scenic_id = scenic_id;
        this.scenic_title = scenic_title;
        this.scenic_intor = scenic_intor;
        this.scenic_intor_img = scenic_intor_img;
        this.scenic_local = scenic_local;
        this.scenic_local_img = scenic_local_img;
        this.scenic_trave = scenic_trave;
        this.scenic_trave_img = scenic_trave_img;
        this.scenic_good = scenic_good;
        this.collect_img = collect_img;
    }

    @Override
    public String toString() {
        return "ScenicEntity{" +
                "scenic_id=" + scenic_id +
                ", scenic_title='" + scenic_title + '\'' +
                ", scenic_intor='" + scenic_intor + '\'' +
                ", scenic_intor_img='" + scenic_intor_img + '\'' +
                ", scenic_local='" + scenic_local + '\'' +
                ", scenic_local_img='" + scenic_local_img + '\'' +
                ", scenic_trave='" + scenic_trave + '\'' +
                ", scenic_trave_img='" + scenic_trave_img + '\'' +
                ", scenic_good=" + scenic_good +
                ", collect_img='" + collect_img + '\'' +
                '}';
    }

    public int getScenic_id() {
        return scenic_id;
    }

    public void setScenic_id(int scenic_id) {
        this.scenic_id = scenic_id;
    }

    public String getScenic_title() {
        return scenic_title;
    }

    public void setScenic_title(String scenic_title) {
        this.scenic_title = scenic_title;
    }

    public String getScenic_intor() {
        return scenic_intor;
    }

    public void setScenic_intor(String scenic_intor) {
        this.scenic_intor = scenic_intor;
    }

    public String getScenic_intor_img() {
        return scenic_intor_img;
    }

    public void setScenic_intor_img(String scenic_intor_img) {
        this.scenic_intor_img = scenic_intor_img;
    }

    public String getScenic_local() {
        return scenic_local;
    }

    public void setScenic_local(String scenic_local) {
        this.scenic_local = scenic_local;
    }

    public String getScenic_local_img() {
        return scenic_local_img;
    }

    public void setScenic_local_img(String scenic_local_img) {
        this.scenic_local_img = scenic_local_img;
    }

    public String getScenic_trave() {
        return scenic_trave;
    }

    public void setScenic_trave(String scenic_trave) {
        this.scenic_trave = scenic_trave;
    }

    public String getScenic_trave_img() {
        return scenic_trave_img;
    }

    public void setScenic_trave_img(String scenic_trave_img) {
        this.scenic_trave_img = scenic_trave_img;
    }

    public int getScenic_good() {
        return scenic_good;
    }

    public void setScenic_good(int scenic_good) {
        this.scenic_good = scenic_good;
    }

    public String getCollect_img() {
        return collect_img;
    }

    public void setCollect_img(String collect_img) {
        this.collect_img = collect_img;
    }
}
