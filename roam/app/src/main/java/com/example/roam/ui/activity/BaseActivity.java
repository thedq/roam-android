package com.example.roam.ui.activity;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.example.roam.R;

public abstract class BaseActivity extends AppCompatActivity {
    private FrameLayout frameLayout;
    public State currentState = State.NONE;
    private View errorView;
    private View loadingView;
    private View successView;
    private View emptyView;
    private View noneView;

    public enum State{
        LOADING,ERROR,SUCCESS,EMPTY,NONE
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        frameLayout = this.findViewById(R.id.base_container);
        loadStateView();

        /**
         *  SearchActivity要改：
         *      FrameLayout添加各个布局
         *      显示的时候根据 setUpState控制什么时候显示什么
         *
         * */

    }

    public void loadStateView(){
        errorView = LayoutInflater.from(this).inflate(R.layout.activity_error,null,false);
        frameLayout.addView(errorView);

        loadingView = LayoutInflater.from(this).inflate(R.layout.activity_loading,null,false);
        frameLayout.addView(loadingView);

        successView = LayoutInflater.from(this).inflate(getSuccessViewId(),null,false);
        frameLayout.addView(successView);

        emptyView = LayoutInflater.from(this).inflate(R.layout.activity_empty,null,false);
        frameLayout.addView(emptyView);

        noneView = LayoutInflater.from(this).inflate(R.layout.activity_none,null,false);
        frameLayout.addView(noneView);

        setState(State.NONE);
    }

    public void setState(State state) {
        this.currentState = state;
        if (currentState == State.SUCCESS) {
            successView.setVisibility(View.VISIBLE);
        } else {
            successView.setVisibility(View.GONE);
        }

        if (currentState == State.LOADING) {
            loadingView.setVisibility(View.VISIBLE);
        } else {
            loadingView.setVisibility(View.GONE);
        }

        if (currentState == State.ERROR) {
            errorView.setVisibility(View.VISIBLE);
        } else {
            errorView.setVisibility(View.GONE);
        }

        if(currentState == State.EMPTY){
            emptyView.setVisibility(View.VISIBLE);
        }else {
            emptyView.setVisibility(View.GONE);
        }

        if(currentState == State.NONE){
            noneView.setVisibility(View.VISIBLE);
        }else {
            noneView.setVisibility(View.GONE);
        }
    }

    public abstract int getSuccessViewId();

    public void onLoading(){
        setState(State.LOADING);
    }

    public void onError(){
        setState(State.ERROR);
    }

    public void onSuccess(){
        setState(State.SUCCESS);
    }

    public void onEmpty(){
        setState(State.EMPTY);
    }

    public void onNone(){
        setState(State.NONE);
    }

}