package com.example.roam.game.emeimountain.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class EmsEntranceBottomSpirit extends CSpirit {
    public EmsEntranceBottomSpirit(Context context) {
        super(context,200,1318,880,1535);
        this.text = GameConstant.EMS_ENTRANCE_BOTTOM_TEXT;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }
}
