package com.example.roam.game.qcmountain.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import com.example.roam.game.qcmountain.control.GameFirstControl;

public class GameFirstUI extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder surfaceHolder;
    private GameFirstControl gameFirstControl;
    /**
     *  1. 继承 SurfaceView 实现 SurfaceHolder.Callback
     *      需要重写三个构造方法，直接使用控件
     *      继承取画布
     *      绑定得画笔
     *      lock 与 unlock 提交
     *      （在三个方法内调用）
     *
     *
     * */

    public GameFirstUI(Context context) {
        super(context);
        init();
    }

    public GameFirstUI(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GameFirstUI(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init(){
        surfaceHolder = this.getHolder();
        surfaceHolder.addCallback(this);
        gameFirstControl = new GameFirstControl(this.getContext());
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        Runner runner = new Runner();
        runner.start();
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {

    }

    class Runner extends Thread{
        @Override
        public void run() {
            while ( gameFirstControl.isVisible ){
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                draw();
            }
        }
    }

    public void draw(){
        Canvas canvas = surfaceHolder.lockCanvas();
        gameFirstControl.draw(canvas);
        surfaceHolder.unlockCanvasAndPost(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gameFirstControl.handleTouch(event);
        return true;
    }

    public GameFirstControl getGameFirstControl() {
        return gameFirstControl;
    }
}
