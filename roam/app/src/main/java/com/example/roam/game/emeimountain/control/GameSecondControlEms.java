package com.example.roam.game.emeimountain.control;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import com.example.roam.game.app.emeimountain.GameFirstEmsActivity;
import com.example.roam.game.app.emeimountain.GameSecondEmsActivity;
import com.example.roam.game.app.emeimountain.GameThirdEmsActivity;
import com.example.roam.game.app.question.GameQuestionActivity;
import com.example.roam.game.emeimountain.model.EmsWanEntranceSpirit;
import com.example.roam.game.emeimountain.model.EmsWanPlaque2Spirit;
import com.example.roam.game.emeimountain.model.EmsWanPlaqueSpirit;
import com.example.roam.game.emeimountain.model.EmsWanRoadSpirit;
import com.example.roam.game.emeimountain.model.SceneAncientTemple;
import com.example.roam.game.emeimountain.view.GameThirdUiEms;
import com.example.roam.game.qcmountain.model.BarOptionSpirit;
import com.example.roam.game.qcmountain.model.layer2.TaoistSpirit;
import com.example.roam.game.tools.BarLayer;
import com.example.roam.game.tools.LeftSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.TaoistLayer;

public class GameSecondControlEms {
    private Context context;
    private SceneAncientTemple sceneAncientTemple;
    private LeftSpirit leftSpirit;
    private BarLayer barLayer;
    private BarOptionSpirit barOptionSpirit;
    private EmsWanPlaqueSpirit emsWanPlaqueSpirit;
    private EmsWanRoadSpirit emsWanRoadSpirit;
    private EmsWanPlaque2Spirit emsWanPlaque2Spirit;
    private EmsWanEntranceSpirit emsWanEntranceSpirit;
    private TaoistSpirit taoistSpirit;
    private TaoistLayer taoistLayer;
    public boolean isVisible = true;

    public GameSecondControlEms(Context context){
        this.context = context;
        sceneAncientTemple = new SceneAncientTemple(context);
        leftSpirit = new LeftSpirit(context);
        barLayer = new BarLayer(context);
        barOptionSpirit = new BarOptionSpirit(context);
        emsWanEntranceSpirit = new EmsWanEntranceSpirit(context);
        emsWanPlaqueSpirit = new EmsWanPlaqueSpirit(context);
        emsWanPlaque2Spirit = new EmsWanPlaque2Spirit(context);
        emsWanRoadSpirit = new EmsWanRoadSpirit(context);
        taoistSpirit = new TaoistSpirit(context);
        taoistSpirit.setTaoistRectLeft(795);
        taoistLayer = new TaoistLayer(context,taoistSpirit,GameConstant.EMS_TAOIST_LAYER);
    }

    public void draw(Canvas canvas){
        if( isVisible ){
            sceneAncientTemple.draw(canvas);
            leftSpirit.draw(canvas);
            emsWanRoadSpirit.draw(canvas);
            emsWanPlaque2Spirit.draw(canvas);
            emsWanPlaqueSpirit.draw(canvas);
            emsWanEntranceSpirit.draw(canvas);
            barLayer.draw(canvas);
            taoistSpirit.draw(canvas);
            taoistLayer.draw(canvas);
            barOptionSpirit.draw(canvas);
        }
    }

    public void onTouch(MotionEvent event){
        switch ( event.getAction() ){
            case MotionEvent.ACTION_DOWN:
                if(!taoistLayer.isClick()) {
                    clickTaoist(event);
                    clickSceneAncientTemple(event);
                    clickLeftSpirit(event);
                    clickEmsEntrance(event);
                    clickEmsPla1(event);
                    clickEmsPla2(event);
                    clickEmsRoad(event);
                    clickDelete(event);
                }else {
                    clickOptionCancel(event);
                    clickOptionCertain(event);
                    clickOptionDelete(event);
                    clickTaoistLayer(event);
                }
                break;
        }
    }

    private void clickTaoist(MotionEvent event){
        if(taoistSpirit.isVisible && taoistSpirit.getRect().contains((int) event.getX(),(int) event.getY())  ){
            taoistLayer.isVisible = true;
            taoistLayer.setClick(true);
            taoistSpirit.isVisible = false;
        }
    }

    private void clickTaoistLayer(MotionEvent event){
        taoistLayer.clickDialogSpirit(event, GameConstant.EMS_TAOIST_LAYER);
        taoistLayer.clickOptionQuestion(event,barOptionSpirit);
        taoistLayer.clickOptionCancel(event);
    }

    private void clickEmsRoad(MotionEvent event){
        emsWanRoadSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickEmsEntrance(MotionEvent event){
        emsWanEntranceSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickEmsPla1(MotionEvent event){
        emsWanPlaqueSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickEmsPla2(MotionEvent event){
        emsWanPlaque2Spirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickDelete(MotionEvent event){
        barLayer.clickDelete(event);
    }

    private void clickOptionCertain(MotionEvent event){
        if (barOptionSpirit.isVisible && barOptionSpirit.getOptionYes().getRect().contains((int) event.getX(),(int) event.getY()) && !barOptionSpirit.isClickThrough() ) {
            Intent intent = new Intent(context, GameQuestionActivity.class);
            intent.putExtra("gameIndex",4);
            leftSpirit.finsh(context);
            context.startActivity(intent);
            barOptionSpirit.setClickThrough(true);
            /**
             * 对照图片资源
             * 放入drawable
             * GameUtils加入索引
             * 创建代码类
             * 复制代码
             * 稍微改动
             * 跳转
             * 解锁徽章
             *
             * */
        }
    }

    private void clickOptionCancel(MotionEvent event){
        barOptionSpirit.clickOptionCancel(event);
    }

    private void clickOptionDelete(MotionEvent event){
        barOptionSpirit.clickOptionDelete(event);
    }

    String TAG = "TAG";
    public void clickSceneAncientTemple(MotionEvent event){
        if (sceneAncientTemple.getRect().contains((int) event.getX(),(int) event.getY())) {
            Log.i(TAG, "clickSceneAncientTemple: x:"+event.getX()+" y:"+event.getY());
        }
    }

    public void clickLeftSpirit(MotionEvent event){
        if (leftSpirit.getRect().contains((int) event.getX(),(int) event.getY())) {
            leftSpirit.finsh(context);
        }
    }
}
