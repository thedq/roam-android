package com.example.roam.game.leshan.control;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import com.example.roam.game.app.jiuzhaigou.GameSecondJzgActvity;
import com.example.roam.game.app.leshan.GameSecondLsActivity;
import com.example.roam.game.leshan.model.LeBarSpirit;
import com.example.roam.game.leshan.model.LeDoorSpirit;
import com.example.roam.game.leshan.model.LeSignatureSpirit;
import com.example.roam.game.leshan.model.SceneEntrance;
import com.example.roam.game.qcmountain.control.GameFirstControl;
import com.example.roam.game.qcmountain.model.BarOptionSpirit;
import com.example.roam.game.tools.BarLayer;
import com.example.roam.game.tools.CatchBadgeSpirit;
import com.example.roam.game.tools.LeftSpirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.utils.BadgeUtils;

public class GameFirstLsControl {
    private Context context;
    private SceneEntrance sceneEntrance;
    private LeBarSpirit leBarSpirit;
    private BarLayer barLayer;
    private BarOptionSpirit barOptionSpirit;
    private LeDoorSpirit leDoorSpirit;
    private LeSignatureSpirit leSignatureSpirit;
    private CatchBadgeSpirit catchBadgeSpirit;
    private LeftSpirit leftSpirit;
    public boolean isVisible = true;

    public GameFirstLsControl(Context context){
        this.context = context;
        sceneEntrance = new SceneEntrance(context);
        leBarSpirit = new LeBarSpirit(context);
        barLayer = new BarLayer(context);
        barOptionSpirit = new BarOptionSpirit(context);
        leDoorSpirit = new LeDoorSpirit(context);
        leSignatureSpirit = new LeSignatureSpirit(context);
        catchBadgeSpirit = new CatchBadgeSpirit(context, GameConstant.LS_BADGE_A);
        leftSpirit = new LeftSpirit(context);
    }

    public void draw(Canvas canvas){
        if(isVisible){
            sceneEntrance.draw(canvas);
            leBarSpirit.draw(canvas);
            leDoorSpirit.draw(canvas);
            leSignatureSpirit.draw(canvas);
            leftSpirit.draw(canvas);
            barOptionSpirit.draw(canvas);
            barLayer.draw(canvas);
            catchBadgeSpirit.draw(canvas);
        }
    }

    public void onTouch(MotionEvent event){
        switch ( event.getAction() ){
            case MotionEvent.ACTION_DOWN:
                clickSceneEntrance(event);
                clickLeBar(event);
                clickDoor(event);
                clickLeSin(event);
                clickOptionCancel(event);
                clickOptionCertain(event);
                clickOptionDelete(event);
                clickDelete(event);
                clickLeft(event);
                break;
        }
    }

    private void clickLeft(MotionEvent event){
        if(leftSpirit.getRect().contains((int) event.getX(),(int) event.getY())){
            leftSpirit.finsh(context);
        }
    }

    private void clickLeSin(MotionEvent event){
        leSignatureSpirit.clickSpiritText(event,barLayer,barOptionSpirit);
    }

    private void clickDoor(MotionEvent event){
        if (!barLayer.isVisible && leDoorSpirit.getRect().contains((int) event.getX(),(int) event.getY())) {
            barOptionSpirit.text = leDoorSpirit.getText();
            barOptionSpirit.isVisible = true;
        }
    }

    private void clickOptionCertain(MotionEvent event){
        if (barOptionSpirit.getOptionYes().getRect().contains((int) event.getX(),(int) event.getY()) && !barOptionSpirit.isClickThrough()) {
            waitToStartActivity();
            barOptionSpirit.setClickThrough(true);
        }
    }

    public void waitToStartActivity(){  // 2
        if (!BadgeUtils.getBadges().getBadgeArray().contains(9)) {
            catchBadgeSpirit.isVisible = true;
            catchBadgeSpirit.setStartAnimation(true); // 启动动画
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(context, GameSecondLsActivity.class);
                            context.startActivity(intent);
                            barOptionSpirit.isVisible = false;
                            BadgeUtils.setBadgeIndex(9);
                            leftSpirit.finsh(context);
                        }
                    });
                }
            }).start();

        }else {
            Intent intent = new Intent(context, GameSecondLsActivity.class);
            leftSpirit.finsh(context);
            context.startActivity(intent);
            barOptionSpirit.isVisible = false;
            BadgeUtils.setBadgeIndex(9);
        }

    }

    private void clickOptionCancel(MotionEvent event){
        if(barOptionSpirit.getOptionNo().getRect().contains((int) event.getX(),(int) event.getY())){
            barOptionSpirit.isVisible = false;
        }
    }

    private void clickOptionDelete(MotionEvent event){
        if (barOptionSpirit.getBarDeleteSpirit().getRect().contains((int) event.getX(),(int) event.getY())) {
            barOptionSpirit.isVisible = false;
        }
    }

    private void clickLeBar(MotionEvent event){
        if (!barOptionSpirit.isVisible && leBarSpirit.getRect().contains((int) event.getX(),(int) event.getY())) {
            barLayer.isVisible = true;
            barLayer.text = leBarSpirit.getText();
        }
    }

    private void clickDelete(MotionEvent event){
        // 所有都可以点击
        // 那么点击的时候，所有都不可点击。所以 1.不搞这个，clickBar放在最后、 2. 当前页面isCLick
        barLayer.clickDelete(event);
    }

    String TAG = "TAG";
    public void clickSceneEntrance(MotionEvent event){
        if (sceneEntrance.getRect().contains((int) event.getX(),(int) event.getY())) {
            Log.i(TAG, "clickSceneEntrance: x:"+event.getX()+" y:"+event.getY());
        }
    }
}
