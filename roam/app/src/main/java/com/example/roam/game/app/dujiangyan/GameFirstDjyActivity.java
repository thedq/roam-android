package com.example.roam.game.app.dujiangyan;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.roam.R;
import com.example.roam.game.dujiangyan.control.GameFirstControlDJY;
import com.example.roam.game.dujiangyan.view.GameFirstUiDJY;
import com.example.roam.utils.StatusBarUtils;

public class GameFirstDjyActivity extends AppCompatActivity {
    private GameFirstUiDJY gameFirstUiDJY;
    private GameFirstControlDJY gameFirstControlDJY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_first_djy);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        gameFirstUiDJY = this.findViewById(R.id.scene1);
        gameFirstControlDJY = gameFirstUiDJY.getGameFirstControlDJY();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameFirstControlDJY.isVisible = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameFirstControlDJY.isVisible = true;
    }
}