package com.example.roam.game.emeimountain.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.GameConstant;

public class SceneGlodTop extends Spirit {

    public SceneGlodTop(Context context) {
        super(context, 0, 0, 1080, 1794);
        this.bitmap = BitmapFactory.decodeResource(context.getResources(), GameConstant.SCENE3_GLAD_TOP);
    }

    @Override
    public void draw(Canvas canvas) {
        if( isVisible ){
            canvas.drawBitmap(bitmap,null,rect,null);
        }
    }
}
