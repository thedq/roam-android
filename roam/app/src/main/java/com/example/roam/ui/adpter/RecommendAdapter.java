package com.example.roam.ui.adpter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.roam.R;

import java.util.ArrayList;

public class RecommendAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> dataArray = new ArrayList<>();

    public RecommendAdapter(Context context){
        this.context= context;
    }

    @Override
    public int getCount() {
        return dataArray.size();
    }

    @Override
    public Object getItem(int position) {
        return dataArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String strRecommend = dataArray.get(position);
        if( convertView == null ) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_recommend,parent,false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.txtRecommend = convertView.findViewById(R.id.txt_recommend);
            viewHolder.txtRecommend.setText(strRecommend);
            convertView.setTag(viewHolder);
        }else {
            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            viewHolder.txtRecommend.setText(strRecommend);
        }

        return convertView;
    }

    public void setDataArray(ArrayList<String> dataArray){
        this.dataArray.clear();
        this.dataArray.addAll(dataArray);
        notifyDataSetChanged();
    }

    class ViewHolder{
        TextView txtRecommend;
    }

    public ArrayList<String> getDataArray() {
        return dataArray;
    }
}
