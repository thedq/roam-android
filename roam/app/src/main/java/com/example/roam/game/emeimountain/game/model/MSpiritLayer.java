package com.example.roam.game.emeimountain.game.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.util.State;

import java.util.ArrayList;
import java.util.Random;

public class MSpiritLayer {

    private Context context;
    private MSpiritPool mSpiritPool;
    private ArrayList<MSpirit> mSpiritArray;
    private int [] xLocation = new int[]{0,220,440,660,880};
    private Random random;
    private int count = 0;
    private State currentState;


    public MSpiritLayer(Context context){
        this.context = context;
        currentState = State.NONE;
        random = new Random();
        mSpiritPool = new MSpiritPool(context);
        mSpiritArray = new ArrayList<>();
        MSpirit mSpirit1 = mSpiritPool.getMSpirit();
        MSpirit mSpirit2 = mSpiritPool.getMSpirit();
        mSpirit1.getRect().left = xLocation[0];
        mSpirit1.getRect().right = mSpirit1.rect.left + mSpirit1.width;
        mSpirit2.getRect().left = xLocation[1];
        mSpirit2.getRect().right = mSpirit2.rect.left + mSpirit2.width;
        mSpiritArray.add(mSpirit1);
        mSpiritArray.add(mSpirit2);
    }

    public void setAgain(){
        mSpiritArray.clear();
        mSpiritPool = new MSpiritPool(context);
        MSpirit mSpirit1 = mSpiritPool.getMSpirit();
        MSpirit mSpirit2 = mSpiritPool.getMSpirit();
        mSpirit1.getRect().left = xLocation[0];
        mSpirit1.getRect().right = mSpirit1.rect.left + mSpirit1.width;
        mSpirit2.getRect().left = xLocation[1];
        mSpirit2.getRect().right = mSpirit2.rect.left + mSpirit2.width;
        mSpiritArray.add(mSpirit1);
        mSpiritArray.add(mSpirit2);
        this.currentState = State.START;
        count = 0;
    }

    public void draw(Canvas canvas){
        if( currentState == State.START ) {
            logic(); // 过线即添加
            for (int i = 0; i < mSpiritArray.size(); i++) {
                mSpiritArray.get(i).draw(canvas);
            }
            for (int i = 0; i < mSpiritArray.size(); i++) {
                if (mSpiritArray.get(i).isVisible == false) {          // 不可见就回收
                    MSpirit removeSpirit = mSpiritArray.remove(i);
                    mSpiritPool.setmSpirit(removeSpirit);
                }
            }
        }

        if( currentState == State.LOSE || currentState == State.WIN){
            for (MSpirit mSpirit : mSpiritArray) {
                mSpirit.setAdd(0); //设置下落速度
                mSpirit.draw(canvas);
            }
        }
    }

    public void winAndGoAhead(){
        currentState = State.START;
        for (MSpirit mSpirit : mSpiritArray) {
            mSpirit.setAdd(25); //设置下落速度
        }
    }

    public void lose(){
        currentState = State.LOSE;
    }

    public void win(){
        currentState = State.WIN;
    }

    private void logic() {        // 过线就添加
        if ( mSpiritArray.size() != 0 && mSpiritArray.get(mSpiritArray.size()-1).isPassLine() && currentState == State.START ) {
            insert();
        }
    }

    private void insert() {
        MSpirit mSpirit = mSpiritPool.getMSpirit();
        mSpirit.getRect().left = xLocation[random.nextInt(xLocation.length)];
        mSpirit.getRect().right = mSpirit.getRect().left + mSpirit.width;
        mSpirit.getRect().top = -100;
        mSpirit.getRect().bottom = mSpirit.getRect().top + mSpirit.getHeight();
        mSpirit.isVisible = true;
        mSpirit.setType(random.nextInt(4));
        mSpiritArray.add(mSpirit);
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<MSpirit> getmSpiritArray() {
        return mSpiritPool.getmSpiritArrayEvent();
    }


    public State getCurrentState() {
        return currentState;
    }

    public void setCurrentState(State currentState) {
        this.currentState = currentState;
    }
}
