package com.example.roam.game.app.leshan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.roam.R;
import com.example.roam.game.leshan.control.GameThirdLsControl;
import com.example.roam.game.leshan.view.GameThirdLsUI;
import com.example.roam.utils.BadgeUtils;
import com.example.roam.utils.StatusBarUtils;

public class GameThirdLsActivity extends AppCompatActivity {
    private GameThirdLsUI gameThirdLsUI;
    private GameThirdLsControl gameThirdLsControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_third_ls);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.purple_500);//设置状态栏颜色为银白色

        Intent intent = getIntent();
        boolean isFinsh = intent.getBooleanExtra("isFinsh", false);
        if(isFinsh){
            BadgeUtils.setBadgeIndex(11);
        }
        gameThirdLsUI = this.findViewById(R.id.scene3);
        gameThirdLsControl = gameThirdLsUI.getGameThirdLsControl();
        gameThirdLsControl.setFinsh(isFinsh);
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameThirdLsControl.isVisible = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameThirdLsControl.isVisible = true;
    }
}