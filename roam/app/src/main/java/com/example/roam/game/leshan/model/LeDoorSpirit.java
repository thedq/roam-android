package com.example.roam.game.leshan.model;

import android.content.Context;
import android.graphics.Canvas;

import com.example.roam.game.qcmountain.model.CSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.util.GameConstant;
import com.example.roam.game.util.PaintUtil;

public class LeDoorSpirit extends CSpirit {
    public LeDoorSpirit(Context context) {
        super(context, 240, 1050, 1080, 1668);

        this.text = GameConstant.LE_DOOR_TEXT;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (isVisible) {
            canvas.drawRect(rect, PaintUtil.getPaint());
        }
    }

}
