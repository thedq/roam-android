package com.example.roam.game.util;

import android.content.Context;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import com.example.roam.R;
import com.example.roam.game.qcmountain.model.BarOptionSpirit;
import com.example.roam.game.qcmountain.model.Spirit;
import com.example.roam.game.qcmountain.model.layer2.DialogSpirit;
import com.example.roam.game.qcmountain.model.layer2.OptionLayer;
import com.example.roam.game.qcmountain.model.layer2.TaoistSpirit;

public class TaoistLayer extends Spirit {
    private MaskRect maskRect;
    private TaoistSpirit taoistSpirit ,roadTaoist;
    private DialogSpirit dialogSpirit;
    private OptionLayer optionLayer;
    private boolean isClick=false;

    public TaoistLayer(Context context, TaoistSpirit roadTaoist,String text) {
        super(context, 0, 0, 1080, 1920);
        this.isVisible = false;
        maskRect = new MaskRect(context);
        taoistSpirit = new TaoistSpirit(context);
        taoistSpirit.setTaoistRectLeft(0);
        taoistSpirit.setTaoistRectTop(1010);
        taoistSpirit.setTaoistBitmap(R.drawable.npc_flip);
        // 点击关闭text不能误点taoistLayer （text.isvisible时候不能点击, DELETE点击放在最后）
        this.roadTaoist = roadTaoist;
        dialogSpirit = new DialogSpirit(context);
        dialogSpirit.isVisible =true;
        optionLayer = new OptionLayer(context);
        optionLayer.isVisible = true;
        dialogSpirit.setText(text);
    }

    @Override
    public void draw(Canvas canvas) {
        if (isVisible) {
            maskRect.draw(canvas);
            taoistSpirit.draw(canvas);
            dialogSpirit.draw(canvas);
            optionLayer.draw(canvas);
        }
    }

    String TAG ="TAG";
    public void clickDialogSpirit(MotionEvent event,String textLayer){
        if(isVisible && dialogSpirit.getRect().contains((int) event.getX(),(int) event.getY())){
            // 实现功能： 点击一次，展示后面的对话
            // 想法：提前分为String[]  或者 获取传递剩下。
            StringBuffer saveText = getDialogSpirit().getSaveText();
            String newText = null;
            newText = textLayer.substring(textLayer.indexOf(saveText.toString()) + saveText.length());
            Log.i(TAG, "clickDialogSpirit: newText: "+newText);
            getDialogSpirit().setText(newText);
        }
    }

    public void clickOptionCancel(MotionEvent event){
        if( isVisible && optionLayer.getOptionSpiritLeave().getRect().contains((int) event.getX() ,(int) event.getY())){
            isVisible = false;
            roadTaoist.isVisible = true;
            isClick = false;
        }
    }

    public void clickOptionQuestion(MotionEvent event, BarOptionSpirit barOptionSpirit){
        if(isVisible && optionLayer.getOptionSpiritQuest().getRect().contains((int) event.getX(),(int) event.getY())){
            barOptionSpirit.isVisible = true;
            barOptionSpirit.setText("答题才能继续前进....");
        }
    }


    public MaskRect getMaskRect() {
        return maskRect;
    }

    public TaoistSpirit getTaoistSpirit() {
        return taoistSpirit;
    }

    public DialogSpirit getDialogSpirit() {
        return dialogSpirit;
    }

    public OptionLayer getOptionLayer() {
        return optionLayer;
    }

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }
}
